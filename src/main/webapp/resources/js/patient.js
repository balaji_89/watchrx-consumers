function editPatient(patientId){
	console.log("patinetId::::::"+patientId);
	window.location="/WatchRx/service/patient/patient?patientId="+patientId;
	/*$.ajax({
        url: '/WatchRx/service/patient/getPatient',
        type: 'GET',
        dataType: 'json',
        data: "patientId=" + patientId,
        success: function (data, textStatus, xhr) {
            console.log(data);
        },
        error: function (xhr, textStatus, errorThrown) {
           console.log('Error in Authentication'+errorThrown.message);
       }
    });*/
}

function patientAssignment(patientId){
	var patientId = $("#patient option:selected").val();
	console.log("patinetId::::::"+patientId);
	window.location="/service/watch/patientAssignment?patientId="+patientId;
}

function deletePatient(patientId){
	bootbox.confirm("Are you sure to delete Patient?", function(confirmed) {
		  if(confirmed) {
				$.ajax({
			        url: '/service/patient/deletePatient',
			        type: 'POST',
			        dataType: 'json',
			        data: "patientId=" + patientId,
			        success: function (data, textStatus, xhr) {
			        	if (data.success){
			            window.location="/service/patient/patientSummary";
			        	}
			        },
			        error: function (xhr, textStatus, errorThrown) {
			           console.log('Error in Authentication'+errorThrown);
			       }
			    });
		  }
		}); 

}

function deletePrescription(patientId,prescriptionId){
	bootbox.confirm("Are you sure to delete Patient Prescription?", function(confirmed) {
		  if(confirmed) {
				$.ajax({
			        url: "/service/patient/deletePrescription?patientId="+patientId+"&prescriptionId="+prescriptionId,
			        type: 'POST',
			        dataType: 'json',
			        data: "deletePrescription=" + prescriptionId,
			        success: function (data, textStatus, xhr) {
			        	if (data.success){
			            window.location="/service/patient/prescriptionSummary?patientId="+patientId;
			        	}
			        },
			        error: function (xhr, textStatus, errorThrown) {
			           console.log('Error in Authentication'+errorThrown);
			       }
			    });
		  }
		}); 

}

