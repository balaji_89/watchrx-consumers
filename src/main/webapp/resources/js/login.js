function login(){
	var username = $("#login-username").val ();
	var password = $("#login-password").val ();
	console.log ("username" + username);
	console.log ("password" + password);
	$("#sign_error").text("");
	if(username == null || username.trim() =="" ||password == null || password.trim() ==""){
		$("#sign_error").text("Please Input Username / Password.");
		$("#login-username").focus();
		return false;
	}	
	$.ajax({
        url: '/login',
        type: 'POST',
        dataType: 'json',
        data: "userName=" + username+"&password="+password,
        success: function (data, textStatus, xhr) {
            console.log(data);
			if (data.success){
				window.location ="/postlogin";
			}else{
				$("#sign_error").text(data.messages[0]);
				$("#email").focus();
			}
        },
        error: function (xhr, textStatus, errorThrown) {
           console.log('Error in Authentication'+errorThrown.message);
       }
    });
	
}

function onSignIn(){
	window.location ="/service/watch/watchSummary";
}
