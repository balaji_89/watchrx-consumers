<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div id="maincontainer" class="no-sidebar">
	<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->

	<div id="contentwrapper">
		<div>
			<div class="container-fluid" id="contentcolumn">
				<div class="row " style="padding-bottom: 16px !important;">
					<div class="col-xs-4">
						<div class="down-arrow">							
							<c:choose>
						    <c:when test="${patientVO.patientId >0}">
						       Edit Parent
						    </c:when>    
						    <c:otherwise>
						    Add Parent
						    </c:otherwise>
						</c:choose>
						</div>
					</div>
					<div class="col-xs-6 pull-right">
						<a href="/service/patient/patientSummary"
							class="btn btn-primary btn-large btn-af pull-right"
							style="background: green !important; padding-left: 50px !important; padding-right: 50px !important;"><span></span>
							Discard</a>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<form:form class="form-horizontal" method="POST" modelAttribute="patientVO" 
							action="/service/patient/savePatient" id="addPatientForm" enctype="multipart/form-data">
							<form:input type="hidden" path="patientId" />
								<form:input type="hidden" path="picPath" />
							<div class="well">
								<div class="row" style="padding: 8px">
									<div class="col-xs-2">
											<label  class="control-label">Upload Image</label>
											<br/>
											<form:errors cssClass="error-msg" path="picPath" />
										</div>
									<%-- 	<div class="col-xs-4">
											<form:input id = "picPath" path="picPath" class="form-control"/>
											<form:errors cssClass="error-msg" path="picPath" />
										</div> --%>
									<div class="fileinput fileinput-new" data-provides="fileinput"> 
										<div class="fileinput-new thumbnail" style="width: 50px; height: 50px;" placeholder="Upload avatar"> 
										<img height="75px" width="75px"
											src="${patientVO.picPath}" alt="" />
										</div> <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 75px; max-height: 75px;">
										</div> <div> <span class="btn btn-success btn-file">
										<span class="fileinput-new">Select image</span>
										<span class="fileinput-exists">Change</span>
										<form:input type="file" path="imageFile"/>
										</span> <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a> 
										</div> 
										</div> 
								</div>
								<div class="row " style="padding: 4px">
									<div class="col-xs-2">
										<label class="control-label col-sm-2" for="firstName">Name:*</label>
									</div>
									<div class="col-xs-4">
										<form:input path="firstName" type="text" class="form-control"
											placeholder="FirstName" />
										<form:errors cssClass="error-msg" path="firstName" />
									</div>
									<div class="col-xs-4">
										<form:input path="lastName" type="text" 
											class="form-control" placeholder="LastName" />
										<form:errors cssClass="error-msg" path="lastName" />
									</div>
								</div>
								<div class="row " style="padding: 4px">
									<div class="col-xs-2">
										<label class="control-label col-sm-2">Marital Status:*</label>
									</div>
									<div class="col-xs-3">
										<form:select class="form-control" path="maritalStatus">
										  <form:option value="-1">--Choose Marital Status--</form:option>
											<form:options items="${maritalStatusList}"/>
										</form:select>
										<form:errors cssClass="error-msg" path="maritalStatus" />
									</div>
									<div class="col-xs-1">
										<label class="control-label col-sm-2">Language:*</label>
									</div>
									<div class="col-xs-3">
										<form:select class="form-control" path="language">
										<form:option value="-1">--Choose Language--</form:option>
										<form:options items="${languages}"/>
										</form:select>
										<form:errors cssClass="error-msg" path="language" />
									</div>
									<div class="col-xs-1">
										<label class="col-sm-2" for="dob">DOB:*</label>
									</div>
									<div class='col-xs-2'>
										<form:input id='dob' type='text' class="form-control" path="dob" />
										<form:errors cssClass="error-msg" path="dob" data-date-format="DD/MM/YYYY "/>
									</div>
								</div>
								<div class="row " style="padding: 4px">
									<div class="col-xs-2">
										<label class="control-label col-sm-2">Employment</label>
									</div>
									<div class="col-xs-4">
										<form:input type="text" id="employment" class="form-control"
											placeholder="Employment" path="employment" /> 
										<form:errors cssClass="error-msg" path="employment" />
									</div>
									<label style=" display: none;" class="control-label col-sm-3" for ="ssn">Social Security
										Number</label>
									<div style=" display: none;" class="col-xs-3">
										<form:input type="text" id="ssn" class="form-control" data-mask="999-99-9999"
											placeholder="Social Security Number" path="ssn" /> 
										<form:errors cssClass="error-msg" path="ssn" />
									</div>
								</div>
								<div class="row " style="padding: 4px">
									<div class="col-xs-2">
										<label class="control-label col-sm-2" for="insuranceCompany">Insurance:</label>
									</div>
									<div class="col-xs-4">
										<form:input type="text" id="insuranceCompany" class="form-control"
											placeholder="Company" path="insuranceCompany" />
									</div>
									<div class="col-xs-4">
										<form:input type="text" id="insurancePhNumber" class="form-control" 
											placeholder="Number" path="insurancePhNumber" />
									</div>
								</div>
								<div class="row " style="padding: 4px">
									<div class="col-xs-2">
										<label class="control-label col-sm-2" for="address1">Address:*</label>
									</div>
									<div class="col-xs-8">
										<form:input type="text" id="address1" class="form-control"
											path="address.address1" placeholder="Enter Address" /> 
										<form:errors cssClass="error-msg" path="address.address1" />
									</div>
								</div>
								<div class="row " style="padding: 4px">
									<div class="col-xs-2">
										<label class="control-label col-sm-2">Address2:*</label>
									</div>
									<div class="col-xs-8">
										<form:input type="text" placeholder="Additional Address(Optinal)"
											id="address2" class="form-control" path="address.address2" />
									</div>
								</div>
								<div class="row " style="padding: 4px">
									<div class="col-xs-2">
										<label class="control-label col-sm-2" for="city">Location:*</label>
									</div>
									<div class="col-xs-2">
										<form:input type="text" id="city" class="form-control"
											placeholder="City" path="address.city" /> 
										<form:errors cssClass="error-msg" path="address.city" />
									</div>
									<div class="col-xs-2" for="state">
										<form:input type="text" id="state" class="form-control"
											placeholder="State" path="address.state" /> 
										<form:errors cssClass="error-msg" path="address.state" />
									</div>
									<div class="col-xs-2" for="zip">
										<form:input type="text" id="zip" class="form-control"
											placeholder="Zip" path="address.zip" /> 
										<form:errors cssClass="error-msg" path="address.zip" />
									</div>
								</div>
								<div class="row " style="padding: 4px">
									<div class="col-xs-2">
										<label class="control-label col-sm-2" for="phoneNumber">Phone:*</label>
									</div>
									<div class="col-xs-4">
										<form:input type="text" id="phoneNumber" data-mask="999-999-9999"
											class="form-control required" placeholder="Phone Number"
											path="phoneNumber" /> 
										<form:errors cssClass="error-msg" path="phoneNumber" />
									</div>
									<div class="col-xs-4">
										<form:input type="text" id="altPhoneNumber" class="form-control" data-mask="999-999-9999"
											placeholder="Alt Phone Number" path="altPhoneNumber" />
									</div>
								</div>
								<div class="row " style="padding: 4px">
									<div class="col-xs-2">
										<label class="control-label col-sm-2" for="visitReason">Visit Reason</label>
									</div>
									<div class="col-xs-4">
										<form:textarea class="form-control" id="visitReason"
											placeholder="Visit Reason" path="visitReason"></form:textarea>
										<form:errors cssClass="error-msg" path="visitReason" />
									</div>

									<label class="control-label col-sm-3">Ever Been a
										Patient Here?</label>
									<div class="col-xs-1">
										<form:checkbox path="revisit" class="form-control"/>
									</div>
									
								</div>
								<fieldset class="scheduler-border">
   								 <legend class="scheduler-border">SOS</legend>
								<div class="row " style="padding: 4px">
									<div class="col-xs-2">
										<label class="control-label col-sm-2">Name:*</label>
									</div>
									<div class="col-xs-4">
										<form:input type="text" id="namesos" class="form-control"
											placeholder="Name" path="nameSos" /> 
										<form:errors cssClass="error-msg" path="nameSos" />
									</div>
										<div class="col-xs-2">
										<label class="control-label col-sm-2" for="phoneNumber">Phone:*</label>
									</div>
									<div class="col-xs-4">
										<form:input type="number" id="phoneNumbersos" 
											class="form-control required" placeholder="Phone Number"
											path="phoneNumbersos" /> 
										<form:errors cssClass="error-msg" path="phoneNumbersos" />
									</div>
									<div class="col-xs-3 pull-right">
									   &nbsp;<button id="clearno"  style="float:right;">Clear Phone</button>&nbsp;
										&nbsp;<button id="addphoneno" style="float:right;">Add Phone</button>&nbsp;
									</div>
								</div>
								<div class="row " style="padding: 6px">
									<div class="col-xs-3 ">
										<label>Phone No</label>
									</div>
									<div class="col-xs-4">
											<form:textarea  rows="5" cols="50" id='phoneslots' type='text' class="form-control"
											placeholder="phone number" path="mobilenoSoS" readonly="readonly" />
									</div>
									</div>
									</fieldset>
								<div class="row " style="padding: 6px">
									<div class="col-xs-3 ">
										<label>Time Slots* (Four hours difference)</label>
									</div>
									<div class="col-xs-4">
										<label style="width:110px">Early Morning</label>&nbsp;
										<form:select path="earlyMorningHour" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${hourList}" />
										</form:select>
										&nbsp;
										<form:select path="earlyMorningMin" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${minuteList}" />
										</form:select>
										<br><label style="width:110px">Breakfast</label>&nbsp;
										<form:select path="breakFastHour" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${hourList}" />
										</form:select>
										&nbsp;
										<form:select path="breakFastMin" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${minuteList}" />
										</form:select>
										<br><label style="width:110px">Lunch</label>&nbsp;
										<form:select path="lunchHour" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${hourList}" />
										</form:select>
										&nbsp;
										<form:select path="lunchMin" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${minuteList}" />
										</form:select>
										<br><label style="width:110px">Afternoon Snack</label>&nbsp;
										<form:select path="noonSnackHour" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${hourList}" />
										</form:select>
										&nbsp;
										<form:select path="noonSnackMin" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${minuteList}" />
										</form:select>
										<br><label style="width:110px">Dinner</label>&nbsp;
										<form:select path="dinnerHour" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${hourList}" />
										</form:select>
										&nbsp;
										<form:select path="dinnerMin" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${minuteList}" />
										</form:select>
										<br><label style="width:110px">Bed</label>&nbsp;
										<form:select path="bedHour" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${hourList}" />
										</form:select>
										&nbsp;
										<form:select path="bedMin" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${minuteList}" />
										</form:select>
									</div>
								</div>
								<div class="col-xs-2 pull-right" >
										<input type="submit" id="submit_btn"
											class="btn btn-primary btn-block"
											style="float: center; padding-left: 50px !important; padding-right: 50px !important; background: #FD8019 !important"
											value="Submit" />
									</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>
<script src="<c:url value="/resources/js/fileinput.js" />"></script>
<script>$(document).ready(function() {
	$( "#addphoneno" ).click(function() {
		  var phoneslots = $('#phoneslots').val();

		  var namesos = $('#namesos').val();
		  var phoneNumbersos = $('#phoneNumbersos').val();
if(namesos.length<0){
	alert("Please enter valid name.");
	return false;
}else if(phoneNumbersos.length<0){
	alert("Please enter valid phone number.");
	return false;
}
		  var total = namesos+":"+phoneNumbersos;
		  sessionStorage.setItem('phoneArray', phoneslots);

		  var values = sessionStorage.getItem('phoneArray');
		  var res;
		  if(values!=null  && typeof values!="undefined"){
			  console.log(values);
			  if(values.length>0){
		   res = values.concat(","+total);
			  }else{
				   res = values.concat(total);
			  }
		  }
		 
		  var result =  res.replace("undefined|", "")
		   sessionStorage.setItem('phoneArray', result);
		  $("#phoneslots").val( result);
		  return false;
	});
	$( "#clearno" ).click(function() {
	// var values = '';

	// sessionStorage.setItem('timeArray', values);
	 sessionStorage.clear();
	  $("#phoneslots").val("");
	  return false;
	});
	function validateDonotSelect(value,element,param)
    {
        if(value == param)
        {
          return false;
        }
        else
        {
            return true;
        }      
    }
    jQuery.validator.addMethod("do_not_select",validateDonotSelect,"Please select an option");
	$("#nav-bar li").removeClass("active");
    $("#patient-nav").addClass("active");
    $('#addPatientForm').validate({
        rules: {
        	picPath: {
				required: false				
			      },
			firstName: {
				required: true,
				lettersonly: true				
			},
			lastName: {
				required: true,
				lettersonly: true				
			},
			employment: {
				required: false				
			},
			ssn: {
				required: false,
				pattern: /^\d{3}-\d{2}-\d{4}$/
			},
			insuranceCompany: {
				required: false				
			},
			insurancePhNumber: {
				required: false,
				alphanumeric: true,  				
  				maxlength: 50
			},
			'address.address1': {
				required: true				
			},
			'address.city': {
				required: true				
			},
			'address.state': {
				required: true,	
				stateUS: true
			},
			'address.zip': {
				required: true,
				zipcodeUS: true
			},
			phoneNumber: {
				required: true,
				pattern: /^\d{3}-\d{3}-\d{4}$/
			},
			altPhoneNumber: {
				phoneUS: true				
			},
			visitReason: {
				required: false				
			},
			dob: {
				required: true,
				date: true
			},
			maritalStatus: {
				do_not_select:'-1'
			},
			language: {
				do_not_select:'-1'
			}
			
        },
        messages: {
        	picPath: {
                required: "You need to enter an image path"       
            },
			firstName: {
				required: "You need to enter a firstname"				
			},
			lastName: {
				required: "You need to enter a lastname"				
			},
			employment: {
				required: "You need to provide an employment status"				
			},
			ssn: {
				required: "You need to provide a SSN",
				pattern: "SSN needs to be a number in XXX-XX-XXXX format"
			},
			insuranceCompany: {
				required: "You need to enter the name of an insurance company"				
			},
			insurancePhNumber: {
				required: "You need to enter the insurance number",
				digits: "You need to enter only digits",
  				minlength: "You need to enter atleast 10 digits",
  				maxlength: "You cannot enter more than 10 digits"
			},
			'address.address1': {
				required: "You need to enter an address"				
			},
			'address.city': {
				required: "You need to enter a US city"				
			},
			'address.state': {
				required: "You need to enter a US state",
				stateUS: "You need to enter a valid US state abbreviation"
			},
			'address.zip': {
				required: "You need to enter a US zipcode",	
				zipcodeUS: "You need to enter a valid US zipcode"				
			},
			phoneNumber: {
				required: "You need to enter a phone number in XXX-XXX-XXXX format",
				phoneUS: "You need to enter a valid US phone number"
			},
			altPhoneNumber: {
				phoneUS: "You need to enter a valid US phone number"				
			},
			visitReason: {
				required: "You need to enter a valid reason for your visit",
				minlength: "You need to enter at least enter 4 characters"
			},
			maritalStatus: {
				do_not_select:"You need to select a marital status"
			},
			language: {
				do_not_select:"You need to select a language"
			}
			
        }
    });
    $('#dob').datepicker({	
    	 changeYear:true,
    	 yearRange: "1900:2115"
		}).datepicker("dob", $('#dob').val());

 	} );
 	

	</script>