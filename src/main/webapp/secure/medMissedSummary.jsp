	<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
		<div id="maincontainer" class="no-sidebar">
		<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->
		
			<div id="contentwrapper">
				<div >

					<div class="container-fluid" id="contentcolumn">
					<div class="row-fluid">
						<div class="span12">
						
							<div class="well">
								
									<!-- ** widget header ** -->
									<div class="row " style="padding:4px;padding-bottom:16px !important;">
									 <div class="col-xs-6" >
									 <div class="down-arrow" >Medication List (${fn:length(infoList)})</div>
									  </div>
									  	<div class="col-xs-6 pull-right">
						<a href="/service/patient/alertSummaryDBrd"
							class="btn btn-primary btn-large btn-af pull-right"
							style="background: green !important; padding-left: 50px !important; padding-right: 50px !important;"><span></span>
							Back</a>
					</div>	 
									</div>
									
									<!-- ** ./ widget header ** -->
									<table id="medTable" cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered medium-font" id="internal-doctor">
										<thead style="background:green;color:white">
											<tr>
												<th data-orderable="true">Medicine Name</th>
												<th data-orderable="true">Strength</th>
												<th data-orderable="true">Dosage</th>
												<th data-orderable="true">Time Slots</th>
												<th data-orderable="true">Days of Week</th>
												<th data-orderable="true">Before or After Food</th>
												
											</tr>
										</thead>								 
										  <tbody>
								        		<c:forEach var="info"  items="${infoList}" varStatus="loop">        
								            <tr>
								                <td>${info.medicineName}</td>
								                <td>${info.strength}</td>
								                <td>${info.dosage}</td>
								                <td>${info.timeSlots}</td>
								                <td>${info.daysOfWeek}</td>
								                <td>${info.beforeOrAfterFood}</td>
								            </tr>
								            </c:forEach>
								            		
								        </tbody>
									</table>
							</div>
							<!-- ** ./ widget ** -->
						</div>
					</div>
					</div>				
				</div>
			</div>
			
		</div>
		<script>
		$("#alert-nav").addClass("active");
		 $(document).ready(function() {
				
		     $("#nav-bar li").removeClass("active");
		     $("#alert-nav").addClass("active");
		   
		    
		    

			} );
	</script>
