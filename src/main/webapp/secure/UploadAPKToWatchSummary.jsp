<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div id="maincontainer" class="no-sidebar">
	<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->

	<div id="contentwrapper">
		<div>

			<div class="container-fluid" id="contentcolumn">
				
					<div class="row-fluid">
						<div class="span12">
							<div class="row ">
								<div class="col-xs-6">
									<label
										style="font-size: 15px !important; font-style: normal !important; color: black !important">Upload APK TO Watch
										</label>
								</div>
									<div class="col-xs-6 pull-right">
						<a href="/service/watch/APKVersionSummary"
							class="btn btn-primary btn-large btn-af pull-right"
							style="background: green !important; padding-left: 50px !important; padding-right: 50px !important;"><span></span>
							Discard</a>
					</div>
							</div>
							
						  <form:form method="POST" class="form-horizontal" action="/service/watch/APKVersionPublish" modelAttribute="watchAPKVO" enctype="multipart/form-data">
						
							<div class="well">


								<div class="row " style="padding: 6px">
									<div class="col-xs-2 ">
										<label>Select APK Version*</label>
									</div>
									<div class="col-xs-4 ">
									<form:select name="imeiWatch" class="form-control"
											path="apkVO">
											<form:options items="${watchAPKVO.apkVO}" />
										</form:select>
										</div>
									<div class="col-xs-2 ">
										<label>Select Watches  *</label>
									</div>
									<div class="col-xs-4 ">
									<form:select name="imei" multiple="true" class="form-control" path="watchVO">
   									<form:options items="${watchAPKVO.watchVO}" />
									</form:select>
									
									</div>
								</div>
								<div class="row " style="padding: 4px">

									<div class="col-xs-1 pull-right">
										<input type="submit" class="btn btn-primary "
											style="font-size: 18px !important; background: #FD8019 !important"
											value="Submit" />
									</div>
								</div>
								
																</div>
																</form:form>
						</div>
			</div>
		</div>
	</div>
</div>

<script>

	$(document).ready(function() {
	
		$("#nav-bar li").removeClass("active");
		$("#apk-nav").addClass("active");
		
	});
</script>