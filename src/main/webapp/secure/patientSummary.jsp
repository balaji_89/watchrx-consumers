
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script src="<c:url value="/resources/js/patient.js" />"></script>
<div id="maincontainer" class="no-sidebar">
	<div id="contentwrapper">
		<div class="container-fluid" id="contentcolumn">
			<div class="row-fluid">
				<div class="span12">

					<div class="well">

						<div class="row "
							style="padding: 4px; padding-bottom: 16px !important;">
							<div class="col-xs-6">
								<div class="down-arrow">Parent List
									(${fn:length(patientList)})</div>
							</div>
							<c:if test="${user != null}">
								<div class="col-xs-2">
									<a href="/service/patient/patient?patientId=0"
										style="font-size: 18px !important; background: #FD8019 !important"
										class="btn btn-primary"><span><i class=" fa fa-plus"></i></span>
										Add New</a>
								</div>
							</c:if>
						</div>
						<table cellpadding="0" id="patientDataTable" cellspacing="0"
							border="0"
							class="display table table-striped table-bordered medium-font">
							<thead style="background: green; color: white">
								<tr>
									<th data-orderable="true">ID</th>
									<th data-orderable="true">Parent Name</th>
									<th data-orderable="true">Pri. CareGiver</th>
									<th data-orderable="true">Sec. CareGiver</th>
<!-- 									<th data-orderable="true">Clinician (Night)</th> -->
<!-- 									<th data-orderable="true">Pri. Doctor</th> -->
<!-- 									<th data-orderable="true">Specialist</th> -->
									<th data-orderable="true">Watch IMEI Number</th>
									<th data-orderable="true">Prescription</th>
									<c:if test="${user != null}">
										<th data-orderable="false">Actions</th>
									</c:if>
								</tr>
							</thead>

							<tbody>
								<c:forEach var="patient" items="${patientList}">
									<tr>
										<td>${patient.patientId}</td>
										<td><img height="40px" width="40px"
											src="${patient.picPath}" alt="" />${patient.firstName} ${patient.lastName}
											</td>

										<td>${patient.noonClinician}</td>
										<td>${patient.noonClinician}</td>
										<%-- <td>${patient.nightClinician}</td>
										<td>${patient.priDoctor.lastName}
											${patient.priDoctor.firstName}</td>
										<td>${patient.specialist.lastName}
											${patient.specialist.firstName}</td> --%>
										<td>${patient.watch.imeiNumber}</td>
										<td><a
											href="/service/patient/prescriptionSummary?patientId=${patient.patientId}"
											style="background: #FD8019 !important"
											class="btn btn-primary btn-block">Add / Edit</a></td>
										<c:if test="${user != null}">	
											<td>&nbsp;&nbsp;<a
												href="/service/patient/patient?patientId=${patient.patientId}"
												style="font-size: 18px !important; color: #FD8019"
												class="fa fa-edit  fa-2x"></a> &nbsp;&nbsp; <a href="#"><i
													onclick="deletePatient(${patient.patientId}); return false;"
													style="font-size: 18px !important; color: #FD8019"
													class="fa fa-trash  fa-2x"></i></a>
											</td>
										</c:if>
									</tr>
								</c:forEach>

							</tbody>
						</table>
					</div>
					<!-- ** ./ widget ** -->
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	
	$(document).ready(function() {
     $("#nav-bar li").removeClass("active");
     $("#patient-nav").addClass("active");
     $("#patientDataTable").dataTable({
    	 "bPaginate":true,
          "sPaginationType":"full_numbers",
           "iDisplayLength":10,
           "bLengthChange": false,
           "bFilter": false
    });
    
	} );
	</script>