<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
		<div id="maincontainer" class="no-sidebar">
		<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->
		
			<div id="contentwrapper">
				<div >						
					<div class="container-fluid" id="contentcolumn">	
						<div class="row " style="padding:4px;padding-bottom:16px !important;">
					<div class="col-xs-3">
						<div class="down-arrow">
							<c:choose>
								<c:when test="${watchVO.watchId >0}">
						       Edit Watch
						    </c:when>
								<c:otherwise>
						    Add Watch
						    </c:otherwise>
							</c:choose>
						</div>
					</div>
					<div class="col-xs-6 pull-right">
											<a href="/service/watch/watchSummary" class="btn btn-primary btn-large btn-af pull-right" style="background:green !important;padding-left:50px !important;padding-right:50px !important;"><span></span> Discard</a>
										</div>
									</div>					
					<div class="row-fluid">
						<div class="span12">
						<form:form class="form-horizontal" method="POST" id="addWatchForm"
							modelAttribute="watchVO" action="/service/watch/saveWatch">
							<form:input type="hidden" path="watchId" />
							<div class="well">
							
								<div class="row " style="border-bottom:2px">
									 <div class="col-xs-3" >
									 <label   for="imeiNumber">IMEI Number</label>
									  </div> 
									  <div class="col-xs-4" >
									    <form:input   id ="imeiNumber" class="form-control" placeholder="Enter Watch IMEI Number" path="imeiNumber" />
									  <form:errors cssClass="error-msg" path="imeiNumber" />									 
									</div>
								</div>
					<div class="row " style="padding:4px">
									 <div class="col-xs-12" >
									 <input type="submit" id="submit_btn" class="btn btn-primary "
												style="float: center; padding-left: 50px !important; padding-right: 50px !important; background: #FD8019 !important"
												value="Done" />
									  </div>	 
									  
									</div>
							</div>
							</form:form>
						</div>
					</div>
					</div>				
				</div>
			</div>
		</div>
<script type="text/javascript">
        $(function(){
            $('#addWatchForm').validate({
                rules: {
                    imeiNumber: {
      				required: true,
      				digits: true,
      				minlength: 15,
      				maxlength: 15
    			      }                   
                },
                messages: {
                    imeiNumber: {
                        required: "You need to enter an IMEI number",
                        digits: "You can only enter digits",
                        minlength: "You need to enter 15 digits",
          				maxlength: "You can only enter 15 digits"
                    }
                }
            });

        });
</script>
		