	<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script	src="<c:url value="/resources/js/patient.js" />"></script>
		<div id="maincontainer" class="no-sidebar">
		<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->
		
			<div id="contentwrapper">
			<form:form class="form-horizontal" method="POST"
							modelAttribute="assignVO" action="/service/watch/saveAssignForConsumer" id="assignConsumerform">
				<div >
					<div class="container-fluid" id="contentcolumn">
					<div class="row " style="border-bottom:2px">
										<div class="col-xs-3 pull-right">
						<div>
								<div><a href="/service/watch/getPatientWatch"
							class="btn btn-primary btn-large btn-af pull-right"
							style="background: green !important; padding-left: 50px !important; padding-right: 50px !important;"><span></span>
							Unassign Watch   ==>  Parent </a></div>
					</div>
								</div>
					<div class="row " style="padding:4px;padding-bottom:16px !important;">
									 <div class="col-xs-2" >
									 <div class="down-arrow" >Assign</div>
								  </div>	 
									  
									</div>
					<div class="row-fluid">
						<div class="span12">
						
							<!-- ** widget ** -->
								<div class="well">
							
							        
								<table cellpadding="0" style="width:70% !important;" cellspacing="0" border="0" class="display table table-striped  medium-font" id="internal-doctor">
										
										<tbody>
										
									
<!-- 										<tr> -->
<!-- 											<td style="font-size: 20px; width: 30%">Clinician -->
<!-- 												(Afternoon)</td> -->
<%-- 											<td><form:select path="noonClinicianId" class="form-control"> --%>
<%-- 											 <form:option value="-1" >--Choose Clinician(Afternoon)--</form:option> --%>
<%-- 													<c:forEach var="idPair" items="${noonClinicians}"> --%>
<%-- 														<form:option value="${idPair.clinicianId}">${idPair.lastName} ${idPair.firstName} </form:option> --%>
<%-- 													</c:forEach> --%>
<%-- 												</form:select> --%>
<%-- 												 <form:errors cssClass="error-msg" path="noonClinicianId" /></td> --%>

<!-- 										</tr> -->
<!-- 										<tr> -->
<!-- 												<td style="font-size:20px;width:30%">Clinician  (Night)</td> -->
<%-- 												<td> <form:select path="nightClinicianId" class="form-control"> --%>
<%-- 												 <form:option value="-1" >--Choose Clinician(Night)--</form:option> --%>
<%-- 													<c:forEach var="idPair" items="${nightClinicians}"> --%>
<%-- 														<form:option value="${idPair.clinicianId}">${idPair.lastName} ${idPair.firstName} </form:option> --%>
<%-- 													</c:forEach> --%>
<%-- 												</form:select> --%>
<%-- 												<form:errors cssClass="error-msg" path="nightClinicianId" /> --%>
<!-- 												</td> -->
									
<!-- 											</tr> -->
<!-- 											  <tr> -->
<!-- 												<td style="font-size:20px;width:30%">Primary Doctor</td> -->
<%-- 												<td> <form:select path="priDoctorId" class="form-control"> --%>
<%-- 												 <form:option value="-1" >--Pri.Doctor--</form:option> --%>
<%-- 													<c:forEach var="idPair" items="${priDoctorList}"> --%>
<%-- 														<form:option value="${idPair.doctorId}">${idPair.lastName} ${idPair.firstName} </form:option> --%>
<%-- 													</c:forEach> --%>
<%-- 												</form:select> --%>
<%-- 												<form:errors cssClass="error-msg" path="priDoctorId" /><br> --%>
<!-- 												</td> -->
<!-- 												</tr>    -->
<!-- 											<tr> -->
<!-- 												<td style="font-size:20px;width:30%">Specialist</td> -->
<%-- 												<td> <form:select path="specialistDoctorId" class="form-control"> --%>
<%-- 												 <form:option value="-1" >--Choose Specialist--</form:option> --%>
<%-- 													<c:forEach var="idPair" items="${specialistList}"> --%>
<%-- 														<form:option value="${idPair.doctorId}">${idPair.lastName} ${idPair.firstName} (${idPair.specialityName}) </form:option> --%>
<%-- 													</c:forEach> --%>
<%-- 												</form:select> --%>
<%-- 												<form:errors cssClass="error-msg" path="specialistDoctorId" /> --%>
<!-- 												</td> -->
										
<!-- 											</tr> -->

											<tr>
												<td style="font-size:20px;width:30%">Watch</td>
												<td> <form:select path="watchId" class="form-control">
												 <form:option value="-1" >--Choose Watch--</form:option>
													<c:forEach var="idPair" items="${watchList}">
														<form:option value="${idPair.watchId}">${idPair.imeiNumber} </form:option>
													</c:forEach>
												</form:select>
												<form:errors cssClass="error-msg" path="watchId" />
												</td>
										
											</tr> 
											<tr>
										 <td style="font-size: 20px; width: 30%">Parent
												</td> 
									    <td><form:select path="patientId" id="patient" class="form-control" onchange="patientAssignment(); return false;">
									    <form:option value="-1" >Choose a Parent</form:option>
									    <c:forEach var="idPair"  items="${patientList}">     
									  <form:option value="${idPair.patientId}">${idPair.patientName}  </form:option>
									  </c:forEach>
									  </form:select>
									   <form:errors cssClass="error-msg" path="patientId" />
									</td></tr>									
										</tbody>
									</table>
									<div class="row " style="width:73% !important;padding:1px" >
									 <div class="col-xs-12" >
									<input type="submit" class="btn btn-primary pull-right" style="padding-left:50px !important;padding-right:50px !important;background:#FD8019 !important" value="Done"/>
									</div>
									</div>
							</div>
							<!-- ** ./ widget ** -->
						</div>
					</div>
					</div>				
				</div>
				</form:form>
			</div>
<script>
	
	</script>