	<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script	src="<c:url value="/resources/js/doctor.js" />"></script>
		<div id="maincontainer" class="no-sidebar">
		<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->
		
			<div id="contentwrapper">
				<div >

					<div class="container-fluid" id="contentcolumn">
					<div class="row-fluid">
						<div class="span12">
						
							<div class="well">
								
									<!-- ** widget header ** -->
									<div class="row " style="padding:4px;padding-bottom:16px !important;">
									 <div class="col-xs-6" >
									 <div class="down-arrow" >Doctor List (${fn:length(doctorList)})</div>
									  </div>	 
									   <div class="col-xs-6">
												<a href="/service/doctor/doctor?doctorId=0" style="font-size:18px !important;background:#FD8019 !important" class="btn btn-primary btn-large"><span><i class="fa fa-plus"></i></span> Add New</a>
												</div>
									</div>
									<!-- ** ./ widget header ** -->
									<table id="doctorDataTable" cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered medium-font" id="internal-doctor">
										<thead style="background:green;color:white">
											<tr>
												<th data-orderable="false">ID</th>
												<th data-orderable="true">Name</th>
												<th data-orderable="true">Speciality</th>
												<th data-orderable="true">Availability </th>
												<th data-orderable="false">Actions </th>
											</tr>
										</thead>								 
										  <tbody>
								        		<c:forEach var="doctor"  items="${doctorList}" varStatus="loop">        
								            <tr>
								             	<td>${doctor.doctorId}</td>
								                <td><img height="40px" width="40px" src="${doctor.picPath}" alt=""  />${doctor.firstName} ${doctor.lastName}</td>								                
								                <td>${doctor.specialityName}</td>
								                <td><c:choose>
								<c:when test="${doctor.available}">
									       Yes
									    </c:when>
								<c:otherwise>
									    No
									    </c:otherwise>
									    </c:choose></td>
												<td><a href="/service/doctor/doctor?doctorId=${doctor.doctorId}" style="font-size:18px !important;color:#FD8019" class="fa fa-edit" ></a>
												&nbsp;&nbsp;
												<a href="#"><i style="font-size:18px !important;color:#FD8019" onclick="deleteDoctor(${doctor.doctorId}); return false;" class="fa fa-trash"></a></i></td>
								            </tr>
								            </c:forEach>
								            		
								        </tbody>
									</table>
							</div>
							<!-- ** ./ widget ** -->
						</div>
					</div>
					</div>				
				</div>
			</div>
			
		</div>
		<script>$(document).ready(function() {
     $("#nav-bar li").removeClass("active");
     $("#doctor-nav").addClass("active");
     $("#doctorDataTable").dataTable({
    	 "bPaginate":true,
          "sPaginationType":"full_numbers",
           "iDisplayLength":10,
           "bLengthChange": false,
           "bFilter": false
    });
	} );
	</script>
	