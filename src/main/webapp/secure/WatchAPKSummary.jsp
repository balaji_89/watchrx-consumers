<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div id="maincontainer" class="no-sidebar">
	<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->

	<div id="contentwrapper">
		<div>

			<div class="container-fluid" id="contentcolumn">


				<div class="row-fluid">
					<div class="span12">
						<div class="row ">
							<div class="col-xs-10">
								<label
									style="font-size: 15px !important; font-style: normal !important; color: black !important">Software
									upgrade </label>
							</div>

						</div>


						<div class="well">
							<div>${status}</div>
							<div class="row "
								style="padding: 4px; padding-bottom: 16px !important;">
								<div class="col-xs-3">
									<div class="down-arrow">APK List (${fn:length(apkList)})</div>
								</div>
								<div class="col-xs-3">
									<a href="/service/watch/APKVersion"
										style="font-size: 18px !important; background: #FD8019 !important"
										class="btn btn-primary btn-large"><span><i
											class="fa fa-plus"></i></span> Upload Software</a>
								</div>
								<div
									class="col-xs-3">
									<a href="/service/watch/WatchToAPK"
										style="font-size: 18px !important; background: #FD8019 !important"
										class="btn btn-primary btn-large"><span><i
											class="fa fa-tasks"></i></span>Publish Software</a>
								</div>
								<div class="col-xs-3">
									<a href="/service/watch/appSoftwareUpgradeSummary"
										style="font-size: 18px !important; background: #FD8019 !important"
										class="btn btn-primary btn-large"><span><i
											class="fa fa-tasks"></i></span>S/W upgrade status</a>
								</div>
							</div>
								
								<table cellpadding="0" cellspacing="0" border="0"
								class="display table table-striped table-bordered medium-font"
								id="clinicianDatatable">
								<thead style="background: green; color: white">
									<tr>
										<th data-orderable="false">ID</th>
<!-- 										<th data-orderable="true">Watch</th> -->
										<th data-orderable="true">APK Version</th>
										<th data-orderable="true">Make</th>
										<th data-orderable="true">Model</th>
										<th data-orderable="false">Uploaded Time</th>
										<th data-orderable="false">Android Version</th>
										<th data-orderable="false">APK URL</th>
									</tr>
								</thead>

								<tbody>
									<c:forEach var="clinician" items="${apkList}"
										varStatus="loop">
										<tr>
											<td>${clinician.apkID}</td>
											<td>${clinician.apkVersion}</td>
											<td>${clinician.make}</td>
											<td>${clinician.model}</td>
											
											<td>${clinician.uploadedTime}</td>
											
											<td>${clinician.androidVersion}</td>
											
											<td>${clinician.apkUrl}</td>
											
										
										</tr>
									</c:forEach>
								</tbody>
							</table>
																</div>
						</div>
			</div>
		</div>
	</div>
</div>

<script>

	$(document).ready(function() {
	
		$("#nav-bar li").removeClass("active");
		$("#apk-nav").addClass("active");
		
	});
</script>