<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div id="maincontainer" class="no-sidebar">
	<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->

	<div id="contentwrapper">
		<div>

			<div class="container-fluid" id="contentcolumn">
				<form:form class="form-horizontal" method="POST"
					modelAttribute="apkVO"
					action="/service/watch/saveAPKVersion" enctype="multipart/form-data">
					
					<div class="row-fluid">
						<div class="span12">
							<div class="row ">
								<div class="col-xs-6">
									<label
										style="font-size: 15px !important; font-style: normal !important; color: black !important">Software upgrade
										</label>
								</div>
									<div class="col-xs-6 pull-right">
						<a href="/service/watch/APKVersionSummary"
							class="btn btn-primary btn-large btn-af pull-right"
							style="background: green !important; padding-left: 50px !important; padding-right: 50px !important;"><span></span>
							Discard</a>
					</div>
							</div>
							
						
							<div class="well">


								<div class="row " style="padding: 6px">
									<div class="col-xs-2 ">
										<label>Enter APK Version*</label>
									</div>
									<div class="col-xs-4 ">
										<form:input id="apkVer" path="apkVersion" class="form-control" />
										<form:errors cssClass="error-msg" path="apkVersion" />
									</div>
									<div class="col-xs-2 ">
										<label>Supported Android Version *</label>
									</div>
									<div class="col-xs-4 ">
										<form:input id='andver' type='text' class="form-control"
											 path="androidVersion" />
										<form:errors cssClass="error-msg" path="androidVersion" />
									</div>
								</div>
								<div class="row " style="padding: 6px">
									<div class="col-xs-2 ">
										<label>Enter Make*</label>
									</div>
									<div class="col-xs-4 ">
										<form:input id="apkVer" path="make" class="form-control" />
										<form:errors cssClass="error-msg" path="make" />
									</div>
									<div class="col-xs-2 ">
										<label>Enter Model *</label>
									</div>
									<div class="col-xs-4 ">
										<form:input id='andver' type='text' class="form-control"
											 path="model" />
										<form:errors cssClass="error-msg" path="model" />
									</div>
									
								</div>
								<div class="row" style="padding: 8px">
									<div class="col-xs-2">
											<label  class="control-label">Upload APK*</label>
											<br/>
										</div>
									<div class="fileinput fileinput-new" data-provides="fileinput"> 
										<div class="fileinput-new thumbnail" style="width: 50px; height: 50px;" placeholder="Upload avatar"> 
										
										</div> <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 75px; max-height: 75px;">
										</div> <div> <span class="btn btn-success btn-file">
										<span class="fileinput-new">Select apk</span>
										<form:input type="file" path="imageFile"/>
										</span> <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a> 
										</div> 
										</div> 
								</div>
								<div class="row " style="padding: 4px">

									<div class="col-xs-1 pull-right">
										<input type="submit" class="btn btn-primary "
											style="font-size: 18px !important; background: #FD8019 !important"
											value="Submit" />
									</div>
								</div>
																</div>
						</div>
				</form:form>
			</div>
		</div>
	</div>
</div>

<script>

	$(document).ready(function() {
	
		$("#nav-bar li").removeClass("active");
		$("#apk-nav").addClass("active");
		
	});
</script>