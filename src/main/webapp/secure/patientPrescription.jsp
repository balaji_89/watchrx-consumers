<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script src="<c:url value="/resources/js/patient.js" />"></script>
<div id="maincontainer" class="no-sidebar">
	<div id="contentwrapper">
		<div>
			<div class="container-fluid" id="contentcolumn">

				<div class="row-fluid">
					<div class="span12">
						<div class="row ">
							<div class="col-xs-10">
								<label
									style="font-size: 15px !important; font-style: normal !important; color: black !important">Patient
									Profile</label>
							</div>
							<div class="col-xs-2">
								<label
									style="font-size: 15px !important; font-style: normal !important; color: black !important">Type</label>
							</div>
						</div>
						<div class="row "
							style="border-bottom: 1px solid black !important">
							<div class="col-xs-10">
								<img height="60px" width="60px" src="${patientVO.picPath}" /> <label
									style="font-size: 20px !important; font-style: normal !important; color: black !important">
									${patientVO.firstName} ${patientVO.lastName }</label>
							</div>
							<div class="col-xs-2">

								<label
									style="font-size: 20px !important; font-style: normal !important; color: black !important">Home</label>
							</div>
						</div>
						<div class="row " style="padding: 4px; padding-bottom: 16px !important;">
								<div class="col-xs-6">
									<div class="down-arrow">Medical Prescription List
										(${fn:length(prescriptionList)})</div>
								</div>
							<div class="col-xs-3 pull-right">
							        <c:if test="${user != null }">
									<a
										href="/service/patient/prescription?patientId=${patientVO.patientId}&prescriptionId=0"
										class="btn btn-primary btn-af"
										style="font-size: 18px !important; background: #FD8019 !important"><span><i
											class="fa fa-plus"></i></span> Add New</a>&nbsp;
									</c:if>
									<a
										href="/service/patient/patientSummary?patientId=${patientVO.patientId}">
										<input type="button" class="btn btn-primary btn-large btn-af"
										style="background: green !important; padding-left: 40px !important; padding-right: 40px !important;font-size: 18px !important;"
										value="Done" />
									</a>
									 
							</div>
						</div>
						<div class="well">
							<table id="prescriptionDataTable" cellpadding="0" cellspacing="0"
								border="0" class="display table table-striped table-bordered "
								id="internal-doctor">
								<thead style="background: green; color: white">
									<tr>
										<th data-orderable="false">ID</th>
										<th data-orderable="true">Medicine Name</th>
										<th data-orderable="true">Strength</th>
										<th data-orderable="true">Time Slot</th>
										<th data-orderable="true">Days Of Week</th>
										<th data-orderable="true">Before Or After Food</th>
										<th data-orderable="true">Dosage</th>
										<c:if test="${user != null }">
										<th data-orderable="false">Actions</th>
										</c:if>

									</tr>
								</thead>

								<tbody>
									<c:forEach var="patientPrescription"
										items="${prescriptionList}">
										<tr>
											<td>${patientPrescription.prescriptionId}</td>
											<td><img height="40px" width="40px"
												src="${patientPrescription.picPath}"
												alt="" />${patientPrescription.medicine}</td>
											<td>${patientPrescription.quantity}</td>
											<td>${patientPrescription.timeSlots}${patientPrescription.fixTimeslots}</td>
											<td>${patientPrescription.dayOfWeek}</td>
											<td>${patientPrescription.afterMeal}</td>
											<td>${patientPrescription.dosage}</td>
											<c:if test="${user != null }">
												<td><a
													href="/service/patient/prescription?patientId=${patientPrescription.patient.patientId}&prescriptionId=${patientPrescription.prescriptionId}"
													style="font-size: 18px !important; color: #FD8019"
													class="fa fa-edit  fa-2x"></a> &nbsp;&nbsp; <a href="#">
														<i
														onclick="deletePrescription(${patientPrescription.patient.patientId},${patientPrescription.prescriptionId}); return false;"
														style="font-size: 18px !important; color: #FD8019"
														class="fa fa-trash  fa-2x"></i>
												</a>
												</td>
											</c:if>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							
						</div>
						<!-- ** ./ widget ** -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>$(document).ready(function() {
     $('#datetimepicker1').datepicker();
	
	     $("#nav-bar li").removeClass("active");
	     $("#patient-nav").addClass("active");
	     var dataT = $("#prescriptionDataTable").dataTable({
	    	 "bPaginate":true,
	          "sPaginationType":"full_numbers",
	           "iDisplayLength":10,
	           "bLengthChange": false,
	           "bFilter": false
	    });
	     $('#datetimepicker1').keyup(function(){
	         oTable.search($(this).val()).draw() ;
	   });
	     
	     
		} );
		</script>
</script>
