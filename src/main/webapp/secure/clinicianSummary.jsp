<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script src="<c:url value="/resources/js/clinician.js" />"></script>
<div id="maincontainer" class="no-sidebar">
	<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->

	<div id="contentwrapper">
		<div>
			<div class="container-fluid" id="contentcolumn">
				<div class="row-fluid">
					<div class="span12">

						<div class="well">
							<div class="row "
								style="padding: 4px; padding-bottom: 16px !important;">
								<div class="col-xs-6">
									<div class="down-arrow">Clinician List
										(${fn:length(clinicianList)})</div>
								</div>
								<div class="col-xs-6">
									<a href="/service/clinician/clinician?clinicianId=0"
										style="font-size: 18px !important; background: #FD8019 !important"
										class="btn btn-primary btn-large"><span><i
											class="fa fa-plus"></i></span> Add New</a>
								</div>
							</div>
							<table cellpadding="0" cellspacing="0" border="0"
								class="display table table-striped table-bordered medium-font"
								id="clinicianDatatable">
								<thead style="background: green; color: white">
									<tr>
										<th data-orderable="false">ID</th>
										<th data-orderable="true">Name</th>
										<th data-orderable="true">Clinician Type</th>
										<th data-orderable="true">Shift</th>
										<th data-orderable="true">Availability</th>
										<th data-orderable="false">Actions</th>
									</tr>
								</thead>

								<tbody>
									<c:forEach var="clinician" items="${clinicianList}"
										varStatus="loop">
										<tr>
											<td>${clinician.clinicianId}</td>
											<td><img height="50px" width="50px"
												src="${clinician.picPath }" />
												${clinician.firstName } ${clinician.lastName }</td>
											<td>${clinician.specialityName}</td>
											<td>${clinician.shiftName}</td>
											<td><c:choose>
													<c:when test="${clinician.available}">
									       Yes
									    </c:when>
													<c:otherwise>
									    No
									    </c:otherwise>
												</c:choose></td>
											<td><a
												href="/service/clinician/clinician?clinicianId=${clinician.clinicianId}"
												<i style="font-size:18px !important;color:#FD8019" class="fa fa-edit"></i></a>&nbsp;&nbsp;
												<a href="#"><i
													style="font-size: 18px !important; color: #FD8019"
													class="fa fa-trash"
													onclick="deleteClinician(${clinician.clinicianId}); return false;"></i></a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<!-- ** ./ widget ** -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
		$(document).ready(function() {
     $("#nav-bar li").removeClass("active");
     $("#clinician-nav").addClass("active");
     $("#clinicianDatatable").dataTable({
    	 "bPaginate":true,
          "sPaginationType":"full_numbers",
           "iDisplayLength":10,
           "bLengthChange": false,
           "bFilter": false
    });
	} );
	</script>