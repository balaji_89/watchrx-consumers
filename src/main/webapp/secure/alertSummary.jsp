	<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script	src="<c:url value="/resources/js/doctor.js" />"></script>
<script	src="<c:url value="/resources/js/datatable/dataTables.buttons.min.js" />"></script>
<script	src="<c:url value="/resources/js/datatable/buttons.flash.min.js" />"></script>
<script	src="<c:url value="/resources/js/datatable/jszip.min.js" />"></script>
<script	src="<c:url value="/resources/js/datatable/pdfmake.min.js" />"></script>
<script	src="<c:url value="/resources/js/datatable/vfs_fonts.js" />"></script>
<script	src="<c:url value="/resources/js/datatable/buttons.html5.min.js" />"></script>
<script	src="<c:url value="/resources/js/datatable/buttons.print.min.js" />"></script>


		<div id="maincontainer" class="no-sidebar">
		<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->
		
			<div id="contentwrapper">
				<div >

					<div class="container-fluid" id="contentcolumn">
					<div class="row-fluid">
						<div class="span12">
						
							<div class="well">
								
									<!-- ** widget header ** -->
									<div class="row " style="padding:4px;padding-bottom:16px !important;">
									 <div class="col-xs-6" >
 									 <div class="down-arrow" >Alert List (${fn:length(alertList)})  </div> 
									  </div>	
									  <div class="col-xs-3 pull-right">
						<a id="load"
							class="btn btn-primary btn-large btn-af pull-right"
							style="background: green !important; padding-left: 50px !important; padding-right: 50px !important;"><span></span>
							Dashboard</a>
					</div> 
									</div>
									<!--<div class="row " style="padding: 4px">
									<div class="col-xs-2"><label class="control-label">Select Date</label></div>
									<div class="col-xs-2"><input type="text" id="datepicker"></div>
									</div>-->
									
									<!-- ** ./ widget header ** -->
									<table id="alertSummaryTable" cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered medium-font" >
										<thead style="background:green;color:white">
											<tr>
												<th data-orderable="true">Patient Name</th>
												<th data-orderable="true">Alert Description</th>
												<th data-orderable="true">Alert Type </th>
												<th data-orderable="true">Created Date </th>
												<th data-orderable="true">Missed Time </th>
												<th data-orderable="true">Missed Time Slots</th>
												<th data-orderable="true">Updated On</th>
												<th data-orderable="true">Action</th>
												
											</tr>
										</thead>								 
										  <tbody>
								        		<c:forEach var="alert"  items="${alertList}" varStatus="loop"> 
								        		       
								            <tr>
								                <td>${alert.patientName}</td>
								                <td>${alert.alertDescription}</td>
								                <td>${alert.alertType}</td>
								                <td>${alert.createdDate}</td>
								                <td>${alert.missedTime}</td>
								                <td>${alert.missedTimeSlot}</td>
								                <td>${alert.updatedOn}</td>
								               <td> <a href="javascript:ViewMedication('${alert.missedMedicationIds}');"><i style="font-size:18px !important;color:#FD8019"  class="fa fa-info-circle"></a></i></td>
								            </tr>
								            </c:forEach>
								            		
								        </tbody>
									</table>
							</div>
							<!-- ** ./ widget ** -->
						</div>
					</div>
					</div>				
				</div>
			</div>
			
		</div>
		 <!-- Modal -->
<div id="graphDiv" style="display:none"></div>
  
		<script>
		 function ViewMedication(medId){
		    	var medicationIds = JSON.parse("[" + medId + "]");
		    	            window.open("/service/patient/getMedicationInfoById?medicationIds="+medicationIds, "_self");
		    	      
		    		}
	 $(document).ready(function() {
		
     $("#nav-bar li").removeClass("active");
     $("#alert-nav").addClass("active");
     $("#alertSummaryTable").dataTable({
    	 "bPaginate":true,
          "sPaginationType":"full_numbers",
           "iDisplayLength":10,
           "bLengthChange": false,
           "bFilter": true,
           dom: 'Bfrtip',
           buttons: [
                      'excel'
                 ]
    });
    $( "#datepicker" ).datepicker({
         changeMonth: true,//this option for allowing user to select month
         changeYear: true, //this option for allowing user to select from year range
         dateFormat: 'yy-mm-dd',
         onSelect: 		function (date){        	 
			window.location="/service/patient/alertSummaryByDate?date="+date;
		}

       });
    
    $('#load').click(function () {

    	   $('#graphDiv').load('/resources/js/morris.js-0.5.1/examples/days.html', function (content) {
    	      $('#graphDiv').dialog({
    	         resizable: true,
    	         height: 500,
    	         width: 700,
    	         modal: true,
    	         title:'Alert Summary'
    	      });
    	      $('#graphDiv').dialog('open');
    	   });

    	});

	} );
	</script>
