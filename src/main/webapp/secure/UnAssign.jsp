	<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script	src="<c:url value="/resources/js/doctor.js" />"></script>
		<div id="maincontainer" class="no-sidebar">
		<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->
		
			<div id="contentwrapper">
				<div >

					<div class="container-fluid" id="contentcolumn">
					<div class="row-fluid">
						<div class="span12">
						
							<div class="well">
								
									<!-- ** widget header ** -->
									<div class="row " style="padding:4px;padding-bottom:16px !important;">
									 <div class="col-xs-6" >
 									 <div class="down-arrow" >List (${fn:length(data)})  </div> 
									  </div>
									  <div class="col-xs-6 pull-right">
						<a href="/service/watch/watchAssign"
							class="btn btn-primary btn-large btn-af pull-right"
							style="background: green !important; padding-left: 50px !important; padding-right: 50px !important;"><span></span>
							Discard</a>
					</div>	 
									</div>
								
									<!--<div class="row " style="padding: 4px">
									<div class="col-xs-2"><label class="control-label">Select Date</label></div>
									<div class="col-xs-2"><input type="text" id="datepicker"></div>
									</div>-->
									
									<!-- ** ./ widget header ** -->
									<table id="PCTable" cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered medium-font" >
										<thead style="background:green;color:white">
											<tr>
												<th data-orderable="true">Id</th>
												<th data-orderable="true">Watch</th>
												<th data-orderable="true">Care Giver </th>
									<th data-orderable="true">Unassign </th>
												
											</tr>
										</thead>								 
										  <tbody>
								        		<c:forEach var="data"  items="${data}" varStatus="loop"> 
								        		       
								            <tr>
								                <td>${data.id}</td>
								                <td>${data.watchNo}</td>
								                <td>${data.nurseName}</td>
								               <td> <a href="javascript:unAssignPatient('${data.id}');"><i style="font-size:18px !important;color:#FD8019"  class="fa fa-ban fa-fw"></a></i></td>
								            </tr>
								            </c:forEach>
								            		
								        </tbody>
									</table>
							</div>
							<!-- ** ./ widget ** -->
						</div>
					</div>
					</div>				
				</div>
				
			</div>
			
		</div>
		     
		<script>
		 function unAssignPatient(id){
			 bootbox.confirm("Are you sure to unasign the patient?", function(confirmed) {
				  if(confirmed) {
				$.ajax({
			        url: '/service/watch/unassignPC',
			        type: 'POST',
			        dataType: 'json',
			        data: "id=" + id,
			        success: function (data, textStatus, xhr) {
			            console.log(data);
			            window.location="/service/watch/getPatientClinician";
			        },
			        error: function (xhr, textStatus, errorThrown) {
			           console.log('Error in Authentication'+errorThrown.message);
			       }
			    });
				  }
				}); 
		    	      
		    		}
	
	 $(document).ready(function() {
		
     $("#nav-bar li").removeClass("active");
     $("#assign-nav").addClass("active");
     $("#PCTable").dataTable({
    	 "bPaginate":true,
          "sPaginationType":"full_numbers",
           "iDisplayLength":10,
           "bLengthChange": false,
           "bFilter": false
    });
     $("#PWTable").dataTable({
    	 "bPaginate":true,
          "sPaginationType":"full_numbers",
           "iDisplayLength":10,
           "bLengthChange": false,
           "bFilter": false
    });
    
    

	} );
	</script>
