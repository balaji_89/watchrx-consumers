	<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script	src="<c:url value="/resources/js/watch.js" />"></script>
		<div id="maincontainer" class="no-sidebar">
		<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->
		
			<div id="contentwrapper">
				<div >

				

					<div class="container-fluid" id="contentcolumn">
					<div class="row-fluid">
						<div class="span12">
						
							<div class="well">
								
									<!-- ** widget header ** -->
									<div class="row " style="padding:4px;padding-bottom:16px !important;">
									 <div class="col-xs-6" >
									 <div class="down-arrow" >Watch List (${fn:length(watchList)})</div>
								  </div>	 
									   <div class="col-xs-6">
												<a href="/service/watch/watch?watchId=0" style="font-size:18px !important;background:#FD8019 !important" class="btn btn-primary btn-large"><span><i class=" fa fa-plus"></i></span> Add New</a>
												</div>
									</div>
									<!-- ** ./ widget header ** -->
									<table id="watchDataTable" cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered medium-font" id="internal-doctor">
										<thead style="background:green;color:white">
											<tr>
												<th data-orderable="false">ID</th>
												<th data-orderable="true">IMEI Number</th>												
												<th data-orderable="true">Patient Name</th>
												<th data-orderable="false">Actions </th>
											</tr>
										</thead>
								 
										<tbody>
										<c:forEach var="watch"  items="${watchList}" varStatus="loop">     
											<tr>
												<td>${watch.watchId}</td>
												<td>${watch.imeiNumber}</td>
												<td>${watch.patient.lastName} ${watch.patient.firstName}</td>												
											<td><a
												href="/service/watch/watch?watchId=${watch.watchId}"
												<i style="font-size:18px !important;color:#FD8019" class="fa fa-edit"></i></a>&nbsp;&nbsp;
												<a href="#" ><i style="font-size: 18px !important; color: #FD8019"
												class="fa fa-trash" onclick="deleteWatch(${watch.watchId}); return false;"></a></i></td>
										</tr>
										</c:forEach>	       
										</tbody>
									</table>
							</div>
							<!-- ** ./ widget ** -->
						</div>
					</div>
					</div>				
				</div>
			</div>
		</div>
		<script>$(document).ready(function() {
	  $("#nav-bar li").removeClass("active");
	     $("#watch-nav").addClass("active");
	     $("#watchDataTable").dataTable({
	    	 "bPaginate":true,
	          "sPaginationType":"full_numbers",
	           "iDisplayLength":10,
	           "bLengthChange": false,
	           "bFilter": false
	    });
	} );
	</script>