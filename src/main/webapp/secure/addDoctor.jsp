<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div id="maincontainer" class="no-sidebar">
	<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->

	<div id="contentwrapper">
		<div>


			<div class="container-fluid" id="contentcolumn">
				<div class="row " style="padding-bottom: 16px !important;">
					<div class="col-xs-6">
						<div class="down-arrow">
							<c:choose>
								<c:when test="${doctorVO.doctorId>0}">
									       Edit Doctor
									    </c:when>
								<c:otherwise>
									    Add Doctor
									    </c:otherwise>
									    </c:choose>
						</div>
					</div>

					<div class="col-xs-6 pull-right">
						<a href="/service/doctor/doctorSummary"
							class="btn btn-primary btn-large btn-af pull-right"
							style="background: green !important; padding-left: 50px !important; padding-right: 50px !important;"><span></span>
							Discard</a>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<form:form class="form-horizontal" method="POST"
							modelAttribute="doctorVO" action="/service/doctor/saveDoctor" id="adddoctorform" enctype="multipart/form-data">
							<form:input type="hidden" path="doctorId" />
							<form:input type="hidden" path="picPath" />
							<div class="well">


								<form id="inputsample">

									<div class="row" style="padding: 8px">
										<div class="col-xs-2">
											<label class="control-label" >Upload Image</label>
											<br/>
											<form:errors cssClass="error-msg" path="picPath" />
										</div>
									
										<div class="fileinput fileinput-new" data-provides="fileinput"> 
										<div class="fileinput-new thumbnail" style="width: 50px; height: 50px;" placeholder="Upload avatar"> 
										<img height="75px" width="75px"
											src="${doctorVO.picPath}" alt="" />
										</div> <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 75px; max-height: 75px;">
										</div> <div> <span class="btn btn-success btn-file">
										<span class="fileinput-new">Select image</span>
										<span class="fileinput-exists">Change</span>
										<form:input type="file" path="imageFile"/>
										</span> <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a> 
										</div> 
										</div> 
									</div>
									<div class="row " style="padding: 4px">
										<div class="col-xs-2">
											<label class="control-label col-sm-2" for="firstName">Name:</label>
										</div>
										<div class="col-xs-4">
											<form:input type="text" id="firstName" class="form-control"
												placeholder="FirstName" path="firstName" />
											<form:errors cssClass="error-msg" path="firstName" />
										</div>
										<div class="col-xs-4">
											<form:input type="text" id="lastName" class="form-control"
												placeholder="LastName" path="lastName" />
											<form:errors cssClass="error-msg" path="lastName" />
										</div>
									</div>
									<div class="row " style="padding: 4px">
										<div class="col-xs-2">
											<label class="control-label col-sm-2" for="phoneNumber">Phone:</label>
										</div>
										<div class="col-xs-4">
											<form:input type="text" id="phoneNumber" data-mask="999-999-9999"
												class="form-control required" placeholder="Phone Number"
												path="phoneNumber" />
											<form:errors cssClass="error-msg" path="phoneNumber" />
										</div>
										<div class="col-xs-4">
											<form:input type="text" path="altPhoneNumber" class="form-control" data-mask="999-999-9999"
												placeholder="Alt Phone Number"  />
										</div>
									</div>
									<div class="row " style="padding: 4px">
										<div class="col-xs-2">
											<label class="control-label col-sm-2" for="hospital">Hospital:</label>
										</div>
										<div class="col-xs-4">
											<form:input type="text" id="hospital" class="form-control"
												placeholder="Hospital Name" path="hospital" />
											<form:errors cssClass="error-msg" path="hospital" />
										</div>
									</div>
									<div class="row " style="padding: 4px">
										<div class="col-xs-2">
											<label class="control-label col-sm-2" for="address1">Address:</label>
										</div>
										<div class="col-xs-8">
											<form:input type="text" id="address1" class="form-control"
												path="address.address1" />
											<form:errors cssClass="error-msg" path="address.address1" />
										</div>
									</div>
									<div class="row " style="padding: 4px">
										<div class="col-xs-2">
											<label class="control-label col-sm-2" for="address2">Address2:</label>
										</div>
										<div class="col-xs-8">
											<form:input placeholder="Additional Address(Optinal)"
												id="address2" class="form-control" path="address.address2" />
										</div>
									</div>
									<div class="row " style="padding: 4px">
										<div class="col-xs-2">
											<label class="control-label col-sm-2" for="city">Location:</label>
										</div>
										<div class="col-xs-2">
											<form:input type="text" id="city" class="form-control"
												placeholder="City" path="address.city" />
											<form:errors cssClass="error-msg" path="address.city" />
										</div>
										<div class="col-xs-2">
											<form:input type="text" id="state" class="form-control"
												placeholder="State" path="address.state" />
											<form:errors cssClass="error-msg" path="address.state" />
										</div>
										<div class="col-xs-2">
											<form:input type="text" id="zip" class="form-control"
												placeholder="Zip" path="address.zip" />
											<form:errors cssClass="error-msg" path="address.zip" />
										</div>
									</div>
									<div class="row " style="padding: 4px">
										<div class="col-xs-2">
											<label class="control-label col-sm-2" for="speciality">Speciality:</label>
										</div>
										<div class="col-xs-4">
											<form:select name="speciality" class="form-control"
												path="speciality">
												<form:option value="-1">--Choose Speciality--</form:option>
												<form:options items="${specialities}" />
											</form:select>
											<form:errors cssClass="error-msg" path="speciality" />
										</div>
									</div>
									<div class="row " style="padding: 4px">
										<div class="col-xs-2">
											<label class="control-label col-sm-2" for="userName">Username:</label>
										</div>
										<div class="col-xs-4">
											<form:input type="text" class="form-control" id="userName"
												placeholder="Username" path="userName" />
											<form:errors cssClass="error-msg" path="userName" />
										</div>
									</div>
									<div class="row " style="padding: 4px">
										<div class="col-xs-2">
											<label class="control-label col-sm-2" for="password">Password:</label>
										</div>
										<div class="col-xs-4">
											<form:input type="password" id="password"
												class="form-control" placeholder="Password" path="password" />
											<form:errors cssClass="error-msg" path="password" />
										</div>
										<div class="col-xs-6">
											<input type="submit" id="submit_btn" class="btn btn-primary "
												style="float: center; padding-left: 50px !important; padding-right: 50px !important; background: #FD8019 !important"
												value="Done" />
										</div>
									</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>$(document).ready(function() {
	function validateDonotSelect(value,element,param)
    {
        if(value == param)
        {
          return false;
        }
        else
        {
            return true;
        }      
    }
	$.validator.addMethod("letterswithbasicpuncextended", function(value, element) {
		return this.optional(element) || /^[a-z\-.,()'"\s\\&]+$/i.test(value);
	});
    jQuery.validator.addMethod("do_not_select",validateDonotSelect,"Please select an option");
	$("#nav-bar li").removeClass("active");
    $("#patient-nav").addClass("active");
    $('#adddoctorform').validate({
        rules: {
        	picPath: {
				required: true				
			      },
			firstName: {
				required: true,
				lettersonly: true
			},
			lastName: {
				required: true,
				lettersonly: true
			},
			'address.address1': {
				required: true				
			},
			'address.city': {
				required: true				
			},
			'address.state': {
				required: true,	
				stateUS: true
			},
			'address.zip': {
				required: true,
				zipcodeUS: true
			},
			phoneNumber: {
				required: true,
				phoneUS: true
			},
			altPhoneNumber: {
				phoneUS: true				
			},
			speciality: {
				do_not_select:'-1'
			},
			userName: {
				required: true
			},
			password: {
				required: true
			},
			hospital: {
				required: true,
				letterswithbasicpuncextended: true
			}
			
        },
        messages: {
        	picPath: {
                required: "You need to enter an image path"       
            },
			firstName: {
				required: "You need to enter a firstname"				
			},
			lastName: {
				required: "You need to enter a lastname"				
			},
			'address.address1': {
				required: "You need to enter an address"				
			},
			'address.city': {
				required: "You need to enter a US city"				
			},
			'address.state': {
				required: "You need to enter a US state",
				stateUS: "You need to enter a valid US state abbreviation"
			},
			'address.zip': {
				required: "You need to enter a US zipcode",	
				zipcodeUS: "You need to enter a valid US zipcode"				
			},
			phoneNumber: {
				required: "You need to enter a phone number in XXX-XXX-XXXX format",
				phoneUS: "You need to enter a valid US phone number"
			},
			altPhoneNumber: {
				phoneUS: "You need to enter a valid US phone number"				
			},
			speciality: {
				do_not_select:"You need to select a speciality"
			},
			userName: {
				required: "User name is required",
  				minlength: "Username should have a minimum of 15 characters",
  				maxlength: "Username can only have 73 characters"
			},
			password: {
				required: "Password is required",
  				maxlength: "Password can only have 6 characters"
			},
			hospital: {
				required: "Hospital name is required",
				letterswithbasicpuncextended: "You have entered illegal characters"
			}
			
        }
    });
     $("#nav-bar li").removeClass("active");
     $("#doctor-nav").addClass("active");
	} );
	</script>