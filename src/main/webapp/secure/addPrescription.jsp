<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div id="maincontainer" class="no-sidebar">
	<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->

	<div id="contentwrapper">
		<div>

			<div class="container-fluid" id="contentcolumn">
				<form:form class="form-horizontal" method="POST"
					modelAttribute="prescriptionVO"
					action="/service/patient/savePatentPrescription" enctype="multipart/form-data">
					<form:input type="hidden" path="patient.patientId" />
					<form:input type="hidden" path="prescriptionId" />
					<div class="row-fluid">
						<div class="span12">
							<div class="row ">
								<div class="col-xs-10">
									<label
										style="font-size: 15px !important; font-style: normal !important; color: black !important">Patient
										Profile</label>
								</div>
								<div class="col-xs-2">
									<label
										style="font-size: 15px !important; font-style: normal !important; color: black !important">Type</label>
								</div>
							</div>
							<div class="row "
								style="border-bottom: 1px solid black !important">
								<div class="col-xs-10">
									<img height="60px" width="60px" src="${prescriptionVO.patient.picPath}" /> <label
										style="font-size: 20px !important; font-style: normal !important; color: black !important">
										${prescriptionVO.patient.firstName} ${prescriptionVO.patient.lastName}</label>
								</div>
								<div class="col-xs-2">

									<label
										style="font-size: 20px !important; font-style: normal !important; color: black !important">Home
										</label>
								</div>
							</div>
							<div class="row " style="padding: 6px">

								<div class="row " style="padding-bottom: 16px !important;">
									<div class="col-xs-6">
										<div class="down-arrow">
											<c:choose>
												<c:when test="${prescriptionVO.prescriptionId>0}">
																       Edit Medical Prescription
																    </c:when>
												<c:otherwise>
																    New Medical Prescription
																    </c:otherwise>
											</c:choose>
										</div>
									</div>

									<div class="col-xs-6 pull-right">
										<a
											href="/service/patient/prescriptionSummary?patientId=${prescriptionVO.patient.patientId}"
											class="btn btn-primary btn-large btn-af pull-right"
											style="background: green !important; padding-left: 50px !important; padding-right: 50px !important;"><span></span>
											Discard</a>
									</div>
								</div>
							</div>
							<div class="well">


								<div class="row " style="padding: 6px">
									<div class="col-xs-2 ">
										<label>Medication*</label>
									</div>
									<div class="col-xs-6 ">
										<form:input id="medicine" path="medicine" class="form-control" />
										<form:errors cssClass="error-msg" path="medicine" />
									</div>
									<div class="col-xs-2 ">
										<label>Dosage*</label>
									</div>
									<div class="col-xs-2 ">
										<form:input id='dosage' type='text' class="form-control"
											placeholder="Dosage" path="dosage" />
										<form:errors cssClass="error-msg" path="dosage" />
									</div>
								</div>
								<div class="row " style="padding: 6px">
									<div class="col-xs-2 ">
										<label>Form*</label>
									</div>
									<div class="col-xs-6 ">
										<form:select name="medicineForm" class="form-control"
											path="medicineForm">
											<form:option value="-1">--Choose Form--</form:option>
											<form:options items="${medicineFormList}" />
										</form:select>
										<form:errors cssClass="error-msg" path="medicineForm" />
									</div>
									
								</div>
								<div class="row " style="padding: 6px">
									<div class="col-xs-2 ">
										<label>Before Or After Food</label>
									</div>
									<div class="col-xs-2">
										<form:select id="enableFixed" path="afterMeal" class="form-control" onchange="selFixed()">
											<form:option selected="selected" value="After">After</form:option>
											<form:option value="Before">Before</form:option>
											<form:option value="Any">Any</form:option>
											<form:option value="Fixed">Fixed</form:option>
										</form:select>
									</div>
									
									<div class="col-xs-1 ">
										<label>Color*</label>
									</div>
									<div class="col-xs-2 ">
										<form:input id='color' type='text' class="form-control"
											placeholder="color" path="color" />
										<form:errors cssClass="error-msg" path="color" />
									</div>
									<div class="col-xs-1 ">
										<label>Description</label>
									</div>
									<div class="col-xs-4 ">
										<form:textarea class="form-control" id="description"
											placeholder="Description" path="description"></form:textarea>
										<form:errors cssClass="error-msg" path="description" />
									</div>
								</div>
								
								<div class="row " id="fixedDiv" style="display:none" style="padding: 6px">
								<div class="col-xs-2 ">
										<label>Time Slots*</label>
									</div>
									<div class="col-xs-2">
										<form:select path="hour" id="hour" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${hourList}" />
										</form:select>
										&nbsp;
										<form:select path="minute" id="minute" class="form-control" style="display: inline !important;width: 70px !important;margin-bottom: 5px;">
											<form:options items="${minuteList}" />
										</form:select>
									</div>
									<div class="col-xs-3 ">
									   <button id="clearTime"  style="float:right;">Clear Time</button>&nbsp;
										<button id="time" style="float:right;">Add Time</button>&nbsp;
									</div>
									<div class="col-xs-4 ">
										<form:input  id='timeslots' type='text' class="form-control"
											placeholder="time slots" path="fixTimeslots" readonly="readonly" />
									</div>
									
								</div>
								<div class="row " style="padding: 6px">
									<div class="col-xs-2 ">
										<label>Refill</label>
									</div>
									<div class="col-xs-1 ">
										<form:checkbox id="refill" path="refill" class="form-control"/>
									</div>

									<div class="col-xs-1 ">
										<label>Strength*</label>
									</div>
									<div class="col-xs-2 ">
										<form:input id='quantity' type='text' class="form-control"
											placeholder="Strength" path="quantity" />
										<form:errors cssClass="error-msg" path="quantity" />
									</div>
									<div class="row" style="padding: 8px">
									<div class="col-xs-2">
											<label  class="control-label">Upload Image</label>
											<br/>
											<form:errors cssClass="error-msg" path="picPath" />
										</div>
									<%-- 	<div class="col-xs-4">
											<form:input id = "picPath" path="picPath" class="form-control"/>
											<form:errors cssClass="error-msg" path="picPath" />
										</div> --%>
									<div class="fileinput fileinput-new" data-provides="fileinput"> 
										<div class="fileinput-new thumbnail" style="width: 50px; height: 50px;" placeholder="Upload avatar"> 
										<img height="75px" width="75px"
											src="${prescriptionVO.picPath}" alt="" />
										</div> <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 75px; max-height: 75px;">
										</div> <div> <span class="btn btn-success btn-file">
										<span class="fileinput-new">Select image</span>
										<span class="fileinput-exists">Change</span>
										<form:input type="file" path="imageFile"/>
										</span> <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a> 
										</div> 
										</div> 
								</div>
								</div>
								<div class="row" style="padding: 6px">
									<span id="shiftDate" >
									<div class="col-xs-2 ">
										<label>Time Slots</label>
									</div>
									<div class="col-xs-2 ">
										<form:checkbox path="earlyMorningTime" />&nbsp;Early Morning
										<br><form:checkbox path="breakFastTime" />&nbsp;Breakfast
										<br><form:checkbox path="lunchTime" />&nbsp;Lunch
										<br><form:checkbox path="noonSnackTime" />&nbsp;AfternoonSnack
										<br><form:checkbox path="dinnerTime" />&nbsp;Dinner
										<br><form:checkbox path="bedTime" />&nbsp;Bed
									</div>
								</span>
									<div class="col-xs-2 ">
										<label>Days of Week</label>
									</div>
									<div class="col-xs-2 ">
										<form:checkbox path="sunday" />&nbsp;Sunday
										<br><form:checkbox path="monday" />&nbsp;Monday
										<br><form:checkbox path="tuesday" />&nbsp;Tuesday
										<br><form:checkbox path="wednesday" />&nbsp;Wednesday
										<br><form:checkbox path="thursday" />&nbsp;Thursday
										<br><form:checkbox path="friday" />&nbsp;Friday
										<br><form:checkbox path="saturday" />&nbsp;Saturday
									</div>
								
								<div class="row " style="padding: 4px">

									<div class="col-xs-1 pull-right">
										<input type="submit" class="btn btn-primary "
											style="font-size: 18px !important; background: #FD8019 !important"
											value="Submit" />
									</div>
								</div>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>

<script>
if($("#enableFixed").val()=="Fixed"){
	$("#fixedDiv").show();
	$("#shiftDate").hide();
}
function selFixed() {
    var  selectedValue= $("#enableFixed").val();
    if(selectedValue=="Fixed"){
    	$("#fixedDiv").show();
    	$("#shiftDate").hide();
    }else{
    	$("#fixedDiv").hide();
    	$("#shiftDate").show();
    }
   }
	$(document).ready(function() {
	
		$("#nav-bar li").removeClass("active");
		$("#patient-nav").addClass("active");
		$('#endDate').datepicker({
			changeYear : true,
			yearRange : "1900:2115",
			dateFormat : 'dd-mm-yy'
		}).datepicker("endDate", $('#endDate').val());
		$('#datePicker1').datepicker({
			changeYear : true,
			yearRange : "1900:2115",
			dateFormat : 'dd-mm-yy'
		}).datepicker("datePicker1", $('#datePicker1').val());
		
		
		
		$( "#time" ).click(function() {
				 var hr = $('#hour').val();
				  var min = $('#minute').val();
				  var timeslots = $('#timeslots').val();

				  var total = hr+":"+min;
				  sessionStorage.setItem('timeArray', timeslots);

				  var values = sessionStorage.getItem('timeArray');
				  var res;
				  if(values!=null  && typeof values!="undefined"){
					  console.log(values);
					  if(values.length>0){
				   res = values.concat("|"+total);
					  }else{
						   res = values.concat(total);
					  }
				  }
				  sessionStorage.setItem('timeArray', res);
				//  var result =  res.replace("undefined|", "")
				  $("#timeslots").val( res);
			});
		$( "#clearTime" ).click(function() {
			 // var values = '';

			 // sessionStorage.setItem('timeArray', values);
			  sessionStorage.clear();
			   $("#timeslots").val("");
			});
		
	});
</script>