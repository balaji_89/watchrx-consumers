	<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script	src="<c:url value="/resources/js/doctor.js" />"></script>
		<div id="maincontainer" class="no-sidebar">
		<!---- ************* ./ CLASS FOR NO SIDEBAR ********* -->
		
			<div id="contentwrapper">
				<div >

					<div class="container-fluid" id="contentcolumn">
					<div class="row-fluid">
						<div class="span12">
						
							<div class="well">
								
									<!-- ** widget header ** -->
									<div class="row " style="padding:4px;padding-bottom:16px !important;">
									 <div class="col-xs-6" >
									 <div class="down-arrow" >Patient List (${fn:length(ruokList)})  ${statsInfo}</div>
									  </div>	 
									</div>
									<div class="row " style="padding: 4px">
									<div class="col-xs-2"><label class="control-label">Select Date</label></div>
									<div class="col-xs-2"><input type="text" id="datepicker"></div>
									</div>
									
									<!-- ** ./ widget header ** -->
									<table id="ruokDataTable" cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-bordered medium-font" id="internal-doctor">
										<thead style="background:green;color:white">
											<tr>
												<th data-orderable="true">Patient Name</th>
												<th data-orderable="true">Status</th>
												<th data-orderable="true">Date </th>
											</tr>
										</thead>								 
										  <tbody>
								        		<c:forEach var="ruok"  items="${ruokList}" varStatus="loop">        
								            <tr>
								                <td>${ruok.name}</td>
								                <td>${ruok.status}</td>
								                <td>${ruok.date}</td>
								            </tr>
								            </c:forEach>
								            		
								        </tbody>
									</table>
							</div>
							<!-- ** ./ widget ** -->
						</div>
					</div>
					</div>				
				</div>
			</div>
			
		</div>
		<script>		
	 $(document).ready(function() {
     $("#nav-bar li").removeClass("active");
     $("#ruok-nav").addClass("active");
     $("#ruokDataTable").dataTable({
    	 "bPaginate":true,
          "sPaginationType":"full_numbers",
           "iDisplayLength":10,
           "bLengthChange": false,
           "bFilter": false
    });
    $( "#datepicker" ).datepicker({
         changeMonth: true,//this option for allowing user to select month
         changeYear: true, //this option for allowing user to select from year range
         dateFormat: 'yy-mm-dd',
         onSelect: 		function (date){        	 
			window.location="/service/patient/ruokSummaryByDate?date="+date;
		}

       });
	} );
	</script>
