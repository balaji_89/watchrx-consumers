<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="robots" content="all">

<title><tiles:insertAttribute name="title" /></title>
<link rel="stylesheet"	href="<c:url value="/resources/css/bootstrap/bootstrap-3.3.2.min.css" />">
<link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.min.css" />">
<link rel="stylesheet" href="<c:url value="/resources/css/style.css" />">
<link rel="stylesheet" href="<c:url value="/resources/css/buttons.css" />">
<link rel="stylesheet" href="<c:url value="/resources/css/jquery/jquery-ui/jquery-ui-1.11.2.css" />">
<link rel="stylesheet" href="<c:url value="/resources/css/jquery/jquery.dataTables.1.10.7.css" />">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap/bootstrap-toggle-2.2.0.min.css" />">
<link rel="stylesheet" href="<c:url value="/resources/css/jasny-bootstrap.css" />"  rel="stylesheet">
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap/bootstrap-toggle-2.2.0.min.css" />"  rel="stylesheet">

<script src="<c:url value="/resources/js/jquery/jquery-2.1.3.min.js" />"></script>
<script	src="<c:url value="/resources/js/bootstrap/bootstrap-3.3.2.min.js" />"></script>
<script src="<c:url value="/resources/js/jasny-bootstrap.js" />"></script>
<script src="<c:url value="/resources/js/fileinput.js" />"></script>
<script src="<c:url value="/resources/js/jquery/jquery-ui-1.11.2.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery/jquery.dataTables-1.10.7.min.js" />"></script>
<script src="<c:url value="/resources/js/jquery/bootbox-4.4.0/bootbox.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap/bootstrap-toggle-2.2.0.min.js" />"></script>
<script src="<c:url value="/resources/js/jqueryvalidation/jquery.validate.js" />"></script>
<script	src="<c:url value="/resources/js/jqueryvalidation/additional-methods.js" />"></script>
<!-- Favicon -->
<link rel="shortcut icon"	href="<c:url value="/resources/images/favicon.ico" />">
<style>
		  .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 40px; }
		  .toggle.ios .toggle-handle { border-radius: 40px; }
	
		</style>
</head>
<body>
	<div class="wrapper">
		<tiles:insertAttribute name="header" />
		<tiles:insertAttribute name="body" />
		<tiles:insertAttribute name="navbar" />
	</div>
</body>
</html>
