<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<header>
	<div class="blue navbar navbar-static nomargin">
		<div class="navbar-inner">
			<div class="container" style="width: auto; height: 38px">
				<label class="brand" style="text-align:center !important;font-size:24px !important;color: #ffffff !important;">WatchRx</label>
				<div class="pull-right" style="margin-top: 7px !important;">
					<font style="color:#ffffff;text-decoration: none;font-weight: bold;"><i class="fa fa-user"></i> &nbsp;Hello, ${user.firstName} ${user.lastName}</font>&nbsp;&nbsp;&nbsp;&nbsp;<a href="/logout" style="color:#ffffff;text-decoration: none;" ><img src="/resources/images/Logout.png" />&nbsp;Logout</a>
				</div>
			</div>
		</div>
	</div>
</header>
