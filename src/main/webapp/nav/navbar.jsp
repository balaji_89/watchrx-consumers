<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<navbar>
	<div id="leftcolumn">

				<!-- ** left panel ** -->
				<div id="leftpanel">
					<div class="leftpanel-wrapper">
					
						<ul id="nav-bar" class="nav nav-pills nav-stacked">
						<c:if test="${user != null && (user.roleType == 1 || user.roleType == 2)}">
							<li id="watch-nav" class="active">
								<a href="/service/watch/watchSummary">
									<img src="/resources/images/watch.png" /></br>
									WATCH
								</a>
							</li>
							<li id="doctor-nav">
								<a href="/service/doctor/doctorSummary">
									<img src="/resources/images/Doctor.png" /></br>
									DOCTOR
								</a>
							</li>
							
							<li id="clinician-nav" >
								<a href="/service/clinician/clinicianSummary">
									<img src="/resources/images/Nurse.png" /></br>
									CLINICIAN
								</a>
							</li>
						</c:if>
							<li id="patient-nav">
								<a href="/service/patient/patientSummary">
									<img src="/resources/images/Patient.png" /></br>
									Parent
								</a>
							</li>
							<li id="alert-nav">
								<a href="/service/patient/alertSummaryDBrd">
									<img src="/resources/images/Alert-icon.png" /></br>
									Alert
								</a>
							</li>
							<c:if test="${user != null && (user.roleType != 1)}">
							<li id="assign-nav1">
								<a href="/service/watch/watchAssignForConsumer">
									<i class="fa fa-table fa-3x"></i>
									</br>ASSIGN
								</a>
							</li>
							</c:if>
							<c:if test="${user != null && (user.roleType == 1 || user.roleType == 2)}">
							<li id="assign-nav">
								<a href="/service/watch/watchAssign">
									<i class="fa fa-table fa-3x"></i>
									</br>ASSIGN
								</a>
							</li>
							
							
							<c:if test="${user != null && (user.roleType == 1 )}">
							<li id="apk-nav">
								<a href="/service/watch/APKVersionSummary">
									<img src="/resources/images/reload-system-software-update-icone-7320-48.png" />
									</br>S/w Upgrade
								</a>
							</li>
							</c:if>
							<li id="ruok-nav">
								<a href="/service/patient/ruokSummary">
									<img src="/resources/images/OK.png" />
								</a>
							</li>
							
						</c:if>
						</ul>
					</div>
				</div>
			</div>
</navbar>