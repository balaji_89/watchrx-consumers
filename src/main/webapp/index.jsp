<!DOCTYPE html>
<html lang="en">

	<!-- ==============
	******** Filename: layout-no-sb.html ********
	******** Layout: No Sidebar ********
	================== -->

	<head>
		<meta charset="utf-8">
		<title>WatchRx - Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="" />
		<meta name="author" content="" />

		<%@page contentType="text/html;charset=UTF-8"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="robots" content="all">
<meta name="google-signin-client_id" content="132597499285-kt7av2kegp2a9q96rrdd6gfg58di02dg.apps.googleusercontent.com">

<link rel="stylesheet"	href="<c:url value="/resources/css/bootstrap/bootstrap-3.3.2.min.css" />">
<link rel="stylesheet" href="<c:url value="/resources/css/style.css" />">
<!-- Fonts -->
<!-- Icons/Glyphs -->
<link rel="stylesheet" href="<c:url value="/resources/css/font-awesome.min.css" />">
<!-- Favicon -->
<link rel="stylesheet"	href="<c:url value="/resources/css/jquery/buttons.dataTables.min.css" />">

<link rel="shortcut icon"	href="<c:url value="/resources/images/favicon.ico" />">
<script src="<c:url value="/resources/js/jquery/jquery-2.1.3.min.js" />"></script>
<script	src="<c:url value="/resources/js/bootstrap/bootstrap-3.3.2.min.js" />"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="<c:url value="/resources/js/jqueryvalidation/jquery.validate.min.js" />"></script>
<script	src="<c:url value="/resources/js/jqueryvalidation/additional-methods.min.js" />"></script>


<script src="<c:url value="/resources/js/login.js" />"></script>

			<style>	
			
		  .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 40px; }
		  .toggle.ios .toggle-handle { border-radius: 40px; }
		</style>
	</head>
	<body class="login-bg" >   
         <div class="container">    
        <div id="loginbox" style="margin-top:100px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">  
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div  style="text-align:center !important;font-size:30px !important;padding-bottom: 5px;">WatchRx </div>
                    </div>     
                    <div style="padding-top:30px" class="panel-body" >
                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                         <form id="loginform" class="form-horizontal" role="form">
                         <p id="sign_error" style="color:red"></p>
                             <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username">                                        
                                    </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input id="login-password" type="password" class="form-control" name="password" placeholder="password">
                                    </div>
                           <div class="input-group" style="width:100%">
                                <div class="form-group">
                                   <div class="col-sm-12 controls">
                                      <a id="btn-login" href="#" class="btn btn-success pull-right" style="float:center;padding-left:50px !important;padding-right:50px !important;" onclick="javascript:login();return false;">Login  </a>
                                    </div>
<!--                                     <div class="g-signin2" data-onsuccess="onSignIn"></div> -->
                                </div>
                             </form>     
                        </div>                     
                    </div>  
        </div>
     </div>
 	</body>
</html>
