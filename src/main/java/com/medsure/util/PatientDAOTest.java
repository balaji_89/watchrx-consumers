package com.medsure.util;

import java.util.ArrayList;
import java.util.List;

import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.medsure.ui.entity.caregiver.response.PatientCGInfo;
import com.medsure.ui.entity.caregiver.response.PatientGCInfos;
import com.medsure.ui.entity.patient.common.MedicationInfo;
import com.medsure.ui.entity.patient.response.PatientDetails;

public class PatientDAOTest {

	public static final String[] medicineName = { "Lasix", "Zoloft",
			"Lisinopril", "Aricept", "Seroquel", "Ativan", "Coumadin",
			"Remeron", "Synthroid", "Protonix" };
	
	public static final String[] description = {
			"used for treating high blood pressure",
			"Used for treating depression",
			"Used for treating congestive heart failure, and to improve survival after a heart attack",
			"used for treating dementia of the Alzheimers type.",
			"an antipsychotic medication",
			"used to treat anxiety associated with symptoms of depression",
			"used to treat or prevent blood clots",
			"used for treating depression",
			"used to treat low thyroid activity",
			"Used to treat erosive esophagitis and other conditions involving excess stomach acid." };
	public static final String[] dosage = { "10mg", "20mg", "30mg", "40mg",
			"50mg", "60mg", "70mg", "80mg", "90mg", "100mg" };
	public static final String[] quantity = { "2", "3", "3", "5", "2", "3",
			"5", "2", "1", "3" };
	public static final String[] image = { "medicine/lasix.JPG", "medicine/zoloft.jpg",
		"medicine/lisinopril.jpg", "medicine/aricept.jpg", "medicine/seroquel.jpg", "medicine/ativan.jpg", "medicine/ativan.jpg",
		"medicine/remeron.jpg", "medicine/synthroid.jpg", "medicine/protonix.jpg" };
	// public static final String[] dosage ={"","","","","","","","","",""};
	public static final String[] intakeTime = { "6:00 am", "8:00 am", "10:00 am",
			"12:00 am", "7:00 pm", "11:00 am", "9:00 pm", "10:00 pm", "6:00 pm",
			"11:45 pm" };
	public static final String[] color = { "white", "blue", "white", "yellow",
			"yellow", "white", "blue", "yellow", "blue", "yellow" };
	public static final String[] address = { "Boston, Massachusetts",
			"Baltimore, Maryland", "Los Angeles", "Los Angeles, California",
			"New York, New York", "Chicago, Illinois", "Seattle, Washington",
			"Durham, North Carolina" };
	public static final String[] mobileNo = { "660 247 5651", "660 247 5652",
			"660 247 5653", "660 247 5654", "660 247 5655", "660 247 5656",
			"660 247 5657", "660 247 5658" };
	public static final String[] visitReason = { "Cardiac Arrest", "Neuritis",
			"Psoriasis", "Enlarged left heart", "Cardiac Arrest",
			"Osteoporosis", "Sciatica", "Dementia" };
	public static final String[] insuranceCompany = { "United Health Care",
			"United Health Care", "United Health Care", "United Health Care",
			"United Health Care", "United Health Care", "United Health Care",
			"United Health Care" };
	public static final String[] insuranceId = { "213 84 7691", "213 84 7692",
			"213 84 7693", "213 84 7694", "213 84 7695", "213 84 7696",
			"213 84 7697", "213 84 7698" };
	public static final String[] patientimage = { "patient/patient-1.jpg", "patient/patient-2.jpg", "patient/patient-3.jpg",
			"patient/patient-4.jpg", "patient/patient-5.jpg", "patient/patient-6.jpg", "patient/patient-7.jpg", "patient/patient-8.jpg" };
	
	public static final String[] nurseId = {"12","13","14","15"};
	public static final String[] patientId = {"5,4","1,0","3,6","2,1"};

	public void insertPatient() {
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		for (int i = 0; i <= 6; i++) {
			Entity e = new Entity("patient");
			e.setProperty("patientId", i);
			e.setProperty("imeiNo", Math.random());
			// e.setProperty("address", address[i]);
			e.setProperty("address", address[i]);
			e.setProperty("mobileNo", mobileNo[i]);
			e.setProperty("visitReason", visitReason[i]);
			e.setProperty("insuranceCompany", insuranceCompany[i]);
			e.setProperty("insuranceId", insuranceId[i]);
			e.setProperty("patientimage", patientimage[i]);
			ds.put(e);
		}

	}
	
	public void insertCGPatient() {
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		for (int i = 0; i < nurseId.length; i++) {
			Entity e = new Entity("cgandpatient");
			e.setProperty("nurseId", nurseId[i]);
			e.setProperty("patientIds", patientId[i]);
			ds.put(e);
		}

	}

	public void insertMedicineForPatient() {
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		for (int i = 0; i <= 8; i++) {
			Entity e = new Entity("medicationinfo");
			e.setProperty("patientId", i);
			e.setProperty("medicineName", medicineName[i]);
			e.setProperty("intaketime", intakeTime[i]);
			e.setProperty("image", image[i]);
			e.setProperty("quantity", quantity[i]);
			e.setProperty("color", color[i]);
			e.setProperty("quantity", quantity[i]);
			e.setProperty("dosage", dosage[i]);
			e.setProperty("description", description[i]);
			ds.put(e);
		}
	}

	public PatientDetails getPatientInfo(String imeiNo) {
		PatientDetails pd = new PatientDetails();
		DatastoreService ds1 = DatastoreServiceFactory.getDatastoreService();
		System.out.println("insert medicine");
		Query q = new Query("patient");
		PreparedQuery ps = ds1.prepare(q);
		for (Entity p : ps.asIterable()) {
			if (p.getProperty("imeiNo").toString().equalsIgnoreCase(imeiNo)) {
				System.out.println(p.getProperty("imeiNo").toString());
				pd.setAddress(p.getProperty("address").toString());
				pd.setImage(readTextFileOnly(p.getProperty("patientimage").toString()));
				pd.setImeiNo(p.getProperty("imeiNo").toString());
				pd.setInsuranceCompany(p.getProperty("insuranceCompany")
						.toString());
				pd.setInsuranceId(p.getProperty("insuranceId").toString());
				pd.setPatientId(p.getProperty("patientId").toString());
				pd.setPhone(p.getProperty("mobileNo").toString());
				pd.setVisitReason(p.getProperty("visitReason").toString());
			}
			
		}
		
		Query q1 = new Query("medicationinfo");
		PreparedQuery ps1 = ds1.prepare(q1);
		List<MedicationInfo> list = new ArrayList<MedicationInfo>();
	/*	e.setProperty("patientId", i);
		e.setProperty("medicineName", medicineName[i]);
		e.setProperty("intaketime", intakeTime[i]);
		e.setProperty("image", image[i]);
		e.setProperty("quantity", quantity[i]);
		e.setProperty("color", color[i]);
		e.setProperty("quantity", quantity[i]);
		e.setProperty("dosage", dosage[i]);
		e.setProperty("description", description[i]);*/	
		for (Entity p1 : ps1.asIterable()) {
			MedicationInfo mi = new MedicationInfo();
			mi.setColor(p1.getProperty("color").toString());
			mi.setDescription(p1.getProperty("description").toString());
			mi.setStrength(p1.getProperty("dosage").toString());
			mi.setImage(readTextFileOnly(p1.getProperty("image").toString()));
			mi.setMedicineName(p1.getProperty("medicineName").toString());
			mi.setDosage(p1.getProperty("quantity").toString());
			list.add(mi);
		}
		pd.setMedicationDetail(list);
		pd.setPatientId("p1");
		pd.setResponseCode("001");
		pd.setResponseType("1");
		pd.setResponseMessage("operationsuccessful");
		pd.setStatus("success");
		
		return pd;
		
	}
	public PatientCGInfo getPatientForCG(String patientId) {
		PatientCGInfo pd = new PatientCGInfo();
		DatastoreService ds1 = DatastoreServiceFactory.getDatastoreService();
		System.out.println("insert medicine");
		Query q = new Query("patient");
		PreparedQuery ps = ds1.prepare(q);
		for (Entity p : ps.asIterable()) {
			if (p.getProperty("patientId").toString().equalsIgnoreCase(patientId)) {
				System.out.println(p.getProperty("imeiNo").toString());
				pd.setAddress(p.getProperty("address").toString());
				pd.setImage(readTextFileOnly(p.getProperty("patientimage").toString()));
				pd.setImeiNo(p.getProperty("imeiNo").toString());
				pd.setInsuranceCompany(p.getProperty("insuranceCompany")
						.toString());
				pd.setInsuranceId(p.getProperty("insuranceId").toString());
				pd.setPatientId(p.getProperty("patientId").toString());
				pd.setPhone(p.getProperty("mobileNo").toString());
				pd.setVisitReason(p.getProperty("visitReason").toString());
				pd.setPatientId(p.getProperty("patientId").toString());
			}
			
		}
		pd.setVisitShift("morning");
		return pd;
		
	}
	
	public PatientGCInfos patientCGInfos(String nurseId){
		Query q = new Query("cgandpatient");
		String patientId = null;
		PatientGCInfos infos = new PatientGCInfos();
		List<PatientCGInfo> list = new ArrayList<PatientCGInfo>();
		DatastoreService ds1 = DatastoreServiceFactory.getDatastoreService();
		PreparedQuery ps = ds1.prepare(q);
		for (Entity p : ps.asIterable()) {
			if (p.getProperty("nurseId").toString().equalsIgnoreCase(nurseId)) {
				
				patientId = p.getProperty("patientIds").toString();
			}
		}
			String[] tempArray = patientId.split(",");
			for (int i =0;i<tempArray.length;i++){
				PatientCGInfo info =  getPatientForCG(tempArray[i]);
				list.add(info);
			}
			infos.setPatientInfo(list);
			infos.setNurseId(nurseId);
			infos.setResponseCode("001");
			infos.setResponseType("1");
			infos.setResponseMessage("operationsuccessful");
			infos.setStatus("success");
		return infos;
		
	}
	public String readTextFileOnly(String image) {
		AppIdentityService appIdentityService = AppIdentityServiceFactory.getAppIdentityService();
		String defaultBucketName = appIdentityService.getDefaultGcsBucketName();
		String fileName = image;
		GcsFilename gcsFile =  new GcsFilename(defaultBucketName, fileName);
	    ImagesService is = ImagesServiceFactory.getImagesService(); 
	    String filename = String.format("/gs/%s/%s", gcsFile.getBucketName(), gcsFile.getObjectName());
	    String servingUrl = is.getServingUrl(ServingUrlOptions.Builder.withGoogleStorageFileName(filename));
		return servingUrl;
	}
}
