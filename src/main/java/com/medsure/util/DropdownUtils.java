package com.medsure.util;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Table;
import com.medsure.service.ReferenceService;

public class DropdownUtils {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static Table<String,Integer,String> referenceTable;

	@Autowired
	private ReferenceService referenceService;
	
	@PostConstruct
	private void init() {
		referenceTable = referenceService.getReferenceData();
		
	}
	
	public static Map<Integer,String> getRefDataByRefType (String refType){
		return referenceTable.row(refType);
	}
	
	public static String getRefNameByRefTypeAndRefId (String refType, Integer id){
		return referenceTable.get(refType,id);
	}
	

	public  static java.util.Map<String, String> hours(){
		java.util.Map<String, String> hours= new java.util.LinkedHashMap<String, String>();
		 for(int i=1;i<24;i++){
			 if(i<10){
			 hours.put("0"+i, "0"+i);
			 }else{
				 hours.put(""+i, ""+i);
			 }
		 }
		 return hours;
	}
	
	public  static java.util.Map<String, String> minutes(){
		java.util.Map<String, String> mins= new java.util.LinkedHashMap<String, String>();
		 for(int i=0;i<61;i=i+5){
			 if(i<10){
				 mins.put("0"+i, "0"+i);
			 }else{
				 mins.put(""+i, ""+i);
			 }
		 }
		 return mins;
	}
	
}
