package com.medsure.util;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;

public class AuthTokenChecker {

private final List mClientIDs;
private final String mAudience;
private final GoogleIdTokenVerifier mVerifier;
private final JsonFactory mJFactory;
private String mProblem = "Verification failed. (Time-out?)";

private  Logger log ;

public AuthTokenChecker(String[] clientIDs, String audience) {
    mClientIDs = Arrays.asList(clientIDs);
    mAudience = audience;
    NetHttpTransport transport = new NetHttpTransport();
    mJFactory = new GsonFactory();
    mVerifier = new GoogleIdTokenVerifier(transport, mJFactory);
   log = Logger.getLogger(AuthTokenChecker.class.getName()); 
}

public boolean check(String tokenString) {
    GoogleIdToken.Payload payload = null;
    boolean isVerified = false;
    try {
        GoogleIdToken token = GoogleIdToken.parse(mJFactory, tokenString);
        if (mVerifier.verify(token)) {
        	isVerified = true;
            GoogleIdToken.Payload tempPayload = token.getPayload();
            if (!tempPayload.getAudience().equals(mAudience)){
                mProblem = "Audience mismatch";
                log.severe("Audience mismatch");
            }
            else if (!mClientIDs.contains(tempPayload.getIssuee())){
                mProblem = "Client ID mismatch";
                log.severe("Client ID mismatch");
            }
            else{
                payload = tempPayload;
                log.severe(payload.getEmail().toString());
            }
        }
    } catch (GeneralSecurityException e) {
        mProblem = "Security issue: " + e.getLocalizedMessage();
    } catch (IOException e) {
        mProblem = "Network problem: " + e.getLocalizedMessage();
    }
    log.severe("CHECK END");
    return isVerified;
}

public String problem() {
    return mProblem;
}
}
