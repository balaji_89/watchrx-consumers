package com.medsure.factory;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.medsure.service.AddressService;
import com.medsure.service.AdhereLogService;
import com.medsure.service.AlertService;
import com.medsure.service.ClinicianService;
import com.medsure.service.DoctorService;
import com.medsure.service.PatientService;
import com.medsure.service.RUOkService;
import com.medsure.service.ReferenceService;
import com.medsure.service.UserService;
import com.medsure.service.WatchService;

public class WatchRxFactory implements ApplicationContextAware {

	private static ApplicationContext context;

	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		this.context = context;
	}

	public static ApplicationContext getApplicationContext() {
		return context;
	}

	public static PatientService getPatientService() {
		return (PatientService) getApplicationContext().getBean("patientService");
	}
	
	public static AddressService getAddressService() {
		return (AddressService) getApplicationContext().getBean("addressService");
	}
	
	public static ClinicianService getClinicianService() {
		return (ClinicianService) getApplicationContext().getBean("clinicianService");
	}
	
	public static DoctorService getDoctorService() {
		return (DoctorService) getApplicationContext().getBean("doctorService");
	}
	
	public static WatchService getWatchService() {
		return (WatchService) getApplicationContext().getBean("watchService");
	}
	
	public static ReferenceService getReferenceService() {
		return (ReferenceService) getApplicationContext().getBean("referenceService");
	}
	
	public static AdhereLogService getAdhereLogService() {
		return (AdhereLogService) getApplicationContext().getBean("adhereLogService");
	}
	
	public static AlertService getAlertService() {
		return (AlertService) getApplicationContext().getBean("alertService");
	}
	
	public static UserService getUserService() {
		return (UserService) getApplicationContext().getBean("userService");
	}
	
	public static RUOkService getruOkService() {
		return (RUOkService) getApplicationContext().getBean("ruOkService");
	}
}
