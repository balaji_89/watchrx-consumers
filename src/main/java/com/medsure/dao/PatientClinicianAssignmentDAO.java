/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxPatientClinicianAssignmnt;


/**
 * @author snare
 *
 */

public interface PatientClinicianAssignmentDAO extends BaseDAO<WatchrxPatientClinicianAssignmnt>{

}
