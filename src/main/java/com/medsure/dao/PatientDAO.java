/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxPatient;


/**
 * @author snare
 *
 */
public interface PatientDAO extends BaseDAO<WatchrxPatient>{
	
}
