/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxPatientPrescription;


/**
 * @author snare
 *
 */

public interface PatientPrescriptionDAO extends BaseDAO<WatchrxPatientPrescription>{

}
