/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxDoctor;

/**
 * @author snare
 *
 */
public interface DoctorDAO extends BaseDAO<WatchrxDoctor>{

}
