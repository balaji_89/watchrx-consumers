/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxMedicationAdhereLog;

/**
 * @author snare
 *
 */
public interface MedicationAdhereLogDAO extends BaseDAO<WatchrxMedicationAdhereLog>{

}
