package com.medsure.dao;

import java.io.Serializable;
import java.util.List;

/**
 * @author snare
 *
 */

public interface BaseDAO<E> {

	public E save(E e);
	
	public void save(List<E> es);

	public List<E> getAll();

	public E getById(Serializable id);

	public List<E> findByProperty(String propertyName, Object value);

	public void delete(Serializable id);

	public void deleteAll(List<E> e);
	
	public void delete(E e);
	
	public void deleteByProperty(String propertyName, Object value);
	
}
