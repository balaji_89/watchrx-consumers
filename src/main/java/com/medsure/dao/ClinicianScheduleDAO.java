/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxClinicianSchedule;


/**
 * @author snare
 *
 */
public interface ClinicianScheduleDAO extends BaseDAO<WatchrxClinicianSchedule>{

}
