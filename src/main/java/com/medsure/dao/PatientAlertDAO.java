/**
 * 
 */
package com.medsure.dao;

import java.util.List;

import com.medsure.model.WatchrxPatientAlert;
import com.medsure.ui.entity.caregiver.request.GetAlerts;


/**
 * @author snare
 *
 */
public interface PatientAlertDAO extends BaseDAO<WatchrxPatientAlert>{


	List<WatchrxPatientAlert> getAlertsByDateByPatientId(GetAlerts patientId);
	
}
