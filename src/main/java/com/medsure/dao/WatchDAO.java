/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxWatch;


/**
 * @author snare
 *
 */
public interface WatchDAO extends BaseDAO<WatchrxWatch>{
	
}
