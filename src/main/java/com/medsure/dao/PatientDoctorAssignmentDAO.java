/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxPatientDoctorAssignmnt;


/**
 * @author snare
 *
 */

public interface PatientDoctorAssignmentDAO extends BaseDAO<WatchrxPatientDoctorAssignmnt>{

}
