/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxAddress;


/**
 * @author snare
 *
 */

public interface AddressDAO extends BaseDAO<WatchrxAddress>{

}
