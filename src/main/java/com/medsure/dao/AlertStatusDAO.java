/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxAlertStatus;


/**
 * @author snare
 *
 */

public interface AlertStatusDAO extends BaseDAO<WatchrxAlertStatus>{

	public void deleteAlertStatusByPatientId (Long patientId);
}
