/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxReference;


/**
 * @author snare
 *
 */
public interface ReferenceDAO extends BaseDAO<WatchrxReference>{

}
