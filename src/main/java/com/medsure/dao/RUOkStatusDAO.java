/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxRuOkStatus;


/**
 * @author snare
 *
 */

public interface RUOkStatusDAO extends BaseDAO<WatchrxRuOkStatus>{

}
