/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxPatientWatchAssignmnt;


/**
 * @author snare
 *
 */

public interface PatientWatchAssignmentDAO extends BaseDAO<WatchrxPatientWatchAssignmnt>{

}
