/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxWatchAPK;


/**
 * @author snare
 *
 */
public interface WatchAPKDAO extends BaseDAO<WatchrxWatchAPK>{
	
}
