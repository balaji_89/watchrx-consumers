/**
 * 
 */
package com.medsure.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.medsure.common.Constants;
import com.medsure.dao.PatientPrescriptionDAO;
import com.medsure.model.WatchrxPatientPrescription;

/**
 * @author snare
 *
 */
@Component
public class PatientPrescriptionDAOImpl extends BaseDAOImpl<WatchrxPatientPrescription> implements PatientPrescriptionDAO {

	@SuppressWarnings("unchecked")
	@Override
    public List<WatchrxPatientPrescription> getAll() {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	List<WatchrxPatientPrescription> entities = (List<WatchrxPatientPrescription>)em.createQuery("FROM WatchrxPatientPrescription WHERE status='"+Constants.Status.ACTIVE+"'")
       	        .getResultList();
          return entities;
    }

    /**
     * Gets a entity by property id
     */
    @SuppressWarnings("unchecked")
	public List<WatchrxPatientPrescription> findByProperty(String propertyName, Object value) {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	Query query = em.createQuery("SELECT e FROM WatchrxPatientPrescription e WHERE status='"+Constants.Status.ACTIVE+"' AND e."+propertyName +"= "+value);
    	List<WatchrxPatientPrescription> list = query.getResultList();
      	return list;
    }
   
}

