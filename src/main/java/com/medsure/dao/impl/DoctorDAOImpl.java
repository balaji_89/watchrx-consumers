package com.medsure.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.medsure.common.Constants;
import com.medsure.dao.DoctorDAO;
import com.medsure.model.WatchrxDoctor;

/**
 * @author snare
 *
 */
@Component
public class DoctorDAOImpl extends BaseDAOImpl<WatchrxDoctor> implements DoctorDAO {

	@SuppressWarnings("unchecked")
	@Override
    public List<WatchrxDoctor> getAll() {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	List<WatchrxDoctor> entities = (List<WatchrxDoctor>)em.createQuery("FROM WatchrxDoctor WHERE status='"+Constants.Status.ACTIVE+"'")
       	        .getResultList();
         return entities;
    }

    /**
     * Gets a entity by property id
     */
    @SuppressWarnings("unchecked")
	public List<WatchrxDoctor> findByProperty(String propertyName, Object value) {
    	
    	EntityManager em = entityManagerFactory.createEntityManager();
    	
    	Query query = em.createQuery("SELECT e FROM WatchrxDoctor e WHERE status='"+Constants.Status.ACTIVE+"' AND e."+propertyName +"= :propertyValue");
    	
    	query.setParameter("propertyValue", value);
    	List<WatchrxDoctor> list = query.getResultList();
      	return list;
    }
}