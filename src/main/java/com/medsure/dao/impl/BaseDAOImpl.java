package com.medsure.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.medsure.dao.BaseDAO;

/**
 * @author snare
 *
 */
public class BaseDAOImpl<E> implements BaseDAO<E> {

	private static final Logger logger = LoggerFactory.getLogger(BaseDAOImpl.class);
	
    private Class<E> entityClass;

    @Autowired
    EntityManagerFactory entityManagerFactory;
    
    @SuppressWarnings("unchecked")
	public BaseDAOImpl() {
         this.entityClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    /**
     * saves a entity
     */
    @SuppressWarnings("unchecked")
	@Override
    public E save(E e) {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	em.getTransaction().begin();
    	e = em.merge(e);
    	em.getTransaction().commit();
    	em.close();
        return e;
    }
    
   @Override
   public void save(List<E> es) {
	   EntityManager em = entityManagerFactory.createEntityManager();
   	   em.getTransaction().begin();
		for (int i = 0; i < es.size(); i++) {
			E e = es.get(i);
			em.merge(e);
       }
	   em.getTransaction().commit();
       em.close();
   }

    /**
     * Gets all entities
     */
    @SuppressWarnings("unchecked")
	@Override
    public List<E> getAll() {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	List<E> entities = (List<E>)em.createQuery("FROM "+entityClass.getSimpleName() )
       	        .getResultList();
        return entities;
    }

    /**
     * Gets a entity by primary id
     */
    @SuppressWarnings("unchecked")
	@Override
    public E getById(Serializable id) {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	E e = (E) em.find(entityClass, id);
         return e;
    }

    /**
     * Gets a entity by property id
     */
    @SuppressWarnings("unchecked")
	public List<E> findByProperty(String propertyName, Object value) {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	Query query = em.createQuery("SELECT e FROM " + entityClass.getName() + " e WHERE e."+propertyName +"= :propertyValue");
    	//SELECT e FROM com.medsure.model.WatchrxPatient e WHERE status='Y' AND e.watchrxPatient.ssn= :propertyValue
    	query.setParameter("propertyValue", value);
    	List<E> list = query.getResultList();
      	return list;
    }
    
    /**
     * Gets a entity by property id
     */
    @SuppressWarnings("unchecked")
	public void deleteByProperty(String propertyName, Object value) {
    	logger.info("In deleteByProperty propertyName: " + propertyName + " value " + value);
    	EntityManager em = entityManagerFactory.createEntityManager();
    	em.getTransaction().begin();
    	Query query = em.createQuery("DELETE FROM " + entityClass.getName() + " e WHERE e."+propertyName +"= :propertyValue");
    	query.setParameter("propertyValue", value);
    	query.executeUpdate();
    	em.getTransaction().commit();
    	em.close();
    }

    /**
     * Deletes a entity
     */
    @Override
    public void delete(E e) {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	em.getTransaction().begin();
    	em.remove(em.merge(e));
    	em.getTransaction().commit();
    	em.close();
    }

    public void delete(Serializable id) {
        delete(getById(id));
    }
    
    @Override
    public void deleteAll(List<E> es) {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	em.getTransaction().begin();
    	for (int i = 0; i < es.size(); i++) {
    		E e = es.get(i);
    		em.remove(em.merge(e));
    	}    		
    	em.getTransaction().commit();
    	em.close();
    }
}
