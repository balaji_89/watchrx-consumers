package com.medsure.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.medsure.common.Constants;
import com.medsure.dao.ClinicianDAO;
import com.medsure.model.WatchrxClinician;

/**
 * @author snare
 *
 */
@Component
public class ClinicianDAOImpl extends BaseDAOImpl<WatchrxClinician> implements ClinicianDAO {

	@SuppressWarnings("unchecked")
	@Override
    public List<WatchrxClinician> getAll() {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	List<WatchrxClinician> entities = (List<WatchrxClinician>)em.createQuery("FROM WatchrxClinician WHERE status='"+Constants.Status.ACTIVE+"'")
       	        .getResultList();
        return entities;
    }

    /**
     * Gets a entity by property id
     */
    @SuppressWarnings("unchecked")
	public List<WatchrxClinician> findByProperty(String propertyName, Object value) {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	Query query = em.createQuery("SELECT e FROM WatchrxClinician e WHERE status='"+Constants.Status.ACTIVE+"' AND e."+propertyName +"= :propertyValue");
    	query.setParameter("propertyValue", value);
    	List<WatchrxClinician> list = query.getResultList();
      	return list;
    }
 
}