/**
 * 
 */
package com.medsure.dao.impl;

import org.springframework.stereotype.Component;

import com.medsure.dao.ClinicianScheduleDAO;
import com.medsure.model.WatchrxClinicianSchedule;


/**
 * @author snare
 *
 */
@Component
public class ClinicianScheduleDAOImpl extends BaseDAOImpl<WatchrxClinicianSchedule> implements ClinicianScheduleDAO {

}
