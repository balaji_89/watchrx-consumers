/**
 * 
 */
package com.medsure.dao.impl;

import org.springframework.stereotype.Component;

import com.medsure.dao.PatientWatchAssignmentDAO;
import com.medsure.model.WatchrxPatientWatchAssignmnt;


/**
 * @author snare
 *
 */
@Component
public class PatientWatchAssignmentDAOImpl extends BaseDAOImpl<WatchrxPatientWatchAssignmnt> implements PatientWatchAssignmentDAO {

}

