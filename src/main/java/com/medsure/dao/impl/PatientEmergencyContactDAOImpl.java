/**
 * 
 */
package com.medsure.dao.impl;

import org.springframework.stereotype.Component;

import com.medsure.dao.PatientEmergencyContactDAO;
import com.medsure.model.WatchrxPatientEmergencyContact;


/**
 * @author snare
 *
 */

@Component
public class PatientEmergencyContactDAOImpl extends BaseDAOImpl<WatchrxPatientEmergencyContact> implements PatientEmergencyContactDAO {

}
