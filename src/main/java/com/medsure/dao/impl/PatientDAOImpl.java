package com.medsure.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.medsure.common.Constants;
import com.medsure.dao.PatientDAO;
import com.medsure.model.WatchrxPatient;

/**
 * @author snare
 *
 */
@Component
public class PatientDAOImpl extends BaseDAOImpl<WatchrxPatient> implements PatientDAO {

	@SuppressWarnings("unchecked")
	@Override
    public List<WatchrxPatient> getAll() {
		System.out.println("In PatientDAOImpl:getAll");
    	EntityManager em = entityManagerFactory.createEntityManager();
    	List<WatchrxPatient> entities = (List<WatchrxPatient>)em.createQuery("FROM WatchrxPatient WHERE status='"+Constants.Status.ACTIVE+"'")
       	        .getResultList();
        return entities;
    }

    /**
     * Gets a entity by property id
     */
    @SuppressWarnings("unchecked")
	public List<WatchrxPatient> findByProperty(String propertyName, Object value) {
    	System.out.println("In PatientDAOImpl:findByProperty");
    	EntityManager em = entityManagerFactory.createEntityManager();    	
    	Query query = em.createQuery("SELECT e FROM WatchrxPatient e WHERE status='"+Constants.Status.ACTIVE+"' AND e."+propertyName +"= :propertyValue");
    	query.setParameter("propertyValue", value);
    	List<WatchrxPatient> list = query.getResultList();
       	return list;
    }
 }