package com.medsure.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.medsure.common.Constants;
import com.medsure.dao.WatchDAO;
import com.medsure.model.WatchrxWatch;

/**
 * @author snare
 *
 */
@Component
public class WatchDAOImpl extends BaseDAOImpl<WatchrxWatch> implements WatchDAO {

	@SuppressWarnings("unchecked")
	@Override
    public List<WatchrxWatch> getAll() {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	List<WatchrxWatch> entities = (List<WatchrxWatch>)em.createQuery("FROM WatchrxWatch WHERE status='"+Constants.Status.ACTIVE+"'")
       	        .getResultList();
        return entities;
    }

    /**
     * Gets a entity by property id
     */
    @SuppressWarnings("unchecked")
	public List<WatchrxWatch> findByProperty(String propertyName, Object value) {
    	EntityManager em = entityManagerFactory.createEntityManager();
    	Query query = em.createQuery("SELECT e FROM WatchrxWatch e WHERE status='"+Constants.Status.ACTIVE+"' AND e."+propertyName +"= "+value);
    	List<WatchrxWatch> list = query.getResultList();
     	return list;
    }
}