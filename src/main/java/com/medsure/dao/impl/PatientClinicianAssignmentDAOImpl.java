package com.medsure.dao.impl;

import org.springframework.stereotype.Component;

import com.medsure.dao.PatientClinicianAssignmentDAO;
import com.medsure.model.WatchrxPatientClinicianAssignmnt;

/**
 * @author snare
 *
 */
@Component
public class PatientClinicianAssignmentDAOImpl extends BaseDAOImpl<WatchrxPatientClinicianAssignmnt> implements PatientClinicianAssignmentDAO {

	
}