/**
 * 
 */
package com.medsure.dao.impl;

import org.springframework.stereotype.Component;

import com.medsure.dao.PatientDoctorAssignmentDAO;
import com.medsure.model.WatchrxPatientDoctorAssignmnt;


/**
 * @author snare
 *
 */
@Component
public class PatientDoctorAssignmentDAOImpl extends BaseDAOImpl<WatchrxPatientDoctorAssignmnt> implements PatientDoctorAssignmentDAO {

}
