package com.medsure.dao.impl;

import org.springframework.stereotype.Component;

import com.medsure.dao.WatchCGAppInstallStatusDAO;
import com.medsure.model.WatchrxWatchCGAppInstallStatus;

/**
 * @author snare
 *
 */
@Component
public class WatchCGAppInstallStatusDAOImpl extends BaseDAOImpl<WatchrxWatchCGAppInstallStatus> implements WatchCGAppInstallStatusDAO {

}