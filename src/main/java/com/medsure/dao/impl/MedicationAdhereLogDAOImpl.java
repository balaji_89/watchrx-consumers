/**
 * 
 */
package com.medsure.dao.impl;

import org.springframework.stereotype.Component;

import com.medsure.dao.MedicationAdhereLogDAO;
import com.medsure.model.WatchrxMedicationAdhereLog;

/**
 * @author snare
 *
 */
@Component
public class MedicationAdhereLogDAOImpl extends BaseDAOImpl<WatchrxMedicationAdhereLog> implements MedicationAdhereLogDAO {

}
