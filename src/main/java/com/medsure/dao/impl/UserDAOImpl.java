package com.medsure.dao.impl;

import org.springframework.stereotype.Component;

import com.medsure.dao.UserDAO;
import com.medsure.model.WatchrxUser;

/**
 * @author snare
 *
 */
@Component
public class UserDAOImpl extends BaseDAOImpl<WatchrxUser> implements UserDAO {

}