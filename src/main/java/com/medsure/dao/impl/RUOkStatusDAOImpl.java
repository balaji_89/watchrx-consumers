/**
 * 
 */
package com.medsure.dao.impl;

import org.springframework.stereotype.Component;

import com.medsure.dao.RUOkStatusDAO;
import com.medsure.model.WatchrxRuOkStatus;


/**
 * @author balaji R
 *
 */

@Component
public class RUOkStatusDAOImpl extends BaseDAOImpl<WatchrxRuOkStatus> implements RUOkStatusDAO {

	
}