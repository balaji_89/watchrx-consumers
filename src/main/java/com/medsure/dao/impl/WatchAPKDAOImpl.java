package com.medsure.dao.impl;

import org.springframework.stereotype.Component;

import com.medsure.dao.WatchAPKDAO;
import com.medsure.model.WatchrxWatchAPK;

/**
 * @author snare
 *
 */
@Component
public class WatchAPKDAOImpl extends BaseDAOImpl<WatchrxWatchAPK> implements WatchAPKDAO {

	
}