/**
 * 
 */
package com.medsure.dao.impl;

import org.springframework.stereotype.Component;

import com.medsure.dao.AddressDAO;
import com.medsure.model.WatchrxAddress;


/**
 * @author snare
 *
 */
@Component
public class AddressDAOImpl extends BaseDAOImpl<WatchrxAddress> implements AddressDAO {

}
