/**
 * 
 */
package com.medsure.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.medsure.dao.AlertStatusDAO;
import com.medsure.model.WatchrxAlertStatus;


/**
 * @author snare
 *
 */

@Component
public class AlertStatusDAOImpl extends BaseDAOImpl<WatchrxAlertStatus> implements AlertStatusDAO {

	@Override
	public void deleteAlertStatusByPatientId (Long patientId){
	
    	EntityManager em = entityManagerFactory.createEntityManager();
    	em.getTransaction().begin();
    	Query query = em.createQuery("DELETE FROM WatchrxAlertStatus e WHERE e.watchrxPatientAlert.patientAlertId IN (SELECT patientAlertId FROM WatchrxPatientAlert WHERE watchrxPatient.patientId="+patientId+")");
     	query.executeUpdate();
    	em.getTransaction().commit();
  	}
}