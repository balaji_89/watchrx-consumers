/**
 * 
 */
package com.medsure.dao.impl;

import org.springframework.stereotype.Component;

import com.medsure.dao.ClinicianWatchAssignmentDAO;
import com.medsure.model.WatchrxClinicianWatchAssignmnt;


/**
 * @author balaji
 *
 */
@Component
public class ClinicianWatchAssignmentDAOImpl extends BaseDAOImpl<WatchrxClinicianWatchAssignmnt> implements ClinicianWatchAssignmentDAO {

}

