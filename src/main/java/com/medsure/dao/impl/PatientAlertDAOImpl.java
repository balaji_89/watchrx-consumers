/**
 * 
 */
package com.medsure.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TemporalType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.medsure.dao.PatientAlertDAO;
import com.medsure.model.WatchrxPatientAlert;
import com.medsure.service.impl.AlertServiceImpl;
import com.medsure.ui.entity.caregiver.request.GetAlerts;


/**
 * @author snare
 *
 */

@Component
public class PatientAlertDAOImpl extends BaseDAOImpl<WatchrxPatientAlert> implements PatientAlertDAO {

	private static final Logger logger = LoggerFactory.getLogger(PatientAlertDAOImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<WatchrxPatientAlert> getAlertsByDateByPatientId (GetAlerts patientId){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
		Date startDate = null;
		Date endDate = null;
		long patientId1 = Long.parseLong(patientId.getPatientId());
		List<WatchrxPatientAlert> allEvents = null;
    	EntityManager em = entityManagerFactory.createEntityManager();

		if(patientId.getEndDate()==null){
			try {
				startDate = format.parse(patientId.getStartDate());
				logger.info(startDate+"startDate");
				allEvents = em.createQuery(
	    			    "SELECT e FROM WatchrxPatientAlert e WHERE  e.watchrxPatient.patientId="+patientId1+" AND e.createdDate BETWEEN :startDate AND :startDate")  
	    			  .setParameter("startDate", startDate, TemporalType.TIMESTAMP)  
	    			  .setParameter("startDate", startDate, TemporalType.TIMESTAMP)  
	    			  .getResultList();
				logger.info(allEvents.size()+"allEvents");

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}else if(patientId.getStartDate()!=null && patientId.getEndDate()!=null){
			try {
				startDate = format.parse(patientId.getStartDate());
				 endDate = format.parse(patientId.getEndDate()); 
					logger.info(startDate+"startDate"+endDate+"endDate");

				 allEvents = em.createQuery(
		    			    "SELECT e FROM WatchrxPatientAlert e WHERE  e.watchrxPatient.patientId="+patientId1+" AND e.createdDate BETWEEN :startDate AND :endDate")  
		    			  .setParameter("startDate", startDate, TemporalType.TIMESTAMP)  
		    			  .setParameter("endDate", endDate, TemporalType.TIMESTAMP)  
		    			  .getResultList();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}

		  return allEvents ;
		  
  	}
}
