package com.medsure.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Component;

import com.medsure.dao.ReferenceDAO;
import com.medsure.model.WatchrxReference;

/**
 * @author snare
 *
 */
@Component
public class ReferenceDAOImpl extends BaseDAOImpl<WatchrxReference> implements ReferenceDAO {

}