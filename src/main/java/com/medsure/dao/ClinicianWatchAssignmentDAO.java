/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxClinicianWatchAssignmnt;


/**
 * @author snare
 *
 */

public interface ClinicianWatchAssignmentDAO extends BaseDAO<WatchrxClinicianWatchAssignmnt>{

}
