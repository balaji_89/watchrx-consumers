/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxUser;

/**
 * @author snare
 *
 */
public interface UserDAO extends BaseDAO<WatchrxUser>{

}
