/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxWatchCGAppInstallStatus;


/**
 * @author snare
 *
 */
public interface WatchCGAppInstallStatusDAO extends BaseDAO<WatchrxWatchCGAppInstallStatus>{
	
}
