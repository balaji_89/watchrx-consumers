/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxClinician;


/**
 * @author snare
 *
 */
public interface ClinicianDAO extends BaseDAO<WatchrxClinician>{

}
