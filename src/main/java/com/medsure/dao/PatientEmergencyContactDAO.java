/**
 * 
 */
package com.medsure.dao;

import com.medsure.model.WatchrxPatientEmergencyContact;


/**
 * @author snare
 *
 */

public interface PatientEmergencyContactDAO extends BaseDAO<WatchrxPatientEmergencyContact>{

}
