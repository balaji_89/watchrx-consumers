package com.medsure.ui.entity.server;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author snare
 *
 */
public class PatientWatchAssignmntVO implements java.io.Serializable {

	private Long patientWatchAssignmntId;

	private PatientVO patientVO;
	
	private WatchVO watchVO;

	public PatientWatchAssignmntVO() {
	}

	public PatientWatchAssignmntVO(WatchVO watchrxWatch,
			PatientVO watchrxPatient) {
		this.watchVO = watchrxWatch;
		this.patientVO = watchrxPatient;
	}

	public Long getPatientWatchAssignmntId() {
		return patientWatchAssignmntId;
	}

	public void setPatientWatchAssignmntId(Long patientWatchAssignmntId) {
		this.patientWatchAssignmntId = patientWatchAssignmntId;
	}

	public PatientVO getPatientVO() {
		return patientVO;
	}

	public void setPatientVO(PatientVO patientVO) {
		this.patientVO = patientVO;
	}

	public WatchVO getWatchVO() {
		return watchVO;
	}

	public void setWatchVO(WatchVO watchVO) {
		this.watchVO = watchVO;
	}

	
}
