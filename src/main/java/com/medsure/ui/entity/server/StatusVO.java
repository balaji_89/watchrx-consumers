package com.medsure.ui.entity.server;

import java.io.Serializable;
import java.util.Arrays;

import com.google.gson.annotations.Expose;

public class StatusVO  implements Serializable {

	private static final long serialVersionUID = 1L;

	@Expose
	private boolean success = true;
	
	@Expose
	private String[] messages;
	
	public boolean isSuccess() {
		return success;
	}
	
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public String[] getMessages() {
		return messages;
	}
	
	public void setMessages(String[] messages) {
		this.messages = messages;
	}

	@Override
	public String toString() {
		return "StatusVO [success=" + success + ", messages="
				+ Arrays.toString(messages) + "]";
	}
}
