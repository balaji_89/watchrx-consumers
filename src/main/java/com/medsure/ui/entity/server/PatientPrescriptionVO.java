package com.medsure.ui.entity.server;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.gmr.web.multipart.GMultipartFile;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

public class PatientPrescriptionVO  extends UserVO implements Serializable {
	
	private PatientVO patient;
	
	private Long prescriptionId;
	@NotEmpty
	private String medicine;
	
	@NotNull
	@Min(1)
	private Integer medicineForm;
	private String medicineFormName;
	
	private String afterMeal;
	
	private String morningTime;
	private String noonTime;
	private String eveningTime;
	private String nightTime;
	
	private boolean earlyMorningTime;
	private boolean breakFastTime;
	private boolean lunchTime;
	private boolean noonSnackTime;
	private boolean dinnerTime;
	private boolean bedTime;
	private boolean sunday;
	private boolean monday;
	private boolean tuesday;
	private boolean wednesday;
	private boolean thursday;
	private boolean friday;
	private boolean saturday;
	private boolean fileModified;

	
	private boolean refill;	
	
	private GMultipartFile imageFile;
	private Integer frequency;
	
	@NotNull
	private Integer quantity;
	
	private Date endDate = new Date ();
	private int status;
	
	@NotEmpty
	private String dosage;
	
	private Integer regimen;
	
	//@NotEmpty
	private String picPath;
	
	private Date createdDate;
	private Date updatedDate;
	
	@NotEmpty
	private String color;
	
	//@NotEmpty
	private String dayOfWeek;
	
	//@NotEmpty
	private String description;
	
	private String timeSlots;
	
	private String hour;
	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public String getMinute() {
		return minute;
	}

	public void setMinute(String minute) {
		this.minute = minute;
	}

	public String getFixTimeslots() {
		return fixTimeslots;
	}

	public void setFixTimeslots(String fixTimeslots) {
		this.fixTimeslots = fixTimeslots;
	}

	private String minute;
	private String fixTimeslots;
	
	@Override
	public String toString() {
		return "PatientPrescriptionVO [patient=" + patient + ", prescriptionId="
				+ prescriptionId+"]";
	}

	public PatientVO getPatient() {
		return patient;
	}

	public void setPatient(PatientVO patient) {
		this.patient = patient;
	}

	public Long getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(Long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	public String getMedicine() {
		return medicine;
	}

	public void setMedicine(String medicine) {
		this.medicine = medicine;
	}

	
	public Integer getMedicineForm() {
		return medicineForm;
	}

	public void setMedicineForm(Integer medicineForm) {
		this.medicineForm = medicineForm;
	}

	public String getAfterMeal() {
		return afterMeal;
	}

	public void setAfterMeal(String afterMeal) {
		this.afterMeal = afterMeal;
	}

	public String getMorningTime() {
		return morningTime;
	}

	public void setMorningTime(String morningTime) {
		this.morningTime = morningTime;
	}

	public String getNoonTime() {
		return noonTime;
	}

	public void setNoonTime(String noonTime) {
		this.noonTime = noonTime;
	}

	public String getEveningTime() {
		return eveningTime;
	}

	public void setEveningTime(String eveningTime) {
		this.eveningTime = eveningTime;
	}

	public String getNightTime() {
		return nightTime;
	}

	public void setNightTime(String nightTime) {
		this.nightTime = nightTime;
	}

	public boolean isRefill() {
		return refill;
	}

	public void setRefill(boolean refill) {
		this.refill = refill;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public Integer getRegimen() {
		return regimen;
	}

	public void setRegimen(Integer regimen) {
		this.regimen = regimen;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getDayOfWeek() {
		
		String days = "";
		if(sunday){
			days = "Su";
		}
		if(monday){
			if(days.length()>0){
				days = days+"|";
			}
			days = days+"Mo";
		}
		if(tuesday){
			if(days.length()>0){
				days = days+"|";
			}
			days =  days+"Tu";
		}
		if(wednesday){
			if(days.length()>0){
				days = days+"|";
			}
			days =  days+"We";
		}
		if(thursday){
			if(days.length()>0){
				days = days+"|";
			}
			days = days+ "Th";
		}
		if(friday){
			if(days.length()>0){
				days = days+"|";
			}
			days = days+ "Fr";
		}
		if(saturday){
			if(days.length()>0){
				days = days+"|";
			}
			days = days+"Sa";
		}
		dayOfWeek = days;
		return dayOfWeek;
	}

	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isEarlyMorningTime() {
		return earlyMorningTime;
	}

	public void setEarlyMorningTime(boolean earlyMorningTime) {
		this.earlyMorningTime = earlyMorningTime;
	}

	public boolean isBreakFastTime() {
		return breakFastTime;
	}

	public void setBreakFastTime(boolean breakFastTime) {
		this.breakFastTime = breakFastTime;
	}

	public boolean isLunchTime() {
		return lunchTime;
	}

	public void setLunchTime(boolean lunchTime) {
		this.lunchTime = lunchTime;
	}

	public boolean isNoonSnackTime() {
		return noonSnackTime;
	}

	public void setNoonSnackTime(boolean noonSnackTime) {
		this.noonSnackTime = noonSnackTime;
	}

	public boolean isDinnerTime() {
		return dinnerTime;
	}

	public void setDinnerTime(boolean dinnerTime) {
		this.dinnerTime = dinnerTime;
	}

	public boolean isBedTime() {
		return bedTime;
	}

	public void setBedTime(boolean bedTime) {
		this.bedTime = bedTime;
	}

	public boolean isSunday() {
		return sunday;
	}

	public void setSunday(boolean sunday) {
		this.sunday = sunday;
	}

	public boolean isMonday() {
		return monday;
	}

	public void setMonday(boolean monday) {
		this.monday = monday;
	}

	public boolean isTuesday() {
		return tuesday;
	}

	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}

	public boolean isWednesday() {
		return wednesday;
	}

	public void setWednesday(boolean wednesday) {
		this.wednesday = wednesday;
	}

	public boolean isThursday() {
		return thursday;
	}

	public void setThursday(boolean thursday) {
		this.thursday = thursday;
	}

	public boolean isFriday() {
		return friday;
	}

	public void setFriday(boolean friday) {
		this.friday = friday;
	}

	public boolean isSaturday() {
		return saturday;
	}

	public void setSaturday(boolean saturday) {
		this.saturday = saturday;
	}

	public String getMedicineFormName() {
		return medicineFormName;
	}

	public void setMedicineFormName(String medicineFormName) {
		this.medicineFormName = medicineFormName;
	}

	public String getTimeSlots() {
		String slots = "";
		if(earlyMorningTime){
			slots = slots+"EarlyMorning";
		}
		if(breakFastTime){
			if(slots.length()>0){			
				slots = slots+"|";
			}
			slots = slots+ "Breakfast";
		}
		if(lunchTime){
			if(slots.length()>0){			
				slots = slots+"|";
			}
			slots = slots+ "Lunch";
		}
		if(noonSnackTime){
			if(slots.length()>0){			
				slots = slots+"|";
			}
			slots = slots+ "AfternoonSnack";
		}
		if(dinnerTime){
			if(slots.length()>0){			
				slots = slots+"|";
			}
			slots = slots+ "Dinner";
		}
		if(bedTime){
			if(slots.length()>0){			
				slots = slots+"|";
			}
			slots = slots+ "Bed";
		}
		timeSlots = slots;
		return timeSlots;
	}

	public void setTimeSlots(String timeSlots) {
		this.timeSlots = timeSlots;
	}

	public GMultipartFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(GMultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	public boolean isFileModified() {
		return fileModified;
	}

	public void setFileModified(boolean fileModified) {
		this.fileModified = fileModified;
	}

}
