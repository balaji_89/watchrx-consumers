package com.medsure.ui.entity.server;

import java.io.Serializable;
import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.gmr.web.multipart.GMultipartFile;
import org.hibernate.validator.constraints.NotEmpty;

public class DoctorVO implements Serializable {

	@NotEmpty
	private String phoneNumber;

	private String altPhoneNumber;

	@NotEmpty
	private String hospital;

	@NotNull
	@Valid
	private AddressVO address;

	@NotNull
	@Min(1)
	private Integer speciality;

	private String specialityName;

	private boolean available;

	private Long doctorId;

	@NotEmpty
	private String userName;

	@NotEmpty
	private String password;

	Integer roleType;

	@NotEmpty
	private String firstName;

	@NotEmpty
	private String lastName;

	// @NotEmpty
	private String picPath;

	private Date createdDate;

	private Date updatedDate;

	private String status;

	private GMultipartFile imageFile;

	private boolean fileModified;

	@Override
	public String toString() {
		return "DoctorVO [phoneNumber=" + phoneNumber + ", altPhoneNumber="
				+ altPhoneNumber + "]";
	}

	public Integer getSpeciality() {
		return speciality;
	}

	public void setSpeciality(Integer speciality) {
		this.speciality = speciality;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public AddressVO getAddress() {
		return address;
	}

	public void setAddress(AddressVO address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAltPhoneNumber() {
		return altPhoneNumber;
	}

	public void setAltPhoneNumber(String altPhoneNumber) {
		this.altPhoneNumber = altPhoneNumber;
	}

	public String getHospital() {
		return hospital;
	}

	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getRoleType() {
		return roleType;
	}

	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSpecialityName() {
		return specialityName;
	}

	public void setSpecialityName(String specialityName) {
		this.specialityName = specialityName;
	}

	public GMultipartFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(GMultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	public boolean isFileModified() {
		return fileModified;
	}

	public void setFileModified(boolean fileModified) {
		this.fileModified = fileModified;
	}
}
