package com.medsure.ui.entity.server;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import com.google.gson.annotations.Expose;

public class MedicalPrescriptionVO  extends UserVO implements Serializable {
	private IDDescriptionVO medication;
	private IDDescriptionVO form;
	private String dosage;
	private String regimen;
	private boolean afterMeal;
	private Date endDate;
	private Date morningTime;
	private Date noonTime;
	private Date eveningTime;
	private Date nightTime;
	private boolean refill;
	private int frequency;
	private String hour;
	private String minute;
	private String timeslots;
		

	@Override
	public String toString() {
		return "MedicalPrescriptionVO [medication=" + medication + ", form="
				+ form+"]";
	}


	public IDDescriptionVO getMedication() {
		return medication;
	}


	public void setMedication(IDDescriptionVO medication) {
		this.medication = medication;
	}


	public IDDescriptionVO getForm() {
		return form;
	}


	public void setForm(IDDescriptionVO form) {
		this.form = form;
	}


	public String getDosage() {
		return dosage;
	}


	public void setDosage(String dosage) {
		this.dosage = dosage;
	}


	public String getRegimen() {
		return regimen;
	}


	public void setRegimen(String regimen) {
		this.regimen = regimen;
	}


	public boolean isAfterMeal() {
		return afterMeal;
	}


	public void setAfterMeal(boolean afterMeal) {
		this.afterMeal = afterMeal;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Date getMorningTime() {
		return morningTime;
	}


	public void setMorningTime(Date morningTime) {
		this.morningTime = morningTime;
	}


	public Date getNoonTime() {
		return noonTime;
	}


	public void setNoonTime(Date noonTime) {
		this.noonTime = noonTime;
	}


	public Date getEveningTime() {
		return eveningTime;
	}


	public void setEveningTime(Date eveningTime) {
		this.eveningTime = eveningTime;
	}


	public Date getNightTime() {
		return nightTime;
	}


	public void setNightTime(Date nightTime) {
		this.nightTime = nightTime;
	}


	public boolean isRefill() {
		return refill;
	}


	public void setRefill(boolean refill) {
		this.refill = refill;
	}


	public int getFrequency() {
		return frequency;
	}


	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}


	public String getHour() {
		return hour;
	}


	public void setHour(String hour) {
		this.hour = hour;
	}


	public String getMinute() {
		return minute;
	}


	public void setMinute(String minute) {
		this.minute = minute;
	}


	public String getTimeslots() {
		return timeslots;
	}


	public void setTimeslots(String timeslots) {
		this.timeslots = timeslots;
	}


}
