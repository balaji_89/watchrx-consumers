package com.medsure.ui.entity.server;

import java.io.Serializable;
import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.gmr.web.multipart.GMultipartFile;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

public class PatientVO implements Serializable {

	private Long patientId;
	private DoctorVO priDoctor;
	private ClinicianVO clinician;
	private String morningClinician;
	private String noonClinician;
	private String nightClinician;
	private String nameSos;
	private String phoneNumbersos;
	
	private String gcmId;
	@NotEmpty
	private String phoneNumber;

	private String altPhoneNumber;

	@Valid
	@NotNull
	private AddressVO address;

	@NotNull
	@Min(1)
	private Integer maritalStatus;

	@NotNull
	@Min(1)
	private Integer language;

	@NotNull
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date dob;

	private String insuranceCompany;

	private String insurancePhNumber;

	//@NotEmpty
	private String visitReason;

	private boolean revisit;

	private WatchVO watch;
	private DoctorVO specialist;

	//@NotEmpty
	private String employment;

	//@NotEmpty
	private String ssn;

	private String userName;

	private String password;

	private int roleType;

	@NotEmpty
	private String firstName;

	@NotEmpty
	private String lastName;

	// @NotEmpty
	private String picPath;

	private Date createdDate;

	private Date updatedDate;

	private String status;

	private GMultipartFile imageFile;

	private boolean fileModified;

	private String earlyMorningHour;
	private String breakFastHour;
	private String lunchHour;
	private String noonSnackHour;
	private String dinnerHour;
	private String bedHour;
	private String earlyMorningMin;
	private String breakFastMin;
	private String lunchMin;
	private String noonSnackMin;
	private String dinnerMin;
	private String bedMin;

	private String timeSlots;
private String mobilenoSoS;
	@Override
	public String toString() {
		return "PatientVO [phoneNumber=" + phoneNumber + ", altPhoneNumber="
				+ altPhoneNumber + ", patientId=" + patientId + "]";
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public DoctorVO getPriDoctor() {
		return priDoctor;
	}

	public void setPriDoctor(DoctorVO pricDoctor) {
		this.priDoctor = pricDoctor;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAltPhoneNumber() {
		return altPhoneNumber;
	}

	public void setAltPhoneNumber(String altPhoneNumber) {
		this.altPhoneNumber = altPhoneNumber;
	}

	public AddressVO getAddress() {
		return address;
	}

	public void setAddress(AddressVO address) {
		this.address = address;
	}

	public Integer getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(Integer maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Integer getLanguage() {
		return language;
	}

	public void setLanguage(Integer language) {
		this.language = language;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getInsuranceCompany() {
		return insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}

	public String getInsurancePhNumber() {
		return insurancePhNumber;
	}

	public void setInsurancePhNumber(String insurancePhNumber) {
		this.insurancePhNumber = insurancePhNumber;
	}

	public String getVisitReason() {
		return visitReason;
	}

	public void setVisitReason(String visitReason) {
		this.visitReason = visitReason;
	}

	public boolean isRevisit() {
		return revisit;
	}

	public void setRevisit(boolean revisit) {
		this.revisit = revisit;
	}

	public ClinicianVO getClinician() {
		return clinician;
	}

	public void setClinician(ClinicianVO clinician) {
		this.clinician = clinician;
	}

	public WatchVO getWatch() {
		return watch;
	}

	public void setWatch(WatchVO watch) {
		this.watch = watch;
	}

	public DoctorVO getSpecialist() {
		return specialist;
	}

	public void setSpecialist(DoctorVO specialist) {
		this.specialist = specialist;
	}

	public String getEmployment() {
		return employment;
	}

	public void setEmployment(String employment) {
		this.employment = employment;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRoleType() {
		return roleType;
	}

	public void setRoleType(int roleType) {
		this.roleType = roleType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMorningClinician() {
		return morningClinician;
	}

	public void setMorningClinician(String morningClinician) {
		this.morningClinician = morningClinician;
	}

	public String getNoonClinician() {
		return noonClinician;
	}

	public void setNoonClinician(String noonClinician) {
		this.noonClinician = noonClinician;
	}

	public String getNightClinician() {
		return nightClinician;
	}

	public void setNightClinician(String nightClinician) {
		this.nightClinician = nightClinician;
	}

	public String getEarlyMorningHour() {
		return earlyMorningHour;
	}

	public void setEarlyMorningHour(String earlyMorningHour) {
		this.earlyMorningHour = earlyMorningHour;
	}

	public String getBreakFastHour() {
		return breakFastHour;
	}

	public void setBreakFastHour(String breakFastHour) {
		this.breakFastHour = breakFastHour;
	}

	public String getLunchHour() {
		return lunchHour;
	}

	public void setLunchHour(String lunchHour) {
		this.lunchHour = lunchHour;
	}

	public String getNoonSnackHour() {
		return noonSnackHour;
	}

	public void setNoonSnackHour(String noonSnackHour) {
		this.noonSnackHour = noonSnackHour;
	}

	public String getDinnerHour() {
		return dinnerHour;
	}

	public void setDinnerHour(String dinnerHour) {
		this.dinnerHour = dinnerHour;
	}

	public String getBedHour() {
		return bedHour;
	}

	public void setBedHour(String bedHour) {
		this.bedHour = bedHour;
	}

	public String getEarlyMorningMin() {
		return earlyMorningMin;
	}

	public void setEarlyMorningMin(String earlyMorningMin) {
		this.earlyMorningMin = earlyMorningMin;
	}

	public String getBreakFastMin() {
		return breakFastMin;
	}

	public void setBreakFastMin(String breakFastMin) {
		this.breakFastMin = breakFastMin;
	}

	public String getLunchMin() {
		return lunchMin;
	}

	public void setLunchMin(String lunchMin) {
		this.lunchMin = lunchMin;
	}

	public String getNoonSnackMin() {
		return noonSnackMin;
	}

	public void setNoonSnackMin(String noonSnackMin) {
		this.noonSnackMin = noonSnackMin;
	}

	public String getDinnerMin() {
		return dinnerMin;
	}

	public void setDinnerMin(String dinnerMin) {
		this.dinnerMin = dinnerMin;
	}

	public String getBedMin() {
		return bedMin;
	}

	public void setBedMin(String bedMin) {
		this.bedMin = bedMin;
	}

	public String getTimeSlots() {
		String slots = "";
		slots = slots + "EarlyMorning-" + earlyMorningHour + ":"
				+ earlyMorningMin;
		slots = slots + "|Breakfast-" + breakFastHour + ":" + breakFastMin;
		slots = slots + "|Lunch-" + lunchHour + ":" + lunchMin;
		slots = slots + "|AfternoonSnack-" + noonSnackHour + ":" + noonSnackMin;
		slots = slots + "|Dinner-" + dinnerHour + ":" + dinnerMin;
		slots = slots + "|Bed-" + bedHour + ":" + bedMin;
		timeSlots = slots;
		return timeSlots;
	}

	public void setTimeSlots(String timeSlots) {
		this.timeSlots = timeSlots;
	}

	public GMultipartFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(GMultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	public boolean isFileModified() {
		return fileModified;
	}

	public void setFileModified(boolean fileModified) {
		this.fileModified = fileModified;
	}

	public String getMobilenoSoS() {
		return mobilenoSoS;
	}

	public void setMobilenoSoS(String mobilenoSoS) {
		this.mobilenoSoS = mobilenoSoS;
	}

	public String getNameSos() {
		return nameSos;
	}

	public void setNameSos(String nameSos) {
		this.nameSos = nameSos;
	}

	public String getPhoneNumbersos() {
		return phoneNumbersos;
	}

	public void setPhoneNumbersos(String phoneNumbersos) {
		this.phoneNumbersos = phoneNumbersos;
	}

	public String getGcmId() {
		return gcmId;
	}

	public void setGcmId(String gcmId) {
		this.gcmId = gcmId;
	}
}
