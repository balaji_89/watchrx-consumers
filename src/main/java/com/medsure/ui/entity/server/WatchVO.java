package com.medsure.ui.entity.server;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.google.gson.annotations.Expose;

public class WatchVO  implements Serializable {
		
		
		private Long watchId;
		
		@NotEmpty
		private String imeiNumber;
		
		
		private IDDescriptionVO status;
		
		private PatientVO patient;	
		
		private PatientWatchAssignmntVO patientWatchAssignmntVO;
		
		public Long getWatchId() {
			return watchId;
		}
		public void setWatchId(Long watchId) {
			this.watchId = watchId;
		}
		public String getImeiNumber() {
			return imeiNumber;
		}
		public void setImeiNumber(String imeiNumber) {
			this.imeiNumber = imeiNumber;
		}
		public IDDescriptionVO getStatus() {
			return status;
		}
		public void setStatus(IDDescriptionVO status) {
			this.status = status;
		}
		public PatientVO getPatient() {
			return patient;
		}
		public void setPatient(PatientVO patient) {
			this.patient = patient;
		}
		public PatientWatchAssignmntVO getPatientWatchAssignmntVO() {
			return patientWatchAssignmntVO;
		}
		public void setPatientWatchAssignmntVO(
				PatientWatchAssignmntVO patientWatchAssignmntVO) {
			this.patientWatchAssignmntVO = patientWatchAssignmntVO;
		}
}
