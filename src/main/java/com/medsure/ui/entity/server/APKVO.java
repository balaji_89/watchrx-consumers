package com.medsure.ui.entity.server;

import java.io.Serializable;

import org.gmr.web.multipart.GMultipartFile;

public class APKVO   implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String apkID;
	
	private String imeiId;
	
	private String apkVersion;
	private GMultipartFile imageFile;
	
	private String make ;
	
	private String model;
	
	private String uploadedTime;
	
	public String getApkID() {
		return apkID;
	}

	public void setApkID(String apkID) {
		this.apkID = apkID;
	}

	public String getImeiId() {
		return imeiId;
	}

	public void setImeiId(String imeiId) {
		this.imeiId = imeiId;
	}

	public String getApkVersion() {
		return apkVersion;
	}

	public void setApkVersion(String apkVersion) {
		this.apkVersion = apkVersion;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getUploadedTime() {
		return uploadedTime;
	}

	public void setUploadedTime(String uploadedTime) {
		this.uploadedTime = uploadedTime;
	}

	public String getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(String string) {
		this.androidVersion = string;
	}

	public String getApkUrl() {
		return apkUrl;
	}

	public void setApkUrl(String apkUrl) {
		this.apkUrl = apkUrl;
	}

	public GMultipartFile getImageFile() {
		return imageFile;
	}

	public void setImageFile(GMultipartFile imageFile) {
		this.imageFile = imageFile;
	}

	private String androidVersion;
	
	private String apkUrl;
	
	
   
}
