package com.medsure.ui.entity.server;

import java.io.Serializable;
import java.util.Arrays;

import javax.validation.constraints.NotNull;

import com.google.gson.annotations.Expose;

public class IDDescriptionVO  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@NotNull
	private Integer id;
	
	private String description;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		description = description;
	}
	
	public  IDDescriptionVO(int Id, String desc) {
		this.id = Id;
		this.description = desc;
	}
	
	public  IDDescriptionVO() {
		
	}

	@Override
	public String toString() {
		return "IDDescriptionVO [id=" + id + ", Description="
				+ description + "]";
	}
}
