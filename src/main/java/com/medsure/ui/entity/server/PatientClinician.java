package com.medsure.ui.entity.server;

public class PatientClinician {

	private String id;
	private String patientName;
	private String watchNo;
	private String nurseName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getNurseName() {
		return nurseName;
	}
	public void setNurseName(String nurseName) {
		this.nurseName = nurseName;
	}
	public String getWatchNo() {
		return watchNo;
	}
	public void setWatchNo(String watchNo) {
		this.watchNo = watchNo;
	}
}
