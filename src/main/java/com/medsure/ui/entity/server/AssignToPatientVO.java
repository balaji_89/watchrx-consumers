package com.medsure.ui.entity.server;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import com.google.gson.annotations.Expose;

public class AssignToPatientVO implements Serializable {

	
	private Long patientId;

	
	
	private Long morningClinicianId;

	private Long noonClinicianId;

	private Long eveningClinicianId;

	private Long nightClinicianId;

	//@NotNull
	//@Min(1)
	private Long priDoctorId;

	private Long specialistDoctorId;

	@NotNull
	@Min(1)
	private Long watchId;

	private Date createdDate;

	private Date updatedDate;

	private int status;
	
	private Long assignementId;

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Long getMorningClinicianId() {
		return morningClinicianId;
	}

	public void setMorningClinicianId(Long morningClinicianId) {
		this.morningClinicianId = morningClinicianId;
	}

	public Long getNoonClinicianId() {
		return noonClinicianId;
	}

	public void setNoonClinicianId(Long noonClinicianId) {
		this.noonClinicianId = this.morningClinicianId;
	}

	public Long getEveningClinicianId() {
		return eveningClinicianId;
	}

	public void setEveningClinicianId(Long eveningClinicianId) {
		this.eveningClinicianId = this.morningClinicianId;
	}

	public Long getNightClinicianId() {
		return nightClinicianId;
	}

	public void setNightClinicianId(Long nightClinicianId) {
		this.nightClinicianId = this.morningClinicianId;
	}

	public Long getPriDoctorId() {
		return priDoctorId;
	}

	public void setPriDoctorId(Long priDoctorId) {
		this.priDoctorId = priDoctorId;
	}

	public Long getSpecialistDoctorId() {
		return specialistDoctorId;
	}

	public void setSpecialistDoctorId(Long specialistDoctorId) {
		this.specialistDoctorId = specialistDoctorId;
	}

	public Long getWatchId() {
		return watchId;
	}

	public void setWatchId(Long watchId) {
		this.watchId = watchId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "AssignToPatientVO [patientId=" + patientId
				+ ", morningclinicianId=" + morningClinicianId + "]";
	}

	public Long getAssignementId() {
		return assignementId;
	}

	public void setAssignementId(Long assignementId) {
		this.assignementId = assignementId;
	}

}
