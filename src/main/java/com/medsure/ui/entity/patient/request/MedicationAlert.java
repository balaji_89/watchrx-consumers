package com.medsure.ui.entity.patient.request;

import java.util.Date;


public class MedicationAlert extends GeneralRequest{
	
	private String missedTime;
	private String alertDescription;
	private String missedTimeSlot;
	private String missedBeforeOrAfterFood;
	private String missedMedicationIds;
	private String alertType;
	private String alertId;
	public String getAlertId() {
		return alertId;
	}
	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}
	public String getMissedTime() {
		return missedTime;
	}
	public void setMissedTime(String missedTime) {
		this.missedTime = missedTime;
	}
	public String getAlertDescription() {
		return alertDescription;
	}
	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}
	public String getMissedTimeSlot() {
		return missedTimeSlot;
	}
	public void setMissedTimeSlot(String missedTimeSlot) {
		this.missedTimeSlot = missedTimeSlot;
	}
	public String getMissedBeforeOrAfterFood() {
		return missedBeforeOrAfterFood;
	}
	public void setMissedBeforeOrAfterFood(String missedBeforeOrAfterFood) {
		this.missedBeforeOrAfterFood = missedBeforeOrAfterFood;
	}
	public String getMissedMedicationIds() {
		return missedMedicationIds;
	}
	public void setMissedMedicationIds(String missedMedicationIds) {
		this.missedMedicationIds = missedMedicationIds;
	}
	public String getAlertType() {
		return alertType;
	}
	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}
	
	public String getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	private Date createdDate;
	private String updatedOn;

}
