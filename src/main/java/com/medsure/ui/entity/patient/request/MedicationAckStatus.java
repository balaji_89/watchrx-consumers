package com.medsure.ui.entity.patient.request;

import java.util.List;

import com.medsure.ui.entity.patient.common.MedicationInfo;

public class MedicationAckStatus extends GeneralRequest{
	
	private boolean medicationAck;
	private List<MedicationInfo> medicationDetails;
	
	public boolean isMedicationAck() {
		return medicationAck;
	}
	public void setMedicationAck(boolean medicationAck) {
		this.medicationAck = medicationAck;
	}
	public List<MedicationInfo> getMedicationDetails() {
		return medicationDetails;
	}
	public void setMedicationDetails(List<MedicationInfo> medicationDetails) {
		this.medicationDetails = medicationDetails;
	}
	
	

}
