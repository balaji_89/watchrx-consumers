package com.medsure.ui.entity.patient.request;

import java.util.List;

import com.medsure.ui.entity.patient.common.MedicationInfo;

public class NurseAvailableInfo extends GeneralRequest{
	
	private String nurseStatus;
	private  List<MedicationInfo> medicationinfo;
	public String getNurseStatus() {
		return nurseStatus;
	}
	public void setNurseStatus(String nurseStatus) {
		this.nurseStatus = nurseStatus;
	}
	public List<MedicationInfo> getMedicationinfo() {
		return medicationinfo;
	}
	public void setMedicationinfo(List<MedicationInfo> medicationinfo) {
		this.medicationinfo = medicationinfo;
	}
	
}