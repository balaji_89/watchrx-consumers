package com.medsure.ui.entity.patient.request;


public class MedicationDone extends GeneralRequest{
	
	private String medTimeStamp;
	private String medReamindTimeStamp;
	private String medAckTimeStamp;
	private String medTimeSlot;
	private String medStatus;
	private String medicineId;
	private String medFood;
	public String getMedTimeStamp() {
		return medTimeStamp;
	}
	public void setMedTimeStamp(String medTimeStamp) {
		this.medTimeStamp = medTimeStamp;
	}
	public String getMedReamindTimeStamp() {
		return medReamindTimeStamp;
	}
	public void setMedReamindTimeStamp(String medReamindTimeStamp) {
		this.medReamindTimeStamp = medReamindTimeStamp;
	}
	public String getMedAckTimeStamp() {
		return medAckTimeStamp;
	}
	public void setMedAckTimeStamp(String medAckTimeStamp) {
		this.medAckTimeStamp = medAckTimeStamp;
	}
	public String getMedTimeSlot() {
		return medTimeSlot;
	}
	public void setMedTimeSlot(String medTimeSlot) {
		this.medTimeSlot = medTimeSlot;
	}
	public String getMedStatus() {
		return medStatus;
	}
	public void setMedStatus(String medStatus) {
		this.medStatus = medStatus;
	}
	public String getMedicineId() {
		return medicineId;
	}
	public void setMedicineId(String medicineId) {
		this.medicineId = medicineId;
	}
	public String getMedFood() {
		return medFood;
	}
	public void setMedFood(String medFood) {
		this.medFood = medFood;
	}
	

}
