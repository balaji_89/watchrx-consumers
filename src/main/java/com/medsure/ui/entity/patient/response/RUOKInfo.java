package com.medsure.ui.entity.patient.response;

import java.util.List;

import com.medsure.ui.entity.patient.request.RUOKStatus;

public class RUOKInfo {
	
	private String statsData;
	private List<RUOKStatus> status;
	public String getStatsData() {
		return statsData;
	}
	public void setStatsData(String statsData) {
		this.statsData = statsData;
	}
	public List<RUOKStatus> getStatus() {
		return status;
	}
	public void setStatus(List<RUOKStatus> status) {
		this.status = status;
	}
	
}
