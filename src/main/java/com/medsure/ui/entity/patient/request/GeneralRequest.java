package com.medsure.ui.entity.patient.request;

public class GeneralRequest {
	
	private String imeiNo;
	private String patientId;
	private String patientName;
	private String gcmRegistrationId;
	
	public String getImeiNo() {
		return imeiNo;
	}
	public void setImeiNo(String imeiNo) {
		this.imeiNo = imeiNo;
	}
	
	/**
	 * @return the gcmRegistrationId
	 */
	public String getGcmRegistrationId() {
		return gcmRegistrationId;
	}
	/**
	 * @param gcmRegistrationId the gcmRegistrationId to set
	 */
	public void setGcmRegistrationId(String gcmRegistrationId) {
		this.gcmRegistrationId = gcmRegistrationId;
	}
	
	public String getPatientId() {
		return patientId;
	}
	
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	

	
}