package com.medsure.ui.entity.patient.request;

public class LogEmergencyInfo extends GeneralRequest {

	private String timeStamp;
	private String description;
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
