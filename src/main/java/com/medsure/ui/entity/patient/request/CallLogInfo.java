package com.medsure.ui.entity.patient.request;

import java.util.List;

public class CallLogInfo extends GeneralRequest{

	private List<String> callDuration;

	/**
	 * @return the callDuration
	 */
	public List<String> getCallDuration() {
		return callDuration;
	}

	/**
	 * @param callDuration the callDuration to set
	 */
	public void setCallDuration(List<String> callDuration) {
		this.callDuration = callDuration;
	}
}
