package com.medsure.ui.entity.patient.request;

import java.util.List;

public class AdherenceLogs {
	
	private List<MedicationDone> adherenceLogs;
	private String patientId;
	
	public List<MedicationDone> getAdherenceLogs() {
		return adherenceLogs;
	}

	public void setAdherenceLogs(List<MedicationDone> adherenceLogs) {
		this.adherenceLogs = adherenceLogs;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

}
