package com.medsure.ui.entity.patient.request;

import com.medsure.ui.entity.patient.response.GeneralInfo;

public class WatchCGAppStatus  {
	
	private String imei;
	private String careGiverloginId;
	private String status;
	private String date;
	private String appVersion;
	private String reason;

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getCareGiverloginId() {
		return careGiverloginId;
	}
	public void setCareGiverloginId(String careGiverloginId) {
		this.careGiverloginId = careGiverloginId;
	}
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}

}
