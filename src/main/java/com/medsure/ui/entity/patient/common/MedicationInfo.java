package com.medsure.ui.entity.patient.common;

public class MedicationInfo {
	
	private String medicineId;
	private String medicineName;
	private String daysOfWeek;
	private String timeSlots;
	private String image;
	private String color;
	private String dosage;
	private String strength;
	private String description;
	private String beforeOrAfterFood;
	private String medicineForm;
	public String getMedicineName() {
		return medicineName;
	}
	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the dosage
	 */
	public String getDosage() {
		return dosage;
	}
	/**
	 * @param dosage the dosage to set
	 */
	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	
	public String getDaysOfWeek() {
		return daysOfWeek;
	}
	
	public void setDaysOfWeek(String daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}
	
	public String getTimeSlots() {
		return timeSlots;
	}
	
	public void setTimeSlots(String timeSlots) {
		this.timeSlots = timeSlots;
	}
	
	public String getBeforeOrAfterFood() {
		return beforeOrAfterFood;
	}
	
	public void setBeforeOrAfterFood(String beforeOrAfterFood) {
		this.beforeOrAfterFood = beforeOrAfterFood;
	}
	public String getStrength() {
		return strength;
	}
	public void setStrength(String strength) {
		this.strength = strength;
	}
	public String getMedicineId() {
		return medicineId;
	}
	public void setMedicineId(String medicineId) {
		this.medicineId = medicineId;
	}
	public String getMedicineForm() {
		return medicineForm;
	}
	public void setMedicineForm(String medicineForm) {
		this.medicineForm = medicineForm;
	}
}
