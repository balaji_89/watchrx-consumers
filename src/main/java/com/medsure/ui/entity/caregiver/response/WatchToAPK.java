package com.medsure.ui.entity.caregiver.response;

import java.util.List;

public class WatchToAPK {
	
	private List<String> apkVO;
	private List<String> watchVO;
	public List<String> getApkVO() {
		return apkVO;
	}
	public void setApkVO(List<String> apkVO) {
		this.apkVO = apkVO;
	}
	public List<String> getWatchVO() {
		return watchVO;
	}
	public void setWatchVO(List<String> watchVO) {
		this.watchVO = watchVO;
	}
	
}
