package com.medsure.ui.entity.caregiver.response;

import java.util.List;

public class LoginResponse extends GeneralInfo{
	
	private List<PatientDetailandNotes> patient;

	/**
	 * @return the patient
	 */
	public List<PatientDetailandNotes> getPatient() {
		return patient;
	}

	/**
	 * @param patient the patient to set
	 */
	public void setPatient(List<PatientDetailandNotes> patient) {
		this.patient = patient;
	}

}
