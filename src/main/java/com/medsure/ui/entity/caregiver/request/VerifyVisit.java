package com.medsure.ui.entity.caregiver.request;

import com.medsure.ui.entity.caregiver.common.CareGiverDetails;

public class VerifyVisit extends CareGiverDetails{

	private String patientId;
	
	private String verifyCode;

	/**
	 * @return the patientId
	 */
	public String getPatientId() {
		return patientId;
	}

	/**
	 * @param patientId the patientId to set
	 */
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	/**
	 * @return the verifyCode
	 */
	public String getVerifyCode() {
		return verifyCode;
	}

	/**
	 * @param verifyCode the verifyCode to set
	 */
	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}
	
}
