package com.medsure.ui.entity.caregiver.request;

import java.util.List;

public class MedicationIds {
	
	private List<Long> medicationIds;

	public List<Long> getMedicationIds() {
		return medicationIds;
	}

	public void setMedicationIds(List<Long> medicationIds) {
		this.medicationIds = medicationIds;
	}
	

}
