package com.medsure.ui.entity.caregiver.request;

import com.medsure.ui.entity.caregiver.common.CareGiverDetails;

public class AssignedPatients extends CareGiverDetails{

	private boolean assignedPatients = true;

	/**
	 * @return the assignedPatients
	 */
	public boolean isAssignedPatients() {
		return assignedPatients;
	}

	/**
	 * @param assignedPatients the assignedPatients to set
	 */
	public void setAssignedPatients(boolean assignedPatients) {
		this.assignedPatients = assignedPatients;
	}
	
}
