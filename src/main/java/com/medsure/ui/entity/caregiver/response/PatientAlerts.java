package com.medsure.ui.entity.caregiver.response;

import java.util.List;

import com.medsure.ui.entity.patient.request.MedicationAlert;

public class PatientAlerts extends GeneralInfo{
	
	private List<MedicationAlert> data;

	/**
	 * @return the data
	 */
	public List<MedicationAlert> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<MedicationAlert> data) {
		this.data = data;
	}
	

}
