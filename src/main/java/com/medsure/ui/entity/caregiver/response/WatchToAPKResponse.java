package com.medsure.ui.entity.caregiver.response;


public class WatchToAPKResponse {
	
	private String apkVersion;
	private String imei;
	public String getApkVersion() {
		return apkVersion;
	}
	public void setApkVersion(String apkVersion) {
		this.apkVersion = apkVersion;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	
	
}
