package com.medsure.ui.entity.caregiver.response;


public class PatientCGInfo  {
       
	private String patientName;
	private String patientId;
	private String imeiNo;
	private String address;
	private String phone;
	private String visitReason;
	private String insuranceCompany;
	private String insuranceId;
	private String image;
	private String visitShift;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getVisitReason() {
		return visitReason;
	}
	public void setVisitReason(String visitReason) {
		this.visitReason = visitReason;
	}
	public String getInsuranceCompany() {
		return insuranceCompany;
	}
	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}
	public String getInsuranceId() {
		return insuranceId;
	}
	public void setInsuranceId(String insuranceId) {
		this.insuranceId = insuranceId;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	
	/**
	 * @return the imeiNo
	 */
	public String getImeiNo() {
		return imeiNo;
	}
	/**
	 * @param imeiNo the imeiNo to set
	 */
	public void setImeiNo(String imeiNo) {
		this.imeiNo = imeiNo;
	}
	/**
	 * @return the visitShift
	 */
	public String getVisitShift() {
		return visitShift;
	}
	/**
	 * @param visitShift the visitShift to set
	 */
	public void setVisitShift(String visitShift) {
		this.visitShift = visitShift;
	}
	
	public String getPatientName() {
		return patientName;
	}
	
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	
	
	
}
