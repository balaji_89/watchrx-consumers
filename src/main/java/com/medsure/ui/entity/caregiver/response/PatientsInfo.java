package com.medsure.ui.entity.caregiver.response;

import java.util.List;

public class PatientsInfo extends GeneralInfo{
	
	private List<PatientInfo> patientLists;

	/**
	 * @return the patientLists
	 */
	public List<PatientInfo> getPatientLists() {
		return patientLists;
	}

	/**
	 * @param patientLists the patientLists to set
	 */
	public void setPatientLists(List<PatientInfo> patientLists) {
		this.patientLists = patientLists;
	}

}
