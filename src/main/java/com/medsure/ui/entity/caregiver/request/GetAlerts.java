package com.medsure.ui.entity.caregiver.request;

import com.medsure.ui.entity.caregiver.common.CareGiverDetails;

public class GetAlerts extends CareGiverDetails{

	private String patientId;
	
	private String startDate = null;
	
	private String endDate = null;
	

	/**
	 * @return the patientId
	 */
	public String getPatientId() {
		return patientId;
	}

	/**
	 * @param nurseId the patientId to set
	 */
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	
}
