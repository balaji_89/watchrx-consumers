package com.medsure.ui.entity.caregiver.request;

import com.medsure.ui.entity.caregiver.common.CareGiverDetails;

public class PatientNotes extends CareGiverDetails{
	
	private String patientId;

	/**
	 * @return the patientId
	 */
	public String getPatientId() {
		return patientId;
	}

	/**
	 * @param patientId the patientId to set
	 */
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

}
