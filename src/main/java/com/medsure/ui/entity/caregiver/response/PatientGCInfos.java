package com.medsure.ui.entity.caregiver.response;

import java.util.List;

public class PatientGCInfos extends GeneralInfo{
	
	private List<PatientCGInfo> patientInfo;

	/**
	 * @return the patientInfo
	 */
	public List<PatientCGInfo> getPatientInfo() {
		return patientInfo;
	}

	/**
	 * @param patientInfo the patientInfo to set
	 */
	public void setPatientInfo(List<PatientCGInfo> patientInfo) {
		this.patientInfo = patientInfo;
	}

}
