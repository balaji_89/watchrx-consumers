package com.medsure.ui.entity.caregiver.request;

import java.util.Date;

public class AlertStatus {
	
	private String patinetId;
	private String alertId;
	private String status;
	private Date date;
	public String getPatinetId() {
		return patinetId;
	}
	public void setPatinetId(String patinetId) {
		this.patinetId = patinetId;
	}
	public String getAlertId() {
		return alertId;
	}
	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCareGiverId() {
		return careGiverId;
	}
	public void setCareGiverId(String careGiverId) {
		this.careGiverId = careGiverId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	private String careGiverId;

}
