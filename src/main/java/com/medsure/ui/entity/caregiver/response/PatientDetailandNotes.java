package com.medsure.ui.entity.caregiver.response;

import java.util.List;

public class PatientDetailandNotes extends GeneralInfo{
	
	private PatientInfo patientDetail;
	private List<PatientNoteInfo> notes;
	public PatientInfo getPatientDetail() {
		return patientDetail;
	}
	public void setPatientDetail(PatientInfo patientDetail) {
		this.patientDetail = patientDetail;
	}
	public List<PatientNoteInfo> getNotes() {
		return notes;
	}
	public void setNotes(List<PatientNoteInfo> notes) {
		this.notes = notes;
	}

}
