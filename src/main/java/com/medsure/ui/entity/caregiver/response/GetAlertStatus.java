package com.medsure.ui.entity.caregiver.response;

import java.util.List;

import com.medsure.ui.entity.caregiver.request.AlertStatus;

public class GetAlertStatus {
	
	private List<AlertStatus> alertStatus;

	public List<AlertStatus> getAlertStatus() {
		return alertStatus;
	}

	public void setAlertStatus(List<AlertStatus> alertStatus) {
		this.alertStatus = alertStatus;
	}
	

}
