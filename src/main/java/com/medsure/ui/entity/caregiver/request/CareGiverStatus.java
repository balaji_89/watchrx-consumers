package com.medsure.ui.entity.caregiver.request;

import com.medsure.ui.entity.caregiver.common.CareGiverDetails;

public class CareGiverStatus extends CareGiverDetails{

	private String status;
	private String patientId;
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public String getPatinetId() {
		return patientId;
	}

	public void setPatinetId(String patientId) {
		this.patientId = patientId;
	}
	
}
