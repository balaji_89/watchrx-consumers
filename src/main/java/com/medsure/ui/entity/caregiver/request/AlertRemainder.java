package com.medsure.ui.entity.caregiver.request;

import com.medsure.ui.entity.caregiver.common.CareGiverDetails;

public class AlertRemainder extends CareGiverDetails {
	
	private String patientId;
	private String alertId;
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public String getAlertId() {
		return alertId;
	}
	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}
	

}
