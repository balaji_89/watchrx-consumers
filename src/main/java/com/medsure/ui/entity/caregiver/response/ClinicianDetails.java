package com.medsure.ui.entity.caregiver.response;

import com.medsure.ui.entity.server.ClinicianVO;

public class ClinicianDetails extends GeneralInfo {

	private ClinicianVO clinicianDetails = null;

	public ClinicianVO getClinicianDetails() {
		return clinicianDetails;
	}

	public void setClinicianDetails(ClinicianVO clinicianDetails) {
		this.clinicianDetails = clinicianDetails;
	}

	
}
