package com.medsure.ui.entity.caregiver.response;

import java.util.List;

public class GetOnlyPatientNotes extends GeneralInfo{
	
	private List<PatientNoteInfo> notes;

	/**
	 * @return the notes
	 */
	public List<PatientNoteInfo> getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(List<PatientNoteInfo> notes) {
		this.notes = notes;
	}

}
