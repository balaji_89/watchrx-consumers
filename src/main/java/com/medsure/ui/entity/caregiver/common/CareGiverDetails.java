package com.medsure.ui.entity.caregiver.common;

public class CareGiverDetails {

	private String careGiverId;
	private String authToken;
	public String getCareGiverId() {
		return careGiverId;
	}
	public void setCareGiverId(String careGiverId) {
		this.careGiverId = careGiverId;
	}
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	
}
