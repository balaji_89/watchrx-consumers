package com.medsure.ui.entity.caregiver.request;

public class AlertById {
	
	private String alertId;
	private String authToken;
	/**
	 * @return the alertId
	 */
	public String getAlertId() {
		return alertId;
	}

	/**
	 * @param alertId the alertId to set
	 */
	public void setAlertId(String alertId) {
		this.alertId = alertId;
	}
	
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
}
