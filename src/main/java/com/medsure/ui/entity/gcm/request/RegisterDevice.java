package com.medsure.ui.entity.gcm.request;

public class RegisterDevice {
	
	private String deviceType;
	private String registerId;
	private String patientId;
	private String nurseId;
	private String imeiNo;
	
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	/**
	 * @return the registerId
	 */
	public String getRegisterId() {
		return registerId;
	}
	/**
	 * @param registerId the registerId to set
	 */
	public void setRegisterId(String registerId) {
		this.registerId = registerId;
	}
	/**
	 * @return the patientId
	 */
	public String getPatientId() {
		return patientId;
	}
	/**
	 * @param patientId the patientId to set
	 */
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	/**
	 * @return the nurseId
	 */
	public String getNurseId() {
		return nurseId;
	}
	/**
	 * @param nurseId the nurseId to set
	 */
	public void setNurseId(String nurseId) {
		this.nurseId = nurseId;
	}
	public String getImeiNo() {
		return imeiNo;
	}
	public void setImeiNo(String imeiNo) {
		this.imeiNo = imeiNo;
	}
	
	

}
