package com.medsure.ui.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.medsure.ui.entity.server.UserVO;

public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

	private final Logger log = LoggerFactory.getLogger(AuthenticationInterceptor.class);

	@Override
	public boolean preHandle(final HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		boolean responseFlag = true;
		try {
			UserVO user = (UserVO)request.getSession().getAttribute("user");
			if (user == null){
				response.sendRedirect("/");
				return false;
			}
		} catch (Exception e) {
			log.error("Exception occured "+e.getMessage(), e);
			e.printStackTrace();
		}
		return responseFlag;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
	}

	
}

