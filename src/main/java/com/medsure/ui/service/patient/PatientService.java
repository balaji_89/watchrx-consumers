package com.medsure.ui.service.patient;

import java.io.IOException;
import java.util.logging.Logger;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.medsure.factory.WatchRxFactory;
import com.medsure.test.patient.PatientTest;
import com.medsure.ui.entity.caregiver.request.AlertStatus;
import com.medsure.ui.entity.patient.common.DeviceStatus;
import com.medsure.ui.entity.patient.request.AdherenceLogs;
import com.medsure.ui.entity.patient.request.CallLogInfo;
import com.medsure.ui.entity.patient.request.LogEmergencyInfo;
import com.medsure.ui.entity.patient.request.MedicationAlert;
import com.medsure.ui.entity.patient.request.RUOKStatus;
import com.medsure.ui.entity.patient.request.RegisterWatch;
import com.medsure.ui.entity.patient.request.WatchCGAppStatus;
import com.medsure.ui.entity.patient.response.GeneralInfo;
import com.medsure.ui.entity.patient.response.PatientDetails;

@Api(name="patientapi",version="v1", description="To register patient i.e. watch")
public class PatientService {

	private static final Logger log = Logger
			.getLogger(PatientService.class.getName());
	@ApiMethod(
            name = "Registerwatch",
            path = "registerwatch",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public PatientDetails registerWatch(RegisterWatch watch){
		log.info("Patient Service invoked the method registerWatch");
	System.out.println(watch.getImeiNo());
		PatientDetails pd = WatchRxFactory.getPatientService().getPatientDetailsByImei(watch);
		return pd;
	}
	
	@ApiMethod(
            name = "AdherenceLogs",
            path = "addadherencelogs",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public GeneralInfo addAdherenceLogs(AdherenceLogs info){
		log.info("Patient Service invoked the method insertAdherenceLogs");
		WatchRxFactory.getAdhereLogService().insertAdhereLog(info);
		PatientTest details = new PatientTest();
		return details.generalInfo();
	}
	
	@ApiMethod(
            name = "Medicationmissed",
            path = "medicationmissed",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public GeneralInfo medicationMissed(MedicationAlert info){
		log.info("Patient Service invoked the method medicationMissed");
		WatchRxFactory.getAlertService().insertAlert(info);
		Gson gson = new Gson();
		PatientTest details = new PatientTest();
		String gcmId = null;
		try {
			
			 gcmId = WatchRxFactory.getClinicianService().getNurseGCMRegIdByPatientId(Long.parseLong(info.getPatientId()));
				AlertStatus status = new AlertStatus();
				status.setAlertId(info.getAlertId());
				status.setCareGiverId(WatchRxFactory.getClinicianService().getClinicianId(Long.parseLong(info.getPatientId())));
				status.setPatinetId(info.getPatientId());
				status.setDate(info.getCreatedDate());
				status.setStatus("new");
				WatchRxFactory.getAlertService().insertAlertStatus(status);

			String medMissed = gson.toJson(info,MedicationAlert.class);
			JsonElement jelem = gson.fromJson(medMissed, JsonElement.class);
			JsonObject jobj = jelem.getAsJsonObject();
			jobj.addProperty("messageType", "medicationMissed");
			String gcmObj = gson.toJson(jobj);
			details.sendMessageToNurse("message",gcmObj,gcmId);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//details.setPatientId("abcd");
		return details.generalInfo();
	}
	
	/*@ApiMethod(
            name = "Remainderackstatus",
            path = "remainderackornotack",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public GeneralInfo medicationNotAck(MedicationAckStatus info){
		System.out.println(info.toString());
		PatientTest details = new PatientTest();
		//details.setPatientId("abcd");
		return details.generalInfo();
	}*/
	
	@ApiMethod(
            name = "Watchalivestatus",
            path = "watchstatus",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public GeneralInfo watchAliveStatus(DeviceStatus info){
		System.out.println(info.toString());
		PatientTest details = new PatientTest();
		//details.setPatientId("abcd");
		return details.generalInfo();
	}
	
	@ApiMethod(
            name = "Tologemergencycall",
            path = "logemergencycall",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public GeneralInfo logEmergencyCall(LogEmergencyInfo info){
		System.out.println(info.toString());
		PatientTest details = new PatientTest();
		//details.setPatientId("abcd");
		return details.generalInfo();
	}
	
	@ApiMethod(
            name = "Nursenotavailable",
            path = "nursenotavail",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public GeneralInfo nurseNotAvail(LogEmergencyInfo info){
		System.out.println(info.toString());
		PatientTest details = new PatientTest();
		//details.setPatientId("abcd");
		return details.generalInfo();
	}
	
	@ApiMethod(
            name = "Totracecalllogs",
            path = "calllog",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public GeneralInfo callLogs(CallLogInfo info){
		System.out.println(info.toString());
		PatientTest details = new PatientTest();
		//details.setPatientId("abcd");
		return details.generalInfo();
	}
	
//	@ApiMethod(
//            name = "savepatientDB",
//            path = "insertPatient"
//           // httpMethod = ApiMethod.HttpMethod.POST
//    )
//	public void insertPatient(){
//		System.out.println("insert");
//		PatientDAOTest details = new PatientDAOTest();
//		//details.setPatientId("abcd");
//		 details.insertPatient();;
//	}
//	
//	@ApiMethod(
//            name = "savemedicineDB",
//            path = "insertMedicine"
//           // httpMethod = ApiMethod.HttpMethod.POST
//    )
//	public void insertMedicationInfo(){
//		System.out.println("insert medicine");
//		PatientDAOTest details = new PatientDAOTest();
//		//details.setPatientId("abcd");
//		 details.insertMedicineForPatient();;
//	}

	
	@ApiMethod(
            name = "Remainderackstatus",
            path = "medicationnotack",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public GeneralInfo medicationNotAck(MedicationAlert info){
		System.out.println(info.toString());
		PatientTest details = new PatientTest();
		//details.setPatientId("abcd");
		String gcmId = WatchRxFactory.getClinicianService().getNurseGCMRegIdByPatientId(Long.parseLong(info.getPatientId()));
		try {
			Gson gson = new Gson();
			String medMissed = gson.toJson(info,MedicationAlert.class);
			JsonElement jelem = gson.fromJson(medMissed, JsonElement.class);
			JsonObject jobj = jelem.getAsJsonObject();
			jobj.addProperty("messageType", "medicationNotAck");
			String gcmObj = gson.toJson(jobj);
			details.sendMessageToNurse("message", gcmObj, gcmId);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return details.generalInfo();
	}
	@ApiMethod(
            name = "insertRUOKStatus",
            path = "insertRUOkStaus",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public void insertRUOkStaus(RUOKStatus status){
		log.info("Patient Service invoked the method insertRUOkStaus");
		WatchRxFactory.getruOkService().insertRUOkStatus(status);
	}
	
	@ApiMethod(
            name = "insertWatchUpgradeStatus",
            path = "insertWatchUpgradeStatus",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public  GeneralInfo insertWatchUpgradeStatus(WatchCGAppStatus status){
		log.info("Patient Service invoked the method insertWatchMobileStatus");
		WatchRxFactory.getWatchService().insertWatchMobileStatus(status);
		GeneralInfo inf = new GeneralInfo();
		inf.setResponseCode("001");
		inf.setResponseMessage("operationsuccessful");
		return inf;
	}
}
