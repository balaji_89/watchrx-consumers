package com.medsure.ui.service.caregiver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.medsure.factory.WatchRxFactory;
import com.medsure.test.caregiver.CareGiverTest;
import com.medsure.ui.entity.caregiver.common.CareGiverDetails;
import com.medsure.ui.entity.caregiver.request.AlertById;
import com.medsure.ui.entity.caregiver.request.AlertByPatient;
import com.medsure.ui.entity.caregiver.request.AlertRemainder;
import com.medsure.ui.entity.caregiver.request.AlertStatus;
import com.medsure.ui.entity.caregiver.request.CareGiverStatus;
import com.medsure.ui.entity.caregiver.request.GetAlerts;
import com.medsure.ui.entity.caregiver.request.Login;
import com.medsure.ui.entity.caregiver.request.MedicationIds;
import com.medsure.ui.entity.caregiver.request.ScheduleAlert;
import com.medsure.ui.entity.caregiver.request.VerifyVisit;
import com.medsure.ui.entity.caregiver.response.ClinicianDetails;
import com.medsure.ui.entity.caregiver.response.GeneralInfo;
import com.medsure.ui.entity.caregiver.response.GetAlertStatus;
import com.medsure.ui.entity.caregiver.response.GetOnlyPatientNotes;
import com.medsure.ui.entity.caregiver.response.PatientAlerts;
import com.medsure.ui.entity.caregiver.response.PatientCGInfo;
import com.medsure.ui.entity.caregiver.response.PatientGCInfos;
import com.medsure.ui.entity.patient.common.MedicationInfo;
import com.medsure.ui.entity.patient.request.MedicationAlert;
import com.medsure.ui.entity.patient.request.WatchCGAppStatus;
import com.medsure.ui.entity.server.ClinicianVO;
import com.medsure.util.AuthTokenChecker;
import com.google.appengine.api.oauth.OAuthRequestException;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@Api(name = "caregiverapi", version = "v1", description = "To serve all care giver request i.e. mobile App")
public class CareGiverService {

	private static final Logger log = Logger
			.getLogger(CareGiverService.class.getName());
	public static List<String> verifyList = new ArrayList<String>();
	
	String[] clientIDs = {"132597499285-idmv1v8si9eufqunssntqrg9k44h27ro.apps.googleusercontent.com"};
	String audience = "https://watchrx-1007.appspot.com";
	AuthTokenChecker checker = new AuthTokenChecker(clientIDs, audience);

	
	@ApiMethod(name = "GetAlertByPatientId", path = "getAlertbypatientid", httpMethod = ApiMethod.HttpMethod.POST)
	public PatientAlerts getAlerts(GetAlerts alertDetail) {
		log.info("Care Giver Service invoked the method getAlerts");
		PatientAlerts alertDetails = new PatientAlerts();
		//if(checker.check(alertDetail.getAuthToken())){
		List<MedicationAlert> alertList = WatchRxFactory.getAlertService().getAlertsByPatientId(alertDetail);
		
		alertDetails.setData(alertList);
		alertDetails.setResponseCode("001");
		alertDetails.setResponseMessage("Operationsuccessful");
		alertDetails.setResponseType("1");
		//}else{
			//alertDetails.setResponseMessage("invalid auth token");
		//}
		// details.setPatientId("abcd");
		return alertDetails;
	}

	@ApiMethod(name = "GetAlertByNurseId", path = "getAlertbynurseid", httpMethod = ApiMethod.HttpMethod.POST)
	public PatientAlerts getAlertsByNurseId(GetAlerts alertDetail) {
		log.info("Care Giver Service invoked the method getAlerts");
		PatientAlerts alertDetails = new PatientAlerts();
		//if(checker.check(alertDetail.getAuthToken())){
		List<MedicationAlert> data = new ArrayList<MedicationAlert>();
		PatientGCInfos patientGCInfos = WatchRxFactory.getPatientService().getPatientsByClinician(new Long (alertDetail.getCareGiverId()));
		for(PatientCGInfo patientCGInfo : patientGCInfos.getPatientInfo()){
			alertDetail.setPatientId(patientCGInfo.getPatientId());
			data.addAll(WatchRxFactory.getAlertService().getAlertsByPatientId(alertDetail));
		}
		alertDetails.setData(data);
		alertDetails.setResponseCode("001");
		alertDetails.setResponseMessage("Operationsuccessful");
		alertDetails.setResponseType("1");
		//}else{
		//	alertDetails.setResponseMessage("invalid auth token");
		//}
		// details.setPatientId("abcd");
		return alertDetails;
		// details.setPatientId("abcd");
	}
	
	@ApiMethod(name = "GetPatientsForNurse", path = "getpatientsfornurse", httpMethod = ApiMethod.HttpMethod.POST)
	public PatientGCInfos getPatientsForNurse(CareGiverDetails nurseId) {
		log.info("Care Giver Service invoked the method getPatientsForNurse");
		PatientGCInfos patientGCInfos = new PatientGCInfos();
		//if(checker.check(nurseId.getAuthToken())){
		 patientGCInfos = WatchRxFactory.getPatientService().getPatientsByClinician(new Long (nurseId.getCareGiverId()));
		//}else{
			patientGCInfos.setResponseMessage("Operationsuccessful");
		//}
		return patientGCInfos;
	}

	@ApiMethod(name = "GetAlertById", path = "getalertbyid", httpMethod = ApiMethod.HttpMethod.POST)
	public MedicationAlert getAlerByAlertId(AlertById alertId) {
		log.info("Care Giver Service invoked the method getAlerByAlertId");
		
		MedicationAlert alert = WatchRxFactory.getAlertService().getAlertByAlertId(Long.parseLong(alertId.getAlertId()));
        return alert;
	}

	@ApiMethod(name = "GetNotesByPatientId", path = "getnotesbypatientid", httpMethod = ApiMethod.HttpMethod.POST)
	public GetOnlyPatientNotes getNotesByPatientId(AlertByPatient alertId) {
		System.out.println(alertId.toString());
		CareGiverTest sdetails = new CareGiverTest();
		// details.setPatientId("abcd");
		return sdetails.patientNotes();
	}

	@ApiMethod(name = "CareGiverEmailLogin", path = "emaillogin", httpMethod = ApiMethod.HttpMethod.POST)
	public GeneralInfo doEmailLogin(Login loginDetails) {System.out.println(loginDetails.toString());
	GeneralInfo info = new GeneralInfo();
	ClinicianVO clinicianVO = WatchRxFactory.getClinicianService().getClinicianByUserName(loginDetails.getLoginId());
	if(checker.check(loginDetails.getAuthToken())){
		log.info("Login Successful");
		info.setResponseMessage("Login succesful");
		info.setNurseId(String.valueOf(clinicianVO.getClinicianId()));
	}else{
		log.info("Login failed");
		info.setResponseMessage("Login failed");
	}
	return info;}

	@ApiMethod(name = "GetScheduleAlert", path = "schedulealert", httpMethod = ApiMethod.HttpMethod.POST)
	public GeneralInfo scheduleAlert(ScheduleAlert details) {
		System.out.println(details.toString());
		CareGiverTest sdetails = new CareGiverTest();
		// details.setPatientId("abcd");
		return sdetails.generalInfo();
	}

	/*@ApiMethod(name = "RemaindPatient", path = "remaindpatient", httpMethod = ApiMethod.HttpMethod.POST)
	public GeneralInfo remaindPatient(AlertRemainder details) {
		System.out.println(details.toString());
		CareGiverTest sdetails = new CareGiverTest();
		// details.setPatientId("abcd");
		return sdetails.generalInfo();
	}*/

	@ApiMethod(name = "SavePatientCallRequest", path = "savecallRequest", httpMethod = ApiMethod.HttpMethod.POST)
	public GeneralInfo saveCallRequest(AlertRemainder details) {
		System.out.println(details.toString());
		CareGiverTest sdetails = new CareGiverTest();
		// details.setPatientId("abcd");
		return sdetails.generalInfo();
	}

	

	@ApiMethod(
            name = "InsertAlertStatus",
            path = "insertalertstatus",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public GeneralInfo insertAlertStatus(AlertStatus info){
		System.out.println(info.toString());
		WatchRxFactory.getAlertService().insertAlertStatus(info);
		CareGiverTest details = new CareGiverTest();
		return details.generalInfo();
	}
	
	@ApiMethod(
            name = "getAlertStatus",
            path = "getalertstatusbycaregiverId",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public GetAlertStatus getAlertStatusByPatientId(AlertStatus info){
		System.out.println(info.toString());
		GetAlertStatus alertStatus = new GetAlertStatus();
		alertStatus.setAlertStatus(WatchRxFactory.getAlertService().getAlertStatusInfo(Long.parseLong(info.getCareGiverId())));
		return alertStatus;
	}
	
	@ApiMethod(
            name = "getAlertStatusByStatus",
            path = "getalertstatusByStatusBycaregiverId",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public GetAlertStatus getalertstatusByStatusBycaregiverId(AlertStatus info){
		System.out.println(info.toString());
		GetAlertStatus alertStatus = new GetAlertStatus();
		alertStatus.setAlertStatus(WatchRxFactory.getAlertService().getAlertStatusInfoByStatusByCG(info));
		return alertStatus;
	}
	
	@ApiMethod(name = "VerifyPatientVisit", path = "verifyvisit", httpMethod = ApiMethod.HttpMethod.POST)
	public GeneralInfo verifyVisit(VerifyVisit details) {
		System.out.println(details.toString());
		CareGiverTest sdetails = new CareGiverTest();
		// details.setPatientId("abcd");
		try {
			String tempString = String.valueOf(CareGiverTest.randInt());
			verifyList.add(tempString);
			String gcmId = WatchRxFactory.getPatientService().getPatientById(Long.parseLong(details.getPatientId())).getGcmId();
			ClinicianVO clinicianVO = WatchRxFactory.getClinicianService().getClinician(Long.parseLong(details.getCareGiverId()));
			JsonObject jo = new JsonObject();
			jo.addProperty("messageType", "visitVerification");
			jo.addProperty("caregiverName", clinicianVO.getUserName());
			jo.addProperty("visitVerificationCode", tempString);
			Gson gson = new Gson();
			String jsonStr = gson.toJson(jo);
			sdetails.sendMessageToPatient("message", jsonStr, gcmId);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sdetails.generalInfo();
	}
	
	@ApiMethod(name = "VerifyVisted", path = "verifyvisited", httpMethod = ApiMethod.HttpMethod.POST)
	public GeneralInfo verifyvisited(VerifyVisit visit) {
		//System.out.println(details.toString());
		GeneralInfo generalInfo = new GeneralInfo();
		if(verifyList.contains(visit.getVerifyCode())){
			generalInfo.setResponseMessage("OperationSuccessful");
		}else{
			generalInfo.setResponseMessage("OperationFailed");

		}
		generalInfo.setResponseType("1");
		generalInfo.setResponseCode("001");
		// details.setPatientId("abcd");
		return generalInfo;
	}

	@ApiMethod(name = "CareGiverStatus", path = "caregiverstatus", httpMethod = ApiMethod.HttpMethod.POST)
	public GeneralInfo careGiverStatus(CareGiverStatus details) {
		System.out.println(details.toString());
		CareGiverTest sdetails = new CareGiverTest();
		// details.setPatientId("abcd");
		try {
			String gcmId = WatchRxFactory.getPatientService().getPatientById(Long.parseLong(details.getPatinetId())).getGcmId();
			ClinicianVO clinicianVO = WatchRxFactory.getClinicianService().getClinician(Long.parseLong(details.getCareGiverId()));
			JsonObject jo = new JsonObject();
			jo.addProperty("messageType", "nurseOnTheWay");
			jo.addProperty("caregiverName", clinicianVO.getUserName());
			jo.addProperty("status", details.getStatus());
			Gson gson = new Gson();
			String jsonStr = gson.toJson(jo);
			sdetails.sendMessageToPatient("message", jsonStr,gcmId);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sdetails.generalInfo();
	}
	
	@ApiMethod(name = "RemindMedicationMissed", path = "remindMedicationMissed", httpMethod = ApiMethod.HttpMethod.POST)
	public GeneralInfo remindMedicationMissed(MedicationAlert details) {
		System.out.println(details.toString());
		CareGiverTest sdetails = new CareGiverTest();
		// details.setPatientId("abcd");
		try {
			Gson gson = new Gson();
			String gcmId = WatchRxFactory.getPatientService().getPatientById(Long.parseLong(details.getPatientId())).getGcmId();
			String alert = gson.toJson(details,MedicationAlert.class);
			JsonElement jelem = gson.fromJson(alert, JsonElement.class);
			JsonObject jobj = jelem.getAsJsonObject();
			jobj.addProperty("messageType", "nurseReminder");
			String gcmObj = gson.toJson(jobj);
			sdetails.sendMessageToPatient("message", gcmObj,gcmId);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sdetails.generalInfo();
	}
	
	@ApiMethod(name = "MedicationInfoByListOfIds", path = "medicationinfobyidlist", httpMethod = ApiMethod.HttpMethod.POST)
	public List<MedicationInfo> medicationInfoByListOfIds(MedicationIds details) {
		System.out.println(details.toString());
			List<MedicationInfo> result = WatchRxFactory.getAlertService().getMedicationByMedicationIds(details.getMedicationIds());
		
		return result;
	}
	@ApiMethod(name = "CareGiverLogin", path = "login", httpMethod = ApiMethod.HttpMethod.POST)
	public GeneralInfo doLogin(Login loginDetails) {
		System.out.println(loginDetails.toString());
		GeneralInfo info = new GeneralInfo();
		try{
		ClinicianVO clinicianVO = WatchRxFactory.getClinicianService().getClinicianByUserName(loginDetails.getLoginId());
		if(clinicianVO.getPassword().equalsIgnoreCase(loginDetails.getPassword())){
			log.info("Login Successful");
			info.setResponseMessage("Login succesful");
			info.setNurseId(String.valueOf(clinicianVO.getClinicianId()));
		}else{
			log.info("Login failed");
			info.setResponseMessage("Login failed");
		}
		}catch(Exception e){
			e.getMessage();
		}
		return info;
	}
	
	@ApiMethod(name = "userInfo", path = "userInfo", httpMethod = ApiMethod.HttpMethod.POST)
	public GeneralInfo getUserDetails( User user) throws OAuthRequestException, IOException {
		GeneralInfo info = new GeneralInfo();
		if (user == null) {
		    info.setResponseMessage("User Id not exist"+"User Email Id not exist");
		  }
		 else {
		    info.setResponseMessage("User Id "+user.getUserId()+"User Email Id "+user.getEmail());
		  }
	  return info;
	}
	@ApiMethod(name = "getUserEmail", path = "getUserEmail", httpMethod = ApiMethod.HttpMethod.POST)	
	public GeneralInfo getMessage(){
		GeneralInfo info = new GeneralInfo();
	UserService userService=UserServiceFactory.getUserService();
	  User user=userService.getCurrentUser();
	  String message;
	  if (user == null) {
	    message="No one is logged in!\nSent from App Engine at " + new Date();
	    info.setResponseMessage("User Id not exist"+"User Email Id not exist");
	  }
	 else {
	    message="Hello, " + user.getEmail() + "!\nSent from App Engine at "+ new Date();
	    info.setResponseMessage("User Id "+user.getUserId()+"User Email Id "+user.getEmail());
	  }
	  log.info("Returning message \"" + message + "\"");
	  return info;
	}
	@ApiMethod(name = "getClinicianDetailsById", path = "getClinicianDetailsById", httpMethod = ApiMethod.HttpMethod.POST)	
	public ClinicianDetails getClinicianDetailsById(CareGiverDetails details){
		ClinicianDetails info = new ClinicianDetails();
		ClinicianVO vo = WatchRxFactory.getClinicianService().getClinician(Long.parseLong(details.getCareGiverId()));
		info.setClinicianDetails(vo);
	  return info;
	}
	
	@ApiMethod(name = "CareGiverPasswordReset", path = "careGiverPasswordReset", httpMethod = ApiMethod.HttpMethod.POST)
	public GeneralInfo doCareGiverPasswordReset(Login loginDetails) {
		System.out.println(loginDetails.toString());
		GeneralInfo info = new GeneralInfo();
		try{
		 WatchRxFactory.getClinicianService().resetPasswordByUserName(loginDetails);
		}catch(Exception e){
			 info.setResponseMessage("Operation failed");;
			e.getMessage();
		}
		 info.setResponseMessage("Operation successful");;
		 return info;
	}
	
	@ApiMethod(
            name = "insertCGAppUpgradeStatus",
            path = "insertCGAppUpgradeStatus",
            httpMethod = ApiMethod.HttpMethod.POST
    )
	public void insertCGAppUpgradeStatus(WatchCGAppStatus status){
		log.info("Patient Service invoked the method insertWatchMobileStatus");
		WatchRxFactory.getWatchService().insertWatchMobileStatus(status);
	}
}
