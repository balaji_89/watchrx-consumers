package com.medsure.ui.service.gcm;

import java.util.logging.Logger;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.medsure.factory.WatchRxFactory;
import com.medsure.ui.entity.gcm.request.RegisterDevice;
import com.medsure.ui.entity.patient.response.GeneralInfo;

/**
 * A registration endpoint class we are exposing for a device's GCM registration
 * id on the backend
 *
 * For more information, see
 * https://developers.google.com/appengine/docs/java/endpoints/
 *
 * NOTE: This endpoint does not use any form of authorization or authentication!
 * If this app is deployed, anyone can access this endpoint! If you'd like to
 * add authentication, take a look at the documentation.
 */
@Api(name = "registration", version = "v1", description = "To register watch and app")
public class RegistrationEndpoint {

	private static final Logger log = Logger
			.getLogger(RegistrationEndpoint.class.getName());

	private static final String DEVICE_TYPE_WATCH = "watch";
	private static final String DEVICE_TYPE_MOBILE = "mobile";

	/**
	 * Register a device to the backend
	 *
	 * @param regId
	 *            The Google Cloud Messaging registration Id to add
	 */
	@ApiMethod(name = "registerGCM", path = "registerGCM", httpMethod = ApiMethod.HttpMethod.POST)
	public GeneralInfo registerDevice(RegisterDevice regId) {
		log.info("Invoked registration endpoint with device type ::"
				+ regId.getDeviceType() + " register id :: "
				+ regId.getRegisterId());
		GeneralInfo response = new GeneralInfo();
		response.setResponseCode("001");
		response.setResponseType("1");
		response.setStatus("success");
		if (regId.getDeviceType().equalsIgnoreCase(DEVICE_TYPE_WATCH)) {
			WatchRxFactory.getPatientService()
					.saveGCMRegID(regId.getRegisterId(),
							regId.getImeiNo());
		} else if (regId.getDeviceType().equalsIgnoreCase(DEVICE_TYPE_MOBILE)) {
			WatchRxFactory.getClinicianService().saveGCMRegID(
					regId.getRegisterId(), Long.parseLong(regId.getNurseId()));
		}

		return response;
	}

}
