package com.medsure.ui.service.gcm;

import static com.medsure.ui.gcm.OfyServiceMobile.ofyMobile;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Named;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.api.server.spi.config.Api;
import com.medsure.ui.gcm.RegistrationRecordMobile;
import com.medsure.ui.gcm.RegistrationRecordWatch;
import static com.medsure.ui.gcm.OfyServiceWatch.ofyWatch;

/**
 * An endpoint to send messages to devices registered with the backend
 *
 * For more information, see
 * https://developers.google.com/appengine/docs/java/endpoints/
 *
 * NOTE: This endpoint does not use any form of authorization or authentication!
 * If this app is deployed, anyone can access this endpoint! If you'd like to
 * add authentication, take a look at the documentation.
 */
@Api(name = "messaging", version = "v1")
public class MessagingEndpoint {
	private static final Logger log = Logger.getLogger(MessagingEndpoint.class
			.getName());

	/** Api Keys can be obtained from the google cloud console */
	private static final String API_KEY = System.getProperty("gcm.api.key");

	/**
	 * Send to the first 10 devices (You can modify this to send to any number
	 * of devices or a specific device)
	 *
	 * @param message
	 *            The message to send
	 */
	public void sendMessageToNurse(@Named("message") String message)
			throws IOException {
		if (message == null || message.trim().length() == 0) {
			log.warning("Not sending message because it is empty");
			return;
		}
		// crop longer messages
		if (message.length() > 1000) {
			message = message.substring(0, 1000) + "[...]";
		}
		Sender sender = new Sender(API_KEY);
		Message msg = new Message.Builder().addData("message", message).build();
		List<RegistrationRecordMobile> records = ofyMobile().load()
				.type(RegistrationRecordMobile.class).limit(10).list();
		for (RegistrationRecordMobile record : records) {
			Result result = sender.send(msg, record.getRegId(), 5);
			if (result.getMessageId() != null) {
				log.info("Message sent to " + record.getRegId());
				String canonicalRegId = result.getCanonicalRegistrationId();
				if (canonicalRegId != null) {
					// if the regId changed, we have to update the datastore
					log.info("Registration Id changed for " + record.getRegId()
							+ " updating to " + canonicalRegId);
					record.setRegId(canonicalRegId);
					ofyMobile().save().entity(record).now();
				}
			} else {
				String error = result.getErrorCodeName();
				if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
					log.warning("Registration Id "
							+ record.getRegId()
							+ " no longer registered with GCM, removing from datastore");
					// if the device is no longer registered with Gcm, remove it
					// from the datastore
					ofyMobile().delete().entity(record).now();
				} else {
					log.warning("Error when sending message : " + error);
				}
			}
		}
	}

	public void sendMessageToPatient(@Named("message") String message)
			throws IOException {
		if (message == null || message.trim().length() == 0) {
			log.warning("Not sending message because it is empty");
			return;
		}
		// crop longer messages
		if (message.length() > 1000) {
			message = message.substring(0, 1000) + "[...]";
		}
		Sender sender = new Sender(API_KEY);
		Message msg = new Message.Builder().addData("message", message).build();
		List<RegistrationRecordWatch> records = ofyWatch().load()
				.type(RegistrationRecordWatch.class).limit(10).list();
		for (RegistrationRecordWatch record : records) {
			Result result = sender.send(msg, record.getRegId(), 5);
			if (result.getMessageId() != null) {
				log.info("Message sent to " + record.getRegId());
				String canonicalRegId = result.getCanonicalRegistrationId();
				if (canonicalRegId != null) {
					// if the regId changed, we have to update the datastore
					log.info("Registration Id changed for " + record.getRegId()
							+ " updating to " + canonicalRegId);
					record.setRegId(canonicalRegId);
					ofyWatch().save().entity(record).now();
				}
			} else {
				String error = result.getErrorCodeName();
				if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
					log.warning("Registration Id "
							+ record.getRegId()
							+ " no longer registered with GCM, removing from datastore");
					// if the device is no longer registered with Gcm, remove it
					// from the datastore
					ofyWatch().delete().entity(record).now();
				} else {
					log.warning("Error when sending message : " + error);
				}
			}
		}
	}

	/*
	 * public void sendMessageToDevice(String message, String registerIId)
	 * throws IOException { if(message == null || message.trim().length() == 0)
	 * { log.warning("Not sending message because it is empty"); return; } //
	 * crop longer messages if (message.length() > 1000) { message =
	 * message.substring(0, 1000) + "[...]"; } Sender sender = new
	 * Sender(API_KEY); Message msg = new Message.Builder().addData("message",
	 * message).build(); List<RegistrationRecordMobile> records =
	 * ofyMobile().load().type(RegistrationRecordMobile.class).limit(10).list();
	 * //for(RegistrationRecordMobile record : records) { Result result =
	 * sender.send(msg, registerIId, 5); if (result.getMessageId() != null) {
	 * log.info("Message sent to " + registerIId); String canonicalRegId =
	 * result.getCanonicalRegistrationId(); if (canonicalRegId != null) { // if
	 * the regId changed, we have to update the datastore
	 * log.info("Registration Id changed for " + registerIId + " updating to " +
	 * canonicalRegId);
	 * 
	 * } } else { String error = result.getErrorCodeName(); if
	 * (error.equals(Constants.ERROR_NOT_REGISTERED)) {
	 * 
	 * } else { log.warning("Error when sending message : " + error); } } //} }
	 */
}