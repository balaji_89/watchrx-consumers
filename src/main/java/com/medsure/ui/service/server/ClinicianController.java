package com.medsure.ui.service.server;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;

import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.medsure.common.Constants;
import com.medsure.service.ClinicianService;
import com.medsure.ui.entity.server.ClinicianVO;
import com.medsure.ui.entity.server.StatusVO;
import com.medsure.ui.util.JsonUtils;
import com.medsure.util.DropdownUtils;

@Controller
@RequestMapping("/service/clinician")
public class ClinicianController {
	
	private static Logger logger = Logger.getLogger(ClinicianController.class);
	
	@Autowired
	ClinicianService clinicianService;
	  
	@RequestMapping(value = "/clinicianSummary", method = RequestMethod.GET)
	public ModelAndView clinicianSummary(HttpServletRequest request) throws Exception {
		logger.info("inside getClinicianSummary:::: ");	
		List<ClinicianVO> clinicianList = clinicianService.getClinicianList();
		ModelAndView mav = new ModelAndView();
		mav.addObject("clinicianList", clinicianList);
		mav.setViewName("clinicianSummary");
		return mav;
	}
	
	@RequestMapping(value = "/clinician", method = RequestMethod.GET)
	public ModelAndView clinician(HttpServletRequest request, long clinicianId) throws Exception {
		logger.info("inside getClinician:::: "+clinicianId);	
		ClinicianVO clinician = null;
		if (clinicianId > 0) {
			clinician = clinicianService.getClinician(clinicianId);
		} else {
			clinician = new ClinicianVO();
		}
		ModelAndView mav = new ModelAndView();
		mav.addObject("clinicianTypes",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.CLINICIAN_TYPE));		
		mav.addObject("shifts",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.SHIFT));
		mav.addObject("clinicianVO", clinician);
		mav.setViewName("clinicianEdit");
		return mav;
	}
	
	@RequestMapping(value = "/saveClinician", method = RequestMethod.POST)
	public ModelAndView saveClinician(HttpServletRequest request, @Valid ClinicianVO  clinicianVO, BindingResult result) throws Exception {
		logger.info("inside saveClinician:::: "+clinicianVO);
		ModelAndView mav = new ModelAndView();

		if (null!=(clinicianVO.getUserName())){
			if (clinicianService.isUsernameExists(clinicianVO)){
				result.addError(new FieldError("clinicianVO", "userName", "User Name already exists."));	
				mav.addObject("error","User Name already exists.");
				logger.info("User Name already exists. :::: ");

			}
		}
		if(result.hasErrors()){

			mav.addObject("clinicianTypes",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.CLINICIAN_TYPE));
			mav.addObject("shifts",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.SHIFT));
			mav.addObject("clinicianVO", clinicianVO);
			mav.setViewName("clinicianEdit");
			return mav;
		}
		logger.info("iter::hasNext::"+clinicianVO.getImageFile().getBytes().length);
		logger.info("iter::hasNext::"+clinicianVO.getImageFile().getOriginalFilename());
		if (clinicianVO.getImageFile().getBytes().length>0) {
			GcsService gcsService =
				    GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
			AppIdentityService appIdentityService = AppIdentityServiceFactory.getAppIdentityService();
			String defaultBucketName = appIdentityService.getDefaultGcsBucketName();
			GcsFilename fileName = new GcsFilename(defaultBucketName+"/clinician", clinicianVO.getImageFile().getOriginalFilename());
			    GcsOutputChannel outputChannel =
			    	    gcsService.createOrReplace(fileName, GcsFileOptions.getDefaultInstance());
			 		    
			    	outputChannel.write(ByteBuffer.wrap(clinicianVO.getImageFile().getBytes()));
			    	outputChannel.close();			  
			    	logger.info("Done writing...");
			   clinicianVO.setPicPath("clinician/"+clinicianVO.getImageFile().getOriginalFilename());
			   clinicianVO.setFileModified(true);
		}else{
			if(clinicianVO.getPicPath() != null && !"".equals(clinicianVO.getPicPath())){
				clinicianVO.setFileModified(false);				
			}else{
				result.addError(new FieldError("clinicianVO","picPath","Please select an Image."));
				mav.addObject("clinicianTypes",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.CLINICIAN_TYPE));
				mav.addObject("shifts",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.SHIFT));
				mav.addObject("clinicianVO", clinicianVO);
				mav.setViewName("clinicianEdit");
				return mav;
			}
		}

		clinicianService.saveClinician(clinicianVO);
		List<ClinicianVO> clinicianList = clinicianService.getClinicianList();		
		mav.addObject("clinicianList", clinicianList);
		mav.setViewName("clinicianSummary");
		return mav;
	}
	
	@RequestMapping(value = "/deleteClinician", method = RequestMethod.POST)
	@ResponseBody
	public String deleteClinician(HttpServletRequest request, long clinicianId) throws Exception {
		logger.info("inside deleteClinician:::: "+clinicianId);	
		clinicianService.deleteClinician(clinicianId);
		StatusVO statusVO = new StatusVO();
		List<String> errorMsgs = new ArrayList<String> ();
		statusVO.setMessages(errorMsgs.toArray(new String[errorMsgs.size()]));
		return JsonUtils.toJSON(statusVO);
	}
}
