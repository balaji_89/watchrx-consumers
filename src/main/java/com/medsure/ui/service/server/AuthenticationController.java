package com.medsure.ui.service.server;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.medsure.common.Constants;
import com.medsure.service.UserService;
import com.medsure.ui.entity.server.ClinicianVO;
import com.medsure.ui.entity.server.StatusVO;
import com.medsure.ui.entity.server.UserVO;
import com.medsure.ui.util.JsonUtils;

@Controller
@RequestMapping("")
public class AuthenticationController {
	
	private static Logger logger = Logger.getLogger(AuthenticationController.class);
	
	@Autowired
	UserService  userService;
	 
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String index(HttpServletRequest request, ModelMap model) throws Exception {		
		return "redirect:/index.jsp" ;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody
	String login(HttpServletRequest request, @RequestParam String userName,
			@RequestParam String password) throws Exception {
		StatusVO statusVO = new StatusVO();
		List<String> errorMsgs = new ArrayList<String> ();
		System.out.println("In login method ::: email: " + userName + " password:::"
				+ password);
		UserVO user = userService.getUser(userName);
		logger.info("profile:::: " + user);
		if (user != null) {
	
			if (user.getPassword().equals(password)) {
				statusVO.setSuccess(true);
			} else {
				statusVO.setSuccess(false);
				errorMsgs.add("Invalid User Name/Password");
			}
		} else {
			statusVO.setSuccess(false);
			errorMsgs.add("Invalid User Name/Password");
		}
		request.getSession().setAttribute("user", user);
		statusVO.setMessages(errorMsgs.toArray(new String[errorMsgs.size()]));
		return JsonUtils.toJSON(statusVO);
	}
	
	@RequestMapping(value = "/postlogin", method = RequestMethod.GET)
	public String postLogin(HttpServletRequest request) throws Exception {
		
		UserVO user = (UserVO)request.getSession().getAttribute("user");
		if (user != null && user.getRoleType() == Constants.UserType.DOCTOR){
			return "redirect:/service/patient/patientSummary";
		}else if (user != null && user.getRoleType() == Constants.UserType.CAREGIVER){
			return "redirect:/service/patient/patientSummary";
		}else{
			return "redirect:/service/watch/watchSummary";
		}
		
		
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, ModelMap model) throws Exception {
		request.getSession().removeAttribute("user");
		request.getSession().invalidate();
		return "redirect:/index.jsp" ;
	}
}
