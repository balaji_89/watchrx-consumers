package com.medsure.ui.service.server;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.medsure.service.RUOkService;
import com.medsure.ui.entity.patient.response.RUOKInfo;

@Controller
@RequestMapping("/service/patient")
public class AreYouOkController {
	
	private static Logger logger = Logger.getLogger(AreYouOkController.class);
	
	  @Autowired
	  RUOkService ruOkService;
	  
		@RequestMapping(value = "/ruokSummary", method = RequestMethod.GET)
		public ModelAndView ruokSummary(HttpServletRequest request) throws Exception {
			logger.info("inside ruokSummary:::: ");	
			RUOKInfo okList = ruOkService.getRUOKStatusInfo();
			ModelAndView mav = new ModelAndView();
			mav.addObject("statsInfo", okList.getStatsData());
			mav.addObject("ruokList", okList.getStatus());
			logger.info("inside getruokSummary:::: "+okList.getStatus().size());	
			mav.setViewName("ruokSummary");
			return mav;
		}
		
		@RequestMapping(value = "/ruokSummaryByDate", method = RequestMethod.GET)
		public ModelAndView ruokSummaryByDate(HttpServletRequest request, String date) throws Exception {
			logger.info("inside ruokSummaryByDate:::: ");
			//System.out.println(date);
			RUOKInfo okList = ruOkService.getRUOKStatusInfoByDate(date);
			ModelAndView mav = new ModelAndView();
			mav.addObject("statsInfo", okList.getStatsData());
			mav.addObject("ruokList", okList.getStatus());
			logger.info("inside getruokSummary:::: "+okList.getStatus().size());	
			mav.setViewName("ruokSummary");
			return mav;
		}
		
}
