package com.medsure.ui.service.server;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.api.client.json.Json;
import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.medsure.common.Constants;
import com.medsure.model.WatchrxWatchCGAppInstallStatus;
import com.medsure.service.ClinicianService;
import com.medsure.service.DoctorService;
import com.medsure.service.PatientService;
import com.medsure.service.RUOkService;
import com.medsure.service.WatchService;
import com.medsure.test.caregiver.CareGiverTest;
import com.medsure.ui.entity.caregiver.response.WatchToAPK;
import com.medsure.ui.entity.caregiver.response.WatchToAPKResponse;
import com.medsure.ui.entity.patient.request.WatchCGAppStatus;
import com.medsure.ui.entity.patient.response.RUOKInfo;
import com.medsure.ui.entity.server.APKVO;
import com.medsure.ui.entity.server.AssignToPatientVO;
import com.medsure.ui.entity.server.ClinicianVO;
import com.medsure.ui.entity.server.PatientClinician;
import com.medsure.ui.entity.server.StatusVO;
import com.medsure.ui.entity.server.UserVO;
import com.medsure.ui.entity.server.WatchVO;
import com.medsure.ui.util.JsonUtils;
import com.medsure.ui.util.WatchRxUtils;
import com.medsure.util.DropdownUtils;



@Controller
@RequestMapping("/service/watch")
public class WatchController {
	private String STORAGE_BASE_URL = "https://storage.googleapis.com/";
	
	private static Logger logger = Logger.getLogger(WatchController.class);
	
	@Autowired
	WatchService watchService;
	
	@Autowired
	PatientService patientService;
	
	@Autowired
	DoctorService doctorService;
	
	@Autowired
	ClinicianService clinicianService;
	
	
	@RequestMapping(value = "/watchSummary", method = RequestMethod.GET)
	public ModelAndView watchSummary(HttpServletRequest request) throws Exception {
		StatusVO statusVO = new StatusVO();
		List<String> errorMsgs = new ArrayList<String> ();	
		logger.info("inside getWatchSummary:::: ");	
		List<WatchVO> watchList = watchService.getWatchList();
		ModelAndView mav = new ModelAndView();
		mav.addObject("watchList", watchList);
		mav.setViewName("watchSummary");
		return mav;
	}
	
	@RequestMapping(value = "/watch", method = RequestMethod.GET)
	public ModelAndView watch(HttpServletRequest request, long watchId) throws Exception {
		logger.info("inside getWatch:::: "+watchId);	
		WatchVO watch = null;
		if(watchId>0){
		watch = watchService.getWatch(watchId);
		}else{
			watch = new WatchVO();
		}
		ModelAndView mav = new ModelAndView();
		mav.addObject("watchVO", watch);
		mav.setViewName("watchEdit");
		return mav;
	}
	
	@RequestMapping(value = "/saveWatch", method = RequestMethod.POST)
	public ModelAndView saveWatch(HttpServletRequest request, @Valid WatchVO watchVO, BindingResult result) throws Exception {
		logger.info("inside saveWatch:::: "+watchVO);	
		ModelAndView mav = new ModelAndView();
		
		if (null!=(watchVO.getImeiNumber())){
			if (watchService.isIMEIExists(watchVO)){
				result.addError(new FieldError("watchVO", "imeiNumber", "IMEI Number already exists."));	
			}
		}
		if(result.hasErrors()){
			mav.addObject("watchVO", watchVO);
			mav.setViewName("watchEdit");
			return mav;
		}
		watchService.saveWatch(watchVO);
		List<WatchVO> watchList = watchService.getWatchList();
		mav.addObject("watchList", watchList);
		mav.setViewName("watchSummary");
		return mav;
	}
	

	@RequestMapping(value = "/watchAssign", method = RequestMethod.GET)
	public ModelAndView watchAssign(HttpServletRequest request) throws Exception {
	//	logger.info("inside getClinician:::: "+prescriptionId);	
		AssignToPatientVO assign = new AssignToPatientVO();		
	//	AssignToPatientVO prescription = WatchRxUtils.getPrescription((int) prescriptionId);
		ModelAndView mav = new ModelAndView();
		List<ClinicianVO> clinicians = new ArrayList<ClinicianVO>();
		clinicians.addAll(clinicianService.getClinicianList(Constants.Shift.MORNIG));
		clinicians.addAll(clinicianService.getClinicianList(Constants.Shift.NOON));
		clinicians.addAll(clinicianService.getClinicianList(Constants.Shift.NIGHT));
		mav.addObject("allClinicians", clinicians);
		//mav.addObject("morningClinicians", clinicianService.getClinicianList(Constants.Shift.MORNIG));
		//mav.addObject("noonClinicians", clinicianService.getClinicianList(Constants.Shift.NOON));
		//mav.addObject("eveningClinicians", WatchRxUtils.clinicians(3));
		//mav.addObject("nightClinicians",  clinicianService.getClinicianList(Constants.Shift.NIGHT));
		mav.addObject("priDoctorList", doctorService.getDoctors());
		mav.addObject("specialistList", doctorService.getDoctors());
		mav.addObject("watchList", watchService.getWatchList());
		mav.addObject("patientList", patientService.getPatientList());
		mav.addObject("assignVO", assign);
		
		mav.setViewName("addAssign");
		return mav;
	}
	@RequestMapping(value = "/watchAssignForConsumer", method = RequestMethod.GET)
	public ModelAndView watchAssignForConsumer(HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView();
	UserVO user = (UserVO)request.getSession().getAttribute("user");
	AssignToPatientVO assign = new AssignToPatientVO();		
	ClinicianVO  clinician = clinicianService.getClinicianByUserName(user.getUserName());
logger.info("::::"+user.getUserId());
		mav.addObject("assignVO", assign);
		mav.addObject("watchList", watchService.getWatchListByClinician(clinician.getClinicianId()));
		mav.addObject("patientList", patientService.getPatientsByClinician(clinician.getClinicianId()).getPatientInfo());
		mav.setViewName("addConsumerAssign");
		
		
		return mav;
	}
	@RequestMapping(value = "/patientAssignment", method = RequestMethod.GET)
	public ModelAndView patientAssignment(HttpServletRequest request, long patientId) throws Exception {
		logger.info("inside assignPatient:::: "+patientId);	
		AssignToPatientVO assign = patientService.getPatientAssinment(patientId);	
		logger.info(assign.getMorningClinicianId());
		logger.info(assign.getNoonClinicianId());
		logger.info(assign.getNightClinicianId());
	//	AssignToPatientVO prescription = WatchRxUtils.getPrescription((int) prescriptionId);
		ModelAndView mav = new ModelAndView();
		List<ClinicianVO> clinicians = new ArrayList<ClinicianVO>();
		clinicians.addAll(clinicianService.getClinicianList(Constants.Shift.MORNIG));
		clinicians.addAll(clinicianService.getClinicianList(Constants.Shift.NOON));
		clinicians.addAll(clinicianService.getClinicianList(Constants.Shift.NIGHT));
		mav.addObject("allClinicians", clinicians);
		mav.addObject("priDoctorList", doctorService.getDoctors());
		mav.addObject("specialistList", doctorService.getDoctors());
		mav.addObject("watchList", watchService.getWatchList());
		mav.addObject("patientList", patientService.getPatientList());
		mav.addObject("assignVO", assign);		
		mav.setViewName("addAssign");
		return mav;
	}
	
	@RequestMapping(value = "/saveAssign", method = RequestMethod.POST)
	public ModelAndView saveAssign(HttpServletRequest request,@ModelAttribute("assignVO") @Valid AssignToPatientVO assignVO, BindingResult result) throws Exception {
		logger.info("inside saveAssign:::: "+assignVO);	
		ModelAndView mav = new ModelAndView();
		if(result.hasErrors()){
			logger.info("inside saveAssign:::: "+result.getAllErrors());	
			mav.addObject("assignVO", assignVO);
			mav.setViewName("addAssign");
			List<ClinicianVO> clinicians = new ArrayList<ClinicianVO>();
			clinicians.addAll(clinicianService.getClinicianList(Constants.Shift.MORNIG));
			clinicians.addAll(clinicianService.getClinicianList(Constants.Shift.NOON));
			clinicians.addAll(clinicianService.getClinicianList(Constants.Shift.NIGHT));
			mav.addObject("allClinicians", clinicians);
			mav.addObject("priDoctorList", doctorService.getDoctors());
			mav.addObject("specialistList", doctorService.getDoctors());
			mav.addObject("watchList", watchService.getWatchList());
			mav.addObject("patientList", patientService.getPatientList());
			return mav;
		}
		patientService.saveClinicianWatchAssign(assignVO);
		List<WatchVO> watchList = watchService.getWatchList();
		mav.addObject("watchList", watchList);
		mav.setViewName("watchSummary");
		return mav;
	}
	
	@RequestMapping(value = "/saveAssignForConsumer", method = RequestMethod.POST)
	public ModelAndView saveAssignForConsumer(HttpServletRequest request,@ModelAttribute("assignVO") @Valid AssignToPatientVO assignVO, BindingResult result) throws Exception {
		logger.info("inside saveAssign:::: "+assignVO);	
		ModelAndView mav = new ModelAndView();
		UserVO user = (UserVO)request.getSession().getAttribute("user");
		ClinicianVO  clinician = clinicianService.getClinicianByUserName(user.getUserName());

		assignVO.setMorningClinicianId(clinician.getClinicianId());

		if(result.hasErrors()){
			logger.info("inside saveAssign:::: "+result.getAllErrors());	
			mav.addObject("assignVO", assignVO);
			mav.setViewName("addConsumerAssign");
			
			mav.addObject("watchList", watchService.getWatchListByClinician(clinician.getClinicianId()));
			mav.addObject("patientList", patientService.getPatientsByClinician(clinician.getClinicianId()).getPatientInfo());
			return mav;
		}
		patientService.savePatientAssign(assignVO);
		List<WatchVO> watchList = watchService.getWatchList();
		mav.addObject("watchList", watchList);
		mav.setViewName("patientSummary");
		return mav;
	}

	@RequestMapping(value = "/deleteWatch", method = RequestMethod.POST)
	@ResponseBody
	public String deleteWatch(HttpServletRequest request, long watchId) throws Exception {
		logger.info("inside deleteWatch:::: "+watchId);	
		watchService.deleteWatch(watchId);
		StatusVO statusVO = new StatusVO();
		List<String> errorMsgs = new ArrayList<String> ();
		statusVO.setMessages(errorMsgs.toArray(new String[errorMsgs.size()]));
		return JsonUtils.toJSON(statusVO);
	}
	@RequestMapping(value = "/getPatientClinician", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView getPatientClinician(HttpServletRequest request) throws Exception {
		logger.info("inside getPatientClinician");
		List<PatientClinician> data = clinicianService.getPatientNurseAssignment();
		ModelAndView view = new ModelAndView();
		view.addObject("data", data);
		view.setViewName("unassignSummary");
		return view;
	}
	
	@RequestMapping(value = "/unassignPC", method = RequestMethod.POST)
	@ResponseBody
	public String unassignPC(HttpServletRequest request, long id) throws Exception {
		logger.info("inside unassignPC:::: "+id);	
		clinicianService.unassign(id);
		StatusVO statusVO = new StatusVO();
		List<String> errorMsgs = new ArrayList<String> ();
		statusVO.setMessages(errorMsgs.toArray(new String[errorMsgs.size()]));
		return JsonUtils.toJSON(statusVO);
	}
	@RequestMapping(value = "/getPatientWatch", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView getPatientWatch(HttpServletRequest request) throws Exception {
		logger.info("inside getPatientWatch");
		UserVO user = (UserVO)request.getSession().getAttribute("user");
		AssignToPatientVO assign = new AssignToPatientVO();		
		ClinicianVO  clinician = clinicianService.getClinicianByUserName(user.getUserName());
		
		List<PatientClinician> data = clinicianService.getPatientWatchAssignment(clinician.getUserName());
		ModelAndView view = new ModelAndView();
		view.addObject("data", data);
		view.setViewName("unassignWatchSummary");
		return view;
	}
	
	@RequestMapping(value = "/unassignPW", method = RequestMethod.POST)
	@ResponseBody
	public String unassignPW(HttpServletRequest request, long id) throws Exception {
		logger.info("inside unassignPW:::: "+id);	
		clinicianService.unassignPW(id);
		StatusVO statusVO = new StatusVO();
		List<String> errorMsgs = new ArrayList<String> ();
		statusVO.setMessages(errorMsgs.toArray(new String[errorMsgs.size()]));
		return JsonUtils.toJSON(statusVO);
	}
	@RequestMapping(value = "/APKVersion", method = RequestMethod.GET)
	public ModelAndView addAPK(HttpServletRequest request) throws Exception {
		APKVO apkVO = null;
			apkVO = new APKVO();
		ModelAndView mav = new ModelAndView();
		mav.addObject("apkVO", apkVO);
		mav.setViewName("addapkSummary");
		return mav;
	}
	@RequestMapping(value = "/saveAPKVersion", method = RequestMethod.POST)
	public ModelAndView saveAPKVersion(HttpServletRequest request, APKVO  apkVO, BindingResult result) throws Exception {
		logger.info("inside saveClinician:::: "+apkVO);
		ModelAndView mav = new ModelAndView();

		logger.info("iter::hasNext::"+apkVO.getImageFile().getBytes().length);
		logger.info("iter::hasNext::"+apkVO.getImageFile().getOriginalFilename());
		if (apkVO.getImageFile().getBytes().length>0) {
			
			GcsService gcsService =
				    GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
			AppIdentityService appIdentityService = AppIdentityServiceFactory.getAppIdentityService();
			String defaultBucketName = appIdentityService.getDefaultGcsBucketName();
			GcsFilename fileName = new GcsFilename(defaultBucketName+"/apk_Watch", apkVO.getImageFile().getOriginalFilename());
			    GcsOutputChannel outputChannel =
			    	    gcsService.createOrReplace(fileName,new  GcsFileOptions.Builder().acl("public-read").build());
			 		    
			    	outputChannel.write(ByteBuffer.wrap(apkVO.getImageFile().getBytes()));
			    	outputChannel.close();			  
			    	logger.info("Done writing...");
			    	apkVO.setApkUrl("apk_Watch/"+apkVO.getImageFile().getOriginalFilename());
		}

		watchService.updateAPKtoWatch(apkVO);
		List<APKVO> list = new ArrayList<APKVO>();
		list = watchService.getAllAPKtoWatch();
		mav.addObject("apkList", list);
		mav.setViewName("apkSummary");
		mav.addObject("status", "");
		return mav;
	}
	@RequestMapping(value = "/APKVersionSummary", method = RequestMethod.GET)
	public ModelAndView apkVersionSummary(HttpServletRequest request, APKVO  apkVO, BindingResult result) throws Exception {
		logger.info("inside saveClinician:::: "+apkVO);
		ModelAndView mav = new ModelAndView();

		List<APKVO> list = new ArrayList<APKVO>();
		list = watchService.getAllAPKtoWatch();
		mav.addObject("apkList", list);
		mav.addObject("status", "");

		mav.setViewName("apkSummary");
		return mav;
	}
	
	@RequestMapping(value = "/WatchToAPK", method = RequestMethod.GET)
	public ModelAndView assignWatchToAPK(HttpServletRequest request, WatchToAPK wapk1, BindingResult result) throws Exception {
		ModelAndView mav = new ModelAndView();
		WatchToAPK wapk = new WatchToAPK();
		List<String> apk = new ArrayList<String>();
		List<String> imei = new ArrayList<String>();

		for (APKVO apkvo : watchService.getAllAPKtoWatch()) {
			apk.add(apkvo.getApkVersion());
		}
		List<WatchVO> apkvo = watchService.getWatchListByGCM();
				logger.info(" @@@@@@**********@ Getting size"+apkvo.size());

		for (WatchVO apk1 : apkvo) {
			if(apk1!=null&&apk1.getImeiNumber()!=null){
			imei.add(apk1.getImeiNumber());
			}
		}
		wapk.setApkVO(apk);
		wapk.setWatchVO(imei);
		mav.addObject("watchAPKVO", wapk);
		
		mav.setViewName("apkWatchAssignSummary");
		return mav;
	}
	
	@RequestMapping(value = "/APKVersionPublish", method = RequestMethod.POST)
	public ModelAndView apkVersionPublish(HttpServletRequest request, WatchToAPK wapk1, BindingResult result) throws Exception {
		String storageURL = STORAGE_BASE_URL;
		ModelAndView mav = new ModelAndView();
		List<APKVO> list = new ArrayList<APKVO>();
		list = watchService.getAllAPKtoWatch();
		mav.addObject("apkList", list);
		mav.setViewName("apkSummary");
		try{
		logger.info(" wapk1 "+wapk1.getApkVO().toString());
		logger.info(" wapk1 "+wapk1.getWatchVO().toString());
List<String> gcm  = new ArrayList<String>();
for (String imei : wapk1.getWatchVO()) {
	gcm.add(watchService.getGCMbyIMEINO(imei));
}

APKVO apkvo = watchService.getAPKDetailsByVersion(wapk1.getApkVO().get(0));
CareGiverTest sdetails = new CareGiverTest();
AppIdentityService appIdentityService = AppIdentityServiceFactory
.getAppIdentityService();
String defaultBucketName = appIdentityService.getDefaultGcsBucketName();
storageURL = storageURL+defaultBucketName+"/";
for (String gc : gcm) {
	if(gc!=null && gc.length()>0){
	JsonObject gson = new JsonObject();
	gson.addProperty("messageType", "softwareUpgrade");
	String apkURL = storageURL+apkvo.getApkUrl();
	logger.info("*********URL  "+apkURL);

	gson.addProperty("url", apkURL);
	sdetails.sendMessageToPatient("message", gson.toString(), gc);
	}
	
}

mav.addObject("status", "Successfully published new software to watches.");
		return mav;}
		catch(Exception e){
			mav.addObject("status", "Failed to publish new software to watches.");

		}
		return mav;
	}
	
	  
		@RequestMapping(value = "/appSoftwareUpgradeSummary", method = RequestMethod.GET)
		public ModelAndView appSoftwareUpgradeSummary(HttpServletRequest request) throws Exception {
			logger.info("inside appSoftwareUpgradeSummary:::: ");	
			List<WatchrxWatchCGAppInstallStatus> okList = watchService.getWatchCGAppStatusList();
			List<WatchCGAppStatus> status = new ArrayList<WatchCGAppStatus>();
			for (WatchrxWatchCGAppInstallStatus watchrxWatchCGAppInstallStatus : okList) {
				WatchCGAppStatus st = new WatchCGAppStatus();
				st.setAppVersion(watchrxWatchCGAppInstallStatus.getAppVersion());
				st.setCareGiverloginId(watchrxWatchCGAppInstallStatus.getCareGiverName());
				st.setImei(watchrxWatchCGAppInstallStatus.getWatchImeiNumber());
				st.setDate(watchrxWatchCGAppInstallStatus.getCreatedDate().toString());
				st.setStatus(watchrxWatchCGAppInstallStatus.getStatus());
				st.setReason(watchrxWatchCGAppInstallStatus.getReason());
				status.add(st);
			}
			ModelAndView mav = new ModelAndView();
			mav.addObject("statsInfo", status);
			logger.info("inside appSoftwareUpgradeSummary:::: "+okList.size());	
			mav.setViewName("appSoftwareUpgradeSummary");
			return mav;
		}
		
}
