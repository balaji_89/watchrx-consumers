package com.medsure.ui.service.server;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.medsure.common.Constants;
import com.medsure.service.DoctorService;
import com.medsure.ui.entity.server.DoctorVO;
import com.medsure.ui.entity.server.StatusVO;
import com.medsure.ui.util.JsonUtils;
import com.medsure.ui.util.WatchRxUtils;
import com.medsure.util.DropdownUtils;

@Controller
@RequestMapping("/service/doctor")
public class DoctorController {
	
	private static Logger logger = Logger.getLogger(DoctorController.class);
	
	  @Autowired
	  DoctorService doctorService;
	  
		@RequestMapping(value = "/doctorSummary", method = RequestMethod.GET)
		public ModelAndView doctorSummary(HttpServletRequest request) throws Exception {
			StatusVO statusVO = new StatusVO();
			List<String> errorMsgs = new ArrayList<String> ();	
			logger.info("inside doctorSummary:::: ");	
			List<DoctorVO> doctorList = doctorService.getDoctors();
			ModelAndView mav = new ModelAndView();
			mav.addObject("doctorList", doctorList);
			logger.info("inside getDoctorSummary:::: "+doctorList.size());	
			mav.setViewName("doctorSummary");
			return mav;
		}
		
		@RequestMapping(value = "/doctor", method = RequestMethod.GET)
		public ModelAndView doctor(HttpServletRequest request, long doctorId) throws Exception {
			logger.info("inside getClinician:::: "+doctorId);	
			DoctorVO doctor = null;
			if(doctorId>0){
				 doctor = doctorService.getDoctor(doctorId);
			}else{
				doctor = new DoctorVO();
			}
			ModelAndView mav = new ModelAndView();
			mav.addObject("specialities",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.SPECIALITY));
			mav.addObject("doctorVO", doctor);
			mav.setViewName("editDoctor");
			return mav;
		}
		
		@RequestMapping(value = "/saveDoctor", method = RequestMethod.POST)
		public ModelAndView saveDoctor(HttpServletRequest request, @Valid DoctorVO doctorVO, BindingResult result) throws Exception {
			logger.info("inside saveDoctor:::: "+doctorVO);	
			ModelAndView mav = new ModelAndView();
			
			if (null!=(doctorVO.getUserName())){
				if (doctorService.isUsernameExists(doctorVO)){
					result.addError(new FieldError("doctorVO", "userName", "User Name already exists."));	
				}
			}
			
			if(result.hasErrors()){
				mav.addObject("doctorVO", doctorVO);
				mav.addObject("specialities",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.SPECIALITY));
				mav.setViewName("editDoctor");
				return mav;
			}
			logger.info("iter::hasNext::"+doctorVO.getImageFile().getBytes().length);
			logger.info("iter::hasNext::"+doctorVO.getImageFile().getOriginalFilename());
			if (doctorVO.getImageFile().getBytes().length>0) {
				GcsService gcsService =
					    GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
				AppIdentityService appIdentityService = AppIdentityServiceFactory.getAppIdentityService();
				String defaultBucketName = appIdentityService.getDefaultGcsBucketName();
				GcsFilename fileName = new GcsFilename(defaultBucketName+"/doctor", doctorVO.getImageFile().getOriginalFilename());
				    GcsOutputChannel outputChannel =
				    	    gcsService.createOrReplace(fileName, GcsFileOptions.getDefaultInstance());
				 		    
				    	outputChannel.write(ByteBuffer.wrap(doctorVO.getImageFile().getBytes()));
				    	outputChannel.close();			  
				   System.out.println("Done writing...");
				   doctorVO.setPicPath("doctor/"+doctorVO.getImageFile().getOriginalFilename());
				   doctorVO.setFileModified(true);
			}else{
				if(doctorVO.getPicPath() != null && !"".equals(doctorVO.getPicPath().trim())){
						doctorVO.setFileModified(false);
				}else{
					result.addError(new FieldError("doctorVO", "picPath", "Please select an Image."));
					logger.info("pic path:::"+doctorVO.getPicPath());
					//doctorVO.setPicPath();
					mav.addObject("doctorVO", doctorVO);
					mav.addObject("specialities",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.SPECIALITY));
					mav.setViewName("editDoctor");
					return mav;
				}
			}
			doctorService.saveDoctor(doctorVO);

			List<DoctorVO> doctorList = doctorService.getDoctors();
			mav.addObject("doctorList", doctorList);
			mav.setViewName("doctorSummary");
			return mav;
		}
		
		@RequestMapping(value = "/deleteDoctor", method = RequestMethod.POST)
		@ResponseBody
		public String deleteDoctor(HttpServletRequest request, long doctorId) throws Exception {
			logger.info("inside deleteDoctor:::: "+doctorId);	
			doctorService.deleteDoctor(doctorId);
			StatusVO statusVO = new StatusVO();
			List<String> errorMsgs = new ArrayList<String> ();
			statusVO.setMessages(errorMsgs.toArray(new String[errorMsgs.size()]));
			return JsonUtils.toJSON(statusVO);
		}
}
