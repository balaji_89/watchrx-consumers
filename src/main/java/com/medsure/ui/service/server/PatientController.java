package com.medsure.ui.service.server;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.medsure.common.Constants;
import com.medsure.factory.WatchRxFactory;
import com.medsure.service.PatientService;
import com.medsure.test.caregiver.CareGiverTest;
import com.medsure.ui.entity.caregiver.request.GetAlerts;
import com.medsure.ui.entity.caregiver.request.MedicationIds;
import com.medsure.ui.entity.caregiver.response.PatientCGInfo;
import com.medsure.ui.entity.caregiver.response.PatientGCInfos;
import com.medsure.ui.entity.patient.common.MedicationInfo;
import com.medsure.ui.entity.patient.request.MedicationAlert;
import com.medsure.ui.entity.patient.request.RegisterWatch;
import com.medsure.ui.entity.patient.response.PatientDetails;
import com.medsure.ui.entity.server.PatientPrescriptionVO;
import com.medsure.ui.entity.server.PatientVO;
import com.medsure.ui.entity.server.StatusVO;
import com.medsure.ui.entity.server.UserVO;
import com.medsure.ui.util.JsonUtils;
import com.medsure.util.DropdownUtils;

@Controller
@RequestMapping("/service/patient")
public class PatientController {
	
	private static Logger logger = Logger.getLogger(PatientController.class);
	
	@Autowired
	PatientService patientService;
	

	@RequestMapping(value = "/patientSummary", method = RequestMethod.GET)
	ModelAndView getPatientSummary(HttpServletRequest request) throws Exception {
		logger.info("inside getPatientSummary:::: ");	
		UserVO user = (UserVO)request.getSession().getAttribute("user");
		List<PatientVO> patientList = patientService.getPatientList(user);
		ModelAndView mav = new ModelAndView();
		mav.addObject("patientList", patientList);
		mav.setViewName("patientSummary");
		
		return mav;
	}
	
	@RequestMapping(value = "/patient", method = RequestMethod.GET)
	public ModelAndView getPatient(HttpServletRequest request, long patientId) throws Exception {
		logger.info("inside getClinician:::: "+patientId);	
		PatientVO patient = null;
		if(patientId>0){
		 patient = patientService.getPatient(patientId);
		}else{
			patient = new PatientVO();
		}
		ModelAndView mav = new ModelAndView();
		mav.addObject("maritalStatusList",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.MARITAL_STATUS));
		mav.addObject("languages",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.LANGUAGE));
		mav.addObject("hourList",DropdownUtils.hours());
		mav.addObject("minuteList",DropdownUtils.minutes());
		mav.addObject("patientVO", patient);
		mav.setViewName("patientEdit");
		return mav;
	}
	
	@RequestMapping(value = "/prescriptionSummary", method = RequestMethod.GET)
	public ModelAndView getPrescriptionSummary(HttpServletRequest request, long patientId) throws Exception {
		logger.info("inside prescriptionSummary:::: "+patientId);	
		List<PatientPrescriptionVO> prescriptionList = patientService.getPatientPrescriptionList(patientId);
		ModelAndView mav = new ModelAndView();
		mav.addObject("prescriptionList", prescriptionList);
		mav.addObject("patientVO", patientService.getPrescriptionPatient(patientId));
		mav.addObject("hourList",DropdownUtils.hours());
		mav.addObject("minuteList",DropdownUtils.minutes());
		mav.setViewName("patientPrescriptionSummary");
		return mav;
	}
	
	@RequestMapping(value = "/prescription", method = RequestMethod.GET)
	public ModelAndView getPrescription(HttpServletRequest request, long prescriptionId,long patientId) throws Exception {
		logger.info("inside prescriptionId:::: "+prescriptionId);
		PatientPrescriptionVO prescription = null;
		if(prescriptionId>0){
		 prescription = patientService.getPatientPrescription(prescriptionId);
		}else{
			 prescription = new PatientPrescriptionVO();
			 PatientVO patientVO = patientService.getPrescriptionPatient(patientId);
			 prescription.setPatient(patientVO);
		}
		ModelAndView mav = new ModelAndView();
		mav.addObject("prescriptionVO", prescription);
		mav.addObject("medicineFormList",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.MEDICINE_FORM));
		mav.addObject("medicineList",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.MEDICINE_LIST));
		mav.addObject("hourList",DropdownUtils.hours());
		mav.addObject("minuteList",DropdownUtils.minutes());
		//mav.addObject("patientVO", WatchRxUtils.getPatient((int) patientId));
		mav.setViewName("patientPrescriptionEdit");
		return mav;
	}
	
	@RequestMapping(value = "/savePatient", method = RequestMethod.POST)
	public ModelAndView savePatient(HttpServletRequest request, @Valid PatientVO patientVO, BindingResult result) throws Exception {
		ModelAndView mav = new ModelAndView();
		
		/*if (null!=(patientVO.getSsn())){
			if (patientService.isSSNExists(patientVO)){
				result.addError(new FieldError("patientVO", "ssn", "SSN already exists."));	
			}
		}*/
		
		if(result.hasErrors()){
			mav.addObject("patientVO", patientVO);
			mav.addObject("maritalStatusList",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.MARITAL_STATUS));
			mav.addObject("languages",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.LANGUAGE));
			mav.setViewName("patientEdit");
			return mav;
		}
		logger.info("patientVO::hasNext::"+patientVO.getImageFile().getBytes().length);
		logger.info("iter::getImageFile::"+patientVO.getImageFile().getName());
		if (patientVO.getImageFile().getBytes().length>0) {
			GcsService gcsService =
				    GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
			AppIdentityService appIdentityService = AppIdentityServiceFactory.getAppIdentityService();
			String defaultBucketName = appIdentityService.getDefaultGcsBucketName();
			GcsFilename fileName = new GcsFilename(defaultBucketName+"/patient", patientVO.getImageFile().getOriginalFilename());
			    GcsOutputChannel outputChannel =
			    	    gcsService.createOrReplace(fileName, GcsFileOptions.getDefaultInstance());
			 		    
			    	outputChannel.write(ByteBuffer.wrap(patientVO.getImageFile().getBytes()));
			    	outputChannel.close();			  
			    	logger.info("Done writing...");
			   patientVO.setPicPath("patient/"+patientVO.getImageFile().getOriginalFilename());
			   patientVO.setFileModified(true);
		}else{
			if(patientVO.getPicPath() != null && !"".equals(patientVO.getPicPath())){			
				patientVO.setFileModified(false);
			}else{
				result.addError(new FieldError("patientVO","picPath","Please select an Image."));
				mav.addObject("patientVO", patientVO);
				mav.addObject("maritalStatusList",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.MARITAL_STATUS));
				mav.addObject("languages",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.LANGUAGE));
				mav.setViewName("patientEdit");
				return mav;
			
			}
		}
		UserVO user = (UserVO)request.getSession().getAttribute("user");

		patientService.savePatient(patientVO,user);
		List<PatientVO> patientList = patientService.getPatientList(user);
		mav.addObject("patientList", patientList);
		mav.setViewName("patientSummary");
		return mav;
	}
	
	@RequestMapping(value = "/savePatentPrescription", method = RequestMethod.POST)
	public ModelAndView savePatentPrescription(HttpServletRequest request, @ModelAttribute("prescriptionVO") @Valid PatientPrescriptionVO prescriptionVO, BindingResult result) throws Exception {
		ModelAndView mav = new ModelAndView();
		if(result.hasErrors()){
			logger.info("Error starts "+result.getFieldError());
			mav.addObject("prescriptionVO", prescriptionVO);
			mav.addObject("medicineFormList",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.MEDICINE_FORM));
			mav.addObject("medicineList",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.MEDICINE_LIST));
			mav.addObject("hourList",DropdownUtils.hours());
			mav.addObject("minuteList",DropdownUtils.minutes());
			mav.setViewName("patientPrescriptionEdit");
			return mav;
		}
		logger.info("patientVO::hasNext::"+prescriptionVO.getImageFile().getBytes().length);
		logger.info("iter::getImageFile::"+prescriptionVO.getImageFile().getName());
		if (prescriptionVO.getImageFile().getBytes().length>0) {
			GcsService gcsService =
				    GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
			AppIdentityService appIdentityService = AppIdentityServiceFactory.getAppIdentityService();
			String defaultBucketName = appIdentityService.getDefaultGcsBucketName();
			GcsFilename fileName = new GcsFilename(defaultBucketName+"/medicine", prescriptionVO.getImageFile().getOriginalFilename());
			    GcsOutputChannel outputChannel =
			    	    gcsService.createOrReplace(fileName, GcsFileOptions.getDefaultInstance());
			 		    
			    	outputChannel.write(ByteBuffer.wrap(prescriptionVO.getImageFile().getBytes()));
			    	outputChannel.close();			  
			    	logger.info("Done writing...");
			    	prescriptionVO.setPicPath("medicine/"+prescriptionVO.getImageFile().getOriginalFilename());
			   prescriptionVO.setFileModified(true);
		}else{
			if(prescriptionVO.getPicPath() != null && !"".equals(prescriptionVO.getPicPath())){			
				prescriptionVO.setFileModified(false);
			}else{
				
				mav.addObject("prescriptionVO", prescriptionVO);
				mav.addObject("medicineFormList",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.MEDICINE_FORM));
				mav.addObject("medicineList",DropdownUtils.getRefDataByRefType(Constants.ReferenceType.MEDICINE_LIST));
				mav.addObject("hourList",DropdownUtils.hours());
				mav.addObject("minuteList",DropdownUtils.minutes());
				mav.setViewName("patientPrescriptionEdit");
				return mav;
			
			}
		}
		patientService.savePrescription(prescriptionVO);
		sendMedicationInfo(prescriptionVO);
		List<PatientPrescriptionVO> prescriptionList =patientService.getPatientPrescriptionList(prescriptionVO.getPatient().getPatientId());		
		mav.addObject("prescriptionList", prescriptionList);
	    mav.addObject("patientVO", prescriptionVO.getPatient());
		mav.setViewName("patientPrescriptionSummary");
		return mav;
	}
	
	
	@RequestMapping(value = "/deletePatient", method = RequestMethod.POST)
	@ResponseBody
	public String deletePatient(HttpServletRequest request, long patientId) throws Exception {
		logger.info("inside deletePatient:::: "+patientId);	
		patientService.deletePatient(patientId);
		StatusVO statusVO = new StatusVO();
		statusVO.setSuccess(true);
		List<String> errorMsgs = new ArrayList<String> ();
		statusVO.setMessages(errorMsgs.toArray(new String[errorMsgs.size()]));
		return JsonUtils.toJSON(statusVO);
	}
	
	@RequestMapping(value = "/deletePrescription", method = RequestMethod.POST)
	@ResponseBody
	public String deletePrescription(HttpServletRequest request, long prescriptionId) throws Exception {
		logger.info("inside deletePrescription:::: "+prescriptionId);	
		patientService.deletePrescription(prescriptionId);
		PatientPrescriptionVO vo = patientService.getPatientPrescription(prescriptionId);
		sendMedicationInfo(vo);
		StatusVO statusVO = new StatusVO();
		statusVO.setSuccess(true);
		List<String> errorMsgs = new ArrayList<String> ();
		statusVO.setMessages(errorMsgs.toArray(new String[errorMsgs.size()]));
		return JsonUtils.toJSON(statusVO);
	}
	
	@RequestMapping(value = "/alertSummaryDBrd", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView alertSummaryDashBoard(HttpServletRequest request) throws Exception {
		logger.info("inside alertSummarydashBoard:::: ");	
		UserVO user = (UserVO)request.getSession().getAttribute("user");
		ModelAndView mav = new ModelAndView();
		List<MedicationAlert> data = new ArrayList<MedicationAlert>();
		if(user.getRoleType()==1){
			 data = WatchRxFactory.getAlertService().getAllAlertsByPatient();
		}else{
			GetAlerts alert = new GetAlerts();
			logger.info("Alertr data user id"+user.getUserId());
			logger.info("Alertr data user id"+user.getUserName());
			Long nurseId = WatchRxFactory.getPatientService().getClinicianByUserId(new Long (user.getUserId()));
		PatientGCInfos patientGCInfos = WatchRxFactory.getPatientService().getPatientsByClinician(nurseId);
		for(PatientCGInfo patientCGInfo : patientGCInfos.getPatientInfo()){
			logger.info("Alertr data patient id"+patientCGInfo.getPatientId());
			alert.setPatientId(patientCGInfo.getPatientId());
			List<MedicationAlert> tmpdata = WatchRxFactory.getAlertService().getAlertsByPatientId(alert);
			logger.info("Alertr data patient id size "+tmpdata.size());
			//mav.addObject("patientName",patientCGInfo.getPatientName());
			data.addAll(tmpdata);
		}
	
		}
		logger.info("Alertr data"+data.toString());
		logger.info("Alertr data size"+data.size());
		mav.addObject("alertList",data);

		mav.setViewName("alertSummary");
return mav;
	}
	@RequestMapping(value = "/getMedicationInfoById", method = RequestMethod.GET)
	@ResponseBody	
	public ModelAndView medicationInfoByListOfIds(MedicationIds details) {
		logger.info("inside medicationInfoByListOfIds:::: "+details.getMedicationIds());	
		ModelAndView mav = new ModelAndView();

			List<MedicationInfo> result = WatchRxFactory.getAlertService().getMedicationByMedicationIds(details.getMedicationIds());
		/*for(MedicationInfo info : result){
			mav.addObject("beforeOrAfterFood",info.getBeforeOrAfterFood());
			mav.addObject("color",info.getColor());
			mav.addObject("daysOfWeek",info.getDaysOfWeek());
			mav.addObject("description",info.getDescription());
			mav.addObject("dosage",info.getDosage());
			mav.addObject("image",info.getImage());
			mav.addObject("medicineName",info.getMedicineName());
			mav.addObject("strength",info.getStrength());
			mav.addObject("timeSlots",info.getTimeSlots());
		}*/
			logger.info("result size :: " +result.size());
		mav.setViewName("medMissedSummary");
		mav.addObject("infoList", result);
		return mav;
	}
	
	/*@RequestMapping(value = "/alertSummaryByDate", method = RequestMethod.GET)
	public ModelAndView ruokSummaryByDate(HttpServletRequest request, String date) throws Exception {
		logger.info("inside alertSummarydashBoard:::: ");	
		UserVO user = (UserVO)request.getSession().getAttribute("user");
		ModelAndView mav = new ModelAndView();
		List<MedicationAlert> data = new ArrayList<MedicationAlert>();
		List<MedicationAlert> dataByDate = new ArrayList<MedicationAlert>();
		if(user.getRoleType()==1){
			dataByDate = WatchRxFactory.getAlertService().getAllAlertsByPatient();
			 for(MedicationAlert alert : dataByDate){
				 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

				    Date date1 = format.parse(date);
				    Date date2 = format.parse(alert.getCreatedDate());
					logger.info("date1"+date1);
					logger.info("date2"+date2);
				 if(DateUtils.isSameDay(date1, date2)){
					 data.add(alert);
				 }
			 }
		}else{
		PatientGCInfos patientGCInfos = WatchRxFactory.getPatientService().getPatientsByClinician(new Long (user.getUserId()));
		for(PatientCGInfo patientCGInfo : patientGCInfos.getPatientInfo()){
			//mav.addObject("patientName",patientCGInfo.getPatientName());
			dataByDate.addAll(WatchRxFactory.getAlertService().getAlertsByPatientId(Long.parseLong(patientCGInfo.getPatientId())));
			for(MedicationAlert alert : dataByDate){
				 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

				    Date date1 = format.parse(date);
				    Date date2 = format.parse(alert.getCreatedDate());
					logger.info("date1"+date1);
					logger.info("date2"+date2);
				 if(DateUtils.isSameDay(date1, date2)){
					 data.add(alert);
				 }
			 }
		}
	
		}
		logger.info("Alertr data"+data.toString());
		logger.info("Alertr data size"+data.size());
		mav.addObject("alertList",data);

		mav.setViewName("alertSummary");
return mav;
	}*/

private void sendMedicationInfo(PatientPrescriptionVO  prescriptionVO){
	logger.info("Sending medication info GCM::");
CareGiverTest sdetails = new CareGiverTest();
RegisterWatch watch = new RegisterWatch();
PatientVO pdvo = WatchRxFactory.getPatientService().getPatient(prescriptionVO.getPatient().getPatientId());
logger.info("Sending medication info patient id::"+prescriptionVO.getPatient().getPatientId());
if(pdvo!=null && pdvo.getWatch()!=null){
String imeiNo = pdvo.getWatch().getImeiNumber();
if(imeiNo != null){
	logger.info("Sending medication info GCM IMEI NO::"+imeiNo);
	watch.setImeiNo(imeiNo);

	PatientDetails pd = WatchRxFactory.getPatientService().getPatientDetailsByImei(watch);

String gcmId = WatchRxFactory.getPatientService().getPatientById(prescriptionVO.getPatient().getPatientId()).getGcmId();
logger.info("Sending medication info GCM ID::"+gcmId);
if(gcmId!=null && gcmId.trim().length()>0){
	JsonObject jo = new JsonObject();
	jo.addProperty("messageType", "UpdatedMedicationInfo");
	jo.addProperty("patientDetails", pd.toString());
	logger.info("Sending medication info GCM ID details::"+pd.toString());

	//jo.addProperty("visitVerificationCode", tempString);
	Gson gson = new Gson();
	String jsonStr = gson.toJson(jo);
	try {
		sdetails.sendMessageToPatient("message", jsonStr, gcmId);
		logger.info("Sending medication info GCM ID Send::");

	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}
}
}

@RequestMapping(value = "/alertSummaryByPatient", method = RequestMethod.GET)
@ResponseBody
public String alertSummaryByPatient(HttpServletRequest request, String patientId) throws Exception {
	logger.info("inside alertSummarydashBoard as graph:::: ");	
	List<MedicationAlert> data = new ArrayList<MedicationAlert>();
	Map<Date,Integer> finalData = null;
	if(patientId.equalsIgnoreCase("All")){
		 finalData = new HashMap<Date,Integer>();
		 data = WatchRxFactory.getAlertService().getAllAlertsByPatient();
		 for (Iterator iterator = data.iterator(); iterator.hasNext();) {
			MedicationAlert medicationAlert = (MedicationAlert) iterator.next();
			if(medicationAlert.getAlertType()!=null && medicationAlert.getAlertType().equalsIgnoreCase("MissedRegularReminder")){
				Date date = DateUtils.truncate( medicationAlert.getCreatedDate(), java.util.Calendar.DAY_OF_MONTH);
				if(finalData.get(date)!=null){
				finalData.put(date, finalData.get(date)+1);
				}else{
					finalData.put(date, 1);

				}
			}
		}
	}else{
		GetAlerts alert = new GetAlerts();
		alert.setPatientId(patientId);
		 finalData = new HashMap<Date,Integer>();
        data = WatchRxFactory.getAlertService().getAlertsByPatientId(alert);
		 for (Iterator iterator = data.iterator(); iterator.hasNext();) {
			MedicationAlert medicationAlert = (MedicationAlert) iterator.next();
			if(medicationAlert.getAlertType()!=null && medicationAlert.getAlertType().equalsIgnoreCase("MissedRegularReminder")){
				Date date = DateUtils.truncate( medicationAlert.getCreatedDate(), java.util.Calendar.DAY_OF_MONTH);
				if(finalData.get(date)!=null){
				finalData.put(date, finalData.get(date)+1);
				}else{
					finalData.put(date, 1);

				}
			}
		}

	}
	logger.info("Alertr data"+data.toString());
	logger.info("Alertr data size"+data.size());
	 Map<Date, Integer> m1 = new TreeMap<Date, Integer>(finalData);
	JsonArray  jArray = new JsonArray();
	for (Entry<Date, Integer> e : m1.entrySet()) {
		JsonObject jo = new JsonObject();
		jo.addProperty("date",convertDate(e.getKey().toString()));
		jo.addProperty("alertCount", e.getValue());
		jArray.add(jo);
	}

return  jArray.toString();
}

private String convertDate(String date){
	//String dateStr = "Mon Jun 18 00:00:00 IST 2012";
	DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
	Date date1 = null;
	try {
		date1 = (Date)formatter.parse(date);
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	//System.out.println(date);        

	Calendar cal = Calendar.getInstance();
	cal.setTime(date1);
	String formatedDate = cal.get(Calendar.DATE) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" +         cal.get(Calendar.YEAR);
	//System.out.println("formatedDate : " + formatedDate); 
	return formatedDate;
}

}
