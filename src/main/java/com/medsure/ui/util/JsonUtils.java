package com.medsure.ui.util;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtils {
	public static String toJSON (Object value) {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(value);
	}
	
	public static String toJSONList (ArrayList value) {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(value,ArrayList.class);
	}
	
}






