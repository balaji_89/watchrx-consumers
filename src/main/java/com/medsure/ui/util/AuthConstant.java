package com.medsure.ui.util;

import com.google.api.server.spi.config.Api;

@Api(
	    name = "outhService",
	    version = "v1",
	    scopes = {AuthConstant.EMAIL_SCOPE},
	    clientIds = {AuthConstant.WEB_CLIENT_ID, AuthConstant.ANDROID_CLIENT_ID},
	    audiences = {AuthConstant.ANDROID_AUDIENCE}
	)

	public class AuthConstant {
	  public static final String WEB_CLIENT_ID = "132597499285-kt7av2kegp2a9q96rrdd6gfg58di02dg.apps.googleusercontent.com";
	  public static final String ANDROID_CLIENT_ID = "132597499285-idmv1v8si9eufqunssntqrg9k44h27ro.apps.googleusercontent.com";
	 // public static final String IOS_CLIENT_ID = "3-ios-apps.googleusercontent.com";
	  public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;

	  public static final String EMAIL_SCOPE = "https://www.googleapis.com/auth/userinfo.email";
	}
