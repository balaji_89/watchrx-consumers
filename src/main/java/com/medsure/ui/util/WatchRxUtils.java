package com.medsure.ui.util;

import java.util.ArrayList;
import java.util.Date;

import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.medsure.ui.entity.server.AddressVO;
import com.medsure.ui.entity.server.AssignToPatientVO;
import com.medsure.ui.entity.server.ClinicianVO;
import com.medsure.ui.entity.server.DoctorVO;
import com.medsure.ui.entity.server.IDDescriptionVO;
import com.medsure.ui.entity.server.PatientPrescriptionVO;
import com.medsure.ui.entity.server.PatientVO;
import com.medsure.ui.entity.server.WatchVO;

public class WatchRxUtils {
	/*public static ArrayList<PatientVO> getPatinetList() {
		ArrayList<PatientVO> patientList = new ArrayList<PatientVO>();
		for (int i = 0; i < 5; i++) {
			PatientVO patient = new PatientVO();
			patient.setClinician(getClinician(i + 1));
			patient.setWatch(getWatch(i + 1));
			// patient.setPatientId(i+1);
			patient.setPhoneNumber("" + 41751316);
			patient.setAltPhoneNumber("" + 41751316);
			patient.setFirstName("John" + i + 1);
			patient.setLastName("Doe" + i + 1);
			patient.setPicPath("/resources/images/profile-pic-9.jpg");
			AddressVO address = new AddressVO();
			address.setAddress1("#34, green Garden Layout" + i + 1);
			address.setAddress2("Manipal County Rd" + i + 1);
			address.setCity("Jersey City");
			address.setState("New Jersey");
			address.setZip("NW3456");
			patient.setAddress(address);
			patient.setInsuranceCompany("Medi Assist" + i + 1);
			patient.setInsurancePhNumber("3456789");
			patient.setVisitReason("Root Canal" + i + 1);
			patient.setPriDoctor(getDoctor(i + 1));
			patient.setEmployment("Teacher" + i + 1);
			patient.setSsn("3578902345");
			patientList.add(patient);
		}
		return patientList;
	}

	public static ArrayList<DoctorVO> getDoctorList() {
		ArrayList<DoctorVO> docList = new ArrayList<DoctorVO>();
		for (int i = 0; i < 5; i++) {
			DoctorVO patient = new DoctorVO();
			patient.setDoctorId((long) (i + 1));
			patient.setPhoneNumber("" + 41751316);
			patient.setAltPhoneNumber("" + 988096067);
			patient.setFirstName("Joel" + i + 1);
			patient.setLastName("Klien" + i + 1);
			patient.setPicPath("/resources/images/judy-9.png");
			AddressVO address = new AddressVO();
			address.setAddress1("#34, green Garden Layout" + i + 1);
			address.setAddress2("Manipal County Rd" + i + 1);
			address.setCity("Jersey City");
			address.setState("New Jersey");
			address.setZip("NW3456" + i + 1);
			patient.setAddress(address);
			patient.setSpeciality(2);
			docList.add(patient);
		}
		return docList;
	}

	public static ArrayList<ClinicianVO> getClinicianList() {
		ArrayList<ClinicianVO> clinicianList = new ArrayList<ClinicianVO>();
		for (int i = 0; i < 5; i++) {
			ClinicianVO patient = new ClinicianVO();
			patient.setFirstName("Alen" + i + 1);
			patient.setLastName("Christ" + i + 1);
			patient.setClinicianId((long) (i + 1));
			patient.setPhoneNumber("" + 41751316);
			patient.setAltPhoneNumber("" + 988096067);
			AddressVO address = new AddressVO();
			address.setAddress1("#34, green Garden Layout" + i + 1);
			address.setAddress2("Manipal County Rd" + i + 1);
			address.setCity("Jersey City");
			address.setState("New Jersey");
			address.setZip("NW3456" + i + 1);
			patient.setAddress(address);
			patient.setSpeciality(2);
			clinicianList.add(patient);
		}
		return clinicianList;
	}

	public static ArrayList<PatientPrescriptionVO> getPrescriptionList(
			long patientId) {
		ArrayList<PatientPrescriptionVO> clinicianList = new ArrayList<PatientPrescriptionVO>();
		for (int i = 0; i < 5; i++) {
			PatientPrescriptionVO patient = new PatientPrescriptionVO();
			patient.setPatient(getPatient(patientId));
			patient.setEndDate(new Date());
			patient.setDosage("20 mg");
			patient.setEveningTime("6:00 PM");
			patient.setMorningTime("6:00 AM");
			patient.setNoonTime("1:00 PM");
			patient.setNightTime("9:00 PM");
			patient.setFrequency(2);
			patient.setMedicine("1");
			patient.setMedicineForm(1);
			patient.setPrescriptionId(new Long(1));
			patient.setQuantity(2);
			patient.setRefill(true);
			patient.setRegimen(5);
			patient.setStatus(1);
			patient.setFirstName("John" + i + 1);
			patient.setLastName("Doe" + i + 1);
			patient.setPicPath("/resources/images/profile-pic-9.jpg");
			clinicianList.add(patient);

		}
		return clinicianList;
	}

	public static PatientPrescriptionVO getPrescription(long prescriptionId) {
		return getPrescriptionList(1).get((int) prescriptionId - 1);
	}

	public static PatientVO getPatient(long patientId) {
		return getPatinetList().get((int) patientId - 1);
	}

	public static DoctorVO getDoctor(int docId) {
		return getDoctorList().get(docId - 1);
	}

	public static ClinicianVO getClinician(int clinicianId) {
		return getClinicianList().get(clinicianId - 1);
	}

	public static ArrayList<WatchVO> getWatchList() {
		ArrayList<WatchVO> watchList = new ArrayList<WatchVO>();
		for (int i = 0; i < 5; i++) {
			WatchVO watch = new WatchVO();
			watch.setWatchId((long) (i + 1));
			watch.setImeiNumber("11-345698-789044-1" + i + 1);
			// watch.setPatient(getPatient(i+1));
			watch.setStatus(new IDDescriptionVO(1, "Assigned" + i + 1));
			watchList.add(watch);
		}
		return watchList;
	}

	public static WatchVO getWatch(int watchId) {
		return getWatchList().get(watchId - 1);
	}

	public static ArrayList<IDDescriptionVO> clinicians(int shiftId) {
		ArrayList<IDDescriptionVO> clinicians = new ArrayList<IDDescriptionVO>();
		for (int i = 1; i < 6; i++) {
			clinicians.add(new IDDescriptionVO(shiftId * 10 + 1, "Allen-"
					+ shiftId + "-" + i));
		}
		return clinicians;
	}

	public static ArrayList<IDDescriptionVO> priList() {
		ArrayList<IDDescriptionVO> clinicians = new ArrayList<IDDescriptionVO>();
		for (int i = 1; i < 6; i++) {
			clinicians.add(new IDDescriptionVO(10 + 1, "Alex-" + i));
		}
		return clinicians;
	}

	public static ArrayList<IDDescriptionVO> specialistList() {
		ArrayList<IDDescriptionVO> clinicians = new ArrayList<IDDescriptionVO>();
		for (int i = 1; i < 6; i++) {
			clinicians.add(new IDDescriptionVO(10 + 1, "Joel-" + i));
		}
		return clinicians;
	}

	public static ArrayList<IDDescriptionVO> watches() {
		ArrayList<IDDescriptionVO> clinicians = new ArrayList<IDDescriptionVO>();
		for (int i = 1; i < 6; i++) {
			clinicians
					.add(new IDDescriptionVO(20 + 1, "11-345698-789044-" + i));
		}
		return clinicians;
	}

	public static AssignToPatientVO getAssign() {
		AssignToPatientVO assign = new AssignToPatientVO();

		return assign;
	}

	public static ArrayList<IDDescriptionVO> getPatientList() {
		ArrayList<IDDescriptionVO> clinicians = new ArrayList<IDDescriptionVO>();
		for (int i = 1; i < 6; i++) {
			clinicians.add(new IDDescriptionVO(10 + 1, "John-" + i));
		}
		return clinicians;
	}*/

	public static String readTextFileOnly(String image) {
		String servingUrl = "";
		try{
			AppIdentityService appIdentityService = AppIdentityServiceFactory
					.getAppIdentityService();
			String defaultBucketName = appIdentityService.getDefaultGcsBucketName();
			System.out.println("defaultBucketName ::::" + defaultBucketName);
			System.out.println("image ::::" + image);
			String fileName = image;
			GcsFilename gcsFile = new GcsFilename(defaultBucketName, fileName);
			ImagesService is = ImagesServiceFactory.getImagesService();
			String filename = String.format("/gs/%s/%s", gcsFile.getBucketName(),
					gcsFile.getObjectName());
			System.out.println("filename  ::::" + filename);

			servingUrl = is.getServingUrl(ServingUrlOptions.Builder
					.withGoogleStorageFileName(filename));
			System.out.println("servingUrl ::::" + servingUrl);

		}catch (Exception e){
			System.out.println("#####"+e.getMessage());
			e.printStackTrace();
		}
		return servingUrl;
	}

}
