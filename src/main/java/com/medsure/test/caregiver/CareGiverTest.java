package com.medsure.test.caregiver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.medsure.ui.entity.caregiver.response.AlertInfo;
import com.medsure.ui.entity.caregiver.response.GeneralInfo;
import com.medsure.ui.entity.caregiver.response.GetOnlyPatientNotes;
import com.medsure.ui.entity.caregiver.response.LoginResponse;
import com.medsure.ui.entity.caregiver.response.PatientDetailandNotes;
import com.medsure.ui.entity.caregiver.response.PatientInfo;
import com.medsure.ui.entity.caregiver.response.PatientNoteInfo;
import com.medsure.ui.entity.caregiver.response.PatientsInfo;

public class CareGiverTest {

	private static final Logger log = Logger
			.getLogger(CareGiverTest.class.getName());
	private static final String API_KEY = System.getProperty("gcm.api.key");
	/*public GetAlerts getAlerts() {
		GetAlerts alerts = new GetAlerts();
		alerts.setNurseId("nurse1");
		alerts.setResponseCode("001");
		alerts.setResponseMessage("Operationsuccessful");
		alerts.setStatus("alerts");
		alerts.setResponseType("1");
		alerts.setData(getListAlertInfo());
		return alerts;
	}*/

	public AlertInfo getAlertInfo() {
		AlertInfo info = new AlertInfo();
		info.setAlert("Misssed Medication");
		info.setAlertDescription("Missed evening medication");
		info.setAlertId("12");
		info.setPatientId("p1");
		info.setTimeStamp("12:00:54");
		return info;
	}

	public List<AlertInfo> getListAlertInfo() {
		List<AlertInfo> alertInfo = new ArrayList<AlertInfo>();
		AlertInfo info = new AlertInfo();
		info.setAlert("Misssed Medication");
		info.setAlertDescription("Missed evening medication");
		info.setAlertId("12");
		info.setPatientId("p1");
		info.setTimeStamp("12:00:54");
		alertInfo.add(info);
		return alertInfo;
	}

	public PatientInfo pInfo() {
		PatientInfo patient = new PatientInfo();
		patient.setPatientName("Judy Smith");
		patient.setCity("Pittsburgh");
		patient.setId("1");
		patient.setImagePath("images/judysmith");
		patient.setState("NJ");
		patient.setMobileNo("00145678945612");
		return patient;

	}

	public PatientsInfo getPatientLists() {
		PatientsInfo pinfo = new PatientsInfo();
		List<PatientInfo> list = new ArrayList<PatientInfo>();
		list.add(pInfo());
		pinfo.setPatientLists(list);
		pinfo.setNurseId("n1");
		pinfo.setResponseCode("001");
		pinfo.setResponseMessage("Operationsuccessful");
		pinfo.setStatus("alerts");
		pinfo.setResponseType("1");
		return pinfo;
	}

	public GetOnlyPatientNotes patientNotes() {
		GetOnlyPatientNotes notes = new GetOnlyPatientNotes();
		List<PatientNoteInfo> info = new ArrayList<PatientNoteInfo>();
		info.add(noteInfo());
		notes.setNotes(info);
		return notes;
	}

	public PatientNoteInfo noteInfo() {
		PatientNoteInfo nInfo = new PatientNoteInfo();
		nInfo.setId("n1");
		nInfo.setNote("prescribtion1");
		nInfo.setNoteDescription("...........");
		nInfo.setPatientId("p1");
		return nInfo;
	}

	public LoginResponse responseLogin() {
		LoginResponse res = new LoginResponse();
		List<PatientDetailandNotes> pnotes = new ArrayList<PatientDetailandNotes>();
		pnotes.add(pnote());
		res.setNurseId("n1");
		res.setPatient(pnotes);
		res.setResponseCode("001");
		res.setResponseMessage("Operationsuccessful");
		res.setStatus("alerts");
		res.setResponseType("1");
		return res;
	}

	public PatientDetailandNotes pnote() {
		PatientDetailandNotes note = new PatientDetailandNotes();
		List<PatientNoteInfo> patientNoteInfo = new ArrayList<PatientNoteInfo>();
		patientNoteInfo.add(noteInfo());
		note.setNotes(patientNoteInfo);
		note.setNurseId("n1");
		note.setPatientDetail(pInfo());
		return note;
	}

	public GeneralInfo generalInfo() {
		GeneralInfo info = new GeneralInfo();
		info.setNurseId("n1");
		info.setResponseCode("001");
		info.setResponseMessage("Operationsuccessful");
		info.setResponseType("1");
		info.setStatus("alerts");
		return info;
	}
	
	public void sendMessageToPatient(String key, String message, String gcmID) throws IOException {
		if (message == null || message.trim().length() == 0) {
			return;
		}
		log.info("key"+key);
		log.info("message"+message);
		log.info("gcmID"+gcmID);
		Sender sender = new Sender(API_KEY);
		Message msg = new Message.Builder().addData(key, message).build();
		Result result = sender.send(msg, gcmID, 5);
		//Result result = sender.send(msg, "", 5);
		String canonicalRegId = result.getCanonicalRegistrationId();
		log.info("canonicalRegId"+canonicalRegId);
	}
	
	public static int randInt() {

	    // Usually this can be a field rather than a method variable
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((999999 - 100000) + 1) + 100000;

	    return randomNum;
	}
}
