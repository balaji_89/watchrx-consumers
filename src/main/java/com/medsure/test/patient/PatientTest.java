package com.medsure.test.patient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.medsure.ui.entity.patient.common.MedicationInfo;
import com.medsure.ui.entity.patient.response.GeneralInfo;
import com.medsure.ui.entity.patient.response.PatientDetails;

public class PatientTest {

	private static final String API_KEY = System.getProperty("gcm.api.key");
	private static final Logger log = Logger
			.getLogger(PatientTest.class.getName());
	public PatientDetails registerWatchTest() {

		PatientDetails pd = new PatientDetails();
		pd.setMedicationDetail(medInfo());
		pd.setPatientId("p1");
		pd.setResponseCode("001");
		pd.setResponseType("1");
		pd.setResponseMessage("operationsuccessful");
		pd.setStatus("success");
		// pd.setUserId("primarkkey");
		return pd;
	}

	private List<MedicationInfo> medInfo() {
		List<MedicationInfo> list = new ArrayList<MedicationInfo>();
		MedicationInfo medInfo = new MedicationInfo();
		medInfo.setColor("red");
		medInfo.setDescription("for BP");
		medInfo.setImage("/img/amolg.png");
		medInfo.setMedicineName("amolg");
		list.add(medInfo);
		return list;

	}

	public GeneralInfo generalInfo() {
		GeneralInfo pd = new GeneralInfo();
		pd.setResponseCode("001");
		pd.setResponseType("1");
		pd.setResponseMessage("operationsuccessful");
		pd.setStatus("success");
		// pd.setUserId("primarkkey");
		return pd;
	}
	
	public void sendMessageToNurse(String key, String message, String gcmID) throws IOException {
		if (message == null || message.trim().length() == 0) {
			return;
		}
		log.info("key"+key);
		log.info("message"+message);
		log.info("gcmID"+gcmID);
		if(gcmID!=null){
		Sender sender = new Sender(API_KEY);
		Message msg = new Message.Builder().addData(key, message).build();
		Result result = sender.send(msg, gcmID, 5);
		log.info("result error "+result.getErrorCodeName());
		//Result result = sender.send(msg, "", 5);
		String canonicalRegId = result.getCanonicalRegistrationId();
		log.info("canonicalRegId"+canonicalRegId);
		}else{
			log.info("gcmID is null. Cannot send messge to caregiver."+gcmID);	
		}
	}

}
