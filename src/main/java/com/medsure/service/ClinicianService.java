/**
 * 
 */
package com.medsure.service;

import java.util.List;

import com.medsure.ui.entity.caregiver.request.Login;
import com.medsure.ui.entity.server.ClinicianVO;
import com.medsure.ui.entity.server.PatientClinician;


/**
 * @author snare
 *
 */
public interface ClinicianService {

	public void saveClinician (ClinicianVO clinicianVO);
	
	public List<ClinicianVO> getClinicianList();
	
	public List<ClinicianVO> getClinicianList(int shiftId);
	
	public ClinicianVO getClinician(Long clinicianId);
	
	public void deleteClinician(Long clinicianId);
	
	public void saveGCMRegID (String gcmId, Long careGiverId);
	
	public String getGCMRegID (Long careGiverId);
	
	public String getNurseGCMRegIdByPatientId(Long patientId);
	
	public ClinicianVO getClinicianByUserName(String userName);

	public String getClinicianId(Long patientId);
	
	public Boolean isUsernameExists(ClinicianVO clinicianVO);

	List<PatientClinician> getPatientNurseAssignment();

	public void unassign(long id);
	
	public void unassignPW(long id);

	public void resetPasswordByUserName(Login loginDetails);

	List<PatientClinician> getPatientWatchAssignment(String username);
}
