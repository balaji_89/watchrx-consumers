/**
 * 
 */
package com.medsure.service;

import com.google.common.collect.Table;

/**
 * @author snare
 *
 */
public interface ReferenceService {

	public Table<String,Integer,String> getReferenceData ();
	
}
