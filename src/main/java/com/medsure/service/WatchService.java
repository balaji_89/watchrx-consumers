/**
 * 
 */
package com.medsure.service;

import java.util.List;

import com.medsure.model.WatchrxWatchCGAppInstallStatus;
import com.medsure.ui.entity.patient.request.WatchCGAppStatus;
import com.medsure.ui.entity.server.APKVO;
import com.medsure.ui.entity.server.AssignToPatientVO;
import com.medsure.ui.entity.server.WatchVO;

/**
 * @author snare
 *
 */
public interface WatchService {
	
	public void saveWatch(WatchVO watchVO);
	
	public List<WatchVO> getWatchList();
	
	public WatchVO getWatch(Long watchId);
	
	public void deleteWatch(Long watchId);
	
	public void assignWatchToPatient(AssignToPatientVO assignToPatientVO);
	
	public Boolean isIMEIExists(WatchVO watchVO);
	
	public boolean updateAPKtoWatch(APKVO apkvo);

	public List<APKVO> getAllAPKtoWatch();
	
	public String  getGCMbyIMEINO(String IMEino);

	APKVO getAPKDetailsByVersion(String apkVersion);

	List<WatchVO> getWatchListByGCM();


	List<WatchVO> getWatchListByClinician(Long clinicianId);
	
	public void insertWatchMobileStatus(WatchCGAppStatus status);
	
	public List<WatchrxWatchCGAppInstallStatus> getWatchCGAppStatusList();

}
