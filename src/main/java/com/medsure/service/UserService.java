/**
 * 
 */
package com.medsure.service;

import com.medsure.ui.entity.server.UserVO;

/**
 * @author snare
 *
 */
public interface UserService {
	
	public UserVO getUser(String userName);

}
