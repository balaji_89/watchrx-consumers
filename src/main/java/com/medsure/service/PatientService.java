package com.medsure.service;

import java.util.List;

import com.medsure.ui.entity.caregiver.response.PatientGCInfos;
import com.medsure.ui.entity.caregiver.response.PatientInfo;
import com.medsure.ui.entity.caregiver.response.PatientsInfo;
import com.medsure.ui.entity.patient.request.RegisterWatch;
import com.medsure.ui.entity.patient.response.PatientDetails;
import com.medsure.ui.entity.server.AssignToPatientVO;
import com.medsure.ui.entity.server.PatientPrescriptionVO;
import com.medsure.ui.entity.server.PatientVO;
import com.medsure.ui.entity.server.UserVO;

/**
 * @author snare
 *
 */
public interface PatientService {

	public PatientsInfo getAllPatients ();
	
	public 	PatientGCInfos getPatientsByClinician (Long nurseId);
	
	public PatientInfo getPatientById(Long patientId);
	
	public 	PatientsInfo getPatientsByDoctor (Long doctorId);
	
	public PatientDetails getPatientDetailsByImei (RegisterWatch watch);
	
	public void savePatient (PatientVO patientVO,UserVO user);
	
	public List<PatientVO> getPatientList(UserVO user);
	
	public List<PatientVO> getPatientList();
	
	public PatientVO getPatient(Long patientId);
	
	public void deletePatient(Long patientId);

	public void savePatientAssign(AssignToPatientVO assignToPatientVO);
	
	public void savePrescription(PatientPrescriptionVO patientPrescriptionVO);
	
	public List<PatientPrescriptionVO> getPatientPrescriptionList(Long patientId);
	
	public PatientPrescriptionVO getPatientPrescription(Long patientPrescriptonId);
	
	public AssignToPatientVO getPatientAssinment(Long patientId );

	public void deletePrescription(Long prescriptionId);
	
	public void saveGCMRegID (String gcmId, String imeiNo);
	
	public String getGCMRegID (Long patientId);
	
	public void updateGCMRegID (String gcmId, long patientId);
	
	public PatientVO getPrescriptionPatient(Long patientId);
	
	public Boolean isSSNExists(PatientVO patientVO);

	public Long getClinicianByUserId(Long nurseId);
	
	public void saveClinicianWatchAssign(AssignToPatientVO assignToPatientVO);

	void savePatientWatchAssign(AssignToPatientVO assignToPatientVO);
	
}
