/**
 * 
 */
package com.medsure.service;

import com.medsure.ui.entity.patient.request.AdherenceLogs;



/**
 * @author snare
 *
 */

public interface AdhereLogService {

	public void insertAdhereLog(AdherenceLogs info);
	
	public AdherenceLogs getAdhereLogInfo(Long patientId);
}
