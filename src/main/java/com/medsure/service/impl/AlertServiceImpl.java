/**
 * 
 */
package com.medsure.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medsure.dao.AlertStatusDAO;
import com.medsure.dao.PatientAlertDAO;
import com.medsure.dao.PatientPrescriptionDAO;
import com.medsure.dao.PatientWatchAssignmentDAO;
import com.medsure.model.WatchrxAlertStatus;
import com.medsure.model.WatchrxClinician;
import com.medsure.model.WatchrxPatient;
import com.medsure.model.WatchrxPatientAlert;
import com.medsure.model.WatchrxPatientPrescription;
import com.medsure.model.WatchrxPatientWatchAssignmnt;
import com.medsure.service.AlertService;
import com.medsure.ui.entity.caregiver.request.AlertStatus;
import com.medsure.ui.entity.caregiver.request.GetAlerts;
import com.medsure.ui.entity.patient.common.MedicationInfo;
import com.medsure.ui.entity.patient.request.MedicationAlert;

/**
 * @author snare
 *
 */
@Component  (value="alertService")
public class AlertServiceImpl implements AlertService {
	
	private static final Logger logger = LoggerFactory.getLogger(AlertServiceImpl.class);
	
	@Autowired
	PatientAlertDAO patientAlertDAO;
	
	@Autowired
	PatientWatchAssignmentDAO patientWatchAssignmentDAO;
	
	@Autowired
	AlertStatusDAO alertStatusDAO;
	
	@Autowired
	PatientPrescriptionDAO patientPrescriptionDAO;
	
	@Override
	public MedicationAlert insertAlert(MedicationAlert info) {
		logger.info (" PatientId :::::: " + info.getPatientId()+":::::::");
		WatchrxPatientAlert watchrxPatientAlert = new WatchrxPatientAlert();
		watchrxPatientAlert.setAlertDescription(info.getAlertDescription());
		watchrxPatientAlert.setAlertType(info.getAlertType());
		if(info.getCreatedDate()!=null){
		watchrxPatientAlert.setCreatedDate(info.getCreatedDate());
		watchrxPatientAlert.setUpdatedOn(info.getCreatedDate());
		}else{
			watchrxPatientAlert.setCreatedDate(new Date());
			watchrxPatientAlert.setUpdatedOn(new Date());

		}
		watchrxPatientAlert.setMissedBeforeOrAfterFood(info.getMissedBeforeOrAfterFood());
		watchrxPatientAlert.setMissedMedicationIds(info.getMissedMedicationIds());
		watchrxPatientAlert.setMissedTime(info.getMissedTime());
		watchrxPatientAlert.setMissedTimeSlot(info.getMissedTimeSlot());
		WatchrxPatient watchrxPatient = new WatchrxPatient ();
		watchrxPatient.setPatientId(new Long(info.getPatientId().trim()));
		watchrxPatientAlert.setWatchrxPatient(watchrxPatient);
		WatchrxPatientAlert alert = patientAlertDAO.save(watchrxPatientAlert);
		info.setAlertId(String.valueOf(alert.getPatientAlertId()));
		return info;
	}

	@Override
	public List<MedicationAlert> getAlertsByPatientId(GetAlerts patientId) {
		// TODO Auto-generated method stub
		List<MedicationAlert> alertList = new ArrayList<MedicationAlert> ();
		List<WatchrxPatientAlert> watchrxPatientAlertList =  null;
		watchrxPatientAlertList =  patientAlertDAO.findByProperty("watchrxPatient.patientId", Long.parseLong(patientId.getPatientId()));
		
		List<WatchrxPatientWatchAssignmnt> patientWatchAssignmntList = patientWatchAssignmentDAO.findByProperty("watchrxPatient.patientId", Long.parseLong(patientId.getPatientId()));
		String imeiNumber = null;
		if (patientWatchAssignmntList != null && patientWatchAssignmntList.size() > 0){
			imeiNumber = patientWatchAssignmntList.get(0).getWatchrxWatch().getWatchImeiNumber();
		}
		if(patientId.getStartDate()!=null && patientId.getEndDate()==null){
			logger.info("*************size"+watchrxPatientAlertList.size());
			for (WatchrxPatientAlert watchrxPatientAlert:watchrxPatientAlertList){
				String s[] = watchrxPatientAlert.getUpdatedOn().toString().split(" ");
				logger.info("*************array"+s[0]+"  "+s[1]+"patientId.getStartDate() "+patientId.getStartDate());

				if(patientId.getStartDate().toString().equalsIgnoreCase(s[0])){
				MedicationAlert medicationAlert = new MedicationAlert ();
				medicationAlert.setAlertId(String.valueOf(watchrxPatientAlert.getPatientAlertId()));
				medicationAlert.setAlertDescription(watchrxPatientAlert.getAlertDescription());
				medicationAlert.setAlertType(watchrxPatientAlert.getAlertType());
				medicationAlert.setCreatedDate(watchrxPatientAlert.getCreatedDate());
				medicationAlert.setGcmRegistrationId(watchrxPatientAlert.getWatchrxPatient().getGcmRegistrationId());
				medicationAlert.setImeiNo(imeiNumber);
				medicationAlert.setMissedBeforeOrAfterFood(watchrxPatientAlert.getMissedBeforeOrAfterFood());
				medicationAlert.setMissedMedicationIds(watchrxPatientAlert.getMissedMedicationIds());
				medicationAlert.setMissedTime(watchrxPatientAlert.getMissedTime());
				medicationAlert.setMissedTimeSlot(watchrxPatientAlert.getMissedTimeSlot());
				medicationAlert.setUpdatedOn(watchrxPatientAlert.getUpdatedOn().toString());
				medicationAlert.setPatientId(""+patientId.getPatientId());
				medicationAlert.setPatientName(watchrxPatientAlert.getWatchrxPatient().getFirstName()+" "+watchrxPatientAlert.getWatchrxPatient().getLastName());
				alertList.add(medicationAlert);
				}
			}
		}else if(patientId.getStartDate()!=null && patientId.getEndDate()!=null){

			logger.info("*************size"+watchrxPatientAlertList.size());
		
			for (WatchrxPatientAlert watchrxPatientAlert:watchrxPatientAlertList){
				String s[] = watchrxPatientAlert.getUpdatedOn().toString().split(" ");
				logger.info("*************array"+s[0]+"  "+s[1]+"patientId.getStartDate() "+patientId.getStartDate());
				List<String> date = null;
				try {
					 date = getDatesBetweenDate(patientId.getStartDate(), patientId.getEndDate());
						logger.info("*************date array"+date.toString());

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(date.contains(s[0])){
					
					logger.info("*************truey");

				MedicationAlert medicationAlert = new MedicationAlert ();
				medicationAlert.setAlertId(String.valueOf(watchrxPatientAlert.getPatientAlertId()));
				medicationAlert.setAlertDescription(watchrxPatientAlert.getAlertDescription());
				medicationAlert.setAlertType(watchrxPatientAlert.getAlertType());
				medicationAlert.setCreatedDate(watchrxPatientAlert.getCreatedDate());
				medicationAlert.setGcmRegistrationId(watchrxPatientAlert.getWatchrxPatient().getGcmRegistrationId());
				medicationAlert.setImeiNo(imeiNumber);
				medicationAlert.setMissedBeforeOrAfterFood(watchrxPatientAlert.getMissedBeforeOrAfterFood());
				medicationAlert.setMissedMedicationIds(watchrxPatientAlert.getMissedMedicationIds());
				medicationAlert.setMissedTime(watchrxPatientAlert.getMissedTime());
				medicationAlert.setMissedTimeSlot(watchrxPatientAlert.getMissedTimeSlot());
				medicationAlert.setUpdatedOn(watchrxPatientAlert.getUpdatedOn().toString());
				medicationAlert.setPatientId(""+patientId.getPatientId());
				medicationAlert.setPatientName(watchrxPatientAlert.getWatchrxPatient().getFirstName()+" "+watchrxPatientAlert.getWatchrxPatient().getLastName());
				alertList.add(medicationAlert);
			}
			}
		}
			else{
				for (WatchrxPatientAlert watchrxPatientAlert:watchrxPatientAlertList){

				MedicationAlert medicationAlert = new MedicationAlert ();
				medicationAlert.setAlertId(String.valueOf(watchrxPatientAlert.getPatientAlertId()));
				medicationAlert.setAlertDescription(watchrxPatientAlert.getAlertDescription());
				medicationAlert.setAlertType(watchrxPatientAlert.getAlertType());
				medicationAlert.setCreatedDate(watchrxPatientAlert.getCreatedDate());
				medicationAlert.setGcmRegistrationId(watchrxPatientAlert.getWatchrxPatient().getGcmRegistrationId());
				medicationAlert.setImeiNo(imeiNumber);
				medicationAlert.setMissedBeforeOrAfterFood(watchrxPatientAlert.getMissedBeforeOrAfterFood());
				medicationAlert.setMissedMedicationIds(watchrxPatientAlert.getMissedMedicationIds());
				medicationAlert.setMissedTime(watchrxPatientAlert.getMissedTime());
				medicationAlert.setMissedTimeSlot(watchrxPatientAlert.getMissedTimeSlot());
				medicationAlert.setUpdatedOn(watchrxPatientAlert.getUpdatedOn().toString());
				medicationAlert.setPatientId(""+patientId.getPatientId());
				medicationAlert.setPatientName(watchrxPatientAlert.getWatchrxPatient().getFirstName()+" "+watchrxPatientAlert.getWatchrxPatient().getLastName());
				alertList.add(medicationAlert);
			
			}
			}
		return alertList;
	}
	
	@Override
	public MedicationAlert getAlertByAlertId(long alertId) {

		MedicationAlert medicationAlert = new MedicationAlert();
		WatchrxPatientAlert watchrxPatientAlert = patientAlertDAO
				.getById(alertId);
		List<WatchrxPatientWatchAssignmnt> patientWatchAssignmntList = patientWatchAssignmentDAO
				.findByProperty("watchrxPatient.patientId", watchrxPatientAlert
						.getWatchrxPatient().getPatientId());
		String imeiNumber = null;
		if (patientWatchAssignmntList != null
				&& patientWatchAssignmntList.size() > 0) {
			medicationAlert.setImeiNo(patientWatchAssignmntList.get(0).getWatchrxWatch()
					.getWatchImeiNumber());
		}

		
		medicationAlert.setAlertDescription(watchrxPatientAlert
				.getAlertDescription());
		medicationAlert.setAlertType(watchrxPatientAlert.getAlertType());
		medicationAlert.setCreatedDate(watchrxPatientAlert.getCreatedDate()
				);
		medicationAlert.setGcmRegistrationId(watchrxPatientAlert
				.getWatchrxPatient().getGcmRegistrationId());
		medicationAlert.setImeiNo(imeiNumber);
		medicationAlert.setMissedBeforeOrAfterFood(watchrxPatientAlert
				.getMissedBeforeOrAfterFood());
		medicationAlert.setMissedMedicationIds(watchrxPatientAlert
				.getMissedMedicationIds());
		medicationAlert.setMissedTime(watchrxPatientAlert.getMissedTime());
		medicationAlert.setMissedTimeSlot(watchrxPatientAlert
				.getMissedTimeSlot());
		medicationAlert.setUpdatedOn(watchrxPatientAlert.getUpdatedOn()
				.toString());
		medicationAlert.setPatientId(""+watchrxPatientAlert
				.getWatchrxPatient().getPatientId());

		return medicationAlert;
	}

	@Override
	public List<MedicationInfo> getMedicationByMedicationIds(
			List<Long> medicationIds) {
		List<MedicationInfo> presList = new ArrayList<MedicationInfo> ();
		for (Long prescrptionId:medicationIds){
			logger.info("prescrptionId"+prescrptionId);
			WatchrxPatientPrescription prescription = patientPrescriptionDAO.getById(prescrptionId);
			if(prescription!=null){
				logger.info("prescrptionId Not nullll ::"+prescription.getPatientPrescriptionId());
				MedicationInfo medicationInfo = new MedicationInfo ();
			medicationInfo.setBeforeOrAfterFood(prescription.getBeforeOrAfterFood());
			medicationInfo.setColor(prescription.getColor());
			medicationInfo.setDaysOfWeek(prescription.getDayOfWeek());
			medicationInfo.setDescription(prescription.getDescription());
			medicationInfo.setStrength(prescription.getDosage());
			medicationInfo.setImage(prescription.getImgPath());
			medicationInfo.setMedicineName(prescription.getMedicineName());
			medicationInfo.setDosage(""+prescription.getQuantity());
			medicationInfo.setTimeSlots(prescription.getTimeSlots());
			presList.add(medicationInfo);
			}
		}
		return presList;
	}

	@Override
	public void insertAlertStatus(AlertStatus info) {
		
		WatchrxAlertStatus watchrxAlertStatus = new WatchrxAlertStatus ();
		watchrxAlertStatus.setStatus(info.getStatus());
		WatchrxClinician watchrxClinician = new WatchrxClinician ();
		watchrxClinician.setClinicianId( Long.parseLong(info.getCareGiverId()));
		WatchrxPatientAlert watchrxPatientAlert = new WatchrxPatientAlert();
		watchrxPatientAlert.setPatientAlertId(Long.parseLong(info.getAlertId()));
		watchrxPatientAlert.setCreatedDate(info.getDate());
		watchrxPatientAlert.setUpdatedOn(info.getDate());
		watchrxAlertStatus.setWatchrxClinician(watchrxClinician);
		watchrxAlertStatus.setWatchrxPatientAlert(watchrxPatientAlert);
		alertStatusDAO.save(watchrxAlertStatus);
	}

	@Override
	public void updateAlertStatus(Long alertStatusId, AlertStatus info) {
		WatchrxAlertStatus watchrxAlertStatus = new WatchrxAlertStatus ();
		watchrxAlertStatus.setAlertStatusId(alertStatusId);
		watchrxAlertStatus.setStatus(info.getStatus());
		WatchrxClinician watchrxClinician = new WatchrxClinician ();
		watchrxClinician.setClinicianId(new Long(info.getCareGiverId()));
		WatchrxPatientAlert watchrxPatientAlert = new WatchrxPatientAlert();
		watchrxPatientAlert.setPatientAlertId(new Long(info.getAlertId()));
		watchrxAlertStatus.setWatchrxClinician(watchrxClinician);
		watchrxAlertStatus.setWatchrxPatientAlert(watchrxPatientAlert);
		alertStatusDAO.save(watchrxAlertStatus);
		
	}

	@Override
	public List<AlertStatus> getAlertStatusInfo(Long careregiverId) {
		List<AlertStatus> alertStatusList = new ArrayList<AlertStatus>();
		List<WatchrxAlertStatus> list = alertStatusDAO.findByProperty("watchrxClinician.clinicianId", careregiverId);
		for (WatchrxAlertStatus watchrxAlertStatus:list){
			AlertStatus alertStatus = new AlertStatus ();
			alertStatus.setCareGiverId(""+careregiverId);
			alertStatus.setAlertId(""+watchrxAlertStatus.getWatchrxPatientAlert().getPatientAlertId());
			alertStatus.setPatinetId(""+watchrxAlertStatus.getWatchrxPatientAlert().getWatchrxPatient().getPatientId());
			alertStatus.setStatus(watchrxAlertStatus.getStatus());
			alertStatusList.add(alertStatus);
		}
		return alertStatusList;
	}
	@Override
	public List<AlertStatus> getAllAlertStatusInfo() {
		List<AlertStatus> alertStatusList = new ArrayList<AlertStatus>();
		List<WatchrxAlertStatus> list = alertStatusDAO.getAll();
		for (WatchrxAlertStatus watchrxAlertStatus:list){
			AlertStatus alertStatus = new AlertStatus ();
			alertStatus.setCareGiverId(""+watchrxAlertStatus.getWatchrxClinician().getClinicianId());
			alertStatus.setAlertId(""+watchrxAlertStatus.getWatchrxPatientAlert().getPatientAlertId());
			alertStatus.setPatinetId(""+watchrxAlertStatus.getWatchrxPatientAlert().getWatchrxPatient().getPatientId());
			alertStatus.setStatus(watchrxAlertStatus.getStatus());
			alertStatusList.add(alertStatus);
		}
		return alertStatusList;
	}
	
	@Override
	public List<MedicationAlert> getAllAlertsByPatient() {
		// TODO Auto-generated method stub
		List<MedicationAlert> alertList = new ArrayList<MedicationAlert> ();
		List<WatchrxPatientAlert> watchrxPatientAlertList =  patientAlertDAO.getAll();
		
		for (WatchrxPatientAlert watchrxPatientAlert:watchrxPatientAlertList){
			MedicationAlert medicationAlert = new MedicationAlert ();
			medicationAlert.setAlertId(String.valueOf(watchrxPatientAlert.getPatientAlertId()));
			medicationAlert.setAlertDescription(watchrxPatientAlert.getAlertDescription());
			medicationAlert.setAlertType(watchrxPatientAlert.getAlertType());
			medicationAlert.setCreatedDate(watchrxPatientAlert.getCreatedDate());
			medicationAlert.setGcmRegistrationId(watchrxPatientAlert.getWatchrxPatient().getGcmRegistrationId());
			//medicationAlert.setImeiNo(watchrxPatientAlert.getWatchrxPatient().getI);
			medicationAlert.setMissedBeforeOrAfterFood(watchrxPatientAlert.getMissedBeforeOrAfterFood());
			medicationAlert.setMissedMedicationIds(watchrxPatientAlert.getMissedMedicationIds());
			medicationAlert.setMissedTime(watchrxPatientAlert.getMissedTime());
			medicationAlert.setMissedTimeSlot(watchrxPatientAlert.getMissedTimeSlot());
			medicationAlert.setUpdatedOn(watchrxPatientAlert.getUpdatedOn().toString());
			medicationAlert.setPatientId(watchrxPatientAlert.getWatchrxPatient().getPatientId().toString());
			medicationAlert.setPatientName(watchrxPatientAlert.getWatchrxPatient().getFirstName()+" "+watchrxPatientAlert.getWatchrxPatient().getLastName());
			alertList.add(medicationAlert);
		}
		return alertList;
	}
	
	private List<String> getDatesBetweenDate(String str_date, String  end_date) throws ParseException{
List<Date> dates = new ArrayList<Date>();
List<String> dateStr = new ArrayList<String>();

DateFormat formatter ; 

formatter = new SimpleDateFormat("yyyy-MM-dd");
Date  startDate = (Date)formatter.parse(str_date); 
Date  endDate = (Date)formatter.parse(end_date);
long interval = 24*1000 * 60 * 60; // 1 hour in millis
long endTime =endDate.getTime() ; // create your endtime here, possibly using Calendar or Date
long curTime = startDate.getTime();
while (curTime <= endTime) {
    dates.add(new Date(curTime));
    curTime += interval;
}
for(int i=0;i<dates.size();i++){
    Date lDate =(Date)dates.get(i);
    String ds = formatter.format(lDate); 
    dateStr.add(ds);
}
return dateStr;
	}

	public List<AlertStatus> getAlertStatusInfoByStatusByCG(AlertStatus info) {
		List<AlertStatus> alertStatusList = new ArrayList<AlertStatus>();
		List<WatchrxAlertStatus> list = alertStatusDAO.findByProperty("watchrxClinician.clinicianId", Long.parseLong(info.getCareGiverId()));
		for (WatchrxAlertStatus watchrxAlertStatus:list){
			if(watchrxAlertStatus.getStatus().equalsIgnoreCase(info.getStatus())){
			AlertStatus alertStatus = new AlertStatus ();
			alertStatus.setCareGiverId(info.getCareGiverId());
			alertStatus.setAlertId(""+watchrxAlertStatus.getWatchrxPatientAlert().getPatientAlertId());
			alertStatus.setPatinetId(""+watchrxAlertStatus.getWatchrxPatientAlert().getWatchrxPatient().getPatientId());
			alertStatus.setStatus(watchrxAlertStatus.getStatus());
			alertStatusList.add(alertStatus);
			}
		}
		return alertStatusList;
	}
}
