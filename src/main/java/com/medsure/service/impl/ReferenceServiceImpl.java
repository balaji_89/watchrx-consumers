
/**
 * 
 */
package com.medsure.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.medsure.dao.ReferenceDAO;
import com.medsure.model.WatchrxReference;
import com.medsure.service.ReferenceService;

/**
 * @author snare
 *
 */
@Component  (value="referenceService")
public class ReferenceServiceImpl implements ReferenceService {
	
	@Autowired
	ReferenceDAO referenceDAO;
	
	@Override
	public Table<String,Integer,String> getReferenceData (){
		Table<String,Integer,String> referenceTable = HashBasedTable.create();
		List<WatchrxReference> list = referenceDAO.getAll();
		for (WatchrxReference watchrxReference:list){
			referenceTable.put(watchrxReference.getId().getReferenceType(), watchrxReference.getId().getReferenceId(), watchrxReference.getId().getReferenceName());
		}
		return referenceTable;
	}
}


