/**
 * 
 */
package com.medsure.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;




import com.medsure.dao.UserDAO;
import com.medsure.model.WatchrxUser;
import com.medsure.service.UserService;
import com.medsure.ui.entity.server.UserVO;

/**
 * @author snare
 *
 */
@Component  (value="userService")
public class UserServiceImpl implements UserService {

	@Autowired
	UserDAO userDAO;
	
	@Override
	public UserVO getUser(String userName) {
	
		List<WatchrxUser> userList = userDAO.findByProperty("userName", userName);
		WatchrxUser watchrxUser=null;
		UserVO userVO = null;
		if(userList != null && userList.size()>0){
			watchrxUser = userList.get(0);
			userVO = new UserVO();
			userVO.setUserName(watchrxUser.getUserName());
			userVO.setUserId(watchrxUser.getUserId());
			userVO.setPassword(watchrxUser.getPassword());
			userVO.setPicPath(watchrxUser.getImgPath());
			userVO.setRoleType(watchrxUser.getUserType());
			userVO.setFirstName(watchrxUser.getFirstName());
			userVO.setLastName(watchrxUser.getLastName());
		}
		return userVO;
	}
	
	
	
}
