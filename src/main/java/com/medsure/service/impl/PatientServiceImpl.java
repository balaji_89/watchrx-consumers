package com.medsure.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medsure.common.Constants;
import com.medsure.dao.AddressDAO;
import com.medsure.dao.AlertStatusDAO;
import com.medsure.dao.ClinicianDAO;
import com.medsure.dao.DoctorDAO;
import com.medsure.dao.MedicationAdhereLogDAO;
import com.medsure.dao.PatientAlertDAO;
import com.medsure.dao.PatientClinicianAssignmentDAO;
import com.medsure.dao.PatientDAO;
import com.medsure.dao.PatientDoctorAssignmentDAO;
import com.medsure.dao.PatientEmergencyContactDAO;
import com.medsure.dao.PatientPrescriptionDAO;
import com.medsure.dao.PatientWatchAssignmentDAO;
import com.medsure.dao.WatchDAO;
import com.medsure.dao.impl.ClinicianWatchAssignmentDAOImpl;
import com.medsure.exception.WatchRxException;
import com.medsure.exception.WatchRxExceptionCodes;
import com.medsure.factory.WatchRxFactory;
import com.medsure.model.WatchrxAddress;
import com.medsure.model.WatchrxClinician;
import com.medsure.model.WatchrxClinicianWatchAssignmnt;
import com.medsure.model.WatchrxDoctor;
import com.medsure.model.WatchrxPatient;
import com.medsure.model.WatchrxPatientClinicianAssignmnt;
import com.medsure.model.WatchrxPatientDoctorAssignmnt;
import com.medsure.model.WatchrxPatientPrescription;
import com.medsure.model.WatchrxPatientWatchAssignmnt;
import com.medsure.model.WatchrxWatch;
import com.medsure.service.PatientService;
import com.medsure.service.WatchService;
import com.medsure.ui.entity.caregiver.response.PatientCGInfo;
import com.medsure.ui.entity.caregiver.response.PatientGCInfos;
import com.medsure.ui.entity.caregiver.response.PatientInfo;
import com.medsure.ui.entity.caregiver.response.PatientsInfo;
import com.medsure.ui.entity.patient.common.MedicationInfo;
import com.medsure.ui.entity.patient.request.RegisterWatch;
import com.medsure.ui.entity.patient.response.PatientDetails;
import com.medsure.ui.entity.server.AddressVO;
import com.medsure.ui.entity.server.AssignToPatientVO;
import com.medsure.ui.entity.server.DoctorVO;
import com.medsure.ui.entity.server.PatientPrescriptionVO;
import com.medsure.ui.entity.server.PatientVO;
import com.medsure.ui.entity.server.UserVO;
import com.medsure.ui.entity.server.WatchVO;
import com.medsure.ui.util.WatchRxUtils;
import com.medsure.util.DropdownUtils;

/**
 * @author snare
 *
 */
@Component (value="patientService")
public class PatientServiceImpl implements PatientService {

	private static final Logger logger = LoggerFactory.getLogger(PatientServiceImpl.class);
	
	@Autowired
	PatientDAO patientDAO;
	
	@Autowired
	AddressDAO addressDAO;
	
	@Autowired
	PatientClinicianAssignmentDAO patientClinicianAssignmentDAO;
	
	@Autowired
	PatientDoctorAssignmentDAO patientDoctorAssignmentDAO;
	
	@Autowired
	PatientWatchAssignmentDAO patientWatchAssignmentDAO;
	
	@Autowired
	PatientPrescriptionDAO patientPrescriptionDAO;
	
	@Autowired
	PatientAlertDAO patientAlertDAO;	
	
	@Autowired
	MedicationAdhereLogDAO medicationAdhereLogDAO;
	
	@Autowired
	AlertStatusDAO alertStatusDAO;
	
	@Autowired
	DoctorDAO doctorDAO;
	
	@Autowired
	ClinicianDAO clinicianDAO;
	
	@Autowired
	PatientEmergencyContactDAO patientEmergencyContactDAO;
	
	@Autowired
	ClinicianWatchAssignmentDAOImpl clinicianWatchAssignmentDAO;
	
	@Autowired
	WatchDAO watchDAO;
	
	@Override
	public PatientsInfo getAllPatients() {
		PatientsInfo pinfo = new PatientsInfo();
		pinfo.setResponseCode("001");
		pinfo.setResponseMessage("Operationsuccessful");
		pinfo.setStatus("alerts");
		pinfo.setResponseType("1");
		List<PatientInfo> patientInfoList = new ArrayList<PatientInfo>();
		List<WatchrxPatient> wrxPatientList = patientDAO.getAll();
		for (WatchrxPatient watchrxPatient:wrxPatientList){
			patientInfoList.add(convertToPatientInfo (watchrxPatient));
		}
		pinfo.setPatientLists(patientInfoList);
		return pinfo;
	}
	
	@Override
	public 	PatientGCInfos getPatientsByClinician (Long nurseId){
		PatientGCInfos pinfo = new PatientGCInfos();
		pinfo.setNurseId(""+nurseId);
		pinfo.setResponseCode("001");
		pinfo.setResponseMessage("Operationsuccessful");
		pinfo.setStatus("success");
		pinfo.setResponseType("1");
		List<PatientCGInfo> patientInfoList = new ArrayList<PatientCGInfo>();
		List<WatchrxPatientClinicianAssignmnt> patientClinicianList = patientClinicianAssignmentDAO.findByProperty("watchrxClinician.clinicianId", nurseId);
		for (WatchrxPatientClinicianAssignmnt watchrxPatientClinicianAssignmnt : patientClinicianList) {
			WatchrxPatient  watchrxPatient =  watchrxPatientClinicianAssignmnt.getWatchrxPatient();
			WatchrxClinician  watchrxClinician =  watchrxPatientClinicianAssignmnt.getWatchrxClinician();
			patientInfoList.add(convertToPatientCGInfo (watchrxPatient));
		}
		pinfo.setPatientInfo(patientInfoList);
		return pinfo;
	}
	@Override
	public 	Long getClinicianByUserId (Long nurseId){
		List<WatchrxClinician> clinicianList = clinicianDAO.findByProperty("watchrxUser.userId", nurseId);
		return clinicianList.get(0).getClinicianId();
	}
	
	@Override
	public 	PatientsInfo getPatientsByDoctor (Long doctorId){
		PatientsInfo pinfo = new PatientsInfo();
		pinfo.setNurseId(""+doctorId);
		pinfo.setResponseCode("001");
		pinfo.setResponseMessage("Operationsuccessful");
		pinfo.setStatus("alerts");
		pinfo.setResponseType("1");
		List<PatientInfo> patientInfoList = new ArrayList<PatientInfo>();
		List<WatchrxPatientDoctorAssignmnt> patientDoctorList = patientDoctorAssignmentDAO.findByProperty("watchrxDoctor.doctorId", doctorId);
		for (WatchrxPatientDoctorAssignmnt watchrxPatientDoctorAssignmnt : patientDoctorList) {
			WatchrxPatient  watchrxPatient =  watchrxPatientDoctorAssignmnt.getWatchrxPatient();
			patientInfoList.add(convertToPatientInfo (watchrxPatient));
		}
		pinfo.setPatientLists(patientInfoList);
		return pinfo;
	}
	
	@Override
	public PatientDetails getPatientDetailsByImei (RegisterWatch watch){	
		logger.info("ImeiNo ::::::: "+ watch.getImeiNo());
	PatientDetails pd = new PatientDetails();
	List<WatchrxPatientWatchAssignmnt> patientWatchAssignmentList;
	List<WatchrxWatch> watchRxList;
	WatchRxException exception;
	pd.setStatus("success");
	
	if(watch.getImeiNo()!=null) {
		watchRxList = watchDAO.findByProperty("watchImeiNumber", watch.getImeiNo());
		patientWatchAssignmentList = patientWatchAssignmentDAO.findByProperty("watchrxWatch.watchImeiNumber", watch.getImeiNo());
		if((watchRxList.size() > 0) &&(patientWatchAssignmentList != null && patientWatchAssignmentList.size() > 0)){
			logger.info("inside if ::::::: ");
			WatchrxPatient patient = patientWatchAssignmentList.get(0).getWatchrxPatient();
			if (patient.getStatus() != null && patient.getStatus().equals(Constants.Status.ACTIVE)){
				pd.setAddress(patient.getWatchrxAddress().getAddress1());
				pd.setImage(WatchRxUtils.readTextFileOnly(patient.getImgPath()));
				pd.setImeiNo(watch.getImeiNo());
				pd.setInsuranceCompany(patient.getInsuranceCompany());
				pd.setInsuranceId(patient.getInsuranceNumber());
				pd.setPatientId(""+patient.getPatientId());
				pd.setPhone(patient.getPhoneNumber());
				pd.setVisitReason(patient.getVisitReason());
				pd.setChosenTimeForTimeSlots(patient.getTimeSlots());
				pd.setSosMobileNo(patient.getMobileNoSoS());
				List<WatchrxPatientPrescription> prescriptionList = patientPrescriptionDAO.findByProperty("watchrxPatient.patientId", patient.getPatientId());
				if (prescriptionList != null){
					List<MedicationInfo> list = new ArrayList<MedicationInfo>();
					for (WatchrxPatientPrescription prescription : prescriptionList) {
						MedicationInfo mi = new MedicationInfo();
						mi.setMedicineId(""+prescription.getPatientPrescriptionId());
						mi.setDaysOfWeek(prescription.getDayOfWeek());
						mi.setTimeSlots(prescription.getTimeSlots());
						mi.setBeforeOrAfterFood(prescription.getBeforeOrAfterFood());
						mi.setColor(prescription.getColor());
						mi.setDescription(prescription.getDescription());
						mi.setStrength(prescription.getDosage());
						mi.setImage(WatchRxUtils.readTextFileOnly(prescription.getImgPath()));
						mi.setMedicineName(prescription.getMedicineName());
						mi.setDosage(""+prescription.getQuantity());
						Map<Integer, String> medFormData = DropdownUtils.getRefDataByRefType(Constants.ReferenceType.MEDICINE_FORM);
						mi.setMedicineForm(medFormData.get(prescription.getMedicineForm()));
						list.add(mi);
					}
				pd.setMedicationDetail(list);
				}
				else{
					exception = new WatchRxException(WatchRxExceptionCodes.MEDICATION_SCHEDULE_UNAVAILABLE);
					//pd.setStatus(exception.getErrCode() +" "+exception.getErrDesc());
					logger.info("Exception thrown :::::::"+ exception.getErrCode() +" "+exception.getErrDesc());
				}
			}
		
			pd.setResponseCode("001");
			pd.setResponseType("1");
			pd.setResponseMessage("Operationsuccessful");
			
			
		}
		else if(watchRxList.size() == 0 || watchRxList.isEmpty()){
			exception = new WatchRxException(WatchRxExceptionCodes.IMEI_UNAVAILABLE);
			pd.setResponseCode(exception.getErrCode());
			pd.setResponseMessage(exception.getErrDesc());
			pd.setStatus("Error");
			logger.info("Exception thrown :::::::"+ exception.getErrCode() +" "+exception.getErrDesc());
		}
		else if(patientWatchAssignmentList.size()== 0 || patientWatchAssignmentList.isEmpty()){
			exception = new WatchRxException(WatchRxExceptionCodes.IMEI_NOTASSIGNED);
			pd.setResponseCode(exception.getErrCode());
			pd.setResponseMessage(exception.getErrDesc());
			pd.setStatus("Error");
			logger.info("Exception thrown :::::::"+ exception.getErrCode() +" "+exception.getErrDesc());
			
		}
	}
	else{
		logger.info("Validating Imei is null");
		exception = new WatchRxException(WatchRxExceptionCodes.IMEI_INVALID);
		pd.setResponseCode(exception.getErrCode());
		pd.setResponseMessage(exception.getErrDesc());
		pd.setStatus("Error");
		logger.info("Exception thrown :::::::"+ exception.getErrCode() +" "+exception.getErrDesc());
	}
		
	
	return pd;
}
	
	@Override
	public PatientInfo getPatientById(Long patientId) {
		return convertToPatientInfo (patientDAO.getById(patientId));
	}
	
	private PatientInfo convertToPatientInfo (WatchrxPatient watchrxPatient){
		if (watchrxPatient == null){
			return null;
		}
		PatientInfo patientInfo = new PatientInfo ();
		if (watchrxPatient.getLastName() != null){
			patientInfo.setPatientName(watchrxPatient.getFirstName() + " " + watchrxPatient.getLastName());
		}else{
			patientInfo.setPatientName(watchrxPatient.getFirstName());
		}
		patientInfo.setId(""+watchrxPatient.getPatientId());
		patientInfo.setImagePath(watchrxPatient.getImgPath());
		patientInfo.setAddress(watchrxPatient.getWatchrxAddress().getAddress1());
		patientInfo.setCity(watchrxPatient.getWatchrxAddress().getCity());
		patientInfo.setState(watchrxPatient.getWatchrxAddress().getState());
		patientInfo.setMobileNo(watchrxPatient.getPhoneNumber());
		patientInfo.setGcmId(watchrxPatient.getGcmRegistrationId());
		return patientInfo;
	}
	
	private PatientCGInfo convertToPatientCGInfo (WatchrxPatient watchrxPatient){
		if (watchrxPatient == null){
			return null;
		}
		PatientCGInfo patientInfo = new PatientCGInfo ();
		if (watchrxPatient.getLastName() != null){
			patientInfo.setPatientName(watchrxPatient.getFirstName() + " " + watchrxPatient.getLastName());
		}else{
			patientInfo.setPatientName(watchrxPatient.getFirstName());
		}
		patientInfo.setAddress(watchrxPatient.getWatchrxAddress().getAddress1());
		patientInfo.setImage(WatchRxUtils.readTextFileOnly(watchrxPatient.getImgPath()));
		List<WatchrxPatientWatchAssignmnt> patientWatchAssignmntList = patientWatchAssignmentDAO.findByProperty("watchrxPatient.patientId", watchrxPatient.getPatientId());
		if (patientWatchAssignmntList != null && patientWatchAssignmntList.size() > 0){
			patientInfo.setImeiNo(patientWatchAssignmntList.get(0).getWatchrxWatch().getWatchImeiNumber());
		}
		patientInfo.setInsuranceCompany(watchrxPatient.getInsuranceCompany());
		patientInfo.setInsuranceId(watchrxPatient.getInsuranceNumber());
		patientInfo.setPatientId(""+watchrxPatient.getPatientId());
		patientInfo.setPhone(watchrxPatient.getPhoneNumber());
		patientInfo.setVisitReason(watchrxPatient.getVisitReason());
		
		return patientInfo;
	}
	
	
	
	@Override
	public void savePatient (PatientVO patientVO, UserVO user) {

		WatchrxPatient watchrxPatient = null;
		WatchrxAddress watchrxAddress = null;
		if (patientVO.getPatientId() != null){
			watchrxPatient = patientDAO.getById(patientVO.getPatientId());
			watchrxAddress = watchrxPatient.getWatchrxAddress();
		}else{
			watchrxPatient = new WatchrxPatient();
			watchrxAddress = new WatchrxAddress ();
		}
		
		watchrxAddress.setAddress1(patientVO.getAddress().getAddress1());
		watchrxAddress.setAddress2(patientVO.getAddress().getAddress2());
		watchrxAddress.setState(patientVO.getAddress().getState());
		watchrxAddress.setCity(patientVO.getAddress().getCity());
		watchrxAddress.setZip(patientVO.getAddress().getZip());
		watchrxAddress = addressDAO.save(watchrxAddress);
		if(patientVO.isFileModified()){
			watchrxPatient.setImgPath(patientVO.getPicPath());
			}
		watchrxPatient.setTimeSlots(patientVO.getTimeSlots());
		watchrxPatient.setDob(patientVO.getDob());
		watchrxPatient.setWatchrxAddress(watchrxAddress);
		watchrxPatient.setFirstName(patientVO.getFirstName());
		watchrxPatient.setAltPhoneNum(patientVO.getAltPhoneNumber());
		watchrxPatient.setEmployment(patientVO.getEmployment());
		watchrxPatient.setLastName(patientVO.getLastName());
		watchrxPatient.setPhoneNumber(patientVO.getPhoneNumber());
		watchrxPatient.setInsuranceCompany(patientVO.getInsuranceCompany());
		watchrxPatient.setInsuranceNumber(patientVO.getInsurancePhNumber());
		watchrxPatient.setLanguageId(patientVO.getLanguage());
		watchrxPatient.setStatus(Constants.Status.ACTIVE);
		watchrxPatient.setCreatedDate(new Date());
		watchrxPatient.setUpdatedDate(new Date());
		watchrxPatient.setIsEverBeenHere(patientVO.isRevisit()==true?"Y":"N");
		watchrxPatient.setMaritalStatusId(patientVO.getMaritalStatus());
		watchrxPatient.setSsn(patientVO.getSsn());
		watchrxPatient.setVisitReason(patientVO.getVisitReason());
		watchrxPatient.setTimeSlots(patientVO.getTimeSlots());
		watchrxPatient.setMobileNoSoS(patientVO.getMobilenoSoS());
		logger.info("patientVO.getMobilenoSoS()*********"+patientVO.getMobilenoSoS());
		WatchrxPatient patient = patientDAO.save(watchrxPatient);
		
		if (user != null && user.getRoleType() == Constants.UserType.CAREGIVER.intValue()){
					List<WatchrxClinician> clinicianList1 = clinicianDAO.findByProperty("watchrxUser.userId", user.getUserId());
					if (clinicianList1 != null && clinicianList1.size() > 0){
						//List<WatchrxPatientClinicianAssignmnt> patientClinicianList1 = patientClinicianAssignmentDAO.findByProperty("watchrxClinician.clinicianId", clinicianList.get(0).getClinicianId());
						logger.info("clinicianList1.get(0).getClinicianId()**********"+clinicianList1.get(0).getClinicianId());
						logger.info("clinicianList1.get(0).getClinicianName()**********"+clinicianList1.get(0).getFirstName());

						List<WatchrxPatientClinicianAssignmnt> clinicianAssignmnts = new ArrayList<WatchrxPatientClinicianAssignmnt>();
						clinicianAssignmnts.add(getPatientClinicanAssinment(
								clinicianList1.get(0).getClinicianId(), patient));
						clinicianAssignmnts.add(getPatientClinicanAssinment(
								clinicianList1.get(0).getClinicianId(), patient));
						clinicianAssignmnts.add(getPatientClinicanAssinment(
								clinicianList1.get(0).getClinicianId(), patient));
						

						patientClinicianAssignmentDAO.save(clinicianAssignmnts);
					}}}

	
	@Override
	public List<PatientVO> getPatientList() {
		List<PatientVO> patientList = new ArrayList<PatientVO>();
		List<WatchrxPatient> patients = patientDAO.getAll();
		for (WatchrxPatient watchrxPatient : patients) {
			patientList.add(getPatientVO(watchrxPatient));
		}
		return patientList;
	}
	
	@Override
	public List<PatientVO> getPatientList(UserVO user) {
		List<PatientVO> patientList = new ArrayList<PatientVO>();
		if (user != null && user.getRoleType() == Constants.UserType.DOCTOR.intValue()){
			List<WatchrxDoctor> doctorList = doctorDAO.findByProperty("watchrxUser.userId", user.getUserId());
			if (doctorList != null && doctorList.size() > 0){
				List<WatchrxPatientDoctorAssignmnt> patientDoctorList = patientDoctorAssignmentDAO.findByProperty("watchrxDoctor.doctorId", doctorList.get(0).getDoctorId());
				Map<Long, PatientVO> patientMap = new LinkedHashMap<Long, PatientVO>();
				for (WatchrxPatientDoctorAssignmnt watchrxPatientDoctorAssignmnt : patientDoctorList) {
					WatchrxPatient  watchrxPatient =  watchrxPatientDoctorAssignmnt.getWatchrxPatient();
					patientMap.put(watchrxPatient.getPatientId(), getPatientVO(watchrxPatient));
				}
				patientList =  new ArrayList<PatientVO>(patientMap.values());
			}
		}else if (user != null && user.getRoleType() == Constants.UserType.CAREGIVER.intValue()){
			List<WatchrxClinician> clinicianList = clinicianDAO.findByProperty("watchrxUser.userId", user.getUserId());
			if (clinicianList != null && clinicianList.size() > 0){
				List<WatchrxPatientClinicianAssignmnt> patientClinicianList = patientClinicianAssignmentDAO.findByProperty("watchrxClinician.clinicianId", clinicianList.get(0).getClinicianId());
				Map<Long, PatientVO> patientMap = new LinkedHashMap<Long, PatientVO>();
				for (WatchrxPatientClinicianAssignmnt watchrxPatientClinicianAssignmnt : patientClinicianList) {
					WatchrxPatient  watchrxPatient =  watchrxPatientClinicianAssignmnt.getWatchrxPatient();
					patientMap.put(watchrxPatient.getPatientId(), getPatientVO(watchrxPatient));
				}
				patientList =  new ArrayList<PatientVO>(patientMap.values());
			}
		}else{
			List<WatchrxPatient> patients = patientDAO.getAll();
			for (WatchrxPatient watchrxPatient : patients) {
				patientList.add(getPatientVO(watchrxPatient));
			}
		}
		return patientList;
	}
	
	private PatientVO getPatientVO(WatchrxPatient watchrxPatient){
		PatientVO patientVO = new PatientVO();
		patientVO.setAltPhoneNumber(watchrxPatient.getAltPhoneNum());
		patientVO.setCreatedDate(watchrxPatient.getCreatedDate());
		patientVO.setPatientId(watchrxPatient.getPatientId());
		patientVO.setFirstName(watchrxPatient.getFirstName());
		patientVO.setLastName(watchrxPatient.getLastName());
		patientVO.setPicPath(WatchRxUtils.readTextFileOnly(watchrxPatient.getImgPath()));
		patientVO.setVisitReason(watchrxPatient.getVisitReason());
		patientVO.setRevisit((watchrxPatient.getIsEverBeenHere() !=null && watchrxPatient.getIsEverBeenHere().equals("Y"))?true:false);
		patientVO.setPhoneNumber(watchrxPatient.getPhoneNumber());
		patientVO.setInsuranceCompany(watchrxPatient.getInsuranceCompany());
		patientVO.setInsurancePhNumber(watchrxPatient.getInsuranceNumber());
		patientVO.setRevisit((watchrxPatient.getIsEverBeenHere() !=null && watchrxPatient.getIsEverBeenHere().equals("Y"))?true:false);
		patientVO.setLanguage(watchrxPatient.getLanguageId());
		patientVO.setMaritalStatus(watchrxPatient.getMaritalStatusId());
		patientVO.setSsn(watchrxPatient.getSsn());
		patientVO.setVisitReason(watchrxPatient.getVisitReason());
		patientVO.setDob(watchrxPatient.getDob());
		patientVO.setEmployment(watchrxPatient.getEmployment());
		patientVO.setUpdatedDate(watchrxPatient.getUpdatedDate());
		patientVO.setAddress(getAddress(watchrxPatient.getWatchrxAddress()));
		patientVO.setMobilenoSoS(watchrxPatient.getMobileNoSoS());

		Set<WatchrxPatientDoctorAssignmnt> doctors = watchrxPatient.getWatchrxPatientDoctorAssignmnts();
		for(WatchrxPatientDoctorAssignmnt assignmnt:doctors){
			DoctorVO doctorVO = new DoctorVO();
			WatchrxDoctor watchrxDoctor = assignmnt.getWatchrxDoctor();
			doctorVO.setFirstName(watchrxDoctor.getFirstName());
			doctorVO.setLastName(watchrxDoctor.getLastName());
			doctorVO.setDoctorId(watchrxDoctor.getDoctorId());
			if(assignmnt.getIsPrimary() != null && assignmnt.getIsPrimary().equals(Constants.Status.ACTIVE)){
				patientVO.setPriDoctor(doctorVO);
			}else{
				patientVO.setSpecialist(doctorVO);
			}
		}
		
		Set<WatchrxPatientClinicianAssignmnt> clinicianAssignmnts = watchrxPatient.getWatchrxPatientClinicianAssignmnts();
		for(WatchrxPatientClinicianAssignmnt assignmnt:clinicianAssignmnts){
			WatchrxClinician watchrxClinician = assignmnt.getWatchrxClinician();
			switch (watchrxClinician.getShiftId()) {
			case Constants.Shift.MORNIG:
				patientVO.setMorningClinician(watchrxClinician.getLastName() +" "+watchrxClinician.getFirstName());
				break;
			case Constants.Shift.NOON:
				patientVO.setNoonClinician(watchrxClinician.getLastName() +" "+watchrxClinician.getFirstName());
				break;
			case Constants.Shift.NIGHT:
				patientVO.setNightClinician(watchrxClinician.getLastName() +" "+watchrxClinician.getFirstName());
				break;
			default:
				break;
			}
		}
		Set<WatchrxPatientWatchAssignmnt> assignedWatches =watchrxPatient.getWatchrxPatientWatchAssignmnts();
		for(WatchrxPatientWatchAssignmnt assignWatch:assignedWatches){
			WatchVO watch = new WatchVO();
			WatchrxWatch watchrxWatch = assignWatch.getWatchrxWatch();
			watch.setWatchId(watchrxWatch.getWatchId());
			watch.setImeiNumber(watchrxWatch.getWatchImeiNumber());
			patientVO.setWatch(watch);
		}
		String timesSlots = watchrxPatient.getTimeSlots();
		if(timesSlots != null && timesSlots.length()>0){
			StringTokenizer stk1 = new StringTokenizer(timesSlots,"|");
			while(stk1.hasMoreElements()){
				String time = stk1.nextToken();
				StringTokenizer timeTypeTokenizer = new StringTokenizer(time,"-");
				while(timeTypeTokenizer.hasMoreElements()){
				String slot = timeTypeTokenizer.nextToken();
				StringTokenizer timeTokenizer = new StringTokenizer(timeTypeTokenizer.nextToken(),":");
				switch(slot){
				case "EarlyMorning":
					patientVO.setEarlyMorningHour(timeTokenizer.nextToken());
					patientVO.setEarlyMorningMin(timeTokenizer.nextToken());
					break;
				case "Breakfast":
					patientVO.setBreakFastHour(timeTokenizer.nextToken());
					patientVO.setBreakFastMin(timeTokenizer.nextToken());
					break;
				case "Lunch":
					patientVO.setLunchHour(timeTokenizer.nextToken());
					patientVO.setLunchMin(timeTokenizer.nextToken());
					break;
				case "AfternoonSnack":
					patientVO.setNoonSnackHour(timeTokenizer.nextToken());
					patientVO.setNoonSnackMin(timeTokenizer.nextToken());
					break;
				case "Dinner":
					patientVO.setDinnerHour(timeTokenizer.nextToken());
					patientVO.setDinnerMin(timeTokenizer.nextToken());
					break;
				case "Bed":
					patientVO.setBedHour(timeTokenizer.nextToken());
					patientVO.setBedMin(timeTokenizer.nextToken());
					break;
				}
				 
				}
			}
		}
		System.out.println("patientVO::::::"+patientVO.toString());
		return patientVO;
	}
		
	private AddressVO getAddress(WatchrxAddress addressVO) {
		AddressVO address = new AddressVO();
		address.setAddress1(addressVO.getAddress1());
		address.setAddress2(addressVO.getAddress2());
		address.setAddressId(addressVO.getAddressId());
		address.setCity(addressVO.getCity());
		address.setState(addressVO.getState());
		address.setZip(addressVO.getZip());
		return address;
	}

	@Override
	public PatientVO getPatient(Long patientId) {
		// TODO Auto-generated method stub
		WatchrxPatient watchrxPatient =  patientDAO.getById(patientId);
		PatientVO patientVO = getPatientVO(watchrxPatient);
	//	patientVO.setPicPath(watchrxPatient.getImgPath());
		return patientVO;
	}
	
	@Override
	public PatientVO getPrescriptionPatient(Long patientId) {
		// TODO Auto-generated method stub
		WatchrxPatient watchrxPatient =  patientDAO.getById(patientId);
		PatientVO patientVO = getPatientVO(watchrxPatient);
		return patientVO;
	}

	@Override
	public void deletePatient(Long patientId) {
		patientWatchAssignmentDAO.deleteByProperty("watchrxPatient.patientId", patientId);
		patientDoctorAssignmentDAO.deleteByProperty("watchrxPatient.patientId", patientId);
		patientClinicianAssignmentDAO.deleteByProperty("watchrxPatient.patientId", patientId);
		patientPrescriptionDAO.deleteByProperty("watchrxPatient.patientId", patientId);
		medicationAdhereLogDAO.deleteByProperty("watchrxPatient.patientId", patientId);
		patientEmergencyContactDAO.deleteByProperty("watchrxPatient.patientId", patientId);
		alertStatusDAO.deleteAlertStatusByPatientId(patientId);
		patientAlertDAO.deleteByProperty("watchrxPatient.patientId", patientId);
		WatchrxPatient watchrxPatient =  patientDAO.getById(patientId);
		patientDAO.delete(patientId);
		addressDAO.delete(watchrxPatient.getWatchrxAddress().getAddressId());
	}

	@Override
	public void savePatientAssign(AssignToPatientVO assignToPatientVO) {
		List<WatchrxPatientDoctorAssignmnt> docsAssigned = patientDoctorAssignmentDAO
				.findByProperty("watchrxPatient.patientId",
						assignToPatientVO.getPatientId());
		List<WatchrxPatientClinicianAssignmnt> assignedClinicians = patientClinicianAssignmentDAO
				.findByProperty("watchrxPatient.patientId", assignToPatientVO.getPatientId());
		List<WatchrxPatientWatchAssignmnt> assignedWatches = patientWatchAssignmentDAO
				.findByProperty("watchrxPatient.patientId", assignToPatientVO.getPatientId());
		//delete all and insert
		patientDoctorAssignmentDAO.deleteAll(docsAssigned);
		patientClinicianAssignmentDAO.deleteAll(assignedClinicians);
		patientWatchAssignmentDAO.deleteAll(assignedWatches);
		
		WatchrxPatientDoctorAssignmnt assignmnt = new WatchrxPatientDoctorAssignmnt();
		List<WatchrxPatientDoctorAssignmnt> assignList = new ArrayList<WatchrxPatientDoctorAssignmnt>();
		WatchrxPatient patient = new WatchrxPatient();
		patient.setPatientId(assignToPatientVO.getPatientId());
		WatchrxDoctor priDoctor = new WatchrxDoctor();
		priDoctor.setDoctorId(assignToPatientVO.getPriDoctorId());
		assignmnt.setWatchrxDoctor(priDoctor);
		assignmnt.setIsPrimary(Constants.Status.ACTIVE);
		assignmnt.setWatchrxPatient(patient);
		assignList.add(assignmnt);

		if (assignToPatientVO.getSpecialistDoctorId() != null && assignToPatientVO.getSpecialistDoctorId() > 0){
			WatchrxPatientDoctorAssignmnt specialist = new WatchrxPatientDoctorAssignmnt();
			WatchrxDoctor specialDoctor = new WatchrxDoctor();
			specialDoctor.setDoctorId(assignToPatientVO.getSpecialistDoctorId());
			specialist.setWatchrxDoctor(specialDoctor);
			specialist.setIsPrimary(Constants.Status.INACTIVE);
			specialist.setWatchrxPatient(patient);
			assignList.add(specialist);
			patientDoctorAssignmentDAO.save(assignList);
		}

		List<WatchrxPatientClinicianAssignmnt> clinicianAssignmnts = new ArrayList<WatchrxPatientClinicianAssignmnt>();
		clinicianAssignmnts.add(getPatientClinicanAssinment(
				assignToPatientVO.getMorningClinicianId(), patient));
		if (assignToPatientVO.getNoonClinicianId() != null && assignToPatientVO.getNoonClinicianId() > 0){
			clinicianAssignmnts.add(getPatientClinicanAssinment(
					assignToPatientVO.getMorningClinicianId(), patient));
		}
		if (assignToPatientVO.getNightClinicianId() != null && assignToPatientVO.getNightClinicianId() > 0){
			clinicianAssignmnts.add(getPatientClinicanAssinment(
					assignToPatientVO.getMorningClinicianId(), patient));
		}

		patientClinicianAssignmentDAO.save(clinicianAssignmnts);

		WatchrxPatientWatchAssignmnt watchAssignmnt = new WatchrxPatientWatchAssignmnt();
		watchAssignmnt.setWatchrxPatient(patient);
		WatchrxWatch watchrxWatch = new WatchrxWatch();
		watchrxWatch.setWatchId(assignToPatientVO.getWatchId());
		watchAssignmnt.setWatchrxWatch(watchrxWatch);
		patientWatchAssignmentDAO.save(watchAssignmnt);
		// TODO Auto-generated method stub
		// if(assignToPatientVO.get)

	}
	
	@Override
	public void savePatientWatchAssign(AssignToPatientVO assignToPatientVO) {
		List<WatchrxPatientWatchAssignmnt> assignedWatches = patientWatchAssignmentDAO
				.findByProperty("watchrxPatient.patientId", assignToPatientVO.getPatientId());
		//delete all and insert
		List<WatchrxPatientClinicianAssignmnt> assignedClinicians = patientClinicianAssignmentDAO
				.findByProperty("watchrxPatient.patientId", assignToPatientVO.getPatientId());
		//delete all and insert
		patientClinicianAssignmentDAO.deleteAll(assignedClinicians);
		patientWatchAssignmentDAO.deleteAll(assignedWatches);
		WatchrxPatient patient = new WatchrxPatient();
		patient.setPatientId(assignToPatientVO.getPatientId());
		


		List<WatchrxPatientClinicianAssignmnt> clinicianAssignmnts = new ArrayList<WatchrxPatientClinicianAssignmnt>();
		clinicianAssignmnts.add(getPatientClinicanAssinment(
				assignToPatientVO.getMorningClinicianId(), patient));
		if (assignToPatientVO.getNoonClinicianId() != null && assignToPatientVO.getNoonClinicianId() > 0){
			clinicianAssignmnts.add(getPatientClinicanAssinment(
					assignToPatientVO.getMorningClinicianId(), patient));
		}
		if (assignToPatientVO.getNightClinicianId() != null && assignToPatientVO.getNightClinicianId() > 0){
			clinicianAssignmnts.add(getPatientClinicanAssinment(
					assignToPatientVO.getMorningClinicianId(), patient));
		}

		patientClinicianAssignmentDAO.save(clinicianAssignmnts);

		WatchrxPatientWatchAssignmnt watchAssignmnt = new WatchrxPatientWatchAssignmnt();
		watchAssignmnt.setWatchrxPatient(patient);
		WatchrxWatch watchrxWatch = new WatchrxWatch();
		watchrxWatch.setWatchId(assignToPatientVO.getWatchId());
		watchAssignmnt.setWatchrxWatch(watchrxWatch);
		patientWatchAssignmentDAO.save(watchAssignmnt);
		// TODO Auto-generated method stub
		// if(assignToPatientVO.get)

	}
	public void saveClinicianWatchAssign(AssignToPatientVO assignToPatientVO) {
		List<WatchrxClinicianWatchAssignmnt> clinicianAssigned = clinicianWatchAssignmentDAO
				.findByProperty("watchrxClinician.clinicianId",
						assignToPatientVO.getPatientId());
		//delete all and insert
		clinicianWatchAssignmentDAO.deleteAll(clinicianAssigned);
		
		WatchrxClinicianWatchAssignmnt assignmnt = new WatchrxClinicianWatchAssignmnt();
		List<WatchrxClinicianWatchAssignmnt> assignList = new ArrayList<WatchrxClinicianWatchAssignmnt>();
		WatchrxWatch watch = new WatchrxWatch();
		watch.setWatchId(assignToPatientVO.getWatchId());
		WatchrxClinician clinician = new WatchrxClinician();
		clinician.setClinicianId(assignToPatientVO.getMorningClinicianId());
		assignmnt.setWatchrxClinician(clinician);
		assignmnt.setWatchrxWatch(watch);
		assignList.add(assignmnt);
		clinicianWatchAssignmentDAO.save(assignList);
	}
	private WatchrxPatientClinicianAssignmnt getPatientClinicanAssinment(Long clinicianId,WatchrxPatient watchrxPatient){
		WatchrxPatientClinicianAssignmnt assignmnt = new WatchrxPatientClinicianAssignmnt();
		WatchrxClinician clinician = new WatchrxClinician();
		clinician.setClinicianId(clinicianId);
		assignmnt.setWatchrxClinician(clinician);
		assignmnt.setWatchrxPatient(watchrxPatient);
		return assignmnt;
	}
	

	@Override
	public void savePrescription(PatientPrescriptionVO prescriptionVO) {
		WatchrxPatientPrescription patientPrescription = null;
		if(prescriptionVO.getPrescriptionId()!= null && prescriptionVO.getPrescriptionId()>0){
			patientPrescription = patientPrescriptionDAO.getById(prescriptionVO.getPrescriptionId());
			patientPrescription.setUpdatedDate(new Date());
		}else{
			patientPrescription = new WatchrxPatientPrescription();
			patientPrescription.setUpdatedDate(new Date());
			patientPrescription.setCreatedDate(new Date());
		}
		patientPrescription.setPatientPrescriptionId(prescriptionVO.getPrescriptionId());
		patientPrescription.setColor(prescriptionVO.getColor());
		//patientPrescription.setCreatedDate(prescriptionVO.getCreatedDate());
		patientPrescription.setDayOfWeek(prescriptionVO.getDayOfWeek());
		patientPrescription.setBeforeOrAfterFood(prescriptionVO.getAfterMeal());
		patientPrescription.setDescription(prescriptionVO.getDescription());
		patientPrescription.setDosage(prescriptionVO.getDosage());
		patientPrescription.setEndDate(prescriptionVO.getEndDate());
		if(prescriptionVO.isFileModified()){
			patientPrescription.setImgPath(prescriptionVO.getPicPath());
			}
		//patientPrescription.setImgPath(prescriptionVO.getPicPath());
		patientPrescription.setMedicineForm(prescriptionVO.getMedicineForm());
		patientPrescription.setMedicineName(prescriptionVO.getMedicine());
		patientPrescription.setQuantity(prescriptionVO.getQuantity());
		patientPrescription.setRefill(prescriptionVO.isRefill()?Constants.Status.ACTIVE:Constants.Status.INACTIVE);
		patientPrescription.setRegimen(prescriptionVO.getRegimen());
		if(patientPrescription.getBeforeOrAfterFood().equalsIgnoreCase("Fixed")){
			patientPrescription.setTimeSlots(prescriptionVO.getFixTimeslots());
		}else{
			patientPrescription.setTimeSlots(prescriptionVO.getTimeSlots());

		}
		patientPrescription.setDayOfWeek(prescriptionVO.getDayOfWeek());
		patientPrescription.setStatus(Constants.Status.ACTIVE);
		WatchrxPatient patient = new WatchrxPatient();
		patient.setPatientId(prescriptionVO.getPatient().getPatientId()); 
		patientPrescription.setWatchrxPatient(patient);
		patientPrescriptionDAO.save(patientPrescription);
		
	}
	
	private PatientPrescriptionVO getPresciptionVO(WatchrxPatientPrescription watchrxPatientPrescription){
		PatientPrescriptionVO patientPrescription =new PatientPrescriptionVO();
		patientPrescription.setPrescriptionId(watchrxPatientPrescription.getPatientPrescriptionId());
		patientPrescription.setColor(watchrxPatientPrescription.getColor());
		patientPrescription.setCreatedDate(watchrxPatientPrescription.getCreatedDate());
		patientPrescription.setDayOfWeek(watchrxPatientPrescription.getDayOfWeek());
		patientPrescription.setDescription(watchrxPatientPrescription.getDescription());
		patientPrescription.setDosage(watchrxPatientPrescription.getDosage());
		patientPrescription.setEndDate(watchrxPatientPrescription.getEndDate());
		patientPrescription.setPicPath(WatchRxUtils.readTextFileOnly(watchrxPatientPrescription.getImgPath()));
		patientPrescription.setMedicineForm(watchrxPatientPrescription.getMedicineForm());
		patientPrescription.setMedicine(watchrxPatientPrescription.getMedicineName());
		patientPrescription.setQuantity(watchrxPatientPrescription.getQuantity());
		patientPrescription.setRefill(watchrxPatientPrescription.getRefill().equals("Y")?true:false);
		patientPrescription.setRegimen(watchrxPatientPrescription.getRegimen());
		//patientPrescription.setTimeSlots(watchrxPatientPrescription.getTimeSlots());
		patientPrescription.setDayOfWeek(watchrxPatientPrescription.getDayOfWeek());
		patientPrescription.setAfterMeal(watchrxPatientPrescription.getBeforeOrAfterFood());
		String daysOfWeek = watchrxPatientPrescription.getDayOfWeek();
		if(daysOfWeek != null && daysOfWeek.length()>0){
			StringTokenizer stk = new StringTokenizer(daysOfWeek,"|");
			while(stk.hasMoreElements()){
				switch(stk.nextToken()){
				case "Su":
					patientPrescription.setSunday(true);
					break;
				case "Mo":
					patientPrescription.setMonday(true);
					break;
				case "Tu":
					patientPrescription.setTuesday(true);
					break;
				case "We":
					patientPrescription.setWednesday(true);
					break;
				case "Th":
					patientPrescription.setThursday(true);
					break;
				case "Fr":
					patientPrescription.setFriday(true);
					break;
				case "Sa":
					patientPrescription.setSaturday(true);
					break;
				}
			}
		}
			
		String timesSlots = watchrxPatientPrescription.getTimeSlots();
		if(timesSlots != null && timesSlots.length()>0){
			StringTokenizer stk1 = new StringTokenizer(timesSlots,"|");
			while(stk1.hasMoreElements()){
				String time = stk1.nextToken();
				switch(time){
				case "EarlyMorning":
					patientPrescription.setEarlyMorningTime(true);
					break;
				case "Breakfast":
					patientPrescription.setBreakFastTime(true);
					break;
				case "Lunch":
					patientPrescription.setLunchTime(true);
					break;
				case "AfternoonSnack":
					patientPrescription.setNoonSnackTime(true);
					break;
				case "Dinner":
					patientPrescription.setDinnerTime(true);
					break;
				case "Bed":
					patientPrescription.setBedTime(true);
					break;
				}
			}
		}
		if(watchrxPatientPrescription.getBeforeOrAfterFood().equalsIgnoreCase("Fixed")){
			patientPrescription.setFixTimeslots(watchrxPatientPrescription.getTimeSlots());
		}else{
			patientPrescription.setTimeSlots(watchrxPatientPrescription.getTimeSlots());

		}
		//String 
		PatientVO patient = new PatientVO();
		WatchrxPatient watchrxPatient = watchrxPatientPrescription.getWatchrxPatient();
		patient.setPatientId(watchrxPatient.getPatientId()); 
		patient.setFirstName(watchrxPatient.getFirstName());
		patient.setLastName(watchrxPatient.getLastName());
		patient.setPicPath(WatchRxUtils.readTextFileOnly(watchrxPatient.getImgPath()));
		patientPrescription.setPatient(patient);
		return patientPrescription;
	}

	@Override
	public List<PatientPrescriptionVO> getPatientPrescriptionList(Long patientId) {
		// TODO Auto-generated method stub
		List<WatchrxPatientPrescription> prescriptionList = patientPrescriptionDAO.findByProperty("watchrxPatient.patientId", patientId);
		List<PatientPrescriptionVO> prescriptionVOList = new ArrayList<PatientPrescriptionVO>();
		Map<Integer, String> medFormData = DropdownUtils.getRefDataByRefType(Constants.ReferenceType.MEDICINE_FORM);
		Map<Integer, String> medData = DropdownUtils.getRefDataByRefType(Constants.ReferenceType.MEDICINE_LIST);
		for(WatchrxPatientPrescription watchrxPatientPrescription:prescriptionList){
			PatientPrescriptionVO patientPrescriptionVO = getPresciptionVO(watchrxPatientPrescription);
			patientPrescriptionVO.setMedicineFormName(medFormData.get(watchrxPatientPrescription.getMedicineForm()));
			logger.info(watchrxPatientPrescription.getBeforeOrAfterFood());
			if(watchrxPatientPrescription.getBeforeOrAfterFood().equalsIgnoreCase("Fixed")){
				patientPrescriptionVO.setTimeSlots(patientPrescriptionVO.getFixTimeslots());
				logger.info("fixed"+patientPrescriptionVO.getFixTimeslots());
			}else{
				patientPrescriptionVO.setTimeSlots(patientPrescriptionVO.getTimeSlots().replaceAll("|", "\n"));
				logger.info("Not fixed"+patientPrescriptionVO.getTimeSlots());

			}
			//patientPrescriptionVO.setTimeSlots(patientPrescriptionVO.getTimeSlots().replaceAll("|", "\n"));
			prescriptionVOList.add(patientPrescriptionVO);
		}
		return prescriptionVOList;
	}

	@Override
	public PatientPrescriptionVO getPatientPrescription(
			Long patientPrescriptonId) {
		WatchrxPatientPrescription watchrxPatientPrescription = patientPrescriptionDAO.getById(patientPrescriptonId);
		PatientPrescriptionVO patientPrescriptionVO = getPresciptionVO(watchrxPatientPrescription);
		patientPrescriptionVO.setPicPath(WatchRxUtils.readTextFileOnly(watchrxPatientPrescription.getImgPath()));

		// TODO Auto-generated method stub
		return patientPrescriptionVO;
	}

	@Override
	public AssignToPatientVO getPatientAssinment(Long patientId) {
		AssignToPatientVO assignToPatientVO = new AssignToPatientVO();
		assignToPatientVO.setPatientId(patientId);
		List<WatchrxPatientDoctorAssignmnt> docsAssigned = patientDoctorAssignmentDAO.findByProperty("watchrxPatient.patientId", patientId);
		for(WatchrxPatientDoctorAssignmnt assignedDoctor:docsAssigned){
			if(assignedDoctor.getIsPrimary().equals(Constants.Status.ACTIVE)){
				assignToPatientVO.setPriDoctorId(assignedDoctor.getWatchrxDoctor().getDoctorId());
			}else{
				assignToPatientVO.setSpecialistDoctorId(assignedDoctor.getWatchrxDoctor().getDoctorId());
			}
		}
		List<WatchrxPatientClinicianAssignmnt> assignedClinicians = patientClinicianAssignmentDAO.findByProperty("watchrxPatient.patientId", patientId);
		for(WatchrxPatientClinicianAssignmnt assignedClinician:assignedClinicians){
			WatchrxClinician clinician = assignedClinician.getWatchrxClinician();
			switch (clinician.getShiftId()) {
			case Constants.Shift.MORNIG:
				assignToPatientVO.setMorningClinicianId(clinician.getClinicianId());
				break;
			case Constants.Shift.NOON:
				assignToPatientVO.setMorningClinicianId(clinician.getClinicianId());
				assignToPatientVO.setNoonClinicianId(clinician.getClinicianId());
				break;
			case Constants.Shift.NIGHT:
				assignToPatientVO.setMorningClinicianId(clinician.getClinicianId());
				assignToPatientVO.setNightClinicianId(clinician.getClinicianId());
				break;
			default:
				break;
			}	
		}
		List<WatchrxPatientWatchAssignmnt> assignedWatches = patientWatchAssignmentDAO.findByProperty("watchrxPatient.patientId", patientId);
		for(WatchrxPatientWatchAssignmnt assignedWatch:assignedWatches){
			assignToPatientVO.setWatchId(assignedWatch.getWatchrxWatch().getWatchId());
		}
		//assignToPatientVO.setAssignementId(assignementId);
		return assignToPatientVO;
	}
	
	@Override
	public void deletePrescription(Long prescriptionId) {
		patientPrescriptionDAO.delete(prescriptionId);
	}
	
	@Override
	public void saveGCMRegID (String gcmId, String imeiNo){
		List<WatchrxPatientWatchAssignmnt> patientWatchAssignmntList = patientWatchAssignmentDAO.findByProperty("watchrxWatch.watchImeiNumber", imeiNo);
		if (patientWatchAssignmntList != null && patientWatchAssignmntList.size() > 0){
			WatchrxPatient patient = patientWatchAssignmntList.get(0).getWatchrxPatient();
			patient.setGcmRegistrationId(gcmId);
			patientDAO.save(patient);
		}
	}
	
	@Override
	public String getGCMRegID (Long patientId){
		WatchrxPatient patient = patientDAO.getById(patientId);
		return patient.getGcmRegistrationId();
	}
	
	@Override
	public void updateGCMRegID (String gcmId, long patientId){
		WatchrxPatient patient = patientDAO.getById(patientId);
		patient.setGcmRegistrationId(gcmId);
		patientDAO.save(patient);
	}
	
	

	/**
	 * 
	 *
	 * @param  patientVO  object with patient details 
	 * @return      true if the patient details has a SSN used by another patient
	 * 
	 */
	@Override
	public Boolean isSSNExists(PatientVO patientVO) {
		System.out.println("In PatientSeviceImpl:isSSNExists");
		System.out.println("patientVO.getPatientId() =");
		System.out.println(patientVO.getPatientId());
		if(patientVO.getPatientId() != null && patientVO.getPatientId() > 0){
			System.out.println("patientVO.getPatientId() > 0");
			WatchrxPatient watchrxPatient = patientDAO.getById(patientVO.getPatientId());
			System.out.println("watchrxPatient.getSsn()==");
			System.out.println(watchrxPatient.getSsn());
			System.out.println("patientVO.getSsn()==");
			System.out.println(patientVO.getSsn());
			if (StringUtils.equalsIgnoreCase(watchrxPatient.getSsn(), patientVO.getSsn())){
				return false;
			}
		}
	
		List<WatchrxPatient> watchrxPatientList = patientDAO.findByProperty("ssn", patientVO.getSsn());		
		if (watchrxPatientList != null && watchrxPatientList.size() > 0){
			return true;
		}else{
			return false;
		}
	}
}
