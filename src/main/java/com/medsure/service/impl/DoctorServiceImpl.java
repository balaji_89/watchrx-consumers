/**
 * 
 */
package com.medsure.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medsure.common.Constants;
import com.medsure.dao.AddressDAO;
import com.medsure.dao.DoctorDAO;
import com.medsure.dao.PatientDoctorAssignmentDAO;
import com.medsure.dao.UserDAO;
import com.medsure.model.WatchrxAddress;
import com.medsure.model.WatchrxDoctor;
import com.medsure.model.WatchrxUser;
import com.medsure.service.DoctorService;
import com.medsure.ui.entity.server.AddressVO;
import com.medsure.ui.entity.server.DoctorVO;
import com.medsure.ui.util.WatchRxUtils;
import com.medsure.util.DropdownUtils;

/**
 * @author snare
 *
 */
@Component
public class DoctorServiceImpl implements DoctorService {

	@Autowired
	DoctorDAO doctorDAO;

	@Autowired
	AddressDAO addressDAO;

	@Autowired
	UserDAO userDAO;
	
	@Autowired
	PatientDoctorAssignmentDAO patientDoctorAssignmentDAO;

	@Override
	public void saveDoctor(DoctorVO doctorVO) {
		WatchrxDoctor watchrxDoctor = getWatchrxDoctor(doctorVO);
		WatchrxAddress watchrxAddress = addressDAO.save(watchrxDoctor.getWatchrxAddress());
		WatchrxUser watchrxUser = userDAO.save(watchrxDoctor.getWatchrxUser());
		watchrxDoctor.setWatchrxAddress(watchrxAddress);
		watchrxDoctor.setWatchrxUser(watchrxUser);
		doctorDAO.save(watchrxDoctor);
	}

	@Override
	public List<DoctorVO> getDoctors() {
		// TODO Auto-generated method stub
		List<WatchrxDoctor> doctors = doctorDAO.getAll();
		List<DoctorVO> doctorList = new ArrayList<DoctorVO>();
		Map<Integer,String> refData = DropdownUtils.getRefDataByRefType(Constants.ReferenceType.SPECIALITY);
		for (WatchrxDoctor watchrxDoctor : doctors) {
			DoctorVO doctorVO = getDoctorVO(watchrxDoctor);
			doctorVO.setSpecialityName(refData.get(doctorVO.getSpeciality()));
			doctorList.add(doctorVO);
		}
		return doctorList;

	}

	private DoctorVO getDoctorVO(WatchrxDoctor watchrxDoctor) {
		DoctorVO doctorVO = new DoctorVO();
		doctorVO.setAltPhoneNumber(watchrxDoctor.getAltPhoneNumber());
		doctorVO.setCreatedDate(watchrxDoctor.getCreatedDate());
		doctorVO.setDoctorId(watchrxDoctor.getDoctorId());
		doctorVO.setRoleType(watchrxDoctor.getDoctorType());
		doctorVO.setFirstName(watchrxDoctor.getFirstName());
		doctorVO.setLastName(watchrxDoctor.getLastName());
		doctorVO.setHospital(watchrxDoctor.getHospitalName());
		doctorVO.setPicPath(WatchRxUtils.readTextFileOnly(watchrxDoctor.getWatchrxUser().getImgPath()));
		doctorVO.setPhoneNumber("" + watchrxDoctor.getPhoneNumber());
		doctorVO.setSpeciality(watchrxDoctor.getSpecialityId());
		doctorVO.setUpdatedDate(watchrxDoctor.getUpdatedDate());
		doctorVO.setAddress(getAddress(watchrxDoctor.getWatchrxAddress()));
		doctorVO.setUserName(watchrxDoctor.getWatchrxUser().getUserName());
		doctorVO.setPassword(watchrxDoctor.getWatchrxUser().getPassword());
		doctorVO.setRoleType(watchrxDoctor.getWatchrxUser().getUserType());
		System.out.println("doctor::::::" + doctorVO.toString());
		return doctorVO;
	}

	private AddressVO getAddress(WatchrxAddress addressVO) {
		AddressVO address = new AddressVO();
		address.setAddress1(addressVO.getAddress1());
		address.setAddress2(addressVO.getAddress2());
		address.setAddressId(addressVO.getAddressId());
		address.setCity(addressVO.getCity());
		address.setState(addressVO.getState());
		address.setZip(addressVO.getZip());
		return address;
	}

	private WatchrxDoctor getWatchrxDoctor(DoctorVO doctorVO) {
		WatchrxDoctor watchrxDoctor = null;
		WatchrxAddress watchrxAddress = null;
		WatchrxUser watchrxUser = null;
		if (doctorVO.getDoctorId() != null) {
			watchrxDoctor = doctorDAO.getById(doctorVO.getDoctorId());
			watchrxAddress = watchrxDoctor.getWatchrxAddress();
			watchrxUser = watchrxDoctor.getWatchrxUser();
			watchrxUser.setUpdatedDate(new Date());
			doctorVO.setUpdatedDate(new Date());
			doctorVO.setCreatedDate(watchrxDoctor.getCreatedDate());
		} else {
			watchrxDoctor = new WatchrxDoctor();
			watchrxAddress = new WatchrxAddress();
			watchrxUser = new WatchrxUser();
			doctorVO.setCreatedDate(new Date());
			doctorVO.setUpdatedDate(new Date());
			watchrxUser.setUpdatedDate(new Date());
			watchrxUser.setCreatedDate(new Date());
		}
		watchrxUser.setStatus(Constants.Status.ACTIVE);
		watchrxAddress.setAddress1(doctorVO.getAddress().getAddress1());
		watchrxAddress.setAddress2(doctorVO.getAddress().getAddress2());
		watchrxAddress.setState(doctorVO.getAddress().getState());
		watchrxAddress.setCity(doctorVO.getAddress().getCity());
		watchrxAddress.setZip(doctorVO.getAddress().getZip());
		watchrxUser.setUserName(doctorVO.getUserName());
		watchrxUser.setPassword(doctorVO.getPassword());
		watchrxUser.setUserType(Constants.UserType.DOCTOR);
		if(doctorVO.isFileModified()){
			watchrxUser.setImgPath(doctorVO.getPicPath());
			}
		watchrxUser.setFirstName(doctorVO.getFirstName());
		watchrxUser.setLastName(doctorVO.getLastName());
		watchrxDoctor.setWatchrxUser(watchrxUser);
		watchrxDoctor.setDoctorType(doctorVO.getSpeciality());
		watchrxDoctor.setWatchrxAddress(watchrxAddress);
		watchrxDoctor.setFirstName(doctorVO.getFirstName());
		watchrxDoctor.setAltPhoneNumber(doctorVO.getAltPhoneNumber());
		watchrxDoctor.setHospitalName(doctorVO.getHospital());
		watchrxDoctor.setLastName(doctorVO.getLastName());
		watchrxDoctor.setPhoneNumber(doctorVO.getPhoneNumber());
		watchrxDoctor.setSpecialityId(doctorVO.getSpeciality());
		watchrxDoctor.setStatus(Constants.Status.ACTIVE);
		watchrxDoctor.setCreatedDate(doctorVO.getCreatedDate());
		watchrxDoctor.setUpdatedDate(doctorVO.getUpdatedDate());
		watchrxDoctor.setStatus(Constants.Status.ACTIVE);
		return watchrxDoctor;
	}

	@Override
	public DoctorVO getDoctor(Long doctorId) {
		// TODO Auto-generated method stub
		WatchrxDoctor watchrxDoctor = doctorDAO.getById(doctorId);
		DoctorVO doctorVO = getDoctorVO(watchrxDoctor);
		//doctorVO.setPicPath(watchrxDoctor.getWatchrxUser().getImgPath());
		return doctorVO;
	}

	@Override
	public void deleteDoctor(Long doctorId) {
		patientDoctorAssignmentDAO.deleteByProperty("watchrxDoctor.doctorId", doctorId);
		WatchrxDoctor watchrxDoctor = doctorDAO.getById(doctorId);
		doctorDAO.delete(doctorId);
		addressDAO.delete(watchrxDoctor.getWatchrxAddress().getAddressId());
		userDAO.delete(watchrxDoctor.getWatchrxUser().getUserId());
	}
	
	@Override
	public Boolean isUsernameExists(DoctorVO doctorVO) {
		if(doctorVO.getDoctorId() != null && doctorVO.getDoctorId() > 0){
			WatchrxDoctor watchrxDoctor = doctorDAO.getById(doctorVO.getDoctorId());
			if (StringUtils.equalsIgnoreCase(watchrxDoctor.getWatchrxUser().getUserName(), doctorVO.getUserName())){
				return false;
			}
		}
	
		List<WatchrxDoctor> watchrxDoctorList = doctorDAO.findByProperty("watchrxUser.userName", doctorVO.getUserName());		
		if (watchrxDoctorList != null && watchrxDoctorList.size() > 0){
			return true;
		}else{
			return false;
		}
	}

}
