/**
 * 
 */
package com.medsure.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medsure.common.Constants;
import com.medsure.dao.AddressDAO;
import com.medsure.dao.ClinicianDAO;
import com.medsure.dao.ClinicianScheduleDAO;
import com.medsure.dao.ClinicianWatchAssignmentDAO;
import com.medsure.dao.PatientClinicianAssignmentDAO;
import com.medsure.dao.PatientWatchAssignmentDAO;
import com.medsure.dao.UserDAO;
import com.medsure.model.WatchrxAddress;
import com.medsure.model.WatchrxClinician;
import com.medsure.model.WatchrxClinicianWatchAssignmnt;
import com.medsure.model.WatchrxPatientClinicianAssignmnt;
import com.medsure.model.WatchrxPatientWatchAssignmnt;
import com.medsure.model.WatchrxUser;
import com.medsure.service.ClinicianService;
import com.medsure.test.patient.PatientTest;
import com.medsure.ui.entity.caregiver.request.Login;
import com.medsure.ui.entity.server.AddressVO;
import com.medsure.ui.entity.server.ClinicianVO;
import com.medsure.ui.entity.server.PatientClinician;
import com.medsure.ui.util.WatchRxUtils;
import com.medsure.util.DropdownUtils;


/**
 * @author snare
 *
 */
@Component  (value="clinicianService")
public class ClinicianServiceImpl implements ClinicianService {
	
	@Autowired
	ClinicianDAO clinicianDAO;
	
	@Autowired
	AddressDAO addressDAO;
	
	@Autowired
	UserDAO userDAO;
	
	@Autowired
	PatientClinicianAssignmentDAO patientClinicianAssignmentDAO;
	
	@Autowired
	ClinicianWatchAssignmentDAO clinicianWatchAssignmentDAO ;
	
	@Autowired
	PatientWatchAssignmentDAO patientWatchAssignmentDAO;
	
	@Autowired
	ClinicianScheduleDAO clinicianScheduleDAO;
	
	private static final Logger log = Logger
			.getLogger(ClinicianServiceImpl.class.getName());
	@Override
	public void saveClinician (ClinicianVO clinicianVO) {

		WatchrxClinician watchrxClinician = null;
		WatchrxAddress watchrxAddress = null;
		WatchrxUser watchrxUser = null;
		if (clinicianVO.getClinicianId() != null){
			watchrxClinician = clinicianDAO.getById(clinicianVO.getClinicianId());
			watchrxAddress = watchrxClinician.getWatchrxAddress();
			watchrxUser = watchrxClinician.getWatchrxUser();
		}else{
			watchrxClinician = new WatchrxClinician();
			watchrxAddress = new WatchrxAddress ();
			watchrxUser = new WatchrxUser ();
		}
		
		watchrxAddress.setAddress1(clinicianVO.getAddress().getAddress1());
		watchrxAddress.setAddress2(clinicianVO.getAddress().getAddress2());
		watchrxAddress.setState(clinicianVO.getAddress().getState());
		watchrxAddress.setCity(clinicianVO.getAddress().getCity());
		watchrxAddress.setZip(clinicianVO.getAddress().getZip());
		watchrxAddress = addressDAO.save(watchrxAddress);
		watchrxUser.setUserName(clinicianVO.getUserName());
		watchrxUser.setPassword(clinicianVO.getPassword());
		watchrxUser.setUserType(Constants.UserType.CAREGIVER);
		watchrxUser.setCreatedDate(new Date());
		watchrxUser.setUpdatedDate(new Date());
		if(clinicianVO.isFileModified()){
			watchrxUser.setImgPath(clinicianVO.getPicPath());
		}
		watchrxUser.setFirstName(clinicianVO.getFirstName());
		watchrxUser.setLastName(clinicianVO.getLastName());
		watchrxUser = userDAO.save(watchrxUser);
		watchrxClinician.setWatchrxUser(watchrxUser);
		
		watchrxClinician.setClinicianType(clinicianVO.getSpeciality());
		watchrxClinician.setWatchrxAddress(watchrxAddress);
		watchrxClinician.setFirstName(clinicianVO.getFirstName());
		watchrxClinician.setAltPhoneNumber(clinicianVO.getAltPhoneNumber());
		watchrxClinician.setHospitalName(clinicianVO.getHospital());
		watchrxClinician.setLastName(clinicianVO.getLastName());
		watchrxClinician.setPhoneNumber(clinicianVO.getPhoneNumber());
		watchrxClinician.setShiftId(clinicianVO.getShift());
		watchrxClinician.setStatus(Constants.Status.ACTIVE);
		watchrxClinician.setCreatedDate(new Date());
		watchrxClinician.setUpdatedDate(new Date());
		
		clinicianDAO.save(watchrxClinician);
	}

	@Override
	public List<ClinicianVO> getClinicianList() {
		List<WatchrxClinician> clinicians = clinicianDAO.getAll();
		List<ClinicianVO> clinicianList = new ArrayList<ClinicianVO>();
		Map<Integer, String> refData = DropdownUtils.getRefDataByRefType(Constants.ReferenceType.CLINICIAN_TYPE);
		Map<Integer, String> shiftRefData = DropdownUtils.getRefDataByRefType(Constants.ReferenceType.SHIFT);
		for(WatchrxClinician watchrxClinician:clinicians){
			ClinicianVO clinicianVO = getClinicianVO(watchrxClinician);
			clinicianVO.setSpecialityName(refData.get(clinicianVO.getSpeciality()));
			clinicianVO.setShiftName(shiftRefData.get(clinicianVO.getShift()));			
			clinicianList.add(clinicianVO);
		}
		// TODO Auto-generated method stub
		return clinicianList;
	}

	@Override
	public ClinicianVO getClinician(Long clinicianId) {
		// TODO Auto-generated method stub
		WatchrxClinician watchrxClinician = clinicianDAO.getById(clinicianId);
		ClinicianVO clinicianVO = getClinicianVO(watchrxClinician);
		//clinicianVO.setPicPath(watchrxClinician.getWatchrxUser().getImgPath());
		return clinicianVO;
	}
	
	@Override
	public ClinicianVO getClinicianByUserName(String userName) {
		ClinicianVO clinicianVO = null;
		// TODO Auto-generated method stub
		List<WatchrxClinician> watchrxClinicianList = clinicianDAO.findByProperty("watchrxUser.userName", userName);
		if (watchrxClinicianList != null && watchrxClinicianList.size() > 0){
			WatchrxClinician watchrxClinician = watchrxClinicianList.get(0);
			clinicianVO = getClinicianVO(watchrxClinician);
			clinicianVO.setPicPath(watchrxClinician.getWatchrxUser().getImgPath());
		}
		return clinicianVO;
	}

	@Override
	public void resetPasswordByUserName(Login loginDetails) {
		List<WatchrxClinician> watchrxClinicianList = clinicianDAO.findByProperty("watchrxUser.userName", loginDetails.getLoginId());
		if (watchrxClinicianList != null && watchrxClinicianList.size() > 0){
			log.info("user name"+watchrxClinicianList.get(0).getWatchrxUser().getUserName());
			WatchrxClinician watchrxClinician = watchrxClinicianList.get(0);
			WatchrxUser user =  watchrxClinician.getWatchrxUser();
			user.setPassword(loginDetails.getPassword());
			log.info("user name 22 "+user.getUserName());
			userDAO.save(user);
			watchrxClinician.setWatchrxUser(user);
			clinicianDAO.save(watchrxClinician);
		}
	}
	@Override
	public void deleteClinician(Long clinicianId) {
		patientClinicianAssignmentDAO.deleteByProperty("watchrxClinician.clinicianId", clinicianId);
		clinicianScheduleDAO.deleteByProperty("watchrxClinician.clinicianId", clinicianId);
		WatchrxClinician watchrxClinician = clinicianDAO.getById(clinicianId);
		clinicianDAO.delete(clinicianId);
		addressDAO.delete(watchrxClinician.getWatchrxAddress().getAddressId());
		userDAO.delete(watchrxClinician.getWatchrxUser().getUserId());
	}
	
	private ClinicianVO getClinicianVO(WatchrxClinician watchrxClinician){
		ClinicianVO clinicianVO = new ClinicianVO();
		clinicianVO.setAltPhoneNumber(watchrxClinician.getAltPhoneNumber());
		clinicianVO.setCreatedDate(watchrxClinician.getCreatedDate());
		clinicianVO.setClinicianId(watchrxClinician.getClinicianId());
		clinicianVO.setSpeciality(watchrxClinician.getClinicianType());
		clinicianVO.setFirstName(watchrxClinician.getFirstName());
		clinicianVO.setLastName(watchrxClinician.getLastName());
		clinicianVO.setHospital(watchrxClinician.getHospitalName());
		clinicianVO.setPicPath(WatchRxUtils.readTextFileOnly(watchrxClinician.getWatchrxUser().getImgPath()));
		clinicianVO.setPhoneNumber(watchrxClinician.getPhoneNumber());
		clinicianVO.setShift(watchrxClinician.getShiftId());
		clinicianVO.setUpdatedDate(watchrxClinician.getUpdatedDate());
		clinicianVO.setAddress(getAddress(watchrxClinician.getWatchrxAddress()));
		clinicianVO.setUserName(watchrxClinician.getWatchrxUser().getUserName());
		clinicianVO.setPassword(watchrxClinician.getWatchrxUser().getPassword());
		clinicianVO.setRoleType(watchrxClinician.getWatchrxUser().getUserType());
		System.out.println("doctor::::::"+clinicianVO.toString());
		return clinicianVO;
	}
	
	private AddressVO getAddress(WatchrxAddress addressVO) {
		AddressVO address = new AddressVO();
		address.setAddress1(addressVO.getAddress1());
		address.setAddress2(addressVO.getAddress2());
		address.setAddressId(addressVO.getAddressId());
		address.setCity(addressVO.getCity());
		address.setState(addressVO.getState());
		address.setZip(addressVO.getZip());
		return address;
	}

	@Override
	public List<ClinicianVO> getClinicianList(int shiftId) {

		List<WatchrxClinician> clinicians = clinicianDAO.findByProperty("shiftId", shiftId);
		List<ClinicianVO> clinicianList = new ArrayList<ClinicianVO>();
		for(WatchrxClinician watchrxClinician:clinicians){
			clinicianList.add(getClinicianVO(watchrxClinician));
		}
		// TODO Auto-generated method stub
		return clinicianList;
	
	}
	
	@Override
	public void saveGCMRegID (String gcmId, Long careGiverId){
		WatchrxClinician watchrxClinician = clinicianDAO.getById(careGiverId);
		watchrxClinician.setGcmRegistrationId(gcmId);
		clinicianDAO.save(watchrxClinician);
	}
	
	@Override
	public String getGCMRegID (Long careGiverId){
		WatchrxClinician watchrxClinician = clinicianDAO.getById(careGiverId);
		return watchrxClinician.getGcmRegistrationId();
	}
	
	@Override
	public String getNurseGCMRegIdByPatientId(Long patientId){
		List<WatchrxPatientClinicianAssignmnt> patientClinicianList = patientClinicianAssignmentDAO.findByProperty("watchrxPatient.patientId", patientId);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		try{
			//Date currentTime = sdf.parse(sdf.format(new Date()));
			int shift = Constants.Shift.MORNIG;
			String gcmId = null;
			/*if (currentTime.before(sdf.parse(Constants.CareGiverShiftTime.NOON.split("-")[1])) && 
					currentTime.after(sdf.parse(Constants.CareGiverShiftTime.NOON.split("-")[0]))) {
				shift = Constants.Shift.NOON;
		    }else if (currentTime.before(sdf.parse(Constants.CareGiverShiftTime.NIGHT.split("-")[1])) && 
					currentTime.after(sdf.parse(Constants.CareGiverShiftTime.NIGHT.split("-")[0]))) {
				shift = Constants.Shift.NIGHT;
		    }*/
			for (WatchrxPatientClinicianAssignmnt watchrxPatientClinicianAssignmnt:patientClinicianList){
				if (watchrxPatientClinicianAssignmnt.getWatchrxClinician().getShiftId().intValue() == Constants.Shift.MORNIG){
					gcmId = watchrxPatientClinicianAssignmnt.getWatchrxClinician().getGcmRegistrationId();
					log.info("gcmId mornig"+gcmId);
					if(gcmId!=null){
					return gcmId;
					}
				}
				if (watchrxPatientClinicianAssignmnt.getWatchrxClinician().getShiftId().intValue() == Constants.Shift.NIGHT){
					gcmId = watchrxPatientClinicianAssignmnt.getWatchrxClinician().getGcmRegistrationId();
					log.info("gcmId night"+gcmId);
					if(gcmId!=null){
					return gcmId;
					}				}
				if (watchrxPatientClinicianAssignmnt.getWatchrxClinician().getShiftId().intValue() == Constants.Shift.NOON){
					gcmId = watchrxPatientClinicianAssignmnt.getWatchrxClinician().getGcmRegistrationId();
					log.info("gcmId noon"+gcmId);
					if(gcmId!=null){
					return gcmId;
					}				}
			}
		}catch (Exception e){
			
		}
		return null;
	}
	
	@Override
	public String getClinicianId(Long patientId){
		List<WatchrxPatientClinicianAssignmnt> patientClinicianList = patientClinicianAssignmentDAO.findByProperty("watchrxPatient.patientId", patientId);
		try{
			//Date currentTime = sdf.parse(sdf.format(new Date()));
			int shift = Constants.Shift.MORNIG;
			/*if (currentTime.before(sdf.parse(Constants.CareGiverShiftTime.NOON.split("-")[1])) && 
					currentTime.after(sdf.parse(Constants.CareGiverShiftTime.NOON.split("-")[0]))) {
				shift = Constants.Shift.NOON;
		    }else if (currentTime.before(sdf.parse(Constants.CareGiverShiftTime.NIGHT.split("-")[1])) && 
					currentTime.after(sdf.parse(Constants.CareGiverShiftTime.NIGHT.split("-")[0]))) {
				shift = Constants.Shift.NIGHT;
		    }*/
			Long clinicianId = null;
			for (WatchrxPatientClinicianAssignmnt watchrxPatientClinicianAssignmnt:patientClinicianList){
				if (watchrxPatientClinicianAssignmnt.getWatchrxClinician().getShiftId().intValue() == Constants.Shift.MORNIG){
					clinicianId = watchrxPatientClinicianAssignmnt.getWatchrxClinician().getClinicianId();
					if(clinicianId!=null){
					return String.valueOf(clinicianId);
					}
				}
				else if (watchrxPatientClinicianAssignmnt.getWatchrxClinician().getShiftId().intValue() == Constants.Shift.NIGHT){
					clinicianId = watchrxPatientClinicianAssignmnt.getWatchrxClinician().getClinicianId();
					if(clinicianId!=null){
						return String.valueOf(clinicianId);					}				
					}
				else if (watchrxPatientClinicianAssignmnt.getWatchrxClinician().getShiftId().intValue() == Constants.Shift.NOON){
					clinicianId = watchrxPatientClinicianAssignmnt.getWatchrxClinician().getClinicianId();
					if(clinicianId!=null){
						return String.valueOf(clinicianId);					}				
					}
			}
		}catch (Exception e){
			
		}
		return null;
	}
	
	@Override
	public Boolean isUsernameExists(ClinicianVO clinicianVO) {
		if(clinicianVO.getClinicianId() != null && clinicianVO.getClinicianId() > 0){
			WatchrxClinician watchrxClinician = clinicianDAO.getById(clinicianVO.getClinicianId());
			if (StringUtils.equalsIgnoreCase(watchrxClinician.getWatchrxUser().getUserName(), clinicianVO.getUserName())){
				return false;
			}
		}
	
		List<WatchrxClinician> watchrxClinicianList = clinicianDAO.findByProperty("watchrxUser.userName", clinicianVO.getUserName());		
		if (watchrxClinicianList != null && watchrxClinicianList.size() > 0){
			return true;
		}else{
			return false;
		}
	}
	@Override
	public List<PatientClinician>  getPatientNurseAssignment(){
		List<WatchrxClinicianWatchAssignmnt> patientClinicianList = clinicianWatchAssignmentDAO.getAll();
		List<PatientClinician> list = new ArrayList<PatientClinician>();

		try{
			for (WatchrxClinicianWatchAssignmnt watchrxPatientClinicianAssignmnt:patientClinicianList){
				PatientClinician clinicainPatient = new PatientClinician();
				clinicainPatient.setId(watchrxPatientClinicianAssignmnt.getClinicianWatchAssignmntId().toString());
				clinicainPatient.setNurseName(watchrxPatientClinicianAssignmnt.getWatchrxClinician().getFirstName()+" "+watchrxPatientClinicianAssignmnt.getWatchrxClinician().getLastName());
				clinicainPatient.setWatchNo(watchrxPatientClinicianAssignmnt.getWatchrxWatch().getWatchImeiNumber());
				list.add(clinicainPatient);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return list;
	}
	@Override
	public List<PatientClinician>  getPatientWatchAssignment(String username){
		List<Long> ids = new ArrayList<Long>();
		List<WatchrxPatientWatchAssignmnt> patientClinicianList = patientWatchAssignmentDAO.getAll();
		List<PatientClinician> list = new ArrayList<PatientClinician>();
		List<WatchrxClinician> watchrxClinicianList = clinicianDAO.findByProperty("watchrxUser.userName", username);
		if (watchrxClinicianList != null && watchrxClinicianList.size() > 0){
			WatchrxClinician watchrxClinician = watchrxClinicianList.get(0);
			Set<WatchrxPatientClinicianAssignmnt> ss = watchrxClinician.getWatchrxPatientClinicianAssignmnts();
			for (WatchrxPatientClinicianAssignmnt watchrxPatientClinicianAssignmnt : ss) {
				ids.add(watchrxPatientClinicianAssignmnt.getWatchrxPatient().getPatientId());
			}
		}
		try{
			for (WatchrxPatientWatchAssignmnt watchrxPatientClinicianAssignmnt:patientClinicianList){
				PatientClinician clinicainPatient = new PatientClinician();
				if(ids.contains(watchrxPatientClinicianAssignmnt.getWatchrxPatient().getPatientId())){
				clinicainPatient.setId(watchrxPatientClinicianAssignmnt.getPatientWatchAssignmntId().toString());
				clinicainPatient.setNurseName(watchrxPatientClinicianAssignmnt.getWatchrxWatch().getWatchImeiNumber());
				clinicainPatient.setPatientName(watchrxPatientClinicianAssignmnt.getWatchrxPatient().getFirstName()+" "+watchrxPatientClinicianAssignmnt.getWatchrxPatient().getLastName());
				list.add(clinicainPatient);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return list;
	}
	@Override
	public void unassign(long id) {
		clinicianWatchAssignmentDAO.deleteByProperty("clinicianWatchAssignmntId", id);
		
	}

	@Override
	public void unassignPW(long id) {
		// TODO Auto-generated method stub
		patientWatchAssignmentDAO.deleteByProperty("patientWatchAssignmntId", id);
		
	}
	
}