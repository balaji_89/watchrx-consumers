/**
 * 
 */
package com.medsure.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medsure.dao.MedicationAdhereLogDAO;
import com.medsure.dao.PatientWatchAssignmentDAO;
import com.medsure.model.WatchrxMedicationAdhereLog;
import com.medsure.model.WatchrxPatient;
import com.medsure.model.WatchrxPatientWatchAssignmnt;
import com.medsure.service.AdhereLogService;
import com.medsure.ui.entity.patient.request.AdherenceLogs;
import com.medsure.ui.entity.patient.request.MedicationDone;

/**
 * @author snare
 *
 */
@Component  (value="adhereLogService")
public class AdhereLogServiceImpl implements AdhereLogService {

	@Autowired
	MedicationAdhereLogDAO medicationAdhereLogDAO;
	
	@Autowired
	PatientWatchAssignmentDAO patientWatchAssignmentDAO;
	
	@Override
	public void insertAdhereLog(AdherenceLogs info) {
		List<WatchrxMedicationAdhereLog> adhereLogList = new ArrayList<WatchrxMedicationAdhereLog> ();
		
		for (MedicationDone medicationDone : info.getAdherenceLogs()){
			WatchrxMedicationAdhereLog watchrxMedicationAdhereLog = new WatchrxMedicationAdhereLog ();
			watchrxMedicationAdhereLog.setMedadhAckTimestamp(medicationDone.getMedAckTimeStamp());
			watchrxMedicationAdhereLog.setMedadhFood(medicationDone.getMedFood());
			watchrxMedicationAdhereLog.setMedadhMedicineId(medicationDone.getMedicineId());
			watchrxMedicationAdhereLog.setMedadhRemindedTimestamp(medicationDone.getMedReamindTimeStamp());
			watchrxMedicationAdhereLog.setMedadhStatus(medicationDone.getMedStatus());
			watchrxMedicationAdhereLog.setMedadhTimeslot(medicationDone.getMedTimeSlot());
			watchrxMedicationAdhereLog.setMedadhTimestamp(medicationDone.getMedTimeStamp());
			WatchrxPatient watchrxPatient = new WatchrxPatient ();
			watchrxPatient.setPatientId(new Long(info.getPatientId()));
			watchrxMedicationAdhereLog.setWatchrxPatient(watchrxPatient);
			adhereLogList.add(watchrxMedicationAdhereLog);
		}
		medicationAdhereLogDAO.save(adhereLogList);
	}

	@Override
	public AdherenceLogs getAdhereLogInfo(Long patientId) {
		
		AdherenceLogs adherenceLogs = new AdherenceLogs ();
		adherenceLogs.setPatientId(""+patientId);
		List<WatchrxMedicationAdhereLog> adhereLogList = medicationAdhereLogDAO.findByProperty("watchrxPatient.patientId", patientId);
		List<WatchrxPatientWatchAssignmnt> patientWatchAssignmntList = patientWatchAssignmentDAO.findByProperty("watchrxPatient.patientId", patientId);
		String imeiNumber = null;
		if (patientWatchAssignmntList != null && patientWatchAssignmntList.size() > 0){
			imeiNumber = patientWatchAssignmntList.get(0).getWatchrxWatch().getWatchImeiNumber();
		}
		List<MedicationDone> adhereLogs = new ArrayList<MedicationDone>();
		for (WatchrxMedicationAdhereLog watchrxMedicationAdhereLog:adhereLogList){
			
			MedicationDone medicationDone = new MedicationDone ();
			medicationDone.setImeiNo(imeiNumber);
			medicationDone.setGcmRegistrationId(watchrxMedicationAdhereLog.getWatchrxPatient().getGcmRegistrationId());
			medicationDone.setMedAckTimeStamp(watchrxMedicationAdhereLog.getMedadhAckTimestamp());
			medicationDone.setMedFood(watchrxMedicationAdhereLog.getMedadhFood());
			medicationDone.setMedicineId(watchrxMedicationAdhereLog.getMedadhMedicineId());
			medicationDone.setMedReamindTimeStamp(watchrxMedicationAdhereLog.getMedadhRemindedTimestamp());
			medicationDone.setMedStatus(watchrxMedicationAdhereLog.getMedadhStatus());
			medicationDone.setMedTimeSlot(watchrxMedicationAdhereLog.getMedadhTimeslot());
			medicationDone.setMedTimeStamp(watchrxMedicationAdhereLog.getMedadhTimestamp());
			medicationDone.setPatientId(""+watchrxMedicationAdhereLog.getWatchrxPatient().getPatientId());
			adhereLogs.add(medicationDone);
		}
		adherenceLogs.setAdherenceLogs(adhereLogs);
		return adherenceLogs;
	}
	
	
}
