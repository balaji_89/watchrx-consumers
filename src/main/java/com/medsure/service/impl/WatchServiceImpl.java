/**
 * 
 */
package com.medsure.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medsure.common.Constants;
import com.medsure.dao.ClinicianWatchAssignmentDAO;
import com.medsure.dao.PatientWatchAssignmentDAO;
import com.medsure.dao.WatchAPKDAO;
import com.medsure.dao.WatchCGAppInstallStatusDAO;
import com.medsure.dao.WatchDAO;
import com.medsure.model.WatchrxClinicianWatchAssignmnt;
import com.medsure.model.WatchrxDoctor;
import com.medsure.model.WatchrxPatientDoctorAssignmnt;
import com.medsure.model.WatchrxPatientWatchAssignmnt;
import com.medsure.model.WatchrxWatch;
import com.medsure.model.WatchrxWatchAPK;
import com.medsure.model.WatchrxWatchCGAppInstallStatus;
import com.medsure.service.WatchService;
import com.medsure.ui.entity.patient.request.WatchCGAppStatus;
import com.medsure.ui.entity.server.APKVO;
import com.medsure.ui.entity.server.AssignToPatientVO;
import com.medsure.ui.entity.server.PatientVO;
import com.medsure.ui.entity.server.WatchVO;
import com.medsure.ui.service.server.PatientController;
import com.medsure.ui.util.WatchRxUtils;

/**
 * @author snare
 *
 */
@Component  (value="watchService")
public class WatchServiceImpl implements WatchService {
	@Autowired
	WatchDAO watchDAO;
	
	@Autowired
	PatientWatchAssignmentDAO patientWatchAssignmentDAO;
	
	@Autowired
	ClinicianWatchAssignmentDAO clinicianWatchAssignmentDAO;
	
	private static Logger logger = Logger.getLogger(WatchServiceImpl.class);

	@Autowired
	WatchAPKDAO watchAPKDAO; 
	
	@Autowired
	WatchCGAppInstallStatusDAO watchCGAppInstallStatusDAO;
	
	@Override
	public void saveWatch(WatchVO watchVO)  {
		// TODO Auto-generated method stub
		WatchrxWatch watchrxWatch = null;
		if(watchVO.getWatchId() != null && watchVO.getWatchId()>0){
			watchrxWatch = watchDAO.getById(watchVO.getWatchId());
			watchrxWatch.setUpdatedDate(new Date());
			
		}else{
			watchrxWatch = new WatchrxWatch();
			watchrxWatch.setStatus(Constants.Status.ACTIVE);
			watchrxWatch.setUpdatedDate(new Date());
			watchrxWatch.setCreatedDate(new Date());
		}
		watchrxWatch.setWatchImeiNumber(watchVO.getImeiNumber());
		watchDAO.save(watchrxWatch);
		
	}

	@Override
	public List<WatchVO> getWatchList() {
		// TODO Auto-generated method stub
		List<WatchrxWatch> watches = watchDAO.getAll();
		List<WatchVO> watchList = new ArrayList<WatchVO>();
		for(WatchrxWatch watchrxWatch:watches){
			WatchVO watch = getWatchVO(watchrxWatch);
			if(watch!=null){
			watchList.add(watch);
			}
		}
		return watchList;
	}
	
	@Override
	public List<WatchVO> getWatchListByClinician(Long clinicianId) {
		// TODO Auto-generated method stub
		logger.info("******"+clinicianId);
		List<WatchrxClinicianWatchAssignmnt> watches = clinicianWatchAssignmentDAO.findByProperty("watchrxClinician.clinicianId", clinicianId);
		logger.info("******size"+watches.size());

		List<WatchVO> watchList = new ArrayList<WatchVO>();
		for(WatchrxClinicianWatchAssignmnt watchrxWatch:watches){
			WatchVO watch = getWatchVO(watchrxWatch.getWatchrxWatch());
			if(watch!=null){
				logger.info("******"+watch.getImeiNumber());

			watchList.add(watch);
			}
		}
		return watchList;
	}
	
	@Override
	public List<WatchVO> getWatchListByGCM() {
		// TODO Auto-generated method stub
		List<WatchrxWatch> watches = watchDAO.getAll();
		List<WatchVO> watchList = new ArrayList<WatchVO>();
		for(WatchrxWatch watchrxWatch:watches){
			WatchVO watch = getWatchVOByGCM(watchrxWatch);
			if(watch!=null){
				logger.info(" @@@@@@@@ Getting watches");
			watchList.add(watch);
			}
		}
		return watchList;
	}

	@Override
	public Boolean isIMEIExists(WatchVO watchVO) {
		if(watchVO.getWatchId() != null && watchVO.getWatchId() > 0){
			WatchrxWatch watchrxWatch = watchDAO.getById(watchVO.getWatchId());
			if (StringUtils.equalsIgnoreCase(watchrxWatch.getWatchImeiNumber(), watchVO.getImeiNumber())){
				return false;
			}
		}
	
		List<WatchrxWatch> watchrxWatchList = watchDAO.findByProperty("watchImeiNumber", watchVO.getImeiNumber());		
		if (watchrxWatchList != null && watchrxWatchList.size() > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public WatchVO getWatch(Long watchId) {
		// TODO Auto-generated method stub
		WatchrxWatch watchrxWatch = watchDAO.getById(watchId);		
		return getWatchVO(watchrxWatch);
	}

	@Override
	public void deleteWatch(Long watchId) {
		patientWatchAssignmentDAO.deleteByProperty("watchrxWatch.watchId", watchId);
		watchDAO.delete(watchId);		
	}
	
	private WatchVO getWatchVO(WatchrxWatch watchrxWatch){
		WatchVO watchVO = new WatchVO();
		watchVO.setImeiNumber(watchrxWatch.getWatchImeiNumber());
		watchVO.setWatchId(watchrxWatch.getWatchId());
	    List<WatchrxPatientWatchAssignmnt> assignmnts = patientWatchAssignmentDAO.findByProperty("watchrxWatch.watchId", watchrxWatch.getWatchId());
	    if(assignmnts != null && assignmnts.size()>0){
	    	 for(WatchrxPatientWatchAssignmnt assignmnt:assignmnts){
	   	    	PatientVO patientVO = new PatientVO();
	   	    	patientVO.setFirstName(assignmnt.getWatchrxPatient().getFirstName());
	   	    	patientVO.setLastName(assignmnt.getWatchrxPatient().getLastName());
	   	    	patientVO.setPicPath(WatchRxUtils.readTextFileOnly(assignmnt.getWatchrxPatient().getImgPath()));
	   	    	watchVO.setPatient(patientVO);
	   	    	break;
	    	 }
	    }
		return watchVO;
	}
	
	private WatchVO getWatchVOByGCM(WatchrxWatch watchrxWatch){
		WatchVO watchVO = null;
		
	    List<WatchrxPatientWatchAssignmnt> assignmnts = patientWatchAssignmentDAO.findByProperty("watchrxWatch.watchId", watchrxWatch.getWatchId());
	    if(assignmnts != null && assignmnts.size()>0){
	    	 for(WatchrxPatientWatchAssignmnt assignmnt:assignmnts){
	    		 watchVO = new WatchVO();
	    		 if(assignmnt.getWatchrxPatient().getGcmRegistrationId()!=null){
	    			 logger.info("!!!!!!!!!!!!!!!!!!GCM Id"+assignmnt.getWatchrxPatient().getGcmRegistrationId());
	    		 watchVO.setImeiNumber(watchrxWatch.getWatchImeiNumber());
	    			watchVO.setWatchId(watchrxWatch.getWatchId());
	   	    	PatientVO patientVO = new PatientVO();
	   	    	patientVO.setFirstName(assignmnt.getWatchrxPatient().getFirstName());
	   	    	patientVO.setLastName(assignmnt.getWatchrxPatient().getLastName());
	   	    	patientVO.setPicPath(WatchRxUtils.readTextFileOnly(assignmnt.getWatchrxPatient().getImgPath()));
	   	    	patientVO.setGcmId(assignmnt.getWatchrxPatient().getGcmRegistrationId());
	   	    	watchVO.setPatient(patientVO);
	    	 }
	   	    	break;
	    	 }
	    }
		return watchVO;
	}

	@Override
	public void assignWatchToPatient(AssignToPatientVO assignToPatientVO) {
		WatchrxPatientDoctorAssignmnt assignmnt = new WatchrxPatientDoctorAssignmnt();
		WatchrxDoctor priDoctor = new WatchrxDoctor();
		priDoctor.setDoctorId(assignToPatientVO.getPriDoctorId());
		assignmnt.setWatchrxDoctor(priDoctor);
		// TODO Auto-generated method stub
		//if(assignToPatientVO.get) 
		
	}

	@Override
	public boolean updateAPKtoWatch(APKVO apkvo) {
		WatchrxWatchAPK apk = new WatchrxWatchAPK();
		apk.setAndroidVersion(apkvo.getApkVersion());
		apk.setApkURL(apkvo.getApkUrl());
		apk.setApkVersion(apkvo.getApkVersion());
		apk.setMake(apkvo.getMake());
		apk.setModel(apkvo.getModel());
		apk.setCreatedDate(new Date());
		watchAPKDAO.save(apk);		
		return false;
	}
	
	@Override
	public List<APKVO> getAllAPKtoWatch() {
		List<WatchrxWatchAPK>
		apk = watchAPKDAO.getAll();
		List<APKVO>
		apk1 = new ArrayList<APKVO>();
		for (WatchrxWatchAPK watchrxWatchAPK : apk) {
			APKVO vo = new APKVO();
			vo.setApkID(watchrxWatchAPK.getApkId().toString());
			vo.setAndroidVersion(watchrxWatchAPK.getAndroidVersion());
			vo.setApkUrl(watchrxWatchAPK.getApkURL());
			vo.setApkVersion(watchrxWatchAPK.getApkVersion());
			vo.setMake(watchrxWatchAPK.getMake());
			vo.setModel(watchrxWatchAPK.getModel());
			vo.setUploadedTime(watchrxWatchAPK.getCreatedDate().toString());
			apk1.add(vo);
		}
			
		return apk1;
	}
	
	public String  getGCMbyIMEINO(String IMEino){
		//List<WatchrxWatch> watchrxWatchList = watchDAO.findByProperty("watchImeiNumber", IMEino);	
		String gcm = null;
	    List<WatchrxPatientWatchAssignmnt> assignmnts = patientWatchAssignmentDAO.findByProperty("watchrxWatch.watchImeiNumber",IMEino);
for (WatchrxPatientWatchAssignmnt watchrxPatientWatchAssignmnt : assignmnts) {
	gcm = watchrxPatientWatchAssignmnt.getWatchrxPatient().getGcmRegistrationId();
	 logger.info("IMEI NO "+IMEino+"  GCM "+gcm);
}
		return gcm;
		
	}
	
	@Override
	public APKVO getAPKDetailsByVersion(String apkVersion) {
		List<WatchrxWatchAPK>
		apk = watchAPKDAO.getAll();
		APKVO
		vo = new APKVO();
		for (WatchrxWatchAPK watchrxWatchAPK : apk) {
			logger.info("apk version  "+watchrxWatchAPK.getApkVersion());
			if(apkVersion.equalsIgnoreCase(watchrxWatchAPK.getApkVersion())){
			vo.setApkID(watchrxWatchAPK.getApkId().toString());
			vo.setAndroidVersion(watchrxWatchAPK.getAndroidVersion());
			vo.setApkUrl(watchrxWatchAPK.getApkURL());
			logger.info("apk version url  "+watchrxWatchAPK.getApkURL());

			vo.setApkVersion(watchrxWatchAPK.getApkVersion());
			vo.setMake(watchrxWatchAPK.getMake());
			vo.setModel(watchrxWatchAPK.getModel());
			vo.setUploadedTime(watchrxWatchAPK.getCreatedDate().toString());
			}
			//apk1.add(vo);
		}
			
		return vo;
	}

	@Override
	public void insertWatchMobileStatus(WatchCGAppStatus status) {
		WatchrxWatchCGAppInstallStatus appStatus = new WatchrxWatchCGAppInstallStatus();
		appStatus.setCareGiverName(status.getCareGiverloginId());
		appStatus.setStatus(status.getStatus());
		appStatus.setWatchImeiNumber(status.getImei());
		appStatus.setCreatedDate(new Date());
		appStatus.setAppVersion(status.getAppVersion());
		appStatus.setReason(status.getReason());
		watchCGAppInstallStatusDAO.save(appStatus);
	}

	@Override
	public List<WatchrxWatchCGAppInstallStatus> getWatchCGAppStatusList() {
		List<WatchrxWatchCGAppInstallStatus> list = watchCGAppInstallStatusDAO.getAll();
		return list;
	}
}

