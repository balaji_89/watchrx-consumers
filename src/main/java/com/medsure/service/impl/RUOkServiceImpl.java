/**
 * 
 */
package com.medsure.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.medsure.dao.MedicationAdhereLogDAO;
import com.medsure.dao.PatientWatchAssignmentDAO;
import com.medsure.dao.RUOkStatusDAO;
import com.medsure.model.WatchrxMedicationAdhereLog;
import com.medsure.model.WatchrxPatient;
import com.medsure.model.WatchrxPatientWatchAssignmnt;
import com.medsure.model.WatchrxRuOkStatus;
import com.medsure.service.AdhereLogService;
import com.medsure.service.RUOkService;
import com.medsure.ui.entity.patient.request.AdherenceLogs;
import com.medsure.ui.entity.patient.request.MedicationDone;
import com.medsure.ui.entity.patient.request.RUOKStatus;
import com.medsure.ui.entity.patient.response.RUOKInfo;

/**
 * @author balaji
 *
 */
@Component  (value="ruOkService")
public class RUOkServiceImpl implements RUOkService {

	private static final String OK = "OK";
	private static final String Not_Ok = "Not Ok";
	private static final String Not_Answer = "Didn't Answer";
	@Autowired
	RUOkStatusDAO ruOkDAO;
	
	@Autowired
	PatientWatchAssignmentDAO patientWatchAssignmentDAO;
	
	public void insertRUOkStatus(RUOKStatus info) {
		WatchrxRuOkStatus status = new WatchrxRuOkStatus();
		status.setDate(info.getDate());
		status.setStatus(info.getStatus());
		WatchrxPatient watchrxPatient = new WatchrxPatient ();
		watchrxPatient.setPatientId(new Long(info.getPatientId()));
		status.setWatchrxPatient(watchrxPatient);
		ruOkDAO.save(status);
	}

	public RUOKInfo getRUOKStatusInfo() {
		//System.out.println("inside getRUOKStatusInfo:::: ");	
		RUOKInfo info = new RUOKInfo();
		int countOk = 0;
		int countNotOk = 0;
		int countNotAnswer = 0;
		List<RUOKStatus> okList = new ArrayList<RUOKStatus>();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 0);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String formatted = format1.format(cal.getTime());
		//System.out.println("formatted date");
		//System.out.println(formatted);
		List<WatchrxRuOkStatus> ruokList = ruOkDAO.findByProperty("date",formatted);
		for(WatchrxRuOkStatus status : ruokList){
			RUOKStatus oksataus = new RUOKStatus();
			/*Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int day = cal.get(Calendar.DAY_OF_MONTH);*/
			
			//if(status.getDate().contains(String.valueOf(day))){
			StringBuffer name = new StringBuffer();
			if(status.getWatchrxPatient().getFirstName()!=null){
				name.append(status.getWatchrxPatient().getFirstName());
			}
			if(status.getWatchrxPatient().getLastName()!=null){
				name.append(" "+status.getWatchrxPatient().getLastName());
			}
			oksataus.setDate(status.getDate());
			oksataus.setName(name.toString());
			oksataus.setStatus(status.getStatus());
			if(status.getStatus()!=null){
			if(status.getStatus().equalsIgnoreCase(OK)){
				countOk = countOk+1;
			}else if(status.getStatus().equalsIgnoreCase(Not_Ok)){
				countNotOk = countNotOk+1;
			}else if(status.getStatus().equalsIgnoreCase(Not_Answer)){
				countNotAnswer = countNotAnswer+1;
			}
			}
			okList.add(oksataus);
			//}
			
		}
		info.setStatsData(OK+":"+countOk+" "+Not_Ok+":"+countNotOk+" "+Not_Answer+":"+countNotAnswer);
		info.setStatus(okList);
		return info;
	}
	
	public RUOKInfo getRUOKStatusInfoByDate(String date) {
		RUOKInfo info = new RUOKInfo();
		int countOk = 0;
		int countNotOk = 0;
		int countNotAnswer = 0;
		List<RUOKStatus> okList = new ArrayList<RUOKStatus>();
//		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
//		Date date1 = null;
//		try {
//			 date1 = formatter.parse(date);
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		/*Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 0);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		String formatted = format1.format(cal.getTime());*/
		//System.out.println("In RUOkServiceImpl.getRUOKStatusInfoByDate, date is: ");
		//System.out.println(date);
		List<WatchrxRuOkStatus> ruokList = ruOkDAO.findByProperty("date",date);
		for(WatchrxRuOkStatus status : ruokList){
			RUOKStatus oksataus = new RUOKStatus();
			/*Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int day = cal.get(Calendar.DAY_OF_MONTH);*/
			
			//if(status.getDate().contains(String.valueOf(day))){
			StringBuffer name = new StringBuffer();
			if(status.getWatchrxPatient().getFirstName()!=null){
				name.append(status.getWatchrxPatient().getFirstName());
			}
			if(status.getWatchrxPatient().getLastName()!=null){
				name.append(" "+status.getWatchrxPatient().getLastName());
			}
			oksataus.setDate(status.getDate());
			oksataus.setName(name.toString());
			oksataus.setStatus(status.getStatus());
			if(status.getStatus()!=null){
			if(status.getStatus().equalsIgnoreCase(OK)){
				countOk = countOk+1;
			}else if(status.getStatus().equalsIgnoreCase(Not_Ok)){
				countNotOk = countNotOk+1;
			}else if(status.getStatus().equalsIgnoreCase(Not_Answer)){
				countNotAnswer = countNotAnswer+1;
			}
			}
			okList.add(oksataus);
			//}
			
		}
		info.setStatsData(OK+":"+countOk+" "+Not_Ok+":"+countNotOk+" "+Not_Answer+":"+countNotAnswer);
		info.setStatus(okList);
		return info;
	}
	
//	public static void main(String[] args) {
//		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
//		Date date1 = null;
//		try {
//			 date1 = formatter.parse("08/05/2015");
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println(date1);
//	}
	
	
}
