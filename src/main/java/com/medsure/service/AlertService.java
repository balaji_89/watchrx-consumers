/**
 * 
 */
package com.medsure.service;

import java.util.List;

import com.medsure.ui.entity.caregiver.request.AlertStatus;
import com.medsure.ui.entity.caregiver.request.GetAlerts;
import com.medsure.ui.entity.patient.common.MedicationInfo;
import com.medsure.ui.entity.patient.request.MedicationAlert;



/**
 * @author snare
 *
 */

public interface AlertService {
	
	public MedicationAlert insertAlert(MedicationAlert info);
	
	public List<MedicationAlert> getAlertsByPatientId(GetAlerts patientId);
	
	public List<MedicationInfo> getMedicationByMedicationIds(List<Long> medicationIds);
	
	public void insertAlertStatus(AlertStatus info);
	
	public void updateAlertStatus(Long alertStatusId, AlertStatus info);
	
	public List<AlertStatus> getAlertStatusInfo (Long careregiverId);
	
	public List<AlertStatus> getAlertStatusInfoByStatusByCG (AlertStatus info);
	
	public MedicationAlert getAlertByAlertId(long alertId);

	List<AlertStatus> getAllAlertStatusInfo();

	List<MedicationAlert> getAllAlertsByPatient();

}
