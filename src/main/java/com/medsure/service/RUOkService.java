/**
 * 
 */
package com.medsure.service;

import com.medsure.ui.entity.patient.request.RUOKStatus;
import com.medsure.ui.entity.patient.response.RUOKInfo;



/**
 * @author snare
 *
 */

public interface RUOkService {

	public void insertRUOkStatus(RUOKStatus info);
	
	public RUOKInfo getRUOKStatusInfo();
	public RUOKInfo getRUOKStatusInfoByDate(String date);
}
