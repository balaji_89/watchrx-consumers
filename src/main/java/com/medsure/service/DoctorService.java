/**
 * 
 */
package com.medsure.service;

import java.util.List;

import com.medsure.ui.entity.server.DoctorVO;


/**
 * @author snare
 *
 */
public interface DoctorService {
	
	public void saveDoctor(DoctorVO doctorVO);
	
	public List<DoctorVO> getDoctors();
	
	public DoctorVO getDoctor(Long doctorId);
	
	public void deleteDoctor(Long doctorId);

	public Boolean isUsernameExists(DoctorVO doctorVO);

}
