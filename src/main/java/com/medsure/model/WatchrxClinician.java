package com.medsure.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_CLINICIAN")
public class WatchrxClinician implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "CLINICIAN_ID", unique = true, nullable = false)
	private Long clinicianId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_USER_ID", nullable = false)
	private WatchrxUser watchrxUser;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ADDRESS_ID")
	private WatchrxAddress watchrxAddress;
	
	@Column(name = "FIRST_NAME", nullable = false, length = 50)
	private String firstName;
	
	@Column(name = "LAST_NAME", length = 50)
	private String lastName;

	
	@Column(name = "PHONE_NUMBER", length = 20)
	private String phoneNumber;
	
	@Column(name = "ALT_PHONE_NUMBER", length = 20)
	private String altPhoneNumber;
	
	@Column(name = "HOSPITAL_NAME", length = 256)
	private String hospitalName;
	
	@Column(name = "CLINICIAN_TYPE")
	private Integer clinicianType;
	
	@Column(name = "SHIFT_ID")
	private Integer shiftId;
	
	@Column(name = "STATUS", length = 1)
	private String status;
	
	@Column(name = "GCM_REGISTRATION_ID", length = 200)
	private String gcmRegistrationId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE", nullable = false, length = 19)
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_DATE", nullable = false, length = 19)
	private Date updatedDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxClinician")
	private Set<WatchrxClinicianSchedule> watchrxClinicianSchedules = new HashSet<WatchrxClinicianSchedule>(
			0);
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxClinician")
	private Set<WatchrxPatientClinicianAssignmnt> watchrxPatientClinicianAssignmnts = new HashSet<WatchrxPatientClinicianAssignmnt>(
			0);
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxClinician")
	private Set<WatchrxAlertStatus> watchrxAlertStatus = new HashSet<WatchrxAlertStatus>(
			0);
	public WatchrxClinician() {
	}

	public WatchrxClinician(WatchrxUser watchrxUser, String firstName,
			String imgPath, Date createdDate, Date updatedDate) {
		this.watchrxUser = watchrxUser;
		this.firstName = firstName;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}

	public WatchrxClinician(
			WatchrxUser watchrxUser,
			WatchrxAddress watchrxAddress,
			String firstName,
			String lastName,
			String phoneNumber,
			String altPhoneNumber,
			String hospitalName,
			Integer clinicianType,
			Integer shiftId,
			String status,
			Date createdDate,
			Date updatedDate,
			Set<WatchrxClinicianSchedule> watchrxClinicianSchedules,
			Set<WatchrxPatientClinicianAssignmnt> watchrxPatientClinicianAssignmnts) {
		this.watchrxUser = watchrxUser;
		this.watchrxAddress = watchrxAddress;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.altPhoneNumber = altPhoneNumber;
		this.hospitalName = hospitalName;
		this.clinicianType = clinicianType;
		this.shiftId = shiftId;
		this.status = status;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.watchrxClinicianSchedules = watchrxClinicianSchedules;
		this.watchrxPatientClinicianAssignmnts = watchrxPatientClinicianAssignmnts;
	}

	
	public Long getClinicianId() {
		return this.clinicianId;
	}

	public void setClinicianId(Long clinicianId) {
		this.clinicianId = clinicianId;
	}

	
	public WatchrxUser getWatchrxUser() {
		return this.watchrxUser;
	}

	public void setWatchrxUser(WatchrxUser watchrxUser) {
		this.watchrxUser = watchrxUser;
	}

	
	public WatchrxAddress getWatchrxAddress() {
		return this.watchrxAddress;
	}

	public void setWatchrxAddress(WatchrxAddress watchrxAddress) {
		this.watchrxAddress = watchrxAddress;
	}

	
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	
	public String getAltPhoneNumber() {
		return this.altPhoneNumber;
	}

	public void setAltPhoneNumber(String altPhoneNumber) {
		this.altPhoneNumber = altPhoneNumber;
	}

	
	public String getHospitalName() {
		return this.hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	
	public Integer getClinicianType() {
		return this.clinicianType;
	}

	public void setClinicianType(Integer clinicianType) {
		this.clinicianType = clinicianType;
	}

	
	public Integer getShiftId() {
		return this.shiftId;
	}

	public void setShiftId(Integer shiftId) {
		this.shiftId = shiftId;
	}

	
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	
	public Set<WatchrxClinicianSchedule> getWatchrxClinicianSchedules() {
		return this.watchrxClinicianSchedules;
	}

	public void setWatchrxClinicianSchedules(
			Set<WatchrxClinicianSchedule> watchrxClinicianSchedules) {
		this.watchrxClinicianSchedules = watchrxClinicianSchedules;
	}

	
	public Set<WatchrxPatientClinicianAssignmnt> getWatchrxPatientClinicianAssignmnts() {
		return this.watchrxPatientClinicianAssignmnts;
	}

	public void setWatchrxPatientClinicianAssignmnts(
			Set<WatchrxPatientClinicianAssignmnt> watchrxPatientClinicianAssignmnts) {
		this.watchrxPatientClinicianAssignmnts = watchrxPatientClinicianAssignmnts;
	}

	public Set<WatchrxAlertStatus> getWatchrxAlertStatus() {
		return watchrxAlertStatus;
	}

	public void setWatchrxAlertStatus(Set<WatchrxAlertStatus> watchrxAlertStatus) {
		this.watchrxAlertStatus = watchrxAlertStatus;
	}

	public String getGcmRegistrationId() {
		return gcmRegistrationId;
	}

	public void setGcmRegistrationId(String gcmRegistrationId) {
		this.gcmRegistrationId = gcmRegistrationId;
	}
}
