package com.medsure.model;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_CLINICIAN_SCHEDULE")
public class WatchrxClinicianSchedule implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "CLINICIAN_SCHEDULE_ID", unique = true, nullable = false)
	private Long clinicianScheduleId;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_CLINICIAN_ID", nullable = false)
	private WatchrxClinician watchrxClinician;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_PATIENT_ID", nullable = false)
	private WatchrxPatient watchrxPatient;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SCHEDULE_TIME", nullable = false, length = 19)
	private Date scheduleTime;
	@Column(name = "VERIFICATION_CODE", length = 10)
	private String verificationCode;
	@Column(name = "STATUS_ID", nullable = false)
	private int statusId;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE", nullable = false, length = 19)
	private Date createdDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_ON", nullable = false, length = 19)
	private Date updatedOn;

	public WatchrxClinicianSchedule() {
	}

	public WatchrxClinicianSchedule(WatchrxClinician watchrxClinician,
			WatchrxPatient watchrxPatient, Date scheduleTime, int statusId,
			Date createdDate, Date updatedOn) {
		this.watchrxClinician = watchrxClinician;
		this.watchrxPatient = watchrxPatient;
		this.scheduleTime = scheduleTime;
		this.statusId = statusId;
		this.createdDate = createdDate;
		this.updatedOn = updatedOn;
	}

	public WatchrxClinicianSchedule(WatchrxClinician watchrxClinician,
			WatchrxPatient watchrxPatient, Date scheduleTime,
			String verificationCode, int statusId, Date createdDate,
			Date updatedOn) {
		this.watchrxClinician = watchrxClinician;
		this.watchrxPatient = watchrxPatient;
		this.scheduleTime = scheduleTime;
		this.verificationCode = verificationCode;
		this.statusId = statusId;
		this.createdDate = createdDate;
		this.updatedOn = updatedOn;
	}

	
	public Long getClinicianScheduleId() {
		return this.clinicianScheduleId;
	}

	public void setClinicianScheduleId(Long clinicianScheduleId) {
		this.clinicianScheduleId = clinicianScheduleId;
	}

	
	public WatchrxClinician getWatchrxClinician() {
		return this.watchrxClinician;
	}

	public void setWatchrxClinician(WatchrxClinician watchrxClinician) {
		this.watchrxClinician = watchrxClinician;
	}

	
	public WatchrxPatient getWatchrxPatient() {
		return this.watchrxPatient;
	}

	public void setWatchrxPatient(WatchrxPatient watchrxPatient) {
		this.watchrxPatient = watchrxPatient;
	}

	
	public Date getScheduleTime() {
		return this.scheduleTime;
	}

	public void setScheduleTime(Date scheduleTime) {
		this.scheduleTime = scheduleTime;
	}

	
	public String getVerificationCode() {
		return this.verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	
	public int getStatusId() {
		return this.statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}
