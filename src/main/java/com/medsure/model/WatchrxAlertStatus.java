package com.medsure.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_ALERT_STATUS")
public class WatchrxAlertStatus implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ALERT_STATUS_ID", unique = true, nullable = false)
	private Long alertStatusId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_PATIENT_ALERT_ID", nullable = false)
	private WatchrxPatientAlert watchrxPatientAlert;
	
	@Column(name = "STATUS", length = 50)
	private String status;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SEND_TO_CAREGIVER_ID", nullable = false)
	private WatchrxClinician watchrxClinician;

	public WatchrxAlertStatus() {
	}

	public Long getAlertStatusId() {
		return alertStatusId;
	}

	public void setAlertStatusId(Long alertStatusId) {
		this.alertStatusId = alertStatusId;
	}

	public WatchrxPatientAlert getWatchrxPatientAlert() {
		return watchrxPatientAlert;
	}

	public void setWatchrxPatientAlert(WatchrxPatientAlert watchrxPatientAlert) {
		this.watchrxPatientAlert = watchrxPatientAlert;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public WatchrxClinician getWatchrxClinician() {
		return watchrxClinician;
	}

	public void setWatchrxClinician(WatchrxClinician watchrxClinician) {
		this.watchrxClinician = watchrxClinician;
	}
}
