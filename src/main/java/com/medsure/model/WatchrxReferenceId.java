package com.medsure.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author snare
 *
 */

@Embeddable
public class WatchrxReferenceId implements java.io.Serializable {

	@Column(name = "REFERENCE_ID", nullable = false)
	private int referenceId;
	
	@Column(name = "REFERENCE_NAME", nullable = false, length = 50)
	private String referenceName;
	
	@Column(name = "REFERENCE_TYPE", nullable = false, length = 50)
	private String referenceType;

	public WatchrxReferenceId() {
	}

	public WatchrxReferenceId(int referenceId, String referenceName,
			String referenceType) {
		this.referenceId = referenceId;
		this.referenceName = referenceName;
		this.referenceType = referenceType;
	}
	
	public int getReferenceId() {
		return this.referenceId;
	}

	public void setReferenceId(int referenceId) {
		this.referenceId = referenceId;
	}
	
	public String getReferenceName() {
		return this.referenceName;
	}

	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}
	
	public String getReferenceType() {
		return this.referenceType;
	}

	public void setReferenceType(String referenceType) {
		this.referenceType = referenceType;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof WatchrxReferenceId))
			return false;
		WatchrxReferenceId castOther = (WatchrxReferenceId) other;

		return (this.getReferenceId() == castOther.getReferenceId())
				&& ((this.getReferenceName() == castOther.getReferenceName()) || (this
						.getReferenceName() != null
						&& castOther.getReferenceName() != null && this
						.getReferenceName()
						.equals(castOther.getReferenceName())))
				&& ((this.getReferenceType() == castOther.getReferenceType()) || (this
						.getReferenceType() != null
						&& castOther.getReferenceType() != null && this
						.getReferenceType()
						.equals(castOther.getReferenceType())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getReferenceId();
		result = 37
				* result
				+ (getReferenceName() == null ? 0 : this.getReferenceName()
						.hashCode());
		result = 37
				* result
				+ (getReferenceType() == null ? 0 : this.getReferenceType()
						.hashCode());
		return result;
	}

}
