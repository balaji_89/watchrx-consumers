package com.medsure.model;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_DOCTOR")
public class WatchrxDoctor implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "DOCTOR_ID", unique = true, nullable = false)
	private Long doctorId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_USER_ID", nullable = false)
	private WatchrxUser watchrxUser;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ADDRESS_ID")
	private WatchrxAddress watchrxAddress;
	
	@Column(name = "FIRST_NAME", nullable = false, length = 50)
	private String firstName;
	
	@Column(name = "LAST_NAME", length = 50)
	private String lastName;
	
	@Column(name = "PHONE_NUMBER", length = 20)
	private String phoneNumber;
	
	@Column(name = "ALT_PHONE_NUMBER", length = 20)
	private String altPhoneNumber;
	
	@Column(name = "HOSPITAL_NAME", length = 256)
	private String hospitalName;
	
	@Column(name = "SPECIALITY_ID")
	private Integer specialityId;
	
	@Column(name = "DOCTOR_TYPE", nullable = false)
	private int doctorType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE", nullable = false, length = 19)
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_DATE", nullable = false, length = 19)
	private Date updatedDate;
	
	@Column(name = "STATUS", length = 1)
	private String status;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxDoctor")
	private Set<WatchrxPatientDoctorAssignmnt> watchrxPatientDoctorAssignmnts = new HashSet<WatchrxPatientDoctorAssignmnt>(
			0);

	public WatchrxDoctor() {
	}

	public WatchrxDoctor(WatchrxUser watchrxUser, String firstName,
			 int doctorType, Date createdDate, Date updatedDate) {
		this.watchrxUser = watchrxUser;
		this.firstName = firstName;
		this.doctorType = doctorType;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}

	public WatchrxDoctor(WatchrxUser watchrxUser,
			WatchrxAddress watchrxAddress, String firstName, String lastName,
			String phoneNumber, String altPhoneNumber,
			String hospitalName, Integer specialityId, int doctorType,
			Date createdDate, Date updatedDate, String status,
			Set<WatchrxPatientDoctorAssignmnt> watchrxPatientDoctorAssignmnts) {
		this.watchrxUser = watchrxUser;
		this.watchrxAddress = watchrxAddress;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.altPhoneNumber = altPhoneNumber;
		this.hospitalName = hospitalName;
		this.specialityId = specialityId;
		this.doctorType = doctorType;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.status = status;
		this.watchrxPatientDoctorAssignmnts = watchrxPatientDoctorAssignmnts;
	}

	
	public Long getDoctorId() {
		return this.doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	
	public WatchrxUser getWatchrxUser() {
		return this.watchrxUser;
	}

	public void setWatchrxUser(WatchrxUser watchrxUser) {
		this.watchrxUser = watchrxUser;
	}

	
	public WatchrxAddress getWatchrxAddress() {
		return this.watchrxAddress;
	}

	public void setWatchrxAddress(WatchrxAddress watchrxAddress) {
		this.watchrxAddress = watchrxAddress;
	}


	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	
	public String getAltPhoneNumber() {
		return this.altPhoneNumber;
	}

	public void setAltPhoneNumber(String altPhoneNumber) {
		this.altPhoneNumber = altPhoneNumber;
	}

	
	public String getHospitalName() {
		return this.hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	
	public Integer getSpecialityId() {
		return this.specialityId;
	}

	public void setSpecialityId(Integer specialityId) {
		this.specialityId = specialityId;
	}

	
	public int getDoctorType() {
		return this.doctorType;
	}

	public void setDoctorType(int doctorType) {
		this.doctorType = doctorType;
	}

	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public Set<WatchrxPatientDoctorAssignmnt> getWatchrxPatientDoctorAssignmnts() {
		return this.watchrxPatientDoctorAssignmnts;
	}

	public void setWatchrxPatientDoctorAssignmnts(
			Set<WatchrxPatientDoctorAssignmnt> watchrxPatientDoctorAssignmnts) {
		this.watchrxPatientDoctorAssignmnts = watchrxPatientDoctorAssignmnts;
	}

}
