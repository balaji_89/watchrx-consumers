package com.medsure.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_PATIENT_EMERGENCY_CONTACT")
public class WatchrxPatientEmergencyContact implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PATIENT_EMERGENCY_CONTACT_ID", unique = true, nullable = false)
	private Long patientEmergencyContactId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_PATIENT_ID", nullable = false)
	private WatchrxPatient watchrxPatient;
	
	@Column(name = "CALL_DOCTOR", length = 1)
	private String callDoctor;
	
	@Column(name = "CALL_COORDINATOR", length = 1)
	private String callCoordinator;
	
	@Column(name = "CALL_CARETAKER", length = 1)
	private String callCaretaker;
	
	@Column(name = "CALL_911", length = 1)
	private String call911;
	
	@Column(name = "STATUS")
	private Integer status;

	public WatchrxPatientEmergencyContact() {
	}

	public WatchrxPatientEmergencyContact(WatchrxPatient watchrxPatient) {
		this.watchrxPatient = watchrxPatient;
	}

	public WatchrxPatientEmergencyContact(WatchrxPatient watchrxPatient,
			String callDoctor, String callCoordinator,
			String callCaretaker, String call911, Integer status) {
		this.watchrxPatient = watchrxPatient;
		this.callDoctor = callDoctor;
		this.callCoordinator = callCoordinator;
		this.callCaretaker = callCaretaker;
		this.call911 = call911;
		this.status = status;
	}
	
	public Long getPatientEmergencyContactId() {
		return this.patientEmergencyContactId;
	}

	public void setPatientEmergencyContactId(Long patientEmergencyContactId) {
		this.patientEmergencyContactId = patientEmergencyContactId;
	}
	
	public WatchrxPatient getWatchrxPatient() {
		return this.watchrxPatient;
	}

	public void setWatchrxPatient(WatchrxPatient watchrxPatient) {
		this.watchrxPatient = watchrxPatient;
	}
	
	public String getCallDoctor() {
		return this.callDoctor;
	}

	public void setCallDoctor(String callDoctor) {
		this.callDoctor = callDoctor;
	}

	
	public String getCallCoordinator() {
		return this.callCoordinator;
	}

	public void setCallCoordinator(String callCoordinator) {
		this.callCoordinator = callCoordinator;
	}
	
	public String getCallCaretaker() {
		return this.callCaretaker;
	}

	public void setCallCaretaker(String callCaretaker) {
		this.callCaretaker = callCaretaker;
	}
	
	public String getCall911() {
		return this.call911;
	}

	public void setCall911(String call911) {
		this.call911 = call911;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
