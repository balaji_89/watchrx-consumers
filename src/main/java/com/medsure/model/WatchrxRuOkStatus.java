package com.medsure.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "WATCHRX_RUOK_STATUS")
public class WatchrxRuOkStatus implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "RUOK_STATUS_ID", unique = true, nullable = false)
	private Long okStatusId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_PATIENT_ID", nullable = false)
	private WatchrxPatient watchrxPatient;
	
	@Column(name = "STATUS", length = 50)
	private String status;
	
	@Column(name = "DATE", length = 50)
	private String date; 
	public WatchrxRuOkStatus() {
	}
	
	public Long getOkStatusId() {
		return okStatusId;
	}
	public void setOkStatusId(Long okStatusId) {
		this.okStatusId = okStatusId;
	}
	public WatchrxPatient getWatchrxPatient() {
		return watchrxPatient;
	}
	public void setWatchrxPatient(WatchrxPatient watchrxPatient) {
		this.watchrxPatient = watchrxPatient;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	

	}

