package com.medsure.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_WATCH")
public class WatchrxWatch implements java.io.Serializable {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "WATCH_ID", unique = true, nullable = false)
	private Long watchId;
	@Column(name = "WATCH_IMEI_NUMBER", nullable = false, length = 50)
	private String watchImeiNumber;
	@Column(name = "STATUS", length = 1)
	private String status;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE", nullable = false, length = 19)
	private Date createdDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_DATE", nullable = false, length = 19)
	private Date updatedDate;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxWatch")
	private Set<WatchrxPatientWatchAssignmnt> watchrxPatientWatchAssignmnts = new HashSet<WatchrxPatientWatchAssignmnt>(
			0);

	public WatchrxWatch() {
	}

	public WatchrxWatch(String watchImeiNumber, Date createdDate,
			Date updatedDate) {
		this.watchImeiNumber = watchImeiNumber;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}

	public WatchrxWatch(String watchImeiNumber, String status,
			Date createdDate, Date updatedDate,
			Set<WatchrxPatientWatchAssignmnt> watchrxPatientWatchAssignmnts) {
		this.watchImeiNumber = watchImeiNumber;
		this.status = status;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.watchrxPatientWatchAssignmnts = watchrxPatientWatchAssignmnts;
	}

	
	public Long getWatchId() {
		return this.watchId;
	}

	public void setWatchId(Long watchId) {
		this.watchId = watchId;
	}

	
	public String getWatchImeiNumber() {
		return this.watchImeiNumber;
	}

	public void setWatchImeiNumber(String watchImeiNumber) {
		this.watchImeiNumber = watchImeiNumber;
	}

	
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	
	public Set<WatchrxPatientWatchAssignmnt> getWatchrxPatientWatchAssignmnts() {
		return this.watchrxPatientWatchAssignmnts;
	}

	public void setWatchrxPatientWatchAssignmnts(
			Set<WatchrxPatientWatchAssignmnt> watchrxPatientWatchAssignmnts) {
		this.watchrxPatientWatchAssignmnts = watchrxPatientWatchAssignmnts;
	}

}
