package com.medsure.model;

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_REFERENCE")
public class WatchrxReference implements java.io.Serializable {

	@EmbeddedId
	@AttributeOverrides({
	@AttributeOverride(name = "referenceId", column = @Column(name = "REFERENCE_ID", nullable = false)),
	@AttributeOverride(name = "referenceName", column = @Column(name = "REFERENCE_NAME", nullable = false, length = 50)),
	@AttributeOverride(name = "referenceType", column = @Column(name = "REFERENCE_TYPE", nullable = false, length = 50)) })
	private WatchrxReferenceId id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE", nullable = false, length = 19)
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_DATE", nullable = false, length = 19)
	private Date updatedDate;

	public WatchrxReference() {
	}

	public WatchrxReference(WatchrxReferenceId id, Date createdDate,
			Date updatedDate) {
		this.id = id;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}
	
	public WatchrxReferenceId getId() {
		return this.id;
	}

	public void setId(WatchrxReferenceId id) {
		this.id = id;
	}
	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
