package com.medsure.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_CLINICIAN_WATCH_ASSIGNMNT")
public class WatchrxClinicianWatchAssignmnt implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "CLINICIAN_WATCH_ASSIGNMNT_ID", unique = true, nullable = false)
	private Long clinicianWatchAssignmntId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_WATCH_ID", nullable = false)
	private WatchrxWatch watchrxWatch;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_CLINICIAN_ID", nullable = false)
	private WatchrxClinician watchrxClinician;

	public WatchrxClinician getWatchrxClinician() {
		return watchrxClinician;
	}

	public void setWatchrxClinician(WatchrxClinician watchrxClinician) {
		this.watchrxClinician = watchrxClinician;
	}

	public WatchrxClinicianWatchAssignmnt() {
	}

	public WatchrxClinicianWatchAssignmnt(WatchrxWatch watchrxWatch,
			WatchrxClinician watchrxClinician) {
		this.watchrxWatch = watchrxWatch;
		this.watchrxClinician = watchrxClinician;
	}

	public Long getClinicianWatchAssignmntId() {
		return this.clinicianWatchAssignmntId;
	}

	public void setClinicianWatchAssignmntId(Long clinicianWatchAssignmntId) {
		this.clinicianWatchAssignmntId = clinicianWatchAssignmntId;
	}
	
	public WatchrxWatch getWatchrxWatch() {
		return this.watchrxWatch;
	}

	public void setWatchrxWatch(WatchrxWatch watchrxWatch) {
		this.watchrxWatch = watchrxWatch;
	}
	
}
