package com.medsure.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_PATIENT_DOCTOR_ASSIGNMNT")
public class WatchrxPatientDoctorAssignmnt implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PATIENT_DOCTOR_ASSIGNMNT_ID", unique = true, nullable = false)
	private Long patientDoctorAssignmntId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_DOCTOR_ID")
	private WatchrxDoctor watchrxDoctor;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_PATIENT_ID", nullable = false)
	private WatchrxPatient watchrxPatient;

	@Column(name = "IS_PRIMARY",  nullable = false)
	private String isPrimary;
	
	public WatchrxPatientDoctorAssignmnt() {
	}

	public WatchrxPatientDoctorAssignmnt(WatchrxPatient watchrxPatient) {
		this.watchrxPatient = watchrxPatient;
	}

	public WatchrxPatientDoctorAssignmnt(WatchrxDoctor watchrxDoctor,
			WatchrxPatient watchrxPatient) {
		this.watchrxDoctor = watchrxDoctor;
		this.watchrxPatient = watchrxPatient;
	}
	
	public Long getPatientDoctorAssignmntId() {
		return this.patientDoctorAssignmntId;
	}

	public void setPatientDoctorAssignmntId(Long patientDoctorAssignmntId) {
		this.patientDoctorAssignmntId = patientDoctorAssignmntId;
	}
	
	public WatchrxDoctor getWatchrxDoctor() {
		return this.watchrxDoctor;
	}

	public void setWatchrxDoctor(WatchrxDoctor watchrxDoctor) {
		this.watchrxDoctor = watchrxDoctor;
	}
	
	public WatchrxPatient getWatchrxPatient() {
		return this.watchrxPatient;
	}

	public void setWatchrxPatient(WatchrxPatient watchrxPatient) {
		this.watchrxPatient = watchrxPatient;
	}

	public String getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(String isPrimary) {
		this.isPrimary = isPrimary;
	}
}
