package com.medsure.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_PATIENT_PRESCRIPTION")
public class WatchrxPatientPrescription implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PATIENT_PRESCRIPTION_ID", unique = true, nullable = false)
	private Long patientPrescriptionId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PATIENT_ID", nullable = false)
	private WatchrxPatient watchrxPatient;
	
	@Column(name = "MEDICINE_NAME", nullable = false, length = 60)
	private String medicineName;
	
	@Column(name = "IMG_PATH", nullable = false, length = 1000)
	private String imgPath;
	
	@Column(name = "DESCRIPTION", nullable = false, length = 1000)
	private String description;
	
	@Column(name = "MEDICINE_FORM", nullable = false)
	private int medicineForm;
	
	@Column(name = "TIME_SLOTS", length = 256)
	private String timeSlots;
	
	@Column(name = "DAY_OF_WEEK", length = 256)
	private String dayOfWeek;
	
	@Column(name = "BEFORE_OR_AFTER_FOOD", length = 10)
	private String beforeOrAfterFood;
	
	@Column(name = "REFILL", length = 1)
	private String refill;
	
	@Column(name = "COLOR", length = 10)
	private String color;

	@Column(name = "QUANTITY", nullable = false)
	private int quantity;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "END_DATE", nullable = false, length = 10)
	private Date endDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE", nullable = false, length = 19)
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_DATE", nullable = false, length = 19)
	private Date updatedDate;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "DOSAGE", nullable = false, length = 10)
	private String dosage;
	
	@Column(name = "REGIMEN")
	private Integer regimen;

	public WatchrxPatientPrescription() {
	}

	public WatchrxPatientPrescription(WatchrxPatient watchrxPatient,
			String medicineName, int medicineForm, int quantity,
			Date endDate, Date createdDate, Date updatedDate, String dosage) {
		this.watchrxPatient = watchrxPatient;
		this.medicineName = medicineName;
		this.medicineForm = medicineForm;
		this.quantity = quantity;
		this.endDate = endDate;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.dosage = dosage;
	}

	public WatchrxPatientPrescription(WatchrxPatient watchrxPatient,
			String medicineName, int medicineForm, String refill, int quantity,
			Date endDate, Date createdDate, Date updatedDate, String status,
			String dosage, Integer regimen) {
		this.watchrxPatient = watchrxPatient;
		this.medicineName = medicineName;
		this.medicineForm = medicineForm;
		this.refill = refill;
		this.quantity = quantity;
		this.endDate = endDate;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.status = status;
		this.dosage = dosage;
		this.regimen = regimen;
	}

	public Long getPatientPrescriptionId() {
		return this.patientPrescriptionId;
	}

	public void setPatientPrescriptionId(Long patientPrescriptionId) {
		this.patientPrescriptionId = patientPrescriptionId;
	}
	
	public WatchrxPatient getWatchrxPatient() {
		return this.watchrxPatient;
	}

	public void setWatchrxPatient(WatchrxPatient watchrxPatient) {
		this.watchrxPatient = watchrxPatient;
	}
	
	public String getMedicineName() {
		return this.medicineName;
	}

	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}
	
	public int getMedicineForm() {
		return this.medicineForm;
	}

	public void setMedicineForm(int medicineForm) {
		this.medicineForm = medicineForm;
	}
	
	public String getRefill() {
		return this.refill;
	}

	public void setRefill(String refill) {
		this.refill = refill;
	}
	
	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getDosage() {
		return this.dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public Integer getRegimen() {
		return this.regimen;
	}

	public void setRegimen(Integer regimen) {
		this.regimen = regimen;
	}

	public String getTimeSlots() {
		return timeSlots;
	}

	public void setTimeSlots(String timeSlots) {
		this.timeSlots = timeSlots;
	}

	public String getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public String getBeforeOrAfterFood() {
		return beforeOrAfterFood;
	}

	public void setBeforeOrAfterFood(String beforeOrAfterFood) {
		this.beforeOrAfterFood = beforeOrAfterFood;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
