package com.medsure.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_MEDICATION_ADHERE_LOG")
public class WatchrxMedicationAdhereLog implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ADHERE_ID", unique = true, nullable = false)
	private Long adhereId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_PATIENT_ID", nullable = false)
	private WatchrxPatient watchrxPatient;
	
	@Column(name = "MEDADH_TIMESTAMP", length = 100)
	private String medadhTimestamp;
	
	@Column(name = "MEDADH_REMINDED_TIMESTAMP", length = 100)
	private String medadhRemindedTimestamp;
	
	@Column(name = "MEDADH_ACK_TIMESTAMP", length = 100)
	private String medadhAckTimestamp;
	
	@Column(name = "MEDADH_FOOD", length = 100)
	private String medadhFood;
	
	@Column(name = "MEDADH_TIMESLOT", length = 100)
	private String medadhTimeslot;
	
	@Column(name = "MEDADH_STATUS", length = 100)
	private String medadhStatus;
	
	@Column(name = "MEDADH_MEDICINE_ID", length = 100)
	private String medadhMedicineId;

	public WatchrxMedicationAdhereLog() {
	}

	public Long getAdhereId() {
		return adhereId;
	}

	public void setAdhereId(Long adhereId) {
		this.adhereId = adhereId;
	}

	public WatchrxPatient getWatchrxPatient() {
		return watchrxPatient;
	}

	public void setWatchrxPatient(WatchrxPatient watchrxPatient) {
		this.watchrxPatient = watchrxPatient;
	}

	public String getMedadhTimestamp() {
		return medadhTimestamp;
	}

	public void setMedadhTimestamp(String medadhTimestamp) {
		this.medadhTimestamp = medadhTimestamp;
	}

	public String getMedadhRemindedTimestamp() {
		return medadhRemindedTimestamp;
	}

	public void setMedadhRemindedTimestamp(String medadhRemindedTimestamp) {
		this.medadhRemindedTimestamp = medadhRemindedTimestamp;
	}

	public String getMedadhAckTimestamp() {
		return medadhAckTimestamp;
	}

	public void setMedadhAckTimestamp(String medadhAckTimestamp) {
		this.medadhAckTimestamp = medadhAckTimestamp;
	}

	public String getMedadhFood() {
		return medadhFood;
	}

	public void setMedadhFood(String medadhFood) {
		this.medadhFood = medadhFood;
	}

	public String getMedadhTimeslot() {
		return medadhTimeslot;
	}

	public void setMedadhTimeslot(String medadhTimeslot) {
		this.medadhTimeslot = medadhTimeslot;
	}

	public String getMedadhStatus() {
		return medadhStatus;
	}

	public void setMedadhStatus(String medadhStatus) {
		this.medadhStatus = medadhStatus;
	}

	public String getMedadhMedicineId() {
		return medadhMedicineId;
	}

	public void setMedadhMedicineId(String medadhMedicineId) {
		this.medadhMedicineId = medadhMedicineId;
	}
}
