package com.medsure.model;

import java.util.HashSet;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_ADDRESS")
public class WatchrxAddress implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ADDRESS_ID", unique = true, nullable = false)
	private Long addressId;
	@Column(name = "ADDRESS1", nullable = false, length = 256)
	private String address1;
	@Column(name = "ADDRESS2", length = 256)
	private String address2;
	@Column(name = "STATE", nullable = false, length = 20)
	private String state;
	@Column(name = "CITY", nullable = false, length = 50)
	private String city;
	@Column(name = "ZIP", nullable = false, length = 20)
	private String zip;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxAddress")
	private Set<WatchrxDoctor> watchrxDoctors = new HashSet<WatchrxDoctor>(0);
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxAddress")
	private Set<WatchrxClinician> watchrxClinicians = new HashSet<WatchrxClinician>(
			0);
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxAddress")
	private Set<WatchrxPatient> watchrxPatients = new HashSet<WatchrxPatient>(0);

	public WatchrxAddress() {
	}

	public WatchrxAddress(String address1, String state, String city, String zip) {
		this.address1 = address1;
		this.state = state;
		this.city = city;
		this.zip = zip;
	}

	public WatchrxAddress(String address1, String address2, String state,
			String city, String zip, Set<WatchrxDoctor> watchrxDoctors,
			Set<WatchrxClinician> watchrxClinicians,
			Set<WatchrxPatient> watchrxPatients) {
		this.address1 = address1;
		this.address2 = address2;
		this.state = state;
		this.city = city;
		this.zip = zip;
		this.watchrxDoctors = watchrxDoctors;
		this.watchrxClinicians = watchrxClinicians;
		this.watchrxPatients = watchrxPatients;
	}

	
	public Long getAddressId() {
		return this.addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	
	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	
	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	
	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	
	public Set<WatchrxDoctor> getWatchrxDoctors() {
		return this.watchrxDoctors;
	}

	public void setWatchrxDoctors(Set<WatchrxDoctor> watchrxDoctors) {
		this.watchrxDoctors = watchrxDoctors;
	}

	
	public Set<WatchrxClinician> getWatchrxClinicians() {
		return this.watchrxClinicians;
	}

	public void setWatchrxClinicians(Set<WatchrxClinician> watchrxClinicians) {
		this.watchrxClinicians = watchrxClinicians;
	}

	
	public Set<WatchrxPatient> getWatchrxPatients() {
		return this.watchrxPatients;
	}

	public void setWatchrxPatients(Set<WatchrxPatient> watchrxPatients) {
		this.watchrxPatients = watchrxPatients;
	}

}
