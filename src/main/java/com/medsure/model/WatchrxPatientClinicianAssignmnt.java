package com.medsure.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_PATIENT_CLINICIAN_ASSIGNMNT")
public class WatchrxPatientClinicianAssignmnt implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PATIENT_CLINICIAN_ASSIGNMNT_ID", unique = true, nullable = false)
	private Long patientClinicianAssignmntId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_CLINICIAN_ID", nullable = false)
	private WatchrxClinician watchrxClinician;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_PATIENT_ID", nullable = false)
	private WatchrxPatient watchrxPatient;

	public WatchrxPatientClinicianAssignmnt() {
	}

	public WatchrxPatientClinicianAssignmnt(WatchrxClinician watchrxClinician,
			WatchrxPatient watchrxPatient) {
		this.watchrxClinician = watchrxClinician;
		this.watchrxPatient = watchrxPatient;
	}
	
	public Long getPatientClinicianAssignmntId() {
		return this.patientClinicianAssignmntId;
	}

	public void setPatientClinicianAssignmntId(Long patientClinicianAssignmntId) {
		this.patientClinicianAssignmntId = patientClinicianAssignmntId;
	}
	
	public WatchrxClinician getWatchrxClinician() {
		return this.watchrxClinician;
	}

	public void setWatchrxClinician(WatchrxClinician watchrxClinician) {
		this.watchrxClinician = watchrxClinician;
	}
	
	public WatchrxPatient getWatchrxPatient() {
		return this.watchrxPatient;
	}

	public void setWatchrxPatient(WatchrxPatient watchrxPatient) {
		this.watchrxPatient = watchrxPatient;
	}
}
