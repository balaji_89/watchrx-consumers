package com.medsure.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_USER")
public class WatchrxUser implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "USER_ID", unique = true, nullable = false)
	private Long userId;
	
	@Column(name = "IMG_PATH", nullable = false, length = 1000)
	private String imgPath;
	
	@Column(name = "USER_NAME", nullable = false, length = 50)
	private String userName;
	
	@Column(name = "FIRST_NAME",  length = 50)
	private String firstName;
	
	@Column(name = "LAST_NAME", length = 50)
	private String lastName;
	
	@Column(name = "PASSWORD", length = 50)
	private String password;
	@Column(name = "USER_TYPE")
	private Integer userType;
	@Column(name = "STATUS", length = 1)
	private String status;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE", nullable = false, length = 19)
	private Date createdDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_DATE", nullable = false, length = 19)
	private Date updatedDate;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxUser")
	private Set<WatchrxClinician> watchrxClinicians = new HashSet<WatchrxClinician>(
			0);
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxUser")
	private Set<WatchrxDoctor> watchrxDoctors = new HashSet<WatchrxDoctor>(0);

	public WatchrxUser() {
	}

	public WatchrxUser(String userName, Date createdDate, Date updatedDate) {
		this.userName = userName;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}

	public WatchrxUser(String userName, String password, Integer userType,
			String status, Date createdDate, Date updatedDate, String imgPath,
			Set<WatchrxClinician> watchrxClinicians,
			Set<WatchrxDoctor> watchrxDoctors) {
		this.userName = userName;
		this.password = password;
		this.userType = userType;
		this.status = status;
		this.imgPath = imgPath;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.watchrxClinicians = watchrxClinicians;
		this.watchrxDoctors = watchrxDoctors;
	}

	
	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public Integer getUserType() {
		return this.userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	
	public Set<WatchrxClinician> getWatchrxClinicians() {
		return this.watchrxClinicians;
	}

	public void setWatchrxClinicians(Set<WatchrxClinician> watchrxClinicians) {
		this.watchrxClinicians = watchrxClinicians;
	}

	
	public Set<WatchrxDoctor> getWatchrxDoctors() {
		return this.watchrxDoctors;
	}

	public void setWatchrxDoctors(Set<WatchrxDoctor> watchrxDoctors) {
		this.watchrxDoctors = watchrxDoctors;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
