package com.medsure.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_PATIENT")
public class WatchrxPatient implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PATIENT_ID", unique = true, nullable = false)
	private Long patientId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_ADDRESS_ID")
	private WatchrxAddress watchrxAddress;
	
	@Column(name = "FIRST_NAME", nullable = false, length = 50)
	private String firstName;
	
	@Column(name = "LAST_NAME", length = 50)
	private String lastName;
	
	@Column(name = "IMG_PATH", nullable = false, length = 1000)
	private String imgPath;
	
	@Column(name = "MARITAL_STATUS_ID")
	private Integer maritalStatusId;
	
	@Column(name = "LANGUAGE_ID")
	private Integer languageId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DOB", nullable = false, length = 20)
	private Date dob;
	
	@Column(name = "EMPLOYMENT", length = 50)
	private String employment;
	
	@Column(name = "SSN", length = 12)
	private String ssn;
	
	@Column(name = "INSURANCE_COMPANY", nullable = false, length = 256)
	private String insuranceCompany;
	
	@Column(name = "INSURANCE_NUMBER", length = 50)
	private String insuranceNumber;
	
	@Column(name = "PHONE_NUMBER", length = 20)
	private String phoneNumber;
	
	@Column(name = "ALT_PHONE_NUM", length = 20)
	private String altPhoneNum;
	
	@Column(name = "VISIT_REASON", length = 100)
	private String visitReason;
	
	@Column(name = "IS_EVER_BEEN_HERE", length = 1)
	private String isEverBeenHere;
	
	@Column(name = "GCM_REGISTRATION_ID", length = 200)
	private String gcmRegistrationId;
	
	@Column(name = "TIME_SLOTS", length = 256)
	private String timeSlots;
	
	@Column(name = "MOBILE_NO", length = 256)
	private String mobileNoSoS;
	
	public String getMobileNoSoS() {
		return mobileNoSoS;
	}

	public void setMobileNoSoS(String mobileNoSoS) {
		this.mobileNoSoS = mobileNoSoS;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE", nullable = false, length = 19)
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_DATE", nullable = false, length = 19)
	private Date updatedDate;
	
	@Column(name = "STATUS", length = 1)
	private String status;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxPatient")
	private Set<WatchrxPatientEmergencyContact> watchrxPatientEmergencyContacts = new HashSet<WatchrxPatientEmergencyContact>(
			0);
			
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxPatient")
	private Set<WatchrxPatientClinicianAssignmnt> watchrxPatientClinicianAssignmnts = new HashSet<WatchrxPatientClinicianAssignmnt>(
			0);
			
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxPatient")
	private Set<WatchrxPatientWatchAssignmnt> watchrxPatientWatchAssignmnts = new HashSet<WatchrxPatientWatchAssignmnt>(
			0);
			
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxPatient")
	private Set<WatchrxPatientPrescription> watchrxPatientPrescriptions = new HashSet<WatchrxPatientPrescription>(
			0);
			
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxPatient")
	private Set<WatchrxClinicianSchedule> watchrxClinicianSchedules = new HashSet<WatchrxClinicianSchedule>(
			0);
			
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxPatient")
	private Set<WatchrxPatientDoctorAssignmnt> watchrxPatientDoctorAssignmnts = new HashSet<WatchrxPatientDoctorAssignmnt>(
			0);
			
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxPatient")
	private Set<WatchrxPatientAlert> watchrxPatientAlerts = new HashSet<WatchrxPatientAlert>(
			0);

	public WatchrxPatient() {
	}

	public WatchrxPatient(String firstName, String imgPath, Date dob,
			String insuranceCompany, Date createdDate, Date updatedDate) {
		this.firstName = firstName;
		this.imgPath = imgPath;
		this.dob = dob;
		this.insuranceCompany = insuranceCompany;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
	}

	public WatchrxPatient(
			WatchrxAddress watchrxAddress,
			String firstName,
			String lastName,
			String imgPath,
			Integer maritalStatusId,
			Integer languageId,
			Date dob,
			String employment,
			String ssn,
			String insuranceCompany,
			String insuranceNumber,
			String phoneNumber,
			String altPhoneNum,
			String visitReason,
			String isEverBeenHere,
			Date createdDate,
			Date updatedDate,
			String status,
			Set<WatchrxPatientEmergencyContact> watchrxPatientEmergencyContacts,
			Set<WatchrxPatientClinicianAssignmnt> watchrxPatientClinicianAssignmnts,
			Set<WatchrxPatientWatchAssignmnt> watchrxPatientWatchAssignmnts,
			Set<WatchrxPatientPrescription> watchrxPatientPrescriptions,
			Set<WatchrxClinicianSchedule> watchrxClinicianSchedules,
			Set<WatchrxPatientDoctorAssignmnt> watchrxPatientDoctorAssignmnts,
			Set<WatchrxPatientAlert> watchrxPatientAlerts) {
		this.watchrxAddress = watchrxAddress;
		this.firstName = firstName;
		this.lastName = lastName;
		this.imgPath = imgPath;
		this.maritalStatusId = maritalStatusId;
		this.languageId = languageId;
		this.dob = dob;
		this.employment = employment;
		this.ssn = ssn;
		this.insuranceCompany = insuranceCompany;
		this.insuranceNumber = insuranceNumber;
		this.phoneNumber = phoneNumber;
		this.altPhoneNum = altPhoneNum;
		this.visitReason = visitReason;
		this.isEverBeenHere = isEverBeenHere;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.status = status;
		this.watchrxPatientEmergencyContacts = watchrxPatientEmergencyContacts;
		this.watchrxPatientClinicianAssignmnts = watchrxPatientClinicianAssignmnts;
		this.watchrxPatientWatchAssignmnts = watchrxPatientWatchAssignmnts;
		this.watchrxPatientPrescriptions = watchrxPatientPrescriptions;
		this.watchrxClinicianSchedules = watchrxClinicianSchedules;
		this.watchrxPatientDoctorAssignmnts = watchrxPatientDoctorAssignmnts;
		this.watchrxPatientAlerts = watchrxPatientAlerts;
	}

	public Long getPatientId() {
		return this.patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public WatchrxAddress getWatchrxAddress() {
		return this.watchrxAddress;
	}

	public void setWatchrxAddress(WatchrxAddress watchrxAddress) {
		this.watchrxAddress = watchrxAddress;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getImgPath() {
		return this.imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	
	public Integer getMaritalStatusId() {
		return this.maritalStatusId;
	}

	public void setMaritalStatusId(Integer maritalStatusId) {
		this.maritalStatusId = maritalStatusId;
	}
	
	public Integer getLanguageId() {
		return this.languageId;
	}

	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}
	
	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	
	public String getEmployment() {
		return this.employment;
	}

	public void setEmployment(String employment) {
		this.employment = employment;
	}
	
	public String getSsn() {
		return this.ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	
	public String getInsuranceCompany() {
		return this.insuranceCompany;
	}

	public void setInsuranceCompany(String insuranceCompany) {
		this.insuranceCompany = insuranceCompany;
	}
	
	public String getInsuranceNumber() {
		return this.insuranceNumber;
	}

	public void setInsuranceNumber(String insuranceNumber) {
		this.insuranceNumber = insuranceNumber;
	}
	
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getAltPhoneNum() {
		return this.altPhoneNum;
	}

	public void setAltPhoneNum(String altPhoneNum) {
		this.altPhoneNum = altPhoneNum;
	}
	
	public String getVisitReason() {
		return this.visitReason;
	}

	public void setVisitReason(String visitReason) {
		this.visitReason = visitReason;
	}
	
	public String getIsEverBeenHere() {
		return this.isEverBeenHere;
	}

	public void setIsEverBeenHere(String isEverBeenHere) {
		this.isEverBeenHere = isEverBeenHere;
	}
	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Set<WatchrxPatientEmergencyContact> getWatchrxPatientEmergencyContacts() {
		return this.watchrxPatientEmergencyContacts;
	}

	public void setWatchrxPatientEmergencyContacts(
			Set<WatchrxPatientEmergencyContact> watchrxPatientEmergencyContacts) {
		this.watchrxPatientEmergencyContacts = watchrxPatientEmergencyContacts;
	}
	
	public Set<WatchrxPatientClinicianAssignmnt> getWatchrxPatientClinicianAssignmnts() {
		return this.watchrxPatientClinicianAssignmnts;
	}

	public void setWatchrxPatientClinicianAssignmnts(
			Set<WatchrxPatientClinicianAssignmnt> watchrxPatientClinicianAssignmnts) {
		this.watchrxPatientClinicianAssignmnts = watchrxPatientClinicianAssignmnts;
	}
	
	public Set<WatchrxPatientWatchAssignmnt> getWatchrxPatientWatchAssignmnts() {
		return this.watchrxPatientWatchAssignmnts;
	}

	public void setWatchrxPatientWatchAssignmnts(
			Set<WatchrxPatientWatchAssignmnt> watchrxPatientWatchAssignmnts) {
		this.watchrxPatientWatchAssignmnts = watchrxPatientWatchAssignmnts;
	}
	
	public Set<WatchrxPatientPrescription> getWatchrxPatientPrescriptions() {
		return this.watchrxPatientPrescriptions;
	}

	public void setWatchrxPatientPrescriptions(
			Set<WatchrxPatientPrescription> watchrxPatientPrescriptions) {
		this.watchrxPatientPrescriptions = watchrxPatientPrescriptions;
	}

	public Set<WatchrxClinicianSchedule> getWatchrxClinicianSchedules() {
		return this.watchrxClinicianSchedules;
	}

	public void setWatchrxClinicianSchedules(
			Set<WatchrxClinicianSchedule> watchrxClinicianSchedules) {
		this.watchrxClinicianSchedules = watchrxClinicianSchedules;
	}
	
	public Set<WatchrxPatientDoctorAssignmnt> getWatchrxPatientDoctorAssignmnts() {
		return this.watchrxPatientDoctorAssignmnts;
	}

	public void setWatchrxPatientDoctorAssignmnts(
			Set<WatchrxPatientDoctorAssignmnt> watchrxPatientDoctorAssignmnts) {
		this.watchrxPatientDoctorAssignmnts = watchrxPatientDoctorAssignmnts;
	}
	
	public Set<WatchrxPatientAlert> getWatchrxPatientAlerts() {
		return this.watchrxPatientAlerts;
	}

	public void setWatchrxPatientAlerts(
			Set<WatchrxPatientAlert> watchrxPatientAlerts) {
		this.watchrxPatientAlerts = watchrxPatientAlerts;
	}

	public String getGcmRegistrationId() {
		return gcmRegistrationId;
	}

	public void setGcmRegistrationId(String gcmRegistrationId) {
		this.gcmRegistrationId = gcmRegistrationId;
	}

	public String getTimeSlots() {
		return timeSlots;
	}

	public void setTimeSlots(String timeSlots) {
		this.timeSlots = timeSlots;
	}
	
	
}
