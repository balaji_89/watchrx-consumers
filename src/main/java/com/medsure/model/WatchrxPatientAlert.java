package com.medsure.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_PATIENT_ALERT")
public class WatchrxPatientAlert implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PATIENT_ALERT_ID", unique = true, nullable = false)
	private Long patientAlertId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_PATIENT_ID", nullable = false)
	private WatchrxPatient watchrxPatient;
	
	@Column(name = "MISSED_TIME", length = 10)
	private String missedTime;
	
	@Column(name = "ALERT_DESCRIPTION", length = 256)
	private String alertDescription;
	
	@Column(name = "MISSED_TIME_SLOT", nullable = false, length = 50)
	private String missedTimeSlot;
	
	@Column(name = "MISSED_BEFORE_OR_AFTER_FOOD", nullable = false , length = 50)
	private String missedBeforeOrAfterFood;
	
	@Column(name = "MISSED_MEDICATION_IDS", nullable = false , length = 100)
	private String missedMedicationIds;
	
	@Column(name = "ALERT_TYPE")
	private String alertType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE", nullable = false, length = 19)
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED_ON", nullable = false, length = 19)
	private Date updatedOn;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "watchrxPatientAlert")
	private Set<WatchrxAlertStatus> watchrxAlertStatus = new HashSet<WatchrxAlertStatus>(
			0);
	
	public WatchrxPatientAlert() {
	}

	public WatchrxPatientAlert(WatchrxPatient watchrxPatient,
			String alertDescription, String alertType, Date createdDate,
			Date updatedOn) {
		this.watchrxPatient = watchrxPatient;
		this.alertDescription = alertDescription;
		this.alertType = alertType;
		this.createdDate = createdDate;
		this.updatedOn = updatedOn;
	}

	public WatchrxPatientAlert(WatchrxPatient watchrxPatient,
			String missedTime, String alertDescription, String alertType,
			Date createdDate, Date updatedOn) {
		this.watchrxPatient = watchrxPatient;
		this.missedTime = missedTime;
		this.alertDescription = alertDescription;
		this.alertType = alertType;
		this.createdDate = createdDate;
		this.updatedOn = updatedOn;
	}

	public Long getPatientAlertId() {
		return this.patientAlertId;
	}

	public void setPatientAlertId(Long patientAlertId) {
		this.patientAlertId = patientAlertId;
	}


	public WatchrxPatient getWatchrxPatient() {
		return this.watchrxPatient;
	}

	public void setWatchrxPatient(WatchrxPatient watchrxPatient) {
		this.watchrxPatient = watchrxPatient;
	}

	public String getMissedTime() {
		return this.missedTime;
	}

	public void setMissedTime(String missedTime) {
		this.missedTime = missedTime;
	}

	public String getAlertDescription() {
		return this.alertDescription;
	}

	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}

	public String getAlertType() {
		return this.alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getMissedTimeSlot() {
		return missedTimeSlot;
	}

	public void setMissedTimeSlot(String missedTimeSlot) {
		this.missedTimeSlot = missedTimeSlot;
	}

	public String getMissedBeforeOrAfterFood() {
		return missedBeforeOrAfterFood;
	}

	public void setMissedBeforeOrAfterFood(String missedBeforeOrAfterFood) {
		this.missedBeforeOrAfterFood = missedBeforeOrAfterFood;
	}

	public String getMissedMedicationIds() {
		return missedMedicationIds;
	}

	public void setMissedMedicationIds(String missedMedicationIds) {
		this.missedMedicationIds = missedMedicationIds;
	}

	public Set<WatchrxAlertStatus> getWatchrxAlertStatus() {
		return watchrxAlertStatus;
	}

	public void setWatchrxAlertStatus(Set<WatchrxAlertStatus> watchrxAlertStatus) {
		this.watchrxAlertStatus = watchrxAlertStatus;
	}
}
