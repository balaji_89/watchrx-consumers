package com.medsure.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author snare
 *
 */

@Entity
@Table(name = "WATCHRX_PATIENT_WATCH_ASSIGNMNT")
public class WatchrxPatientWatchAssignmnt implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PATIENT_WATCH_ASSIGNMNT_ID", unique = true, nullable = false)
	private Long patientWatchAssignmntId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_WATCH_ID", nullable = false)
	private WatchrxWatch watchrxWatch;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_PATIENT_ID", nullable = false)
	private WatchrxPatient watchrxPatient;

	public WatchrxPatientWatchAssignmnt() {
	}

	public WatchrxPatientWatchAssignmnt(WatchrxWatch watchrxWatch,
			WatchrxPatient watchrxPatient) {
		this.watchrxWatch = watchrxWatch;
		this.watchrxPatient = watchrxPatient;
	}

	public Long getPatientWatchAssignmntId() {
		return this.patientWatchAssignmntId;
	}

	public void setPatientWatchAssignmntId(Long patientWatchAssignmntId) {
		this.patientWatchAssignmntId = patientWatchAssignmntId;
	}
	
	public WatchrxWatch getWatchrxWatch() {
		return this.watchrxWatch;
	}

	public void setWatchrxWatch(WatchrxWatch watchrxWatch) {
		this.watchrxWatch = watchrxWatch;
	}
	
	public WatchrxPatient getWatchrxPatient() {
		return this.watchrxPatient;
	}

	public void setWatchrxPatient(WatchrxPatient watchrxPatient) {
		this.watchrxPatient = watchrxPatient;
	}
}
