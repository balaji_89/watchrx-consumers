package com.medsure.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author balaji
 *
 */

@Entity
@Table(name = "WATCHRX_WATCH_CAREGIVER_APP_INSTALL_STATUS")
public class WatchrxWatchCGAppInstallStatus implements java.io.Serializable {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "STATUS_ID", unique = true, nullable = false)
	private Long statusId;
	@Column(name = "WATCH_IMEI_NUMBER", nullable = true, length = 50)
	private String watchImeiNumber;
	@Column(name = "CARE_GIVER_NAME", nullable = true, length = 50)
	private String careGiverName;
	@Column(name = "STATUS", length = 50)
	private String status;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE", nullable = false, length = 19)
	private Date createdDate;
	@Column(name = "REASON", length = 100)
	private String reason;
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Column(name = "APP_VERSION", length = 50)
	private String appVersion;
	
	public String getCareGiverName() {
		return careGiverName;
	}

	public void setCareGiverName(String careGiverName) {
		this.careGiverName = careGiverName;
	}
	public Long getStatusId() {
		return this.statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	
	public String getWatchImeiNumber() {
		return this.watchImeiNumber;
	}

	public void setWatchImeiNumber(String watchImeiNumber) {
		this.watchImeiNumber = watchImeiNumber;
	}

	
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	

}
