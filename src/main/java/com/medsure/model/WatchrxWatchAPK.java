package com.medsure.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author balaji r
 *
 */

@Entity
@Table(name = "WATCHRX_WATCH_APK")
public class WatchrxWatchAPK implements java.io.Serializable {
	
	public Long getApkId() {
		return apkId;
	}

	public void setApkId(Long apkId) {
		this.apkId = apkId;
	}

	public WatchrxWatch getWatchrxWatch() {
		return watchrxWatch;
	}

	public void setWatchrxWatch(WatchrxWatch watchrxWatch) {
		this.watchrxWatch = watchrxWatch;
	}

	public String getApkVersion() {
		return apkVersion;
	}

	public void setApkVersion(String apkVersion) {
		this.apkVersion = apkVersion;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getAndroidVersion() {
		return androidVersion;
	}

	public void setAndroidVersion(String androidVersion) {
		this.androidVersion = androidVersion;
	}

	public String getApkURL() {
		return apkURL;
	}

	public void setApkURL(String apkURL) {
		this.apkURL = apkURL;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "APK_ID", unique = true, nullable = false)
	private Long apkId;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FK_WATCH_ID", nullable = true)
	private WatchrxWatch watchrxWatch;
	@Column(name = "APK_VERSION", length = 50)
	private String apkVersion;
	@Column(name = "MAKE", length = 50)
	private String make;
	@Column(name = "MODEL", length = 50)
	private String model;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPLODED_TIME", nullable = false, length = 19)
	private Date createdDate;
	@Column(name = "ANDROID_VERSION", length = 50)
	private String androidVersion;
	@Column(name = "APK_URL", length = 50)
	private String apkURL;

	public WatchrxWatchAPK() {
	}

}
