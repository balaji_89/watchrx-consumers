package com.medsure.exception;

import org.springframework.stereotype.Component;
import java.lang.*;

public class WatchRxException{
	
	private String errCode;
	private String errDesc;
	
	public WatchRxException(){
		super();
	}
	public WatchRxException(WatchRxExceptionCodes ec){
		this.errCode = ec.getErrCode();
		this.errDesc = ec.getMsg();
		
	}
	
	public String getErrCode(){
		return errCode;
	}
	
	public String getErrDesc(){
		
		return errDesc;
	}
	
	public void setErrCode(String ec){
		this.errCode = ec;
		
	}
	
	public void setErrDesc(String desc){
		
		this.errDesc = desc;
	}

}
