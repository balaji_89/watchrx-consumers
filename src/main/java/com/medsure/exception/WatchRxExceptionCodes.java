package com.medsure.exception;

import org.springframework.stereotype.*;


public enum WatchRxExceptionCodes {
	/**Exception codes ranging from 2 - 25 will be Watchapp error codes **/
	
	IMEI_INVALID("002", "Imei is null or invalid"),
	IMEI_UNAVAILABLE ("003", "Imei is not found in the database"),
	IMEI_NOTASSIGNED ("004","Imei is not associated with any patient"),
	IMEI_ASSIGNED ("005", "Imei is already assigned to a patient"),
	MEDICATION_SCHEDULE_UNAVAILABLE("006", "Medication schedule is unavailable for this Imei");
	
	/**Exception codes ranging from 26 - 50 will be Caregiverapp error codes **/
	
	
	
	
	/**Exception codes ranging from 51 - 75 will be Server error codes **/
	
	private final String errCode;
	private final String msg;
	
	WatchRxExceptionCodes(String errCode, String msg){
		
		this.errCode = errCode;
		this.msg = msg;
		
	}
	
	public String getErrCode(){
		
		return errCode;
	}
	
	public String getMsg(){
		
		return msg;
	}
	
}
