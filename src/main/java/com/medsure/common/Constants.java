package com.medsure.common;



/**
 * The Interface Constants.
 * 
 * @author snare
 */
public interface Constants {
	
	interface Status{
		final String ACTIVE = "Y";
		final String INACTIVE = "N";
	}
	
	interface UserType{
		final Integer ADMIN = 1;
		final Integer COORDINATOR = 2;
		final Integer DOCTOR = 3;
		final Integer CAREGIVER = 4;
	}
	
	interface ReferenceType{
		final String SHIFT = "SHIFT";
		final String MARITAL_STATUS = "MARITAL_STATUS";
		final String LANGUAGE = "LANGUAGE";
		final String SPECIALITY = "SPECIALITY";
		final String CLINICIAN_TYPE = "CLINICIAN_TYPE";
		final String MEDICINE_FORM = "MEDICINE_FORM";
		final String MEDICINE_LIST = "MEDICINE_LIST";
	}
	
	interface Shift{
		final int MORNIG=1;
		final int NOON=2;
		final int NIGHT=3;
	}
	
	interface CareGiverShiftTime{
		final String MORNING="08:01-16:00";
		final String NOON="16:01-24:00";
		final String NIGHT="00:01-08:00";
	}
	
}


